using System;

namespace Utils
{
    public static class CustomMath
    {
        public static int Mul(int value, double modifier, bool condition = true)
        {
            if (!condition)
            {
                return value;
            }

            // TODO should actually round down on 0.5, not up
            return (int) Math.Round(value * modifier, MidpointRounding.AwayFromZero);
        }

        public static int Div(int value, int divisor, bool condition = true)
        {
            return Mul(value, 1.0d / divisor, condition);
        }
    }
}