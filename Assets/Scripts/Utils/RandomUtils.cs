﻿using System;

namespace Utils
{
    public static class RandomUtils
    {
        private static readonly Random Random = new Random();

        public static int RangeInclusive(int minInclusive, int maxInclusive)
        {
            return Random.Next(minInclusive, maxInclusive + 1);
        }

        public static float RangeFloat(float min, float max)
        {
            return (float) Random.NextDouble() * (max - min) + min;
        }

        public static bool Chance(int chance)
        {
            return RangeInclusive(1, 100) <= chance;
        }

        public static bool ChanceOneInMany(int range)
        {
            return RangeInclusive(1, range) == 1;
        }

        public static T Element<T>(T[] collection)
        {
            return collection[RangeInclusive(0, collection.Length - 1)];
        }

        public static int MultiStrikesTwoToFive()
        {
            int value = RangeInclusive(1, 100);
            if (value <= 35)
            {
                return 2;
            }

            if (value <= 70)
            {
                return 3;
            }

            if (value <= 85)
            {
                return 4;
            }

            return 5;
        }

        public static int Next(int maxValue) => Random.Next(maxValue);

        public static bool ShinyChance() => ChanceOneInMany(10);
    }
}