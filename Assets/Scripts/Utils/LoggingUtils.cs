using System;
using System.Linq;
using System.Reflection;

namespace Utils
{
    public static class LoggingUtils
    {
        public static string FormatFields(object obj)
        {
            return string.Join(", ", obj.GetType()
                .GetFields(BindingFlags.Instance | BindingFlags.NonPublic)
                .Select(field =>
                {
                    object value = field.GetValue(obj);
                    if (value == null || !value.GetType().IsArray)
                    {
                        return $"{field.Name}: {value}";
                    }

                    string values = string.Join(", ", ((Array)value)
                        .Cast<object>()
                        .Select(o => o == null ? "null" : o.ToString()));
                    return $"{field.Name}: [{values}]";
                }));
        }
    }
}