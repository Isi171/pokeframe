using Managers.Data;
using Models;
using UnityEngine;

namespace Utils
{
    public static class SpriteUtils
    {
        // TODO cache loading
        public static Sprite LoadSprite(string species, Gender gender, bool shiny, bool back)
        {
            Sprite sprite;
            int id = DataAccess.PokemonBaseData.Get(species).NationalDexNumber;
            string pathBack = back ? "back/" : "";
            string pathShiny = shiny ? "shiny/" : "";
            if (gender != Gender.Female)
            {
                sprite = Resources.Load<Sprite>($"pokemon/battlers/{pathBack}{pathShiny}{id}");
            }
            else
            {
                sprite = Resources.Load<Sprite>($"pokemon/battlers/{pathBack}{pathShiny}female/{id}");
                if (sprite == null)
                {
                    sprite = Resources.Load<Sprite>($"pokemon/battlers/{pathBack}{pathShiny}{id}");
                }
            }

            if (sprite == null)
            {
                Resources.Load<Sprite>("pokemon/battlers/0");
            }

            return sprite;
        }

        // TODO cache loading
        public static Sprite LoadIcon(string species)
        {
            int id = DataAccess.PokemonBaseData.Get(species).NationalDexNumber;
            return Resources.Load<Sprite>($"pokemon/icons/{id}");
        }

        // TODO cache loading
        public static Sprite LoadItem(string item)
        {
            if (item.StartsWith("TM"))
            {
                string move = DataAccess.Items.Get(item).ExtractTmMove();
                string type = DataAccess.Moves.Get(move).Type.ToString().ToLower();
                Sprite tmSprite = Resources.Load<Sprite>($"items/tm/tm-{type}");
                return tmSprite == null ? Resources.Load<Sprite>($"items/unknown") : tmSprite;
            }

            string id = item.ToLower()
                .Replace(" ", "-")
                .Replace(".", "")
                .Replace("'", "")
                .Replace("é", "e");
            Sprite sprite = Resources.Load<Sprite>($"items/{id}");
            return sprite == null ? Resources.Load<Sprite>($"items/unknown") : sprite;
        }
    }
}