using System;
using Models;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Utils
{
    public static class ViewUtils
    {
        private static readonly Color MaleColor = new Color(0f, 0.3794198f, 1f);
        private static readonly Color FemaleColor = new Color(1f, 0f, 0.199367f);
        private static readonly Color UnknownColor = new Color(0f, 0f, 0f);
        private static readonly Tuple<string, Color> Male = new Tuple<string, Color>("♂", MaleColor);
        private static readonly Tuple<string, Color> Female = new Tuple<string, Color>("♀", FemaleColor);
        private static readonly Tuple<string, Color> Unknown = new Tuple<string, Color>("", UnknownColor);

        public static Tuple<string, Color> GetGender(Gender gender)
        {
            switch (gender)
            {
                case Gender.Male:
                    return Male;
                case Gender.Female:
                    return Female;
                case Gender.Unknown:
                    return Unknown;
                default:
                    throw new ArgumentOutOfRangeException(nameof(gender), gender, null);
            }
        }

        public static void ForceSelect(Selectable selectable)
        {
            // https://forum.unity.com/threads/using-select-doesnt-work-for-ui-buttons-after-panel-is-set-inactive-then-active.401179/
            // Fuck me if I know
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(selectable.gameObject);
        }
    }
}