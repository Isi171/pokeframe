using Models.Pokemon;
using View.Battle;

namespace Utils
{
    public static class BattleUtils
    {
        public static Pokemon IfPresentAndNotFainted(BattlerController battler)
        {
            if (battler == null)
            {
                return null;
            }

            if (battler.Pokemon.Fainted)
            {
                return null;
            }

            return battler.Pokemon;
        }
    }
}