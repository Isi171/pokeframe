using System;
using System.Collections.Generic;

namespace Utils
{
    public static class ArrayExtensions
    {
        public static void Shuffle<T>(this T[] array)
        {
            int n = array.Length;
            while (n > 1)
            {
                n--;
                int k = RandomUtils.Next(n + 1);
                (array[k], array[n]) = (array[n], array[k]);
            }
        }

        public static void Swap<T>(this T[] array, T e1, T e2)
        {
            int i1 = -1;
            int i2 = -1;
            for (int i = 0; i < array.Length; i++)
            {
                if (Equals(array[i], e1))
                {
                    i1 = i;
                }

                if (Equals(array[i], e2))
                {
                    i2 = i;
                }
            }

            if (i1 == -1 || i2 == -1)
            {
                throw new Exception("Element not in array");
            }

            array[i1] = e2;
            array[i2] = e1;
        }

        public static void StableSort<T>(this T[] values, Comparison<T> comparison)
        {
            KeyValuePair<int, T>[] keys = new KeyValuePair<int, T>[values.Length];
            for (int i = 0; i < values.Length; i++)
            {
                keys[i] = new KeyValuePair<int, T>(i, values[i]);
            }

            Array.Sort(keys, values, new StabilizingComparer<T>(comparison));
        }

        private sealed class StabilizingComparer<T> : IComparer<KeyValuePair<int, T>>
        {
            private readonly Comparison<T> comparison;

            public StabilizingComparer(Comparison<T> comparison)
            {
                this.comparison = comparison;
            }

            public int Compare(KeyValuePair<int, T> x,
                KeyValuePair<int, T> y)
            {
                int result = comparison(x.Value, y.Value);
                return result != 0 ? result : x.Key.CompareTo(y.Key);
            }
        }
    }
}