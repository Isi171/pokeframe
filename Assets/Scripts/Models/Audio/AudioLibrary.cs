using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Models.Audio
{
    public class AudioLibrary : MonoBehaviour
    {
        [SerializeField] private AudioLibraryGroup[] library;

        public AudioClip Get(string section, string trackId)
        {
            return library
                .Where(group => group.Section.Equals(section, StringComparison.OrdinalIgnoreCase))
                .SelectMany(group => group.Tracks)
                .Where(track => track.Id.Equals(trackId, StringComparison.OrdinalIgnoreCase))
                .Select(track => track.Clip)
                .FirstOrDefault();
        }
    }

    [Serializable]
    public class AudioLibraryGroup
    {
        [SerializeField] private string section;
        [SerializeField] private AudioLibraryTrack[] tracks;

        public string Section => section;
        public IReadOnlyCollection<AudioLibraryTrack> Tracks => tracks;
    }

    [Serializable]
    public class AudioLibraryTrack
    {
        [SerializeField] private string id;
        [SerializeField] private AudioClip clip;

        public string Id => id;
        public AudioClip Clip => clip;
    }
}