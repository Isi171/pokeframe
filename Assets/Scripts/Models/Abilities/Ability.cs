using System.Linq;

namespace Models.Abilities
{
    public class Ability
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public bool Is(string name, params string[] additional)
        {
            return Name == name || additional.Any(altName => Name == altName);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}