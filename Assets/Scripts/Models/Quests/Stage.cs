namespace Models.Quests
{
    public class Stage
    {
        public string Description { get; set; }
        public bool FinishesQuest { get; set; }
    }
}