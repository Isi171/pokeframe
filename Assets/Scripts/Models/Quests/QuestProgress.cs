namespace Models.Quests
{
    public class QuestProgress
    {
        public string Quest { get; set; }
        public int Stage { get; set; }
    }
}