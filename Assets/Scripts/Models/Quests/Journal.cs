using System.Collections.Generic;

namespace Models.Quests
{
    public class Journal
    {
        public List<QuestProgress> Active { get; } = new List<QuestProgress>();
        public List<QuestProgress> Complete { get; } = new List<QuestProgress>();
    }
}