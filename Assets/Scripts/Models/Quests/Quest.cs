using System.Collections.Generic;

namespace Models.Quests
{
    public class Quest
    {
        public string Name { get; set; }
        public Dictionary<int, Stage> Stages { get; set; }
    }
}