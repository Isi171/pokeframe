using System.Collections.Generic;
using Newtonsoft.Json;

namespace Models.Ext
{
    public class LearnsetExt
    {
        [JsonProperty("level")] public Dictionary<int, string[]> Level;
        [JsonProperty("egg")] public string[] Egg;
        [JsonProperty("evolution")] public string[] Evolution;
        [JsonProperty("tm")] public string[] Machine;
        [JsonProperty("tutor")] public string[] Tutor;
    }
}