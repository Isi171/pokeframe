using System.Collections.Generic;
using Newtonsoft.Json;

namespace Models.Ext
{
    public class QuestExt
    {
        [JsonProperty("name")] public string Name;
        [JsonProperty("stages")] public Dictionary<string, StageExt> Stages;
    }
}