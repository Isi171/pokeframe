using Newtonsoft.Json;

namespace Models.Ext
{
    public class StageExt
    {
        [JsonProperty("description")] public string Description;
        [JsonProperty("end")] public bool FinishesQuest;
    }
}