using Newtonsoft.Json;

namespace Models.Ext
{
    public class TrainerBattleReferenceExt
    {
        [JsonProperty("type")] public string Type;
        [JsonProperty("no_exp")] public bool NoExperience;
        [JsonProperty("enemy")] public string Enemy1Alt;
        [JsonProperty("enemy1")] public string Enemy1;
        [JsonProperty("enemy2")] public string Enemy2;
        [JsonProperty("ally")] public string Ally;
        [JsonProperty("prize")] public LootTableExt Prize;
        [JsonProperty("victory_line")] public string VictoryLine;
        [JsonProperty("after_dialogue")] public string AfterBattleDialogue;
        [JsonProperty("music")] public string Music;
        [JsonProperty("music_victory")] public string VictoryMusic;
    }
}