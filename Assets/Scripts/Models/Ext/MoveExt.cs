using System.Collections.Generic;
using Newtonsoft.Json;

namespace Models.Ext
{
    public class MoveExt
    {
        // Required
        [JsonProperty("index")] public int Index;
        [JsonProperty("description")] public string Description;
        [JsonProperty("type")] public string Type;
        [JsonProperty("class")] public string MoveType;
        [JsonProperty("pp")] public int Pp;
        [JsonProperty("accuracy")] public int Accuracy;
        [JsonProperty("target")] public string Target;

        // Optional
        [JsonProperty("priority")] public int Priority;
        [JsonProperty("accuracy_in_weather")] public Dictionary<string, int> ModifiedAccuracyInWeather;

        [JsonProperty("base_power")] public int BasePower;
        [JsonProperty("critical_stage")] public int CriticalStage;
        [JsonProperty("recoil_divisor")] public int RecoilDivisor;
        [JsonProperty("multi_strike")] public bool MultiStrike;
        [JsonProperty("fixed_multi_strike")] public int FixedMultiStrike;
        [JsonProperty("leech")] public bool Leech;
        [JsonProperty("set_damage")] public int SetDamage;
        
        [JsonProperty("burn_chance")] public int BurnChance;
        [JsonProperty("freeze_chance")] public int FreezeChance;
        [JsonProperty("paralysis_chance")] public int ParalysisChance;
        [JsonProperty("poison_chance")] public int PoisonChance;
        [JsonProperty("bad_poison_chance")] public int BadPoisonChance;
        [JsonProperty("sleep_chance")] public int SleepChance;
        [JsonProperty("flinch_chance")] public int FlinchChance;
        [JsonProperty("confusion_chance")] public int ConfusionChance;
        
        [JsonProperty("user_stat_changes_chance")] public int UserStatChangesChance;
        [JsonProperty("user_stat_changes")] public Dictionary<string, int> UserStatChanges;
        [JsonProperty("target_stat_changes_chance")] public int TargetStatChangesChance;
        [JsonProperty("target_stat_changes")] public Dictionary<string, int> TargetStatChanges;
        
        [JsonProperty("weather")] public string Weather;
        [JsonProperty("special_effect")] public string SpecialEffect;
        [JsonProperty("flags")] public string[] Flags;
    }
}