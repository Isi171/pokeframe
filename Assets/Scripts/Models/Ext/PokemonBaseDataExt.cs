using Newtonsoft.Json;

namespace Models.Ext
{
    public class PokemonBaseDataExt
    {
        [JsonProperty("national_dex_number")] public int NationalDexNumber;
        [JsonProperty("male_ratio")] public int MaleRatio;
        [JsonProperty("type1")] public string Type1;
        [JsonProperty("type2")] public string Type2;
        [JsonProperty("ability1")] public string Ability1;
        [JsonProperty("ability2")] public string Ability2;
        [JsonProperty("hidden_ability")] public string HiddenAbility;
        [JsonProperty("hp")] public int Hp;
        [JsonProperty("attack")] public int Attack;
        [JsonProperty("defense")] public int Defense;
        [JsonProperty("special_attack")] public int SpecialAttack;
        [JsonProperty("special_defense")] public int SpecialDefense;
        [JsonProperty("speed")] public int Speed;
        [JsonProperty("egg_group1")] public string EggGroup1;
        [JsonProperty("egg_group2")] public string EggGroup2;
        [JsonProperty("hatch_counter")] public int HatchCounter;
        [JsonProperty("steps_to_hatch")] public int StepsToHatch;
        [JsonProperty("effort")] public string Effort;
        [JsonProperty("evolutions")] public EvolutionExt[] Evolutions;
        [JsonProperty("exp_group")] public string ExpGroup;
        [JsonProperty("base_experience")] public int BaseExperience;
        [JsonProperty("capture_rate")] public int CaptureRate;
        [JsonProperty("base_happiness")] public int BaseHappiness;
        [JsonProperty("height")] public string Height;
        [JsonProperty("weight")] public string Weight;
        [JsonProperty("genus")] public string Genus;
        [JsonProperty("color")] public string Color;
        [JsonProperty("habitat")] public string Habitat;
        [JsonProperty("shape")] public string Shape;
    }
}