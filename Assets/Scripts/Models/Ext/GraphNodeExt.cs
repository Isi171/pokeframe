using Newtonsoft.Json;

namespace Models.Ext
{
    public class GraphNodeExt
    {
        [JsonProperty("lines")] public LineExt[] Lines;
        [JsonProperty("links")] public GraphLinkExt[] Links;
        [JsonProperty("vars")] public string[] VariableChanges;
    }
}