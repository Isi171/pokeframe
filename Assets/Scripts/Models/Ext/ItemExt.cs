using Newtonsoft.Json;

namespace Models.Ext
{
    public class ItemExt
    {
        [JsonProperty("category")] public string Category;
        [JsonProperty("buy")] public int Buy;
        [JsonProperty("sell")] public int Sell;
        [JsonProperty("description")] public string Description;
    }
}