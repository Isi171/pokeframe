using Newtonsoft.Json;

namespace Models.Ext
{
    public class EvolutionExt
    {
        [JsonProperty("method")] public string EvolutionMethod;
        [JsonProperty("level")] public int Level;
        [JsonProperty("item")] public string Item;
        [JsonProperty("move")] public string Move;
        [JsonProperty("party")] public string Party;
        [JsonProperty("species")] public string Species;
    }
}