using Newtonsoft.Json;

namespace Models.Ext
{
    public class MerchantExt
    {
        [JsonProperty("stock")] public string[] Stock;
    }
}