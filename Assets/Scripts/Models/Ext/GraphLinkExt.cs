using Newtonsoft.Json;

namespace Models.Ext
{
    public class GraphLinkExt
    {
        [JsonProperty("line")] public string Line { get; set; }
        [JsonProperty("link")] public string LinksTo { get; set; }
        [JsonProperty("conditions")] public string[] ConditionsAnd { get; set; }
    }
}