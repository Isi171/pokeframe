using System.Collections.Generic;
using Newtonsoft.Json;

namespace Models.Ext.Save
{
    public class StateExt
    {
        [JsonProperty("options")] public OptionsExt Options;
        [JsonProperty("player")] public PlayerTrainerExt Player;
        [JsonProperty("position")] public AbsolutePositionExt CurrentPosition;
        [JsonProperty("last_safe_spot")] public AbsolutePositionExt LastSafeSpot;
        [JsonProperty("journal")] public JournalExt Journal;
        [JsonProperty("variables")] public Dictionary<string, int> Variables;
        [JsonProperty("map_states")] public Dictionary<string, WorldScenePersistentStateExt> MapStates;
    }
}