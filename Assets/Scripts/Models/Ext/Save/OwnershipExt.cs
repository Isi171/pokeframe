using Newtonsoft.Json;

namespace Models.Ext.Save
{
    public class OwnershipExt
    {
        [JsonProperty("met_location")] public string MetLocation;
        [JsonProperty("met_level")] public int MetLevel;
        [JsonProperty("ot_name")] public string OriginalTrainerName;
        [JsonProperty("ot_id")] public string OriginalTrainerId;
    }
}