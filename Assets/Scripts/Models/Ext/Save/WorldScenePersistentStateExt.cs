using System.Collections.Generic;
using Newtonsoft.Json;

namespace Models.Ext.Save
{
    public class WorldScenePersistentStateExt
    {
        [JsonProperty("pickups")] public Dictionary<string, bool> Pickups;
    }
}