using Newtonsoft.Json;

namespace Models.Ext.Save
{
    public class PersistedPokemonExt
    {
        [JsonProperty("species")] public string Species;
        [JsonProperty("level")] public int Level;
        [JsonProperty("excess_experience")] public int ExperienceInExcessOfLevel;
        [JsonProperty("moves")] public MoveSlotExt[] Moves;
        [JsonProperty("item")] public string Item;
        [JsonProperty("ability")] public string Ability;
        [JsonProperty("nature")] public string Nature;
        [JsonProperty("iv")] public int[] Ivs;
        [JsonProperty("ev")] public int[] Evs;
        [JsonProperty("gender")] public string Gender;
        [JsonProperty("friendship")] public int Happiness;
        [JsonProperty("name")] public string Name;
        [JsonProperty("shiny")] public bool Shiny;
        [JsonProperty("ball")] public string Ball;
        [JsonProperty("hp")] public int CurrentHp;
        [JsonProperty("status")] public string Status;
        [JsonProperty("sleep_counter")] public int SleepCounter;
        [JsonProperty("ownership")] public OwnershipExt Ownership;
    }
}