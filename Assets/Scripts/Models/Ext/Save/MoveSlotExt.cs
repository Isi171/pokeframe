using Newtonsoft.Json;

namespace Models.Ext.Save
{
    public class MoveSlotExt
    {
        // TODO add max pp here if they changed via PP UP
        [JsonProperty("move")] public string Move;
        [JsonProperty("pp")] public int CurrentPp;
    }
}