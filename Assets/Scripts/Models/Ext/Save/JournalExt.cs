using Newtonsoft.Json;

namespace Models.Ext.Save
{
    public class JournalExt
    {
        [JsonProperty("active")] public QuestProgressExt[] Active;
        [JsonProperty("complete")] public QuestProgressExt[] Complete;
    }
}