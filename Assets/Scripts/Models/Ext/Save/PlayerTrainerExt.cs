using System.Collections.Generic;
using Newtonsoft.Json;

namespace Models.Ext.Save
{
    public class PlayerTrainerExt
    {
        [JsonProperty("name")] public string Name;
        [JsonProperty("sprite")] public string Sprite;
        [JsonProperty("party")] public PersistedPokemonExt[] Party;
        [JsonProperty("box")] public PersistedPokemonExt[] Box;
        [JsonProperty("bag")] public Dictionary<string, int> Bag;
        [JsonProperty("money")] public int Money;
    }
}