using Newtonsoft.Json;

namespace Models.Ext.Save
{
    public class NewGameExt
    {
        [JsonProperty("spawn")] public AbsolutePositionExt SpawnPoint;
        [JsonProperty("money")] public int Money;
        [JsonProperty("player")] public TrainerExt Player;
    }
}