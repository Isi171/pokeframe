using Newtonsoft.Json;

namespace Models.Ext.Save
{
    public class QuestProgressExt
    {
        [JsonProperty("quest")] public string Quest;
        [JsonProperty("stage")] public int Stage;
    }
}