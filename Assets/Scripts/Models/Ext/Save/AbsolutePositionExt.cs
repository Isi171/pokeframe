using System.Numerics;
using Newtonsoft.Json;

namespace Models.Ext.Save
{
    public class AbsolutePositionExt
    {
        [JsonProperty("map")] public string Map;
        [JsonProperty("coordinates")] public Vector2 Position;
        [JsonProperty("direction")] public string Direction;
    }
}