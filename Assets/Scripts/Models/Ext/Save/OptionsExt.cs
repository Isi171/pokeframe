using Newtonsoft.Json;

namespace Models.Ext.Save
{
    public class OptionsExt
    {
        [JsonProperty("music")] public int MusicVolume;
        [JsonProperty("sfx")] public int EffectsVolume;
        [JsonProperty("timescale")] public int Timescale;
    }
}