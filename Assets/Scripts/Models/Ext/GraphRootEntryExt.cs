using Newtonsoft.Json;

namespace Models.Ext
{
    public class GraphRootEntryExt
    {
        [JsonProperty("node")] public string Node;
        [JsonProperty("conditions")] public string[] ConditionsAnd;
    }
}