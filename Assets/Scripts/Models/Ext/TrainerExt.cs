using System.Collections.Generic;
using Newtonsoft.Json;

namespace Models.Ext
{
    public class TrainerExt
    {
        // Required
        [JsonProperty("name")] public string Name;
        [JsonProperty("class")] public string Class;
        [JsonProperty("sprite")] public string Sprite;
        [JsonProperty("party")] public PokemonExt[] Party;

        // Optional
        [JsonProperty("bag")] public Dictionary<string, int> Bag;
        [JsonProperty("money")] public int Money;
    }
}