using Newtonsoft.Json;

namespace Models.Ext
{
    public class AbilityExt
    {
        [JsonProperty("description")] public string Description;
    }
}