using System.Collections.Generic;
using Newtonsoft.Json;

namespace Models.Ext
{
    public class LootTableExt
    {
        [JsonProperty("money")] public int Money;
        [JsonProperty("items")] public Dictionary<string, int> Items;
    }
}