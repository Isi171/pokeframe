using Newtonsoft.Json;

namespace Models.Ext
{
    public class SpawnTableEntryExt
    {
        [JsonProperty("species")] public string Species;
        [JsonProperty("min")] public int MinLevel;
        [JsonProperty("max")] public int MaxLevel;
    }
}