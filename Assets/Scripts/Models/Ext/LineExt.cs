using Newtonsoft.Json;

namespace Models.Ext
{
    public class LineExt
    {
        [JsonProperty("name")] public string Name;
        [JsonProperty("portrait")] public string Portrait;
        [JsonProperty("text")] public string Text;
        [JsonProperty("effect")] public string Effect;
        [JsonProperty("effect_data")] public string EffectData;
    }
}