using Newtonsoft.Json;

namespace Models.Ext
{
    public class PokemonExt
    {
        // Required
        [JsonProperty("species")] public string Species;
        [JsonProperty("level")] public int Level;

        // Optional
        [JsonProperty("moves")] public string[] Moves;
        [JsonProperty("item")] public string Item;
        [JsonProperty("ability")] public string Ability;
        [JsonProperty("nature")] public string Nature;
        [JsonProperty("iv")] public int[] Ivs;
        [JsonProperty("ev")] public int[] Evs;
        [JsonProperty("gender")] public string Gender;
        [JsonProperty("friendship")] public int Happiness;
        [JsonProperty("name")] public string Name;
        [JsonProperty("shiny")] public bool Shiny;
        [JsonProperty("ball")] public string Ball;
    }
}