﻿using System;

namespace Models.Pokemon
{
    public class Stat
    {
        public StatName Name { get; set; }
        public int Base { get; set; }
        public int Iv { get; set; }
        public int Ev { get; set; }
        public int Derived { get; private set; }

        private int CalculateCommonPart(int level)
        {
            return (int) Math.Floor((2 * Base + Iv + (int) Math.Floor(Ev / 4.0d)) * level / 100.0d);
        }

        public void CalculateDerivedStatHp(int level)
        {
            Derived = CalculateCommonPart(level) + level + 10;
        }

        public void CalculateDerivedStat(int level, Nature nature)
        {
            Derived = (int) Math.Floor((CalculateCommonPart(level) + 5) * nature.GetModifier(Name));
        }
    }
}