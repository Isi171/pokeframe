using System.Collections.Generic;
using System.Linq;

namespace Models.Pokemon
{
    public class Move
    {
        // Core data
        public string Name { get; set; }
        public int Index { get; set; }
        public string Description { get; set; }
        public Type Type { get; set; }
        public MoveType MoveType { get; set; }
        public MoveTarget Target { get; set; }
        public int Pp { get; set; }
        public int Accuracy { get; set; }

        public Dictionary<Weather, int> ModifiedAccuracyInWeather { get; set; }
        public int Priority { get; set; }

        // Direct damage related
        public int BasePower { get; set; }
        public int CriticalStage { get; set; }
        public int RecoilDivisor { get; set; }
        public bool MultiStrike { get; set; }
        public int FixedMultiStrike { get; set; }
        public bool Leech { get; set; }
        public int SetDamage { get; set; }

        // Status
        public int BurnChance { get; set; }
        public int FreezeChance { get; set; }
        public int ParalysisChance { get; set; }
        public int PoisonChance { get; set; }
        public int BadPoisonChance { get; set; }
        public int SleepChance { get; set; }
        public int FlinchChance { get; set; }
        public int ConfusionChance { get; set; }

        // Stat changes
        public int UserStatChangesChance { get; set; }
        public Dictionary<StatName, int> UserStatChanges { get; set; }
        public int TargetStatChangesChance { get; set; }
        public Dictionary<StatName, int> TargetStatChanges { get; set; }

        // Other effects
        public Weather SetWeather { get; set; }
        public SpecialEffect SpecialEffect { get; set; }

        // Flags
        public MoveFlag[] Flags { get; set; }

        public bool HasFlag(MoveFlag flag)
        {
            return Flags.Contains(flag);
        }
        
        public override string ToString()
        {
            return $"{Name}";
        }
    }
}