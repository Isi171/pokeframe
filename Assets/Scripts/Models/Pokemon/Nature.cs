﻿using Utils;

namespace Models.Pokemon
{
    public class Nature
    {
        private static readonly Nature[] Table =
        {
            new Nature {Name = "Hardy", Raised = StatName.Attack, Lowered = StatName.Attack},
            new Nature {Name = "Lonely", Raised = StatName.Attack, Lowered = StatName.Defense},
            new Nature {Name = "Brave", Raised = StatName.Attack, Lowered = StatName.Speed},
            new Nature {Name = "Adamant", Raised = StatName.Attack, Lowered = StatName.SpecialAttack},
            new Nature {Name = "Naughty", Raised = StatName.Attack, Lowered = StatName.SpecialDefense},
            new Nature {Name = "Bold", Raised = StatName.Defense, Lowered = StatName.Attack},
            new Nature {Name = "Docile", Raised = StatName.Defense, Lowered = StatName.Defense},
            new Nature {Name = "Relaxed", Raised = StatName.Defense, Lowered = StatName.Speed},
            new Nature {Name = "Impish", Raised = StatName.Defense, Lowered = StatName.SpecialAttack},
            new Nature {Name = "Lax", Raised = StatName.Defense, Lowered = StatName.SpecialDefense},
            new Nature {Name = "Timid", Raised = StatName.Speed, Lowered = StatName.Attack},
            new Nature {Name = "Hasty", Raised = StatName.Speed, Lowered = StatName.Defense},
            new Nature {Name = "Serious", Raised = StatName.Speed, Lowered = StatName.Speed},
            new Nature {Name = "Jolly", Raised = StatName.Speed, Lowered = StatName.SpecialAttack},
            new Nature {Name = "Naive", Raised = StatName.Speed, Lowered = StatName.SpecialDefense},
            new Nature {Name = "Modest", Raised = StatName.SpecialAttack, Lowered = StatName.Attack},
            new Nature {Name = "Mild", Raised = StatName.SpecialAttack, Lowered = StatName.Defense},
            new Nature {Name = "Quiet", Raised = StatName.SpecialAttack, Lowered = StatName.Speed},
            new Nature {Name = "Bashful", Raised = StatName.SpecialAttack, Lowered = StatName.SpecialAttack},
            new Nature {Name = "Rash", Raised = StatName.SpecialAttack, Lowered = StatName.SpecialDefense},
            new Nature {Name = "Calm", Raised = StatName.SpecialDefense, Lowered = StatName.Attack},
            new Nature {Name = "Gentle", Raised = StatName.SpecialDefense, Lowered = StatName.Defense},
            new Nature {Name = "Sassy", Raised = StatName.SpecialDefense, Lowered = StatName.Speed},
            new Nature {Name = "Careful", Raised = StatName.SpecialDefense, Lowered = StatName.SpecialAttack},
            new Nature {Name = "Quirky", Raised = StatName.SpecialDefense, Lowered = StatName.SpecialDefense}
        };

        public static Nature ForName(string name)
        {
            foreach (Nature nature in Table)
            {
                if (name == nature.Name)
                {
                    return nature;
                }
            }

            return Table[0];
        }

        public static Nature Random()
        {
            return Table[RandomUtils.RangeInclusive(0, Table.Length - 1)];
        }

        public string Name { get; private set; }
        public StatName Raised { get; private set; }
        public StatName Lowered { get; private set; }

        public double GetModifier(StatName stat)
        {
            if (Raised == Lowered)
            {
                return 1.0d;
            }

            if (stat == Raised)
            {
                return 1.1d;
            }

            if (stat == Lowered)
            {
                return 0.9d;
            }

            return 1.0d;
        }
    }
}