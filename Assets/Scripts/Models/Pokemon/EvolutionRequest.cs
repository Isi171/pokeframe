namespace Models.Pokemon
{
    public class EvolutionRequest
    {
        public Pokemon ToEvolve { get; set; }
        public string EvolutionSpecies { get; set; }
        public bool DestroyHeldItem { get; set; }
    }
}