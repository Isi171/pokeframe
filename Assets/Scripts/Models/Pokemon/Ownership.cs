namespace Models.Pokemon
{
    public class Ownership
    {
        public string MetLocation { get; set; }
        public int MetLevel { get; set; }
        public string OriginalTrainerName { get; set; }
        public string OriginalTrainerId { get; set; }

        public bool Valid()
        {
            return !string.IsNullOrEmpty(OriginalTrainerName) && string.IsNullOrEmpty(OriginalTrainerId);
        }

        public void SetTrainer(string name, string id)
        {
            OriginalTrainerName = name;
            OriginalTrainerId = id;
        }

        public void SetLocation(string location, int level)
        {
            MetLocation = location;
            MetLevel = level;
        }
    }
}