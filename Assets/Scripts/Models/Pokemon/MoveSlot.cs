using Managers.Data;

namespace Models.Pokemon
{
    public class MoveSlot
    {
        // TODO add max pp here if they changed via PP UP
        public Move Move { get; }
        public int CurrentPp { get; set; }

        public MoveSlot()
        {
        }

        public MoveSlot(string name)
        {
            Move = DataAccess.Moves.Get(name);
            CurrentPp = Move.Pp;
        }

        public bool IsAssigned()
        {
            return Move != null;
        }

        public override string ToString()
        {
            return IsAssigned() ? $"{Move.Name} ({CurrentPp} PP)" : "(empty)";
        }
    }
}