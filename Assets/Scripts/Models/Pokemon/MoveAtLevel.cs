using System;

namespace Models.Pokemon
{
    public class MoveAtLevel : IComparable
    {
        public int Level { get; set; }
        public string Move { get; set; }

        public int CompareTo(object obj)
        {
            MoveAtLevel other = (MoveAtLevel) obj;

            if (ReferenceEquals(this, other))
            {
                return 0;
            }

            if (ReferenceEquals(null, other))
            {
                return 1;
            }

            if (ReferenceEquals(null, this))
            {
                return -1;
            }

            int levelComparison = Level.CompareTo(other.Level);
            return levelComparison != 0
                ? levelComparison
                : string.Compare(this.Move, other.Move, StringComparison.Ordinal);
        }
    }
}