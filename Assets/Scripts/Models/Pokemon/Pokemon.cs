﻿using System.Collections.Generic;
using System.Linq;
using Daybreak.Log;
using Managers.Data;
using Models.Abilities;
using Models.Battle;
using Models.Items;
using Utils;

namespace Models.Pokemon
{
    public class Pokemon
    {
        private int currentHp;
        public string Name { get; set; }
        public string Species { get; private set; }
        public Stat Hp { get; } = new Stat { Name = StatName.Hp };
        public Stat Attack { get; } = new Stat { Name = StatName.Attack };
        public Stat Defense { get; } = new Stat { Name = StatName.Defense };
        public Stat SpecialAttack { get; } = new Stat { Name = StatName.SpecialAttack };
        public Stat SpecialDefense { get; } = new Stat { Name = StatName.SpecialDefense };
        public Stat Speed { get; } = new Stat { Name = StatName.Speed };
        public Experience Experience { get; } = new Experience();

        public int Level
        {
            get => Experience.Level;
            set => Experience.Level = value;
        }

        public Type Type1 { get; set; }
        public Type Type2 { get; set; }
        public Nature Nature { get; set; }
        public Gender Gender { get; set; }
        public bool Shiny { get; set; }
        public string Ball { get; set; }

        public int CurrentHp
        {
            get => currentHp;
            set
            {
                if (value < 0)
                {
                    currentHp = 0;
                }
                else if (value > Hp.Derived)
                {
                    currentHp = Hp.Derived;
                }
                else
                {
                    currentHp = value;
                }
            }
        }

        public bool Fainted => currentHp == 0;

        public Status Status { get; set; }
        public int SleepCounter { get; set; }
        public VolatilePokemon VolatilePokemon { get; set; }

        public MoveSlot[] Moves { get; } =
        {
            new MoveSlot(), new MoveSlot(), new MoveSlot(), new MoveSlot()
        };

        public int Happiness { get; set; }

        public HeldItem HeldItem { get; } = new HeldItem();

        public Ability Ability { get; set; }

        public Ownership Ownership { get; } = new Ownership();

        public bool HasType(Type type)
        {
            return Type1 == type || Type2 == type;
        }

        public void SetBaseStats(int hp, int attack, int defense, int specialAttack, int specialDefense, int speed)
        {
            Hp.Base = hp;
            Attack.Base = attack;
            Defense.Base = defense;
            SpecialAttack.Base = specialAttack;
            SpecialDefense.Base = specialDefense;
            Speed.Base = speed;
        }

        public void SetIvs(int hp, int attack, int defense, int specialAttack, int specialDefense, int speed)
        {
            Hp.Iv = hp;
            Attack.Iv = attack;
            Defense.Iv = defense;
            SpecialAttack.Iv = specialAttack;
            SpecialDefense.Iv = specialDefense;
            Speed.Iv = speed;
        }

        public void SetEvs(int hp, int attack, int defense, int specialAttack, int specialDefense, int speed)
        {
            Hp.Ev = hp;
            Attack.Ev = attack;
            Defense.Ev = defense;
            SpecialAttack.Ev = specialAttack;
            SpecialDefense.Ev = specialDefense;
            Speed.Ev = speed;
        }

        public void ChangeEvsCheckingLimit(int hp, int attack, int defense, int specialAttack, int specialDefense,
            int speed)
        {
            ChangeEv(Hp, hp);
            ChangeEv(Attack, attack);
            ChangeEv(Defense, defense);
            ChangeEv(SpecialAttack, specialAttack);
            ChangeEv(SpecialDefense, specialDefense);
            ChangeEv(Speed, speed);
        }

        public void ChangeEvCheckingLimit(StatName statName, int amount)
        {
            switch (statName)
            {
                case StatName.Hp:
                    ChangeEv(Hp, amount);
                    break;
                case StatName.Attack:
                    ChangeEv(Attack, amount);
                    break;
                case StatName.Defense:
                    ChangeEv(Defense, amount);
                    break;
                case StatName.SpecialAttack:
                    ChangeEv(SpecialAttack, amount);
                    break;
                case StatName.SpecialDefense:
                    ChangeEv(SpecialDefense, amount);
                    break;
                case StatName.Speed:
                    ChangeEv(Speed, amount);
                    break;
            }
        }

        private void ChangeEv(Stat stat, int amount)
        {
            if (amount == 0)
            {
                return;
            }

            int steps = amount < 0 ? -amount : amount;

            for (int i = 0; i < steps; i++)
            {
                if (amount > 0 && stat.Ev < 252 && GetEvTotal() < 510)
                {
                    stat.Ev++;
                }
                else if (amount < 0 && stat.Ev > 0)
                {
                    stat.Ev--;
                }
            }
        }

        public int GetEvTotal()
        {
            return Hp.Ev + Attack.Ev + Defense.Ev + SpecialAttack.Ev + SpecialDefense.Ev + Speed.Ev;
        }

        public void CalculateDerivedStats()
        {
            Hp.CalculateDerivedStatHp(Level);
            Attack.CalculateDerivedStat(Level, Nature);
            Defense.CalculateDerivedStat(Level, Nature);
            SpecialAttack.CalculateDerivedStat(Level, Nature);
            SpecialDefense.CalculateDerivedStat(Level, Nature);
            Speed.CalculateDerivedStat(Level, Nature);
        }

        public int ChangeHappiness(int amountTier1, int amountTier2, int amountTier3)
        {
            int amount;
            if (Happiness < 100)
            {
                amount = amountTier1;
            }
            else if (Happiness >= 200)
            {
                amount = amountTier3;
            }
            else
            {
                amount = amountTier2;
            }

            Happiness += CustomMath.Mul(amount, 2, Ball == "Luxury Ball" && amount > 0);

            if (Happiness < 0)
            {
                Happiness = 0;
            }
            else if (Happiness > 255)
            {
                Happiness = 255;
            }

            return amount;
        }

        public void Heal()
        {
            CurrentHp = Hp.Derived;
            Status = Status.None;
            foreach (MoveSlot moveSlot in Moves)
            {
                if (moveSlot.IsAssigned())
                {
                    // TODO when PP UP is implemented, this (and other places) will have to refer to another value, not Move.Pp
                    moveSlot.CurrentPp = moveSlot.Move.Pp;
                }
            }
        }

        public void SetMovesByLevel()
        {
            Learnset learnset = DataAccess.Learnsets.Get(Species);
            List<MoveAtLevel> movesByLevel = learnset.Level
                .Where(set => set.Level <= Level)
                .ToList();
            int startingIndex = movesByLevel.Count - 4;
            if (startingIndex < 0)
            {
                startingIndex = 0;
            }

            for (int i = 0; i < 4 && startingIndex + i < movesByLevel.Count; i++)
            {
                string move = movesByLevel[startingIndex + i].Move;
                // TODO temporary until all moves are implemented
                if (move.All(char.IsLower))
                {
                    Log.Warn($"{Species} L{Level} wants the unimplemented {move}");
                    continue;
                }

                Moves[i] = new MoveSlot(move);
            }

            // TODO temporary until all moves are implemented
            if (Moves.All(slot => !slot.IsAssigned()))
            {
                Log.Warn($"{Species} L{Level} has no valid natural moves");
                Moves[0] = new MoveSlot("Tackle");
            }

            // TODO temporary until all moves are implemented
            MoveSlot[] oldSlots = Moves.Where(slot => slot.IsAssigned()).ToArray();
            if (oldSlots.Length < 4)
            {
                for (int i = 0; i < 4; i++)
                {
                    if (i < oldSlots.Length)
                    {
                        Moves[i] = oldSlots[i];
                    }
                    else
                    {
                        Moves[i] = new MoveSlot();
                    }
                }
            }
        }

        public void Transform(string newSpecies)
        {
            int oldCurrentHp = CurrentHp;
            int oldMaxHp = Hp.Derived;
            PokemonBaseData oldData = DataAccess.PokemonBaseData.Get(Species);

            PokemonBaseData data = DataAccess.PokemonBaseData.Get(newSpecies);

            if (Name == Species)
            {
                Name = newSpecies;
            }

            Species = newSpecies;
            Experience.ExpGroup = data.ExpGroup;
            Type1 = data.Type1;
            Type2 = data.Type2;

            SetBaseStats(
                data.Hp,
                data.Attack,
                data.Defense,
                data.SpecialAttack,
                data.SpecialDefense,
                data.Speed);

            CalculateDerivedStats();

            if (oldCurrentHp > 0)
            {
                CurrentHp = Hp.Derived;
                CurrentHp -= oldMaxHp - oldCurrentHp;
                if (CurrentHp == 0)
                {
                    CurrentHp = 1;
                }
            }

            // Keep ability slot
            if (Ability.Name == oldData.Ability1)
            {
                Ability = DataAccess.Abilities.Get(data.Ability1);
            }
            else if (Ability.Name == oldData.Ability2 && data.Ability2 != null)
            {
                Ability = DataAccess.Abilities.Get(data.Ability2);
            }
            else if (Ability.Name == oldData.HiddenAbility && data.HiddenAbility != null)
            {
                Ability = DataAccess.Abilities.Get(data.HiddenAbility);
            }
            else
            {
                Ability = DataAccess.Abilities.Get(data.Ability1);
            }
        }

        public static Pokemon Random(int level)
        {
            string species;
            do
            {
                species = DataAccess.PokemonBaseData.Random;
            } while (species.StartsWith("Mega "));

            Pokemon pokemon = Get(species, level);

            pokemon.Ball = RandomUtils.Element(
                DataAccess.Items.Filter(item => item.Category == ItemCategory.PokeBall).ToArray());

            return pokemon;
        }

        public static Pokemon Get(string species, int level)
        {
            PokemonBaseData data = DataAccess.PokemonBaseData.Get(species);
            Pokemon pokemon = new Pokemon
            {
                Name = species, Species = species, Experience = { ExpGroup = data.ExpGroup }, Level = level,
                Type1 = data.Type1, Type2 = data.Type2
            };

            pokemon.SetBaseStats(
                data.Hp,
                data.Attack,
                data.Defense,
                data.SpecialAttack,
                data.SpecialDefense,
                data.Speed);

            pokemon.SetIvs(
                RandomUtils.RangeInclusive(1, 31),
                RandomUtils.RangeInclusive(1, 31),
                RandomUtils.RangeInclusive(1, 31),
                RandomUtils.RangeInclusive(1, 31),
                RandomUtils.RangeInclusive(1, 31),
                RandomUtils.RangeInclusive(1, 31));

            pokemon.Nature = Nature.Random();

            if (data.MaleRatio == -1)
            {
                pokemon.Gender = Gender.Unknown;
            }
            else
            {
                pokemon.Gender = RandomUtils.RangeInclusive(1, 8) <= data.MaleRatio ? Gender.Male : Gender.Female;
            }

            pokemon.Shiny = RandomUtils.ShinyChance();

            pokemon.CalculateDerivedStats();
            pokemon.CurrentHp = pokemon.Hp.Derived;

            pokemon.Ball = "Poké Ball";

            pokemon.Happiness = data.BaseHappiness;

            pokemon.Ability = DataAccess.Abilities.Get(RandomUtils.Element(
                new[] { data.Ability1, data.Ability2, data.HiddenAbility }
                    .Where(ability => ability != null)
                    .Where(ability => ability != "")
                    .ToArray()));

            return pokemon;
        }

        public override string ToString()
        {
            return $"{Name} L{Level}";
        }
    }
}