using System;

namespace Models.Pokemon
{
    public class Learnset
    {
        public MoveAtLevel[] Level { get; set; }
        public string[] Egg { get; set; } = Array.Empty<string>();
        public string[] Machine { get; set; } = Array.Empty<string>();
        public string[] Tutor { get; set; } = Array.Empty<string>();
        public string[] Evolution { get; set; } = Array.Empty<string>();
    }
}