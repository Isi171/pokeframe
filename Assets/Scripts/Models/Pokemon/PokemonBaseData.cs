namespace Models.Pokemon
{
    public class PokemonBaseData
    {
        public int NationalDexNumber { get; set; }
        public int MaleRatio { get; set; }
        public Type Type1 { get; set; }
        public Type Type2 { get; set; }
        public string Ability1 { get; set; }
        public string Ability2 { get; set; }
        public string HiddenAbility { get; set; }
        public int Hp { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }
        public int SpecialAttack { get; set; }
        public int SpecialDefense { get; set; }
        public int Speed { get; set; }
        public EggGroup EggGroup1 { get; set; }
        public EggGroup EggGroup2 { get; set; }
        public int HatchCounter { get; set; }
        public int StepsToHatch { get; set; }
        public string Effort { get; set; }
        public Evolution[] Evolutions { get; set; }
        public ExpGroup ExpGroup { get; set; }
        public int BaseExperience { get; set; }
        public int CaptureRate { get; set; }
        public int BaseHappiness { get; set; }
        public string Height { get; set; }
        public string Weight { get; set; }
        public string Genus { get; set; }
        public string Color { get; set; }
        public string Habitat { get; set; }
        public string Shape { get; set; }
    }
}