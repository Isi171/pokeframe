namespace Models.Pokemon
{
    public class Evolution
    {
        public EvolutionMethod EvolutionMethod { get; set; }
        public int Level { get; set; }
        public string Item { get; set; }
        public string Move { get; set; }
        public string Party { get; set; }
        public string Species { get; set; }
    }
}