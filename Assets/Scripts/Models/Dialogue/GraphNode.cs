namespace Models.Dialogue
{
    public class GraphNode
    {
        public Line[] Lines { get; set; }
        public GraphLink[] Links { get; set; } = { };
        public VariableChange[] VariableChanges { get; set; } = { };
    }
}