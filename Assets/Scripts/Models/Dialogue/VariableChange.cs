using System;
using Models.State;

namespace Models.Dialogue
{
    public class VariableChange
    {
        public string Name { get; set; }
        public ChangeType Type { get; set; }
        public int Value { get; set; }

        public void Perform(Variables variables)
        {
            switch (Type)
            {
                case ChangeType.Set:
                    variables.Set(Name, Value);
                    break;
                case ChangeType.Mod:
                    variables.Mod(Name, Value);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public override string ToString()
        {
            return $"{Name} {Type} {Value}";
        }
    }

    public enum ChangeType
    {
        Set,
        Mod
    }
}