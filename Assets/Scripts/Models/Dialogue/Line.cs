namespace Models.Dialogue
{
    public class Line
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public string Portrait { get; set; }
        public DialogueEffect Effect { get; set; }
        public string EffectData { get; set; }

        public override string ToString()
        {
            return $"{Name}: {(Text.Length > 20 ? $"{Text.Substring(0, 20)}..." : Text)}";
        }
    }
}