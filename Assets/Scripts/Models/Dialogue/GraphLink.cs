namespace Models.Dialogue
{
    public class GraphLink
    {
        public string Line { get; set; }
        public string LinksTo { get; set; }
        public Condition[] ConditionsAnd { get; set; }

        public override string ToString()
        {
            string conditions = ConditionsAnd == null
                ? ""
                : $" [{string.Join<Condition>(", ", ConditionsAnd)}]";
            return $"{Line} (=> {LinksTo}){conditions}";
        }
    }
}