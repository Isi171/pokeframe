using System;
using Models.State;

namespace Models.Dialogue
{
    public class Condition
    {
        public string Name { get; set; }
        public ConditionType Type { get; set; }
        public int Value { get; set; }

        public bool Evaluate(Variables variables)
        {
            switch (Type)
            {
                case ConditionType.EqualTo:
                    return variables.Get(Name) == Value;
                case ConditionType.GreaterThan:
                    return variables.Get(Name) > Value;
                case ConditionType.GreaterThanOrEqualTo:
                    return variables.Get(Name) >= Value;
                case ConditionType.LesserThan:
                    return variables.Get(Name) < Value;
                case ConditionType.LesserThanOrEqualTo:
                    return variables.Get(Name) <= Value;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public override string ToString()
        {
            return $"{Name} {Type} {Value}";
        }
    }

    public enum ConditionType
    {
        EqualTo,
        GreaterThan,
        GreaterThanOrEqualTo,
        LesserThan,
        LesserThanOrEqualTo
    }
}