namespace Models.Dialogue
{
    public class GraphRoot
    {
        public GraphRootEntry[] OrderedEntries;
    }

    public class GraphRootEntry
    {
        public string Node { get; set; }
        public Condition[] ConditionsAnd { get; set; }
    }
}