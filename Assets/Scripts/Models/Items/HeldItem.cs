using System.Linq;

namespace Models.Items
{
    public class HeldItem
    {
        private Item item;

        public Item Item
        {
            get => Consumed || Destroyed ? null : item;
            set => item = value;
        }

        public bool Consumed { get; set; }
        public bool Destroyed { get; set; }

        public bool Is(string name, params string[] additional)
        {
            return Item?.Name == name || additional.Any(altName => Item?.Name == altName);
        }
    }
}