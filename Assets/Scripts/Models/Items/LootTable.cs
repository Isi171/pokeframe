using System.Collections.Generic;

namespace Models.Items
{
    public class LootTable
    {
        public int Money { get; set; }
        public Dictionary<string, int> Items { get; } = new Dictionary<string, int>();
    }
}