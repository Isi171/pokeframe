using System;
using System.Linq;

namespace Models.Items
{
    public class Item
    {
        private static readonly ItemCategory[] CategoriesUsableInBattle =
        {
            ItemCategory.MedicineHealth,
            ItemCategory.MedicineDrink,
            ItemCategory.MedicineStatus,
            ItemCategory.MedicineRestore,
            ItemCategory.MedicineRevive,
            ItemCategory.MedicineHerbalHealth,
            ItemCategory.MedicineHerbalStatus,
            ItemCategory.MedicineHerbalRevive,
            ItemCategory.MedicinePp,
            ItemCategory.StatusBerry,
            ItemCategory.PpBerry,
            ItemCategory.HealthBerry,
            ItemCategory.Battle,
            ItemCategory.Doll,
            ItemCategory.PokeBall
        };

        private static readonly ItemCategory[] CategoriesAutoTargets =
        {
            ItemCategory.Battle,
            ItemCategory.Doll,
            ItemCategory.PokeBall,
            ItemCategory.Adventure
        };

        private static readonly ItemCategory[] CategoriesCannotBeGivenOrThrownAway =
        {
            ItemCategory.Tm,
            ItemCategory.Key
        };

        private static readonly ItemCategory[] CategoriesUsableOutOfBattle =
        {
            ItemCategory.Adventure,
            ItemCategory.MedicineHealth,
            ItemCategory.MedicineDrink,
            ItemCategory.MedicineStatus,
            ItemCategory.MedicineRestore,
            ItemCategory.MedicineRevive,
            ItemCategory.MedicineHerbalHealth,
            ItemCategory.MedicineHerbalStatus,
            ItemCategory.MedicineHerbalRevive,
            ItemCategory.MedicineSacredAsh,
            ItemCategory.MedicinePp,
            ItemCategory.StatusBerry,
            ItemCategory.PpBerry,
            ItemCategory.HealthBerry,
            ItemCategory.EvBerry,
            ItemCategory.Ability,
            ItemCategory.Experience,
            ItemCategory.Mint,
            ItemCategory.Vitamin,
            ItemCategory.EvolutionStone
        };

        public string Name { get; set; }
        public ItemCategory Category { get; set; }
        public int Buy { get; set; }
        public int Sell { get; set; }
        public string Description { get; set; }

        public bool Countless => Category == ItemCategory.Key || Category == ItemCategory.Tm;
        public bool Sellable => Sell >= 0;
        public bool UsableInBattle => CategoriesUsableInBattle.Contains(Category);
        public bool AutoTargets => CategoriesAutoTargets.Contains(Category);
        public bool CannotBeGivenOrThrownAway => CategoriesCannotBeGivenOrThrownAway.Contains(Category);
        public bool UsableOutOfBattle => CategoriesUsableOutOfBattle.Contains(Category);

        public string ExtractTmMove()
        {
            if (Category != ItemCategory.Tm)
            {
                return null;
            }

            // TODO improve
            int space = Name.IndexOf(" ", StringComparison.Ordinal);
            return Name.Substring(space + 1);
        }
        
        public override string ToString()
        {
            return Name;
        }
    }
}