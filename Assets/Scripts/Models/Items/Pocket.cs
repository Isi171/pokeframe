using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Models.Items
{
    public class Pocket
    {
        public ItemCategory[] Filter { get; }
        public ReadOnlyCollection<ItemSlot> Items => items.Values.ToList().AsReadOnly();

        private readonly Dictionary<string, ItemSlot> items;

        public Pocket(params ItemCategory[] filter)
        {
            Filter = filter;
            items = new Dictionary<string, ItemSlot>();
        }

        public void AddOrRemove(Item item, int quantity)
        {
            if (items.ContainsKey(item.Name))
            {
                if (item.Countless)
                {
                    return;
                }

                ItemSlot slot = items[item.Name];
                slot.Quantity += quantity;
                if (slot.Quantity <= 0)
                {
                    items.Remove(item.Name);
                }
            }
            else
            {
                if (quantity <= 0)
                {
                    return;
                }

                ItemSlot slot = new ItemSlot(item, quantity);
                items.Add(item.Name, slot);
            }
        }

        public bool Contains(string item)
        {
            return items.ContainsKey(item);
        }

        public int Amount(string item)
        {
            return items[item].Quantity;
        }
    }
}