namespace Models.Items
{
    public class ItemSlot
    {
        public Item Item { get; }
        public int Quantity { get; set; }

        public ItemSlot(Item item, int quantity)
        {
            Item = item;
            Quantity = quantity;
        }

        public override string ToString()
        {
            return Item != null ? $"{Item.Name} x{Quantity}" : "(empty)";
        }
    }
}