using System;
using System.Collections.Generic;
using System.Linq;
using Managers.Data;

namespace Models.Items
{
    public class Bag
    {
        public int Money { get; set; }
        public Pocket Items { get; }
        public Pocket Medicine { get; }
        public Pocket Berries { get; }
        public Pocket Battle { get; }
        public Pocket PokeBalls { get; }
        public Pocket Vitamins { get; }
        public Pocket Evolution { get; }
        public Pocket Tm { get; }
        public Pocket Valuables { get; }
        public Pocket Key { get; }

        private readonly Pocket[] pockets;

        public IEnumerable<ItemSlot> All => pockets.SelectMany(pocket => pocket.Items);

        public Bag()
        {
            Items = new Pocket(
                ItemCategory.Adventure,
                ItemCategory.Held,
                ItemCategory.HeldSpecific,
                ItemCategory.HeldBoost,
                ItemCategory.Incense,
                ItemCategory.Gem,
                ItemCategory.HeldEv,
                ItemCategory.Fossil);

            Medicine = new Pocket(
                ItemCategory.MedicineHealth,
                ItemCategory.MedicineDrink,
                ItemCategory.MedicineStatus,
                ItemCategory.MedicineRestore,
                ItemCategory.MedicineRevive,
                ItemCategory.MedicineHerbalHealth,
                ItemCategory.MedicineHerbalStatus,
                ItemCategory.MedicineHerbalRevive,
                ItemCategory.MedicineSacredAsh,
                ItemCategory.MedicinePp);

            Berries = new Pocket(
                ItemCategory.StatusBerry,
                ItemCategory.PpBerry,
                ItemCategory.HealthBerry,
                ItemCategory.EvBerry,
                ItemCategory.SuperEffectiveBerry,
                ItemCategory.PinchBerry,
                ItemCategory.DefenseBerry);

            Battle = new Pocket(
                ItemCategory.Battle,
                ItemCategory.Doll);

            PokeBalls = new Pocket(
                ItemCategory.PokeBall);

            Vitamins = new Pocket(
                ItemCategory.Ability,
                ItemCategory.Experience,
                ItemCategory.Mint,
                ItemCategory.Vitamin);

            Evolution = new Pocket(
                ItemCategory.EvolutionStone,
                ItemCategory.Evolution);

            Tm = new Pocket(
                ItemCategory.Tm);

            Valuables = new Pocket(
                ItemCategory.Treasure,
                ItemCategory.Exchange);

            Key = new Pocket(
                ItemCategory.Key);

            pockets = new[] { Items, Medicine, Berries, Battle, PokeBalls, Vitamins, Evolution, Tm, Valuables, Key };
        }

        public void Add(LootTable loot)
        {
            Money += loot.Money;
            foreach (string item in loot.Items.Keys)
            {
                AddOrRemove(item, loot.Items[item]);
            }
        }

        public void AddOrRemove(string itemName, int quantity = 1)
        {
            Item item = DataAccess.Items.Get(itemName);
            foreach (Pocket pocket in pockets)
            {
                if (pocket.Filter.Contains(item.Category))
                {
                    pocket.AddOrRemove(item, quantity);
                    return;
                }
            }

            throw new ArgumentOutOfRangeException();
        }

        public bool Contains(string itemName)
        {
            ItemCategory category = DataAccess.Items.Get(itemName).Category;
            foreach (Pocket pocket in pockets)
            {
                if (pocket.Filter.Contains(category))
                {
                    return pocket.Contains(itemName);
                }
            }

            return false;
        }

        public int Amount(string itemName)
        {
            ItemCategory category = DataAccess.Items.Get(itemName).Category;
            foreach (Pocket pocket in pockets)
            {
                if (pocket.Filter.Contains(category) && pocket.Contains(itemName))
                {
                    return pocket.Amount(itemName);
                }
            }

            return 0;
        }

        public void QuickFill(params string[] items)
        {
            foreach (string item in items)
            {
                AddOrRemove(item);
            }
        }

        public void TEMP_All()
        {
            foreach (string item in DataAccess.Items.All)
            {
                AddOrRemove(item);
            }
        }
    }
}