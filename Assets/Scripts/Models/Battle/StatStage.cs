namespace Models.Battle
{
    public class StatStage
    {
        private int _value;
        public int Value
        {
            get => _value;
            set
            {
                if (value > 6)
                {
                    _value = 6;
                }
                else if (value < -6)
                {
                    _value = -6;
                }
                else
                {
                    _value = value;
                }
            }
        }
    }
}