using Models.Items;

namespace Models.Battle
{
    public class TrainerBattleReference
    {
        public string Id { get; set; }
        public BattleType Type { get; set; }
        public bool YieldsExperience { get; set; }
        public string Enemy1 { get; set; }
        public string Enemy2 { get; set; }
        public string Ally { get; set; }
        public LootTable Prize { get; set; }
        public string VictoryLine { get; set; }
        public string AfterBattleDialogue { get; set; }
        public string Music { get; set; }
        public string VictoryMusic { get; set; }
    }
}