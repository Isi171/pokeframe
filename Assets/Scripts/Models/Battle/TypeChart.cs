using System.Collections.Generic;

namespace Models.Battle
{
    public static class TypeChart
    {
        private static readonly int[,] Table =
        {
            {1, 1, 1, 1, 1, 3, 1, 0, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {2, 1, 3, 3, 1, 2, 3, 0, 2, 1, 1, 1, 1, 3, 2, 1, 2, 3, 1},
            {1, 2, 1, 1, 1, 3, 2, 1, 3, 1, 1, 2, 3, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 3, 3, 3, 1, 3, 0, 1, 1, 2, 1, 1, 1, 1, 1, 2, 1},
            {1, 1, 0, 2, 1, 2, 3, 1, 2, 2, 1, 3, 2, 1, 1, 1, 1, 1, 1},
            {1, 3, 2, 1, 3, 1, 2, 1, 3, 2, 1, 1, 1, 1, 2, 1, 1, 1, 1},
            {1, 3, 3, 3, 1, 1, 1, 3, 3, 3, 1, 2, 1, 2, 1, 1, 2, 3, 1},
            {0, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 2, 1, 1, 3, 1, 1},
            {1, 1, 1, 1, 1, 2, 1, 1, 3, 3, 3, 1, 3, 1, 2, 1, 1, 2, 1},
            {1, 1, 1, 1, 1, 3, 2, 1, 2, 3, 3, 2, 1, 1, 2, 3, 1, 1, 1},
            {1, 1, 1, 1, 2, 2, 1, 1, 1, 2, 3, 3, 1, 1, 1, 3, 1, 1, 1},
            {1, 1, 3, 3, 2, 2, 3, 1, 3, 3, 2, 3, 1, 1, 1, 3, 1, 1, 1},
            {1, 1, 2, 1, 0, 1, 1, 1, 1, 1, 2, 3, 3, 1, 1, 3, 1, 1, 1},
            {1, 2, 1, 2, 1, 1, 1, 1, 3, 1, 1, 1, 1, 3, 1, 1, 0, 1, 1},
            {1, 1, 2, 1, 2, 1, 1, 1, 3, 3, 3, 2, 1, 1, 3, 2, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1, 2, 1, 0, 1},
            {1, 3, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 2, 1, 1, 3, 3, 1},
            {1, 2, 1, 3, 1, 1, 1, 1, 3, 3, 1, 1, 1, 1, 1, 2, 2, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
        };

        private static readonly Dictionary<int, TypeEffectiveness> ValueKey =
            new Dictionary<int, TypeEffectiveness>
            {
                {0, TypeEffectiveness.Immune},
                {1, TypeEffectiveness.Normal},
                {2, TypeEffectiveness.SuperEffective},
                {3, TypeEffectiveness.NotVeryEffective},
            };

        private static readonly Dictionary<Type, int> ColumnKey = new Dictionary<Type, int>
        {
            {Type.Normal, 0},
            {Type.Fighting, 1},
            {Type.Flying, 2},
            {Type.Poison, 3},
            {Type.Ground, 4},
            {Type.Rock, 5},
            {Type.Bug, 6},
            {Type.Ghost, 7},
            {Type.Steel, 8},
            {Type.Fire, 9},
            {Type.Water, 10},
            {Type.Grass, 11},
            {Type.Electric, 12},
            {Type.Psychic, 13},
            {Type.Ice, 14},
            {Type.Dragon, 15},
            {Type.Dark, 16},
            {Type.Fairy, 17},
            {Type.Typeless, 18},
        };

        public static TypeEffectiveness Get(Type attacker, Type defender)
        {
            return ValueKey[Table[ColumnKey[attacker], ColumnKey[defender]]];
        }

        public static TypeEffectiveness Get(Type attacker, Type defender1, Type defender2)
        {
            TypeEffectiveness one = Get(attacker, defender1);
            TypeEffectiveness two = Get(attacker, defender2);

            if (one == TypeEffectiveness.Immune || two == TypeEffectiveness.Immune)
            {
                return TypeEffectiveness.Immune;
            }

            if (one == TypeEffectiveness.NotVeryEffective && two == TypeEffectiveness.NotVeryEffective)
            {
                return TypeEffectiveness.QuarterEffective;
            }

            if (one == TypeEffectiveness.SuperEffective && two == TypeEffectiveness.SuperEffective)
            {
                return TypeEffectiveness.QuadrupleEffective;
            }

            if (one == TypeEffectiveness.SuperEffective && two == TypeEffectiveness.Normal)
            {
                return TypeEffectiveness.SuperEffective;
            }

            if (one == TypeEffectiveness.Normal && two == TypeEffectiveness.SuperEffective)
            {
                return TypeEffectiveness.SuperEffective;
            }

            if (one == TypeEffectiveness.NotVeryEffective && two == TypeEffectiveness.Normal)
            {
                return TypeEffectiveness.NotVeryEffective;
            }

            if (one == TypeEffectiveness.Normal && two == TypeEffectiveness.NotVeryEffective)
            {
                return TypeEffectiveness.NotVeryEffective;
            }

            return TypeEffectiveness.Normal;
        }
    }
}