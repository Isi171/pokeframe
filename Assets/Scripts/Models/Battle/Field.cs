namespace Models.Battle
{
    public class Field
    {
        public Weather Weather { get; set; }
        public int WeatherCounter { get; set; }
    }
}