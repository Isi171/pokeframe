using System.Collections.Generic;

namespace Models.Battle
{
    public static class Tables
    {
        public static readonly Dictionary<int, double> StatModifiers = new Dictionary<int, double>
        {
            {-6, 2.0d / 8.0d},
            {-5, 2.0d / 7.0d},
            {-4, 2.0d / 6.0d},
            {-3, 2.0d / 5.0d},
            {-2, 2.0d / 4.0d},
            {-1, 2.0d / 3.0d},
            {0, 2.0d / 2.0d},
            {1, 3.0d / 2.0d},
            {2, 4.0d / 2.0d},
            {3, 5.0d / 2.0d},
            {4, 6.0d / 2.0d},
            {5, 7.0d / 2.0d},
            {6, 8.0d / 2.0d}
        };

        public static readonly Dictionary<int, double> AccuracyAndEvasionModifiers = new Dictionary<int, double>
        {
            {-6, 3.0d / 9.0d},
            {-5, 3.0d / 8.0d},
            {-4, 3.0d / 7.0d},
            {-3, 3.0d / 6.0d},
            {-2, 3.0d / 5.0d},
            {-1, 3.0d / 4.0d},
            {0, 3.0d / 3.0d},
            {1, 4.0d / 3.0d},
            {2, 5.0d / 3.0d},
            {3, 6.0d / 3.0d},
            {4, 7.0d / 3.0d},
            {5, 8.0d / 3.0d},
            {6, 9.0d / 3.0d}
        };

        public static readonly Dictionary<TypeEffectiveness, double> TypeEffectivenessModifiers =
            new Dictionary<TypeEffectiveness, double>
            {
                {TypeEffectiveness.Immune, 0.0d},
                {TypeEffectiveness.QuarterEffective, 0.25d},
                {TypeEffectiveness.NotVeryEffective, 0.5d},
                {TypeEffectiveness.Normal, 1.0d},
                {TypeEffectiveness.SuperEffective, 2.0d},
                {TypeEffectiveness.QuadrupleEffective, 4.0d}
            };
    }
}