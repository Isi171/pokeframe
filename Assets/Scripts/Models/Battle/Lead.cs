using View.Battle;

namespace Models.Battle
{
    public class Lead
    {
        public BattlerController Battler { get; set; }
        public Pokemon.Pokemon Pokemon { get; set; }
        public Trainer.Trainer Trainer { get; set; }

        public bool ShouldBeAnnounced(BattleData battleData)
        {
            if (battleData.TrainerBattle)
            {
                return true;
            }

            if (Trainer == battleData.Player)
            {
                return true;
            }

            if (Trainer == battleData.Ally)
            {
                return true;
            }

            return false;
        }
    }
}