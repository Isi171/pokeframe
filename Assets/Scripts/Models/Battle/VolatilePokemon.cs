﻿using System;
using System.Collections.Generic;

namespace Models.Battle
{
    public class VolatilePokemon
    {
        public VolatileStat Attack { get; } = new VolatileStat();
        public VolatileStat Defense { get; } = new VolatileStat();
        public VolatileStat SpecialAttack { get; } = new VolatileStat();
        public VolatileStat SpecialDefense { get; } = new VolatileStat();
        public VolatileStat Speed { get; } = new VolatileStat();
        public StatStage AccuracyStage { get; } = new StatStage();
        public StatStage EvasionStage { get; } = new StatStage();

        public bool Flinched { get; set; }
        public bool Confused => ConfusionCounter > 0;
        public int ConfusionCounter { get; set; }
        public bool Infatuated { get; set; }
        public VolatilePokemon InfatuationSource { get; set; }
        public int BadPoisonCounter { get; set; }
        public HashSet<Pokemon.Pokemon> Opponents { get; } = new HashSet<Pokemon.Pokemon>();

        public VolatilePokemon(Pokemon.Pokemon pokemon)
        {
            UpdateStats(pokemon);
        }

        public void UpdateStats(Pokemon.Pokemon pokemon)
        {
            Attack.Base = pokemon.Attack.Derived;
            Defense.Base = pokemon.Defense.Derived;
            SpecialAttack.Base = pokemon.SpecialAttack.Derived;
            SpecialDefense.Base = pokemon.SpecialDefense.Derived;
            Speed.Base = pokemon.Speed.Derived;
        }

        // From Generation V onward, the Speed stat has boundaries put in place that don't apply to any other stat.
        // After taking stat stages and all other modifiers into account, if the resulting Speed stat is above 10000,
        // it is reduced to 10000. The speed stat is then subtracted from 10000 if and only if Trick Room is in effect,
        // and finally if the Speed stat is greater than or equal to 8192, it is reduced by 8192 to produce the final
        // figure used in speed comparisons. As a result, a Pokémon that runs into the limit of 10000 Speed is further
        // reduced to an effective 1808.

        public StatStage GetStatStageFromStatName(StatName statName)
        {
            switch (statName)
            {
                case StatName.Attack:
                    return Attack.Stage;
                case StatName.Defense:
                    return Defense.Stage;
                case StatName.SpecialAttack:
                    return SpecialAttack.Stage;
                case StatName.SpecialDefense:
                    return SpecialDefense.Stage;
                case StatName.Speed:
                    return Speed.Stage;
                case StatName.Accuracy:
                    return AccuracyStage;
                case StatName.Evasion:
                    return EvasionStage;
                case StatName.Hp:
                    throw new ArgumentOutOfRangeException();
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}