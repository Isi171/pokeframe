namespace Models.Battle
{
    public class VolatileStat
    {
        public int Base { get; set; }
        public StatStage Stage { get; } = new StatStage();

        public int Modified => (int)(Base * Tables.StatModifiers[Stage.Value]);
    }
}