using Models.Items;

namespace Models.Battle
{
    public class BattleData
    {
        public BattleType Type { get; set; }
        public bool TrainerBattle { get; set; }
        public bool YieldsExperience { get; set; }
        public Trainer.Trainer Player { get; set; }
        public Trainer.Trainer Enemy1 { get; set; }
        public Trainer.Trainer Enemy2 { get; set; }
        public Trainer.Trainer Ally { get; set; }
        public LootTable Prize { get; set; }
        public string VictoryLine { get; set; }
        public string VictoryMusic { get; set; }
    }
}