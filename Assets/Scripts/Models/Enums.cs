﻿namespace Models
{
    public enum Type
    {
        Typeless,
        Normal,
        Fighting,
        Flying,
        Poison,
        Ground,
        Rock,
        Bug,
        Ghost,
        Steel,
        Fire,
        Water,
        Grass,
        Electric,
        Psychic,
        Ice,
        Dragon,
        Dark,
        Fairy
    }

    public enum StatName
    {
        Hp,
        Attack,
        Defense,
        SpecialAttack,
        SpecialDefense,
        Speed,
        Accuracy,
        Evasion
    }

    public enum Gender
    {
        Male,
        Female,
        Unknown
    }

    public enum Status
    {
        None,
        Burned,
        Frozen,
        Paralyzed,
        Poisoned,
        BadlyPoisoned,
        Asleep,
        Fainted
    }

    public enum VolatileStatus
    {
        None,
        Confusion,
        Flinch
    }

    public enum MoveType
    {
        Physical,
        Special,
        Status
    }

    public enum MoveTarget
    {
        Target,
        AlliedTarget, // TODO
        EnemyTarget, // TODO
        Self,
        Enemies,
        Ally,
        AllyAndSelf,
        AllNoSelf,
        All,
        AlliedSide,
        EnemySide,
        Field
    }

    public enum Weather
    {
        Clear,
        Rain,
        Sun,
        Hail,
        Sandstorm
    }

    public enum TypeEffectiveness
    {
        Immune,
        QuarterEffective,
        NotVeryEffective,
        Normal,
        SuperEffective,
        QuadrupleEffective
    }

    public enum EggGroup
    {
        None,
        Monster,
        Grass,
        Dragon,
        Water1,
        Water2,
        Water3,
        HumanLike,
        Bug,
        Flying,
        Field,
        Fairy,
        Undiscovered,
        Mineral,
        Amorphous,
        Ditto
    }

    public enum ExpGroup
    {
        Erratic,
        Slow,
        MediumSlow,
        MediumFast,
        Fast,
        Fluctuating
    }

    public enum EvolutionMethod
    {
        Level,
        Friendship,
        FriendshipAndFairyMove,
        UsedItem,
        HeldItem,
        KnownMove,
        AttackGreater,
        DefenseGreater,
        AttackDefenseEqual,
        PartyContains,
        Mega
    }

    public enum BattleType
    {
        Single,
        DoubleOneEnemy,
        DoubleTwoEnemies,
        Multi
    }

    public enum DialogueEffect
    {
        None,
        Battle,
        Healing,
        Barter,
        Gift,
        GiftPokemon,
        Warp,
        Quest
    }

    public enum Direction
    {
        Down,
        Up,
        Left,
        Right
    }

    public enum BoxSorting
    {
        Unsorted,
        ByName,
        ByDexNo
    }

    public enum ItemCategory
    {
        // Items pocket
        Adventure,
        Held,
        HeldSpecific,
        HeldBoost,
        Incense,
        Gem,
        HeldEv,
        Fossil,

        // Medicine pocket
        MedicineHealth,
        MedicineDrink,
        MedicineStatus,
        MedicineRestore,
        MedicineRevive,
        MedicineHerbalHealth,
        MedicineHerbalStatus,
        MedicineHerbalRevive,
        MedicineSacredAsh,
        MedicinePp,

        // Berries pocket
        StatusBerry,
        PpBerry,
        HealthBerry,
        EvBerry,
        SuperEffectiveBerry,
        PinchBerry,
        DefenseBerry,

        // Battle pocket
        Battle,
        Doll,

        // Poké Ball pocket
        PokeBall,

        // Vitamins pocket
        Ability,
        Experience,
        Mint,
        Vitamin,

        // Evolution pocket
        EvolutionStone,
        Evolution,

        // TMs pocket
        Tm,

        // Valuables pocket
        Treasure,
        Exchange,

        // Key items pocket
        Key,
    }

    public enum MoveFlag
    {
        Contact,
        BlockedByProtect,
        IgnoresSubstitute,
        CopiableByMirrorMove,
        BounceableByMagicCoat,
        ThawsUser, // Flame Wheel, Sacred Fire, Flare Blitz, Fusion Flare, Scald, Steam Eruption, Burn Up, Pyro Ball, Scorching Sands 
        BlockedByGravity,
        BlockedByHealBlock,
        Snatchable,
        SoundBased,
        Punch,
        Dance,
        AnimatesAgainstAlly,
        RequiresTargetAsleep,
        RequiresUserAsleep,
        PerfectAccuracyIfUserSameType,
    }

    public enum SpecialEffect
    {
        None,

        FailIfTargetHasStatus,
        FailIfTargetConfused,
        Splash,
        OneHitKo, // Guillotine, Horn Drill, Fissure, Sheer Cold
        CrashDamageOnMiss, // Jump Kick, High Jump Kick
        Trap, // Block, Mean Look, Spider Web
        TrapAndDamage, // Bind, Wrap, Clamp, Fire Spin, Sand Tomb, Whirlpool, Magma Storm
        Reflect,
        LightScreen,
        FaintsUser, // Self-Destruct, Explosion
        ForceSwitchTarget, // Roar, Whirlwind, Dragon Tail, Circle Throw
        PayDay,
        LocksAndFatigues, // Thrash, Outrage, Petal Dance
        RequiresRecharge, // Hyper Beam, Blast Burn, Frenzy Plant, Hydro Cannon...
        ChargeTurn, // Sky Attack, Razor Wind
        ChargeTurnSunny, // Solar Beam
        ChargeTurnUpDefense, // Skull Bash
        ChargeTurnSemiInvulnerable, // Dig, Dive, Fly, Bounce
        Substitute,
        Disable,
        Mist,
        HitsDuringDive, // Surf
        HitsDuringDig, // Earthquake
        Haze,
        WeightBasedPower, // Low Kick, Grass Knot
        Counter,
        LevelBasedDamage, // Seismic Toss, Night Shade
        MirrorMove,
        Mimic,
        LeechSeed,
        Rage,
        Bide,
        Metronome,
        FocusEnergy,
        Transform,
        Psywave,
        Struggle,
        TripleKick,
        Rest,
        SuperFang,
        SwitchOutUser, // Teleport, U-Turn, Volt Switch
        Healing,

        Sketch,
        StealsHeldItem, // Thief, Covet
        NextMoveWillHit, // Mind Reader, Lock-On 
        Safeguard,
        MirrorCoat,
        Nightmare,
        Curse,
        Protection, // Protect, Detect
        SleepTalk,
        FlailLike, // Flail, Reversal
        Conversion,
        Conversion2,
        Spite,
        BellyDrum,
        Spikes,
        Attract,
        ForesightLike, // Foresight, Odor Sleuth, Miracle Eye
        PerishSong,
        DestinyBond,
        Endure,
        RolloutLike, // Rollout, Ice Ball
        FalseSwipe,
        FuryCutter,
        HealPartyStatus, // Heal Bell, Aromatherapy
        Return,
        Frustration,
        Present,
        PainSplit,
        Magnitude,
        BatonPass,
        Encore,
        Pursuit,
        RapidSpin,
        HealingSunny, // Moonlight, Morning Sun, Synthesis
        HiddenPower,
        PsychUp,
        FutureAttack, // Future Sight, Doom Desire
        BeatUp,

        FirstTurnOnly, // Fake Out, First Impression
        Uproar,
        Stockpile,
        SpitUp,
        Swallow,
        Torment,
        Facade,
        FocusPunch,
        SmellingSalts,
        FollowMe,
        NaturePower,
        Charge,
        Taunt,
        HelpingHand,
        Trick,
        RolePlay,
        Wish,
        Assist,
        Ingrain,
        MagicCoat,
        Recycle,
        Revenge, // Revenge, Avalanche
        RemoveScreens, // Brick Break
        Yawn,
        KnockOff,
        Endeavor,
        HpBasedPower, // Eruption, Water Spout
        SkillSwap,
        Imprison,
        Refresh,
        Grudge,
        Snatch,
        SecretPower,
        Camouflage,
        MudSport,
        WaterSport,
        WeatherBall,
        HitsDuringFlightAndBounce, // Sky Uppercut, Hurricane

        HealingRoost,
        Gravity,
        MiracleEye,
        WakeUpSlap,
        SpeedBasedPower, // Gryo Ball, Electro Ball, which are reversed
        FaintsUserHealingWish, // Healing Wish, Lunar Dance
        Brine,
        NaturalGift,
        Feint,
        StealsBerry, // Pluck, Bug Bite
        Tailwind,
        Acupressure,
        MetalBurst,
        Payback,
        Assurance,
        Embargo,
        Fling,
        PsychoShift,
        TrumpCard,
        HealBlock,
        TargetHpBasedDamage, // Wring Out, Crush Grip
        PowerTrick,
        GastroAcid,
        LuckyChant,
        MeFirst,
        Copycat,
        PowerSwap,
        GuardSwap,
        Punishment,
        LastResort,
        WorrySeed,
        SuckerPunch,
        ToxicSpikes,
        HeartSwap,
        AquaRing,
        MagnetRise,
        Defog,
        TrickRoom,
        Captivate,
        StealthRock,
        Judgement,
        ShadowForce
    }
}