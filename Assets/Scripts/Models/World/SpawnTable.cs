using Utils;

namespace Models.World
{
    public class SpawnTable
    {
        public string Name { get; set; }
        public SpawnTableEntry[] Entries { get; set; }
        public SpawnTableEntry RandomEntry => RandomUtils.Element(Entries);
    }
}