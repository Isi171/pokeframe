using Utils;

namespace Models.World
{
    public class SpawnTableEntry
    {
        public string Species { get; set; }
        public int MinLevel { get; set; }
        public int MaxLevel { get; set; }
        public int RandomLevel => RandomUtils.RangeInclusive(MinLevel, MaxLevel);
    }
}