namespace Models.State
{
    public class WorldSceneTransientState
    {
        public ActorState[] Actors { get; set; }
    }
}