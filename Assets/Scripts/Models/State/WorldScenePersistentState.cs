namespace Models.State
{
    public class WorldScenePersistentState
    {
        public PickupState[] Pickups { get; set; }
    }
}