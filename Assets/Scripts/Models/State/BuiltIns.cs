namespace Models.State
{
    public static class BuiltIns
    {
        public static string Victory(string battleId)
        {
            return $"Victory{battleId}";
        }

        public static string QuestStage(string questId)
        {
            return $"Quest{questId}";
        }
    }
}