using UnityEngine;

namespace Models.State
{
    public class AbsolutePosition
    {
        public readonly string Map;
        public readonly Vector2 Position;
        public readonly Direction Direction;

        public AbsolutePosition(string map, Vector2 position, Direction direction)
        {
            Map = map;
            Position = position;
            Direction = direction;
        }
    }
}