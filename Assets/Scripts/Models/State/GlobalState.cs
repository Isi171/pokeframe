using System.Collections.Generic;
using Managers.Data;
using Models.Battle;
using Models.Pokemon;
using Models.Quests;
using UnityEngine;

namespace Models.State
{
    public class GlobalState : MonoBehaviour
    {
        public static GlobalState Instance { get; private set; }

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
            }
            else
            {
                DontDestroyOnLoad(gameObject);
                Instance = this;
            }
        }

        // Persistent
        public Options Options { get; set; } = new Options();
        public Trainer.Trainer PlayerTrainer { get; set; }
        public Variables Variables { get; set; } = new Variables();
        public Journal Journal { get; set; } = new Journal();
        public AbsolutePosition LastSafeSpot { get; set; }
        public string CurrentLocation { get; set; } = "a time before time";
        public string CurrentWorldSceneName { get; set; }

        public Dictionary<string, WorldScenePersistentState> WorldScenePersistentStates { get; set; } =
            new Dictionary<string, WorldScenePersistentState>();

        // Transient
        public string CurrentBattleId { get; set; }
        public Pokemon.Pokemon CurrentWildEncounter { private get; set; }
        public WorldSceneTransientState CurrentWorldSceneTransientState { get; set; }
        public List<EvolutionRequest> EvolutionRequests { get; } = new List<EvolutionRequest>();

        public BattleData CurrentBattleData
        {
            get
            {
                // Wild
                if (CurrentBattleId == null)
                {
                    return new BattleData
                    {
                        Type = BattleType.Single,
                        TrainerBattle = false,
                        YieldsExperience = true,
                        Player = PlayerTrainer,
                        Enemy1 = Trainer.Trainer.FakeWildTrainer(CurrentWildEncounter),
                    };
                }

                // Trainer
                TrainerBattleReference reference = DataAccess.Battles.Get(CurrentBattleId);
                return new BattleData
                {
                    Type = reference.Type,
                    TrainerBattle = true,
                    YieldsExperience = reference.YieldsExperience,
                    Player = PlayerTrainer,
                    Enemy1 = DataAccess.Trainers.Get(reference.Enemy1),
                    Enemy2 = reference.Enemy2 == null ? null : DataAccess.Trainers.Get(reference.Enemy2),
                    Ally = reference.Ally == null ? null : DataAccess.Trainers.Get(reference.Ally),
                    Prize = reference.Prize,
                    VictoryLine = reference.VictoryLine,
                    VictoryMusic = reference.VictoryMusic
                };
            }
        }
    }
}