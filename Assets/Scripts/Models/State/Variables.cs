using System.Collections.Generic;
using System.Linq;
using Models.Dialogue;

namespace Models.State
{
    public class Variables
    {
        private readonly Dictionary<string, int> table = new Dictionary<string, int>();

        public IEnumerable<string> AllKeys => table.Keys;

        public int Get(string name)
        {
            if (table.TryGetValue(name, out int variable))
            {
                return variable;
            }

            // TODO log warn? 
            return 0;
        }

        public void Set(string name, int value)
        {
            table[name] = value;
        }

        public void Mod(string name, int value)
        {
            if (table.TryGetValue(name, out int variable))
            {
                table[name] = variable + value;
                return;
            }

            // TODO log warn?
            table[name] = value;
        }

        public bool And(IEnumerable<Condition> conditions, bool negate = false)
        {
            if (conditions == null)
            {
                return true;
            }

            bool and = conditions.Aggregate(true, (current, condition) => current && condition.Evaluate(this));
            return negate ? !and : and;
        }
    }
}