namespace Models.State
{
    public class NewGame
    {
        public AbsolutePosition SpawnPoint { get; set; }
        public Trainer.Trainer Player { get; set; }
    }
}