using Managers.Audio;
using UnityEngine;

namespace Models.State
{
    public class Options
    {
        public const int MaxMusicVolume = 10;
        public const int MinMusicVolume = 0;
        public int MusicVolume { get; set; } = MaxMusicVolume;

        public const int MaxEffectsVolume = 10;
        public const int MinEffectsVolume = 0;
        public int EffectsVolume { get; set; } = MaxEffectsVolume;

        public const int MaxTimescale = 3;
        public const int MinTimescale = 1;
        public int Timescale { get; set; } = MinTimescale;

        public void ApplyStatic()
        {
            AudioManager.Instance.MusicVolume = (float)MusicVolume / MaxMusicVolume;
            AudioManager.Instance.EffectsVolume = (float)EffectsVolume / MaxEffectsVolume;
            Time.timeScale = Timescale;
        }
    }
}