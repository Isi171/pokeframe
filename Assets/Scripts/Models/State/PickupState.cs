namespace Models.State
{
    public class PickupState
    {
        public string Name { get; set; }
        public bool Spent { get; set; }
    }
}