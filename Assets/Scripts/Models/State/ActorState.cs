using UnityEngine;

namespace Models.State
{
    public class ActorState
    {
        public string Name { get; set; }
        public Vector2 Position { get; set; }
        public Direction Direction { get; set; }
    }
}