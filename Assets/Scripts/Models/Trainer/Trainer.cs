using Models.Items;
using Models.State;

namespace Models.Trainer
{
    public class Trainer
    {
        public string Name { get; set; }
        public string Class { get; set; }
        public string Sprite { get; set; }
        public Bag Bag { get; } = new Bag();
        public Party Party { get; } = new Party();
        public Box Box { get; } = new Box();

        public void Heal()
        {
            foreach (Pokemon.Pokemon pokemon in Party.All)
            {
                pokemon.Heal();
            }

            foreach (Pokemon.Pokemon pokemon in Box.All)
            {
                pokemon.Heal();
            }
        }

        public void AddNew(Pokemon.Pokemon pokemon)
        {
            if (!pokemon.Ownership.Valid())
            {
                // TODO fix trainer id
                pokemon.Ownership.SetTrainer(Name, "123456");
                pokemon.Ownership.SetLocation(GlobalState.Instance.CurrentLocation, pokemon.Level);
            }

            if (Party.Count < 6)
            {
                Party.AddLast(pokemon);
            }
            else
            {
                Box.Add(pokemon);
            }
        }

        public static Trainer FakeWildTrainer(Pokemon.Pokemon encounter)
        {
            Trainer trainer = new Trainer();
            trainer.Party.AddLast(encounter);
            return trainer;
        }

        public override string ToString()
        {
            return $"{Class} {Name}";
        }
    }
}