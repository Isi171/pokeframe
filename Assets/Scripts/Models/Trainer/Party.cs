using System.Collections.Generic;
using System.Linq;
using Utils;

namespace Models.Trainer
{
    public class Party
    {
        private readonly Pokemon.Pokemon[] members;

        public IEnumerable<Pokemon.Pokemon> All => members.Where(pokemon => pokemon != null);
        public int Count => All.Count();
        public int CountAvailable => All.Count(pokemon => !pokemon.Fainted);

        public Party(params Pokemon.Pokemon[] elements)
        {
            members = new Pokemon.Pokemon[6];
            foreach (Pokemon.Pokemon pokemon in elements)
            {
                if (pokemon != null)
                {
                    AddLast(pokemon);
                }
            }
        }

        public Pokemon.Pokemon Get(int position)
        {
            if (position < 0 || position > members.Length)
            {
                return null;
            }

            return members[position];
        }

        public Pokemon.Pokemon FirstAvailable(Pokemon.Pokemon excluding = null)
        {
            return All.FirstOrDefault(pokemon => !pokemon.Fainted && pokemon != excluding);
        }

        public bool Contains(Pokemon.Pokemon pokemon)
        {
            return All.Contains(pokemon);
        }

        public void AddLast(Pokemon.Pokemon pokemon)
        {
            for (int i = 0; i < members.Length; i++)
            {
                if (members[i] == null)
                {
                    members[i] = pokemon;
                    return;
                }
            }
        }

        public void Replace(Pokemon.Pokemon old, Pokemon.Pokemon replacement)
        {
            for (int i = 0; i < members.Length; i++)
            {
                if (members[i] == old)
                {
                    members[i] = replacement;
                    return;
                }
            }
        }

        public void SwapPositions(Pokemon.Pokemon first, Pokemon.Pokemon second)
        {
            members.Swap(first, second);
        }

        public void Remove(Pokemon.Pokemon pokemon)
        {
            for (int i = 0; i < members.Length; i++)
            {
                if (members[i] == pokemon)
                {
                    members[i] = null;
                    break;
                }
            }

            Compact();
        }

        private void Compact()
        {
            Pokemon.Pokemon[] compacted = All.ToArray();
            for (int i = 0; i < members.Length; i++)
            {
                if (i < compacted.Length)
                {
                    members[i] = compacted[i];
                }
                else
                {
                    members[i] = null;
                }
            }
        }
    }
}