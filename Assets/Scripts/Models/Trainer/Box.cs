using System;
using System.Collections.Generic;
using System.Linq;
using Managers.Data;

namespace Models.Trainer
{
    public class Box
    {
        public BoxSorting Sorting { get; set; }
        public bool ReversedSorting { get; set; }
        private readonly List<Pokemon.Pokemon> list = new List<Pokemon.Pokemon>();

        private static string NameSelector(Pokemon.Pokemon pokemon)
        {
            return pokemon.Name;
        }

        private static int SpeciesSelector(Pokemon.Pokemon pokemon)
        {
            return DataAccess.PokemonBaseData.Get(pokemon.Species).NationalDexNumber;
        }

        public IEnumerable<Pokemon.Pokemon> All
        {
            get
            {
                return Sorting switch
                {
                    BoxSorting.Unsorted => ReversedSorting
                        ? list.AsEnumerable()!.Reverse()
                        : list.AsEnumerable(),
                    BoxSorting.ByName => ReversedSorting
                        ? list.OrderByDescending(NameSelector).AsEnumerable()
                        : list.OrderBy(NameSelector).AsEnumerable(),
                    BoxSorting.ByDexNo => ReversedSorting
                        ? list.OrderByDescending(SpeciesSelector).AsEnumerable()
                        : list.OrderBy(SpeciesSelector).AsEnumerable(),
                    _ => throw new ArgumentOutOfRangeException()
                };
            }
        }

        public void Add(Pokemon.Pokemon pokemon) => list.Add(pokemon);

        public void Remove(Pokemon.Pokemon pokemon) => list.Remove(pokemon);
    }
}