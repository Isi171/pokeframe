﻿using Models.Pokemon;
using NUnit.Framework;

namespace Tests
{
    public class PokemonStatsTest
    {
        [Test]
        public void ShouldCorrectlyDeriveStats()
        {
            // Example stats from Bulbapedia https://bulbapedia.bulbagarden.net/wiki/Stat#Example_2
            // Accessed 2021-02-26

            // Given
            Pokemon garchomp = new Pokemon
            {
                Level = 78,
                Nature = Nature.ForName("Adamant")
            };
            garchomp.SetBaseStats(108, 130, 95, 80, 85, 102);
            garchomp.SetIvs(24, 12, 30, 16, 23, 5);
            garchomp.SetEvs(74, 190, 91, 48, 84, 23);

            // When
            garchomp.CalculateDerivedStats();

            // Then
            Assert.AreEqual(289, garchomp.Hp.Derived);
            Assert.AreEqual(278, garchomp.Attack.Derived);
            Assert.AreEqual(193, garchomp.Defense.Derived);
            Assert.AreEqual(135, garchomp.SpecialAttack.Derived);
            Assert.AreEqual(171, garchomp.SpecialDefense.Derived);
            Assert.AreEqual(171, garchomp.Speed.Derived);
        }

        [Test]
        public void ShouldNotRaiseSingleEvAbove252()
        {
            // Given
            Pokemon pokemon = new Pokemon();
            
            // When
            pokemon.ChangeEvsCheckingLimit(260, 0, 0, 0, 0, 0);
            
            // Then
            Assert.AreEqual(252, pokemon.Hp.Ev);
        }
        
        [Test]
        public void ShouldNotRaiseMultipleEvsAbove510Total()
        {
            // Given
            Pokemon pokemon = new Pokemon();
            
            // When
            pokemon.ChangeEvsCheckingLimit(252, 252, 252, 252, 252, 252);
            
            // Then
            Assert.AreEqual(252, pokemon.Hp.Ev);
            Assert.AreEqual(252, pokemon.Attack.Ev);
            Assert.AreEqual(6, pokemon.Defense.Ev);
            Assert.AreEqual(0, pokemon.SpecialAttack.Ev);
            Assert.AreEqual(0, pokemon.SpecialDefense.Ev);
            Assert.AreEqual(0, pokemon.Speed.Ev);
        }
        
        [Test]
        public void ShouldCorrectlySetEachBaseStat()
        {
            // Given
            Pokemon pokemon = new Pokemon();
            
            // When
            pokemon.SetBaseStats(1, 2, 3, 4, 5, 6);
            
            // Then
            Assert.AreEqual(1, pokemon.Hp.Base);
            Assert.AreEqual(2, pokemon.Attack.Base);
            Assert.AreEqual(3, pokemon.Defense.Base);
            Assert.AreEqual(4, pokemon.SpecialAttack.Base);
            Assert.AreEqual(5, pokemon.SpecialDefense.Base);
            Assert.AreEqual(6, pokemon.Speed.Base);
        }
        
        [Test]
        public void ShouldCorrectlySetEachIv()
        {
            // Given
            Pokemon pokemon = new Pokemon();
            
            // When
            pokemon.SetIvs(1, 2, 3, 4, 5, 6);
            
            // Then
            Assert.AreEqual(1, pokemon.Hp.Iv);
            Assert.AreEqual(2, pokemon.Attack.Iv);
            Assert.AreEqual(3, pokemon.Defense.Iv);
            Assert.AreEqual(4, pokemon.SpecialAttack.Iv);
            Assert.AreEqual(5, pokemon.SpecialDefense.Iv);
            Assert.AreEqual(6, pokemon.Speed.Iv);
        }
        
        [Test]
        public void ShouldCorrectlySetEachEvWhenChangeEvs()
        {
            // Given
            Pokemon pokemon = new Pokemon();
            
            // When
            pokemon.ChangeEvsCheckingLimit(1, 2, 3, 4, 5, 6);
            
            // Then
            Assert.AreEqual(1, pokemon.Hp.Ev);
            Assert.AreEqual(2, pokemon.Attack.Ev);
            Assert.AreEqual(3, pokemon.Defense.Ev);
            Assert.AreEqual(4, pokemon.SpecialAttack.Ev);
            Assert.AreEqual(5, pokemon.SpecialDefense.Ev);
            Assert.AreEqual(6, pokemon.Speed.Ev);
        }
        
        [Test]
        public void ShouldCorrectlySetEachEvWhenSetEvs()
        {
            // Given
            Pokemon pokemon = new Pokemon();
            
            // When
            pokemon.SetEvs(1, 2, 3, 4, 5, 6);
            
            // Then
            Assert.AreEqual(1, pokemon.Hp.Ev);
            Assert.AreEqual(2, pokemon.Attack.Ev);
            Assert.AreEqual(3, pokemon.Defense.Ev);
            Assert.AreEqual(4, pokemon.SpecialAttack.Ev);
            Assert.AreEqual(5, pokemon.SpecialDefense.Ev);
            Assert.AreEqual(6, pokemon.Speed.Ev);
        }
    }
}