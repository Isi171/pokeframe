using Daybreak.Log;
using Managers.Audio;
using Managers.Cutscene.Events;
using Managers.Cutscene.Waiting;
using Managers.Shared;
using Models.State;
using UnityEngine;
using View.Actor;
using View.Battle;
using View.Dialogue;
using View.Menus;
using View.Menus.Barter;
using View.Menus.Items;
using View.Menus.Party;
using View.State;
using View.World;

namespace Managers.Cutscene
{
    public class CutsceneManager : EventProcessor<CutsceneEvent>
    {
        [SerializeField] private DialogueBoxController dialogueBox;
        [SerializeField] private ActorInputModule playerInputModule;
        [SerializeField] private PokemonInfoMenuController pokemonInfoMenu;
        [SerializeField] private BagMenuController bagMenu;
        [SerializeField] private TransitionEffectController transitionEffect;
        [SerializeField] private LocationViewer locationViewer;
        [SerializeField] private QuestNotifier questNotifier;
        [SerializeField] private BarterMenuController barterMenu;
        [SerializeField] private LevelUpSummaryController levelUpSummary;
        [SerializeField] private WorldMenuController worldMenu;

        public AdventureStrings Strings { get; private set; }
        public DialogueManager DialogueManager { get; private set; }
        public MoveOverwriteDecider MoveOverwriteDecider { get; private set; }
        public MoveLearningManager MoveLearningManager { get; private set; }
        public EvolutionManager EvolutionManager { get; private set; }
        public AdventureItemManager ItemManager { get; private set; }
        public QuestManager QuestManager { get; private set; }
        public AudioManager AudioManager => AudioManager.Instance;
        public DialogueBoxController DialogueBox => dialogueBox;
        public PokemonInfoMenuController PokemonInfoMenu => pokemonInfoMenu;
        public BagMenuController BagMenu => bagMenu;
        public TransitionEffectController TransitionEffect => transitionEffect;
        public ActorInputModule PlayerInputModule => playerInputModule;
        public LocationViewer LocationViewer => locationViewer;
        public QuestNotifier QuestNotifier => questNotifier;
        public BarterMenuController BarterMenu => barterMenu;
        public LevelUpSummaryController LevelUpSummary => levelUpSummary;
        public int[] OldStats { get; set; }

        public AbsolutePosition PlayerAbsolutePosition => new AbsolutePosition(
            GlobalState.Instance.CurrentWorldSceneName,
            playerInputModule.transform.position,
            playerInputModule.GetComponent<ActorMovementController>().Direction);

        private void Start()
        {
            Strings = new AdventureStrings();
            DialogueManager = new DialogueManager();
            MoveOverwriteDecider = new MoveOverwriteDecider();
            MoveLearningManager = new MoveLearningManager();
            EvolutionManager = new EvolutionManager
            {
                MoveLearningManager = MoveLearningManager
            };
            ItemManager = new AdventureItemManager
            {
                EvolutionManager = EvolutionManager
            };
            QuestManager = new QuestManager();

            AllowNextEvent();
        }

        private void Update()
        {
            if (ShouldWait())
            {
                return;
            }

            DisallowNextEvent();

            CutsceneEvent @event = Dequeue();
            if (@event.Loggable)
            {
                Log.Info(@event);
            }

            WaitMode wait = @event.Resolve(this);
            if (wait == null)
            {
                AllowNextEvent();
            }
            else
            {
                wait.AllowNextEvent = AllowNextEvent;
                wait.WaitForCondition = (condition, cleanup) =>
                    StartCoroutine(WaitForConditionCoroutine(condition, cleanup));
                wait.Await();
            }
        }

        protected override void AllowNextEvent()
        {
            base.AllowNextEvent();
            if (EventCount == 0 && !playerInputModule.InputEnabled && !worldMenu.AnyOpen)
            {
                playerInputModule.EnableInput();
            }
        }

        protected override void DisallowNextEvent()
        {
            base.DisallowNextEvent();
            playerInputModule.DisableInput();
        }
    }
}