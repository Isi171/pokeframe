using System.Linq;
using Managers.Cutscene.Waiting;
using Models.Pokemon;

namespace Managers.Cutscene.Events.Learning
{
    public class LearnMoveMessage : CutsceneEvent
    {
        private readonly Pokemon target;
        private readonly string move;

        public LearnMoveMessage(Pokemon target, string move)
        {
            this.target = target;
            this.move = move;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            bool hasFreeSlots = target.Moves.Any(slot => !slot.IsAssigned());
            manager.PushToFront(manager.DialogueManager.StartSingletonNode(
                hasFreeSlots
                    ? manager.Strings.MoveLearned(target, move)
                    : manager.Strings.WantsToLearnMove(target, move)));
            return null;
        }
    }
}