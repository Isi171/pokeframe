using Managers.Cutscene.Waiting;
using Models.Pokemon;
using Models.Trainer;
using View.Menus.Party;

namespace Managers.Cutscene.Events.Learning
{
    public class LearnMove : CutsceneEvent
    {
        private readonly Pokemon target;
        private readonly string move;

        public LearnMove(Pokemon target, string move)
        {
            this.target = target;
            this.move = move;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            for (int i = 0; i < 4; i++)
            {
                if (!target.Moves[i].IsAssigned())
                {
                    target.Moves[i] = new MoveSlot(move);
                    return null;
                }
            }

            manager.PokemonInfoMenu.SetData(new Party(target), new PartyMenuCustomization
            {
                OnClose = () => manager.MoveOverwriteDecider.OverwriteMove(-1),
                OnClickMoveSlot = slot =>
                {
                    for (int i = 0; i < 4; i++)
                    {
                        if (target.Moves[i] == slot)
                        {
                            manager.MoveOverwriteDecider.OverwriteMove(i);
                            return;
                        }
                    }
                },
                NewMove = move
            });
            manager.PokemonInfoMenu.Show();

            manager.PushToFront(new OverwriteMoveWait(target, move));
            return null;
        }
    }
}