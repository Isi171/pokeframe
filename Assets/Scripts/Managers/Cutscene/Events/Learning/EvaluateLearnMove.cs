using System.Collections.Generic;
using Managers.Cutscene.Waiting;
using Models.Pokemon;

namespace Managers.Cutscene.Events.Learning
{
    public class EvaluateLearnMove : CutsceneEvent
    {
        private readonly Pokemon pokemon;

        public EvaluateLearnMove(Pokemon pokemon)
        {
            this.pokemon = pokemon;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            Queue<CutsceneEvent> events = new Queue<CutsceneEvent>();

            foreach (string move in manager.MoveLearningManager.GetByLevelUp(pokemon, pokemon.Level))
            {
                events.Enqueue(new LearnMoveMessage(pokemon, move));
                events.Enqueue(new LearnMove(pokemon, move));
            }

            manager.PushToFront(events);
            return null;
        }
    }
}