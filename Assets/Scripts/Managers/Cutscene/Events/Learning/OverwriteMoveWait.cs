using Managers.Cutscene.Waiting;
using Managers.Shared;
using Models.Pokemon;

namespace Managers.Cutscene.Events.Learning
{
    public class OverwriteMoveWait : CutsceneEvent
    {
        private readonly Pokemon target;
        private readonly string move;

        public OverwriteMoveWait(Pokemon target, string move)
        {
            Loggable = false;

            this.target = target;
            this.move = move;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            MoveOverwriteDecider moveOverwriteDecider = manager.MoveOverwriteDecider;
            if (!moveOverwriteDecider.MoveOverwriteReady)
            {
                manager.PushToFront(new OverwriteMoveWait(target, move));
                return null;
            }

            // If outside the range, new move got declined
            if (moveOverwriteDecider.MoveSlotToOverwrite >= 0 && moveOverwriteDecider.MoveSlotToOverwrite <= 3)
            {
                target.Moves[moveOverwriteDecider.MoveSlotToOverwrite] = new MoveSlot(move);
            }

            moveOverwriteDecider.ResetMoveOverwrite();
            manager.PokemonInfoMenu.gameObject.SetActive(false);
            return null;
        }
    }
}