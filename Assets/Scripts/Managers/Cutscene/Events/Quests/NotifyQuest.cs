using Managers.Cutscene.Waiting;

namespace Managers.Cutscene.Events.Quests
{
    public class NotifyQuest : CutsceneEvent
    {
        private readonly string state;
        private readonly string name;

        public NotifyQuest(string state, string name)
        {
            this.state = state;
            this.name = name;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            manager.QuestNotifier.Show(state, name);
            return null;
        }
    }
}