using Managers.Cutscene.Waiting;

namespace Managers.Cutscene.Events.Quests
{
    public class AdvanceQuest : CutsceneEvent
    {
        private readonly string questId;
        private readonly int stage;

        public AdvanceQuest(string rawQuest)
        {
            string[] parts = rawQuest.Split('#');
            questId = parts[0];
            stage = int.Parse(parts[1]);
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            manager.PushToFront(manager.QuestManager.AdvanceQuestStage(questId, stage));
            return null;
        }
    }
}