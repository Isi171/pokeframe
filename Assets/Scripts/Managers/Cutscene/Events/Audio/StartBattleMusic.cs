using Managers.Cutscene.Waiting;
using Managers.Data;
using Models.State;

namespace Managers.Cutscene.Events.Audio
{
    public class StartBattleMusic : CutsceneEvent
    {
        public override WaitMode Resolve(CutsceneManager manager)
        {
            string trainerBattle = GlobalState.Instance.CurrentBattleId;

            if (trainerBattle == null)
            {
                // TODO properly define wild battle music somewhere
                manager.AudioManager.PlayMusic("WildBattle#Normal");
                return null;
            }

            manager.AudioManager.PlayMusic(DataAccess.Battles.Get(trainerBattle).Music);
            return null;
        }
    }
}