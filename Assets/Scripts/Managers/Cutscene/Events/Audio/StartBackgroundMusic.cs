using Managers.Cutscene.Waiting;
using View.World;

namespace Managers.Cutscene.Events.Audio
{
    public class StartBackgroundMusic : CutsceneEvent
    {
        public override WaitMode Resolve(CutsceneManager manager)
        {
            LocationHolder locationHolder = FindLocationHolder();
            manager.AudioManager.PlayMusic(locationHolder.Music);
            return null;
        }
    }
}