using Managers.Cutscene.Waiting;

namespace Managers.Cutscene.Events.Audio
{
    public class StartSound : CutsceneEvent
    {
        private readonly string track;

        public StartSound(string track)
        {
            this.track = track;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            manager.AudioManager.PlaySoundEffect(track);
            return null;
        }
    }
}