using Managers.Cutscene.Waiting;

namespace Managers.Cutscene.Events.Audio
{
    public class StartMusic : CutsceneEvent
    {
        private readonly string track;

        public StartMusic(string track)
        {
            this.track = track;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            manager.AudioManager.PlayMusic(track);
            return null;
        }
    }
}