using Managers.Cutscene.Waiting;

namespace Managers.Cutscene.Events.Dialogue
{
    public class EndDialogue : CutsceneEvent
    {
        public override WaitMode Resolve(CutsceneManager manager)
        {
            manager.DialogueBox.Close();
            return null;
        }
    }
}