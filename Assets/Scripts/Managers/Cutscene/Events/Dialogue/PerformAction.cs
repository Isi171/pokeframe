using System;
using Managers.Cutscene.Waiting;

namespace Managers.Cutscene.Events.Dialogue
{
    public class PerformAction : CutsceneEvent
    {
        private readonly Action action;

        public PerformAction(Action action)
        {
            this.action = action;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            action();
            return null;
        }
    }
}