using Managers.Cutscene.Waiting;

namespace Managers.Cutscene.Events.Dialogue
{
    public class StartDialogue : CutsceneEvent
    {
        private readonly string dialogueId;

        public StartDialogue(string dialogueId)
        {
            this.dialogueId = dialogueId;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            manager.PushToFront(manager.DialogueManager.StartDialogue(dialogueId));
            return null;
        }
    }
}