using Managers.Cutscene.Waiting;
using View.Menus;

namespace Managers.Cutscene.Events.Dialogue
{
    public class UpdateKeyActivatedButtons : CutsceneEvent
    {
        private readonly bool enable;

        public UpdateKeyActivatedButtons(bool enable)
        {
            this.enable = enable;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            KeyActivatedButton.GlobalEnable = enable;
            return null;
        }
    }
}