using System.Collections.Generic;
using Managers.Cutscene.Events.Boxes;
using Managers.Cutscene.Events.Dialogue.Effects;
using Managers.Cutscene.Events.Items;
using Managers.Cutscene.Events.Menus;
using Managers.Cutscene.Events.Quests;
using Managers.Cutscene.Events.Transition;
using Managers.Cutscene.Waiting;
using Managers.State;
using Models;

namespace Managers.Cutscene.Events.Dialogue
{
    public class PerformDialogueEffect : CutsceneEvent
    {
        private readonly DialogueEffect effect;
        private readonly string data;

        public PerformDialogueEffect(DialogueEffect effect, string data)
        {
            this.effect = effect;
            this.data = data;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            // The forced dialogue end on battle, warp and barter is not ideal, but seems not to cause issues
            Queue<CutsceneEvent> events = new Queue<CutsceneEvent>();
            switch (effect)
            {
                case DialogueEffect.Battle:
                {
                    events.Enqueue(new EndDialogue());
                    events.Enqueue(new PrepareBattle(data));
                    foreach (CutsceneEvent @event in TransitionManager.StartBattle())
                    {
                        events.Enqueue(@event);
                    }

                    break;
                }
                case DialogueEffect.Healing:
                    events.Enqueue(new HealParty());
                    events.Enqueue(new SetSafeSpot());
                    break;
                case DialogueEffect.Gift:
                    events.Enqueue(new ItemsFound(data, false));
                    break;
                case DialogueEffect.GiftPokemon:
                    events.Enqueue(new ReceiveGiftPokemon(data));
                    events.Enqueue(new SentToBoxMessage(data));
                    break;
                case DialogueEffect.Barter:
                    manager.DialogueBox.Close();
                    events.Enqueue(new OpenBarterMenu(data));
                    break;
                case DialogueEffect.Warp:
                {
                    events.Enqueue(new EndDialogue());
                    foreach (CutsceneEvent @event in TransitionManager.Warp(data))
                    {
                        events.Enqueue(@event);
                    }

                    break;
                }
                case DialogueEffect.Quest:
                    events.Enqueue(new AdvanceQuest(data));
                    break;
            }

            manager.PushToFront(events);
            return null;
        }
    }
}