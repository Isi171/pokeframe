using Managers.Cutscene.Waiting;
using Models.Dialogue;
using Models.State;

namespace Managers.Cutscene.Events.Dialogue
{
    public class ChangeVariables : CutsceneEvent
    {
        private readonly VariableChange[] changes;


        public ChangeVariables(VariableChange[] changes)
        {
            this.changes = changes;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            foreach (VariableChange change in changes)
            {
                change.Perform(GlobalState.Instance.Variables);
            }

            return null;
        }
    }
}