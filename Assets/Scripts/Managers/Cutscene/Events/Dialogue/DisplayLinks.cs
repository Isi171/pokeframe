using Managers.Cutscene.Waiting;
using Models.Dialogue;

namespace Managers.Cutscene.Events.Dialogue
{
    public class DisplayLinks : CutsceneEvent
    {
        private readonly GraphLink[] links;

        public DisplayLinks(GraphLink[] links)
        {
            this.links = links;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            manager.DialogueBox.DisplayAnswers(links);
            return new WaitForCondition(() => manager.DialogueBox.CanAdvance,
                () => manager.DialogueBox.CanAdvance = false);
        }
    }
}