using Managers.Cutscene.Waiting;
using Managers.Data;
using Models.Pokemon;
using Models.State;

namespace Managers.Cutscene.Events.Dialogue.Effects
{
    public class ReceiveGiftPokemon : CutsceneEvent
    {
        private readonly string staticEncounterId;

        public ReceiveGiftPokemon(string staticEncounterId)
        {
            this.staticEncounterId = staticEncounterId;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            Pokemon pokemon = DataAccess.StaticEncounters.Get(staticEncounterId);
            GlobalState.Instance.PlayerTrainer.AddNew(pokemon);
            manager.PushToFront(
                manager.DialogueManager.StartNode(manager.Strings.GotGiftPokemon(pokemon)));
            return null;
        }
    }
}