using Managers.Cutscene.Waiting;
using Models.State;

namespace Managers.Cutscene.Events.Dialogue.Effects
{
    public class SetSafeSpot : CutsceneEvent
    {
        public override WaitMode Resolve(CutsceneManager manager)
        {
            GlobalState.Instance.LastSafeSpot = manager.PlayerAbsolutePosition;
            return null;
        }
    }
}