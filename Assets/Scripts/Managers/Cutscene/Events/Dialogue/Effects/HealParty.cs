using Managers.Cutscene.Waiting;
using Models.State;

namespace Managers.Cutscene.Events.Dialogue.Effects
{
    public class HealParty : CutsceneEvent
    {
        public override WaitMode Resolve(CutsceneManager manager)
        {
            GlobalState.Instance.PlayerTrainer.Heal();
            return null;
        }
    }
}