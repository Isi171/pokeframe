using Managers.Cutscene.Waiting;
using Models.Dialogue;

namespace Managers.Cutscene.Events.Dialogue
{
    public class DisplayLine : CutsceneEvent
    {
        private readonly Line line;
        private readonly bool moreTextWillCome;

        public DisplayLine(Line line, bool moreTextWillCome)
        {
            this.line = line;
            this.moreTextWillCome = moreTextWillCome;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            manager.DialogueBox.DisplayLine(line.Name, line.Portrait, line.Text, moreTextWillCome);
            return new WaitForCondition(() => manager.DialogueBox.CanAdvance,
                () => manager.DialogueBox.CanAdvance = false);
        }
    }
}