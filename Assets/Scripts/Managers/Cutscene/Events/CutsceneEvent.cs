using Managers.Cutscene.Waiting;
using UnityEngine;
using Utils;
using View.World;

namespace Managers.Cutscene.Events
{
    public abstract class CutsceneEvent
    {
        private string Type => GetType().Name;
        public bool Loggable { get; protected set; } = true;
        public abstract WaitMode Resolve(CutsceneManager manager);

        protected LocationHolder FindLocationHolder()
        {
            // TODO improve
            return GameObject.FindWithTag("Location").GetComponent<LocationHolder>();
        }

        public override string ToString()
        {
            string fields = LoggingUtils.FormatFields(this);
            return fields.Length == 0
                ? Type
                : $"{Type} ({fields})";
        }
    }
}