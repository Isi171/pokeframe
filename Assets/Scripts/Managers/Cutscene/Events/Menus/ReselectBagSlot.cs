using Managers.Cutscene.Waiting;
using Models.Items;
using View.Menus.Items;

namespace Managers.Cutscene.Events.Menus
{
    public class ReselectBagSlot : CutsceneEvent
    {
        private readonly BagMenuController bagMenu;
        private readonly ItemSlot itemSlot;

        public ReselectBagSlot(BagMenuController bagMenu, ItemSlot itemSlot)
        {
            this.bagMenu = bagMenu;
            this.itemSlot = itemSlot;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            bagMenu.Select(itemSlot);
            return null;
        }
    }
}