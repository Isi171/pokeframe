using Managers.Cutscene.Waiting;
using Models.State;
using UnityEngine;

namespace Managers.Cutscene.Events.Menus
{
    public class CloseBarterMenu : CutsceneEvent
    {
        public override WaitMode Resolve(CutsceneManager manager)
        {
            Time.timeScale = GlobalState.Instance.Options.Timescale;
            return null;
        }
    }
}