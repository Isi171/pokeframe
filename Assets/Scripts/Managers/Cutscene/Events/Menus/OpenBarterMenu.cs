using Managers.Cutscene.Waiting;
using Managers.Data;
using Models.State;
using UnityEngine;

namespace Managers.Cutscene.Events.Menus
{
    public class OpenBarterMenu : CutsceneEvent
    {
        private readonly string merchantId;

        public OpenBarterMenu(string merchantId)
        {
            this.merchantId = merchantId;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            Time.timeScale = 0f;

            manager.BarterMenu.SetData(DataAccess.Merchants.Get(merchantId), GlobalState.Instance.PlayerTrainer.Bag);
            manager.BarterMenu.Show();

            return new WaitForCondition(() => !manager.BarterMenu.isActiveAndEnabled);
        }
    }
}