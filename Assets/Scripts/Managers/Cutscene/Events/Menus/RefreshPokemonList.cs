using Managers.Cutscene.Waiting;
using Models.Pokemon;

namespace Managers.Cutscene.Events.Menus
{
    public class RefreshPokemonList : CutsceneEvent
    {
        private readonly Pokemon pokemon;

        public RefreshPokemonList(Pokemon pokemon)
        {
            this.pokemon = pokemon;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            manager.BagMenu.RefreshList(pokemon);
            return null;
        }
    }
}