using System.Linq;
using Managers.Cutscene.Waiting;
using Models.State;
using UnityEngine;
using View.Actor;
using View.World;

namespace Managers.Cutscene.Events.Transition
{
    public class SaveMapState : CutsceneEvent
    {
        private readonly bool alsoTransient;

        public SaveMapState(bool alsoTransient = false)
        {
            this.alsoTransient = alsoTransient;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            GlobalState state = GlobalState.Instance;

            SavePersistentState(state);

            if (alsoTransient)
            {
                SaveTransientState(state);
            }

            return null;
        }

        private void SavePersistentState(GlobalState state)
        {
            state.WorldScenePersistentStates[state.CurrentWorldSceneName] = new WorldScenePersistentState
            {
                Pickups = GameObject.FindGameObjectsWithTag("Pickup")
                    .Select(pickup => new PickupState
                        { Name = pickup.name, Spent = pickup.GetComponent<Pickup>().Spent })
                    .ToArray()
            };
        }

        private void SaveTransientState(GlobalState state)
        {
            state.CurrentWorldSceneTransientState = new WorldSceneTransientState
            {
                Actors = GameObject.FindGameObjectsWithTag("Actor")
                    .Select(actor => actor.GetComponent<ActorMovementController>())
                    .Select(actor => new ActorState
                    {
                        Name = actor.name,
                        Position = actor.transform.position,
                        Direction = actor.Direction
                    })
                    .ToArray()
            };
        }
    }
}