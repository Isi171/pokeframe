using System.Collections;
using Managers.Cutscene.Waiting;
using UnityEngine;
using View.Actor.Player;

namespace Managers.Cutscene.Events.Transition
{
    public class UnpauseWarpTrigger : CutsceneEvent
    {
        public override WaitMode Resolve(CutsceneManager manager)
        {
            manager.StartCoroutine(UnpauseTriggerCoroutine(manager));
            return null;
        }

        private IEnumerator UnpauseTriggerCoroutine(CutsceneManager manager)
        {
            // Wait a bit to prevent the warps from looping instantly
            yield return new WaitForSeconds(0.1f);
            manager.PlayerInputModule.GetComponent<WarpTriggerController>().Paused = false;
        }
    }
}