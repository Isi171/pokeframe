using Managers.Cutscene.Waiting;
using Models.State;

namespace Managers.Cutscene.Events.Transition
{
    public class ShowCurrentLocation : CutsceneEvent
    {
        public override WaitMode Resolve(CutsceneManager manager)
        {
            manager.LocationViewer.Show(GlobalState.Instance.CurrentLocation);
            return null;
        }
    }
}