using Managers.Cutscene.Waiting;
using UnityEngine.SceneManagement;

namespace Managers.Cutscene.Events.Transition
{
    public class LoadBattle : CutsceneEvent
    {
        public override WaitMode Resolve(CutsceneManager manager)
        {
            SceneManager.LoadScene("Battle");
            return new Stop();
        }
    }
}