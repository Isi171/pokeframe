using Managers.Cutscene.Waiting;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Managers.Cutscene.Events.Transition
{
    public class LoadMap : CutsceneEvent
    {
        private readonly string scene;

        public LoadMap(string scene)
        {
            this.scene = scene;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            AsyncOperation load = SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
            return new WaitForCondition(() => load.isDone);
        }
    }
}