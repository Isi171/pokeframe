using System;
using System.Linq;
using Managers.Cutscene.Waiting;
using Models;
using UnityEngine;
using View.Actor;
using View.World;

namespace Managers.Cutscene.Events.Transition
{
    public class WarpPlayer : CutsceneEvent
    {
        private readonly Vector2 position;
        private readonly Direction direction;
        private readonly string warpId;

        public WarpPlayer(Vector2 position, Direction direction)
        {
            this.position = position;
            this.direction = direction;
        }

        public WarpPlayer(string warpId)
        {
            this.warpId = warpId;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            if (warpId != null)
            {
                // TODO add error message for warp destination not found
                manager.PlayerInputModule.transform.position = GameObject.FindGameObjectsWithTag("Warp")
                    .Select(obj => obj.GetComponent<Warp>())
                    .First(warp => warp.OwnId.Equals(warpId, StringComparison.OrdinalIgnoreCase))
                    .transform
                    .position;
            }
            else
            {
                manager.PlayerInputModule.transform.position = position;
                manager.PlayerInputModule.GetComponent<ActorMovementController>().Turn(direction);
            }

            return null;
        }
    }
}