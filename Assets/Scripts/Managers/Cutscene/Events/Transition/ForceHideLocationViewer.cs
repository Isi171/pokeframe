using Managers.Cutscene.Waiting;

namespace Managers.Cutscene.Events.Transition
{
    public class ForceHideLocationViewer : CutsceneEvent
    {
        public override WaitMode Resolve(CutsceneManager manager)
        {
            manager.LocationViewer.ForceHide();
            return null;
        }
    }
}