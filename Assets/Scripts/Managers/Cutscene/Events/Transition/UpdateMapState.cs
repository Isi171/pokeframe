using Managers.Cutscene.Waiting;
using Models.State;
using UnityEngine;
using View.Actor;
using View.World;

namespace Managers.Cutscene.Events.Transition
{
    public class UpdateMapState : CutsceneEvent
    {
        private readonly string scene;
        private readonly bool alsoTransient;

        public UpdateMapState(string scene, bool alsoTransient = false)
        {
            this.scene = scene;
            this.alsoTransient = alsoTransient;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            GlobalState state = GlobalState.Instance;

            state.CurrentLocation = FindLocationHolder().Location;
            state.CurrentWorldSceneName = scene;

            UpdateFromPersistentState(state);

            if (alsoTransient)
            {
                UpdateFromTransientState(state);
            }

            return null;
        }

        private void UpdateFromPersistentState(GlobalState state)
        {
            if (!state.WorldScenePersistentStates.TryGetValue(state.CurrentWorldSceneName,
                out WorldScenePersistentState sceneState))
            {
                return;
            }

            GameObject[] scenePickups = GameObject.FindGameObjectsWithTag("Pickup");
            foreach (GameObject scenePickup in scenePickups)
            {
                foreach (PickupState pickupState in sceneState.Pickups)
                {
                    if (pickupState.Name == scenePickup.name)
                    {
                        scenePickup.GetComponent<Pickup>().Spent = pickupState.Spent;
                        break;
                    }
                }
            }
        }

        private void UpdateFromTransientState(GlobalState state)
        {
            WorldSceneTransientState sceneState = state.CurrentWorldSceneTransientState;
            GameObject[] sceneActors = GameObject.FindGameObjectsWithTag("Actor");
            foreach (GameObject sceneActor in sceneActors)
            {
                foreach (ActorState actorState in sceneState.Actors)
                {
                    if (actorState.Name == sceneActor.name)
                    {
                        sceneActor.transform.position = actorState.Position;
                        sceneActor.GetComponent<ActorMovementController>().Turn(actorState.Direction);
                        // TODO improve player resolution, maybe?
                        // TODO check why these workarounds here are still necessary, since switching to the deferred-events style should have prevented the problem
                        if (sceneActor.name == "Player")
                        {
                            // Player is loaded in Core scene, before this method is called, so it must be Turn()-ed
                            sceneActor.GetComponent<ActorMovementController>().Turn(actorState.Direction);
                        }
                        else
                        {
                            // Other actors are loaded with their scene and Turn() would be "overwritten" by their initial direction 
                            sceneActor.GetComponent<ActorInputModule>().OverriddenStartingDirection =
                                actorState.Direction;
                        }

                        break;
                    }
                }
            }
        }
    }
}