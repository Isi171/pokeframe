using Managers.Cutscene.Waiting;
using Models.Pokemon;
using Models.State;

namespace Managers.Cutscene.Events.Transition
{
    public class PrepareBattle : CutsceneEvent
    {
        private readonly string trainerBattleId;
        private readonly Pokemon encounter;

        public PrepareBattle(string trainerBattleId)
        {
            this.trainerBattleId = trainerBattleId;
        }

        public PrepareBattle(Pokemon encounter)
        {
            this.encounter = encounter;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            if (trainerBattleId != null)
            {
                GlobalState.Instance.CurrentBattleId = trainerBattleId;
            }
            else
            {
                GlobalState.Instance.CurrentWildEncounter = encounter;
            }

            return null;
        }
    }
}