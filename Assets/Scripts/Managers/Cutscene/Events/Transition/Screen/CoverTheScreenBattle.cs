using Managers.Cutscene.Waiting;

namespace Managers.Cutscene.Events.Transition.Screen
{
    public class CoverTheScreenBattle : CutsceneEvent
    {
        public override WaitMode Resolve(CutsceneManager manager)
        {
            manager.TransitionEffect.CoverTheScreenBattle();
            return new WaitForCondition(() => manager.TransitionEffect.TransitionDone);
        }
    }
}