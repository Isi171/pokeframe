using Managers.Cutscene.Waiting;

namespace Managers.Cutscene.Events.Transition.Screen
{
    public class UncoverTheScreen : CutsceneEvent
    {
        public override WaitMode Resolve(CutsceneManager manager)
        {
            manager.TransitionEffect.UncoverTheScreen();
            return new WaitForCondition(() => manager.TransitionEffect.TransitionDone);
        }
    }
}