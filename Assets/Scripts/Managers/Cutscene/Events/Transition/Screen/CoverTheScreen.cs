using Managers.Cutscene.Waiting;

namespace Managers.Cutscene.Events.Transition.Screen
{
    public class CoverTheScreen : CutsceneEvent
    {
        public override WaitMode Resolve(CutsceneManager manager)
        {
            manager.TransitionEffect.CoverTheScreen();
            return new WaitForCondition(() => manager.TransitionEffect.TransitionDone);
        }
    }
}