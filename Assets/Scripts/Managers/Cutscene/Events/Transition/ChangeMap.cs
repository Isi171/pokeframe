using Managers.Cutscene.Waiting;
using Models.State;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Managers.Cutscene.Events.Transition
{
    public class ChangeMap : CutsceneEvent
    {
        private readonly string scene;

        public ChangeMap(string scene)
        {
            this.scene = scene;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            // TODO add error message for scene not found
            AsyncOperation load = SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);

            AsyncOperation unload = null;
            string currentScene = GlobalState.Instance.CurrentWorldSceneName;
            if (currentScene != null)
            {
                unload = SceneManager.UnloadSceneAsync(currentScene);
            }

            return new WaitForCondition(() => load.isDone && (unload?.isDone ?? true));
        }
    }
}