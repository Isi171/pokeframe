using Managers.Cutscene.Waiting;
using Models.Pokemon;

namespace Managers.Cutscene.Events.Evolutions
{
    public class EvolutionEffect : CutsceneEvent
    {
        private readonly Pokemon toEvolve;
        private readonly string evolutionSpecies;
        private readonly bool destroyHeldItem;

        public EvolutionEffect(Pokemon toEvolve, string evolutionSpecies, bool destroyHeldItem)
        {
            this.toEvolve = toEvolve;
            this.evolutionSpecies = evolutionSpecies;
            this.destroyHeldItem = destroyHeldItem;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            if (destroyHeldItem)
            {
                toEvolve.HeldItem.Item = null;
            }

            toEvolve.Transform(evolutionSpecies);

            return null;
        }
    }
}