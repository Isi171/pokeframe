using Managers.Cutscene.Waiting;
using Models.Pokemon;

namespace Managers.Cutscene.Events.Evolutions
{
    public class EvolutionAnimation : CutsceneEvent
    {
        private readonly Pokemon toEvolve;
        private readonly string evolutionSpecies;

        public EvolutionAnimation(Pokemon toEvolve, string evolutionSpecies)
        {
            this.toEvolve = toEvolve;
            this.evolutionSpecies = evolutionSpecies;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            // TODO animation
            return null;
        }
    }
}