using Managers.Cutscene.Waiting;
using Models.Dialogue;

namespace Managers.Cutscene.Events.Evolutions
{
    public class EvolutionEndMessage : CutsceneEvent
    {
        private readonly string oldName;
        private readonly string newSpecies;
        private readonly bool fromMenu;

        public EvolutionEndMessage(string oldName, string newSpecies, bool fromMenu)
        {
            this.oldName = oldName;
            this.newSpecies = newSpecies;
            this.fromMenu = fromMenu;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            GraphNode node = manager.Strings.EvolutionEnd(oldName, newSpecies);
            manager.PushToFront(
                fromMenu
                    ? manager.DialogueManager.StartSingletonNode(node)
                    : manager.DialogueManager.StartNode(node));
            return null;
        }
    }
}