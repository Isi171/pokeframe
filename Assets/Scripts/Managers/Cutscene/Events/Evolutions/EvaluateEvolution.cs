using Managers.Cutscene.Waiting;
using Models.Pokemon;

namespace Managers.Cutscene.Events.Evolutions
{
    public class EvaluateEvolution : CutsceneEvent
    {
        private readonly Pokemon pokemon;

        public EvaluateEvolution(Pokemon pokemon)
        {
            this.pokemon = pokemon;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            manager.PushToFront(manager.EvolutionManager.HandleEvolutionLevelUp(pokemon));
            return null;
        }
    }
}