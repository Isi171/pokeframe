using Managers.Cutscene.Waiting;
using Models.Dialogue;
using Models.Pokemon;

namespace Managers.Cutscene.Events.Evolutions
{
    public class EvolutionStartMessage : CutsceneEvent
    {
        private readonly Pokemon toEvolve;
        private readonly bool fromMenu;

        public EvolutionStartMessage(Pokemon toEvolve, bool fromMenu)
        {
            this.toEvolve = toEvolve;
            this.fromMenu = fromMenu;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            GraphNode node = manager.Strings.EvolutionStart(toEvolve);
            manager.PushToFront(
                fromMenu
                    ? manager.DialogueManager.StartSingletonNode(node)
                    : manager.DialogueManager.StartNode(node));
            return null;
        }
    }
}