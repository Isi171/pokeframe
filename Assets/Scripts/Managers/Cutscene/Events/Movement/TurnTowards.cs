using Managers.Cutscene.Waiting;
using Models;
using View.Actor;

namespace Managers.Cutscene.Events.Movement
{
    public class TurnTowards : CutsceneEvent
    {
        private readonly ActorMovementController movementController;
        private readonly Direction direction;

        public TurnTowards(ActorMovementController movementController, Direction direction)
        {
            this.movementController = movementController;
            this.direction = direction;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            movementController.Turn(direction);
            return null;
        }
    }
}