using Managers.Cutscene.Waiting;
using View.Actor;

namespace Managers.Cutscene.Events.Movement
{
    public class Exclaim : CutsceneEvent
    {
        private readonly ExclaimerController exclaimer;

        public Exclaim(ExclaimerController exclaimer)
        {
            this.exclaimer = exclaimer;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            return new StartAnimation(exclaimer, exclaimer.StartAnimation);
        }
    }
}