using Managers.Cutscene.Waiting;
using UnityEngine;
using View.Actor;

namespace Managers.Cutscene.Events.Movement
{
    public class MoveNpcTowards : CutsceneEvent
    {
        private readonly NpcInputModule npcInput;
        private readonly Collider2D source;
        private readonly Collider2D target;

        public MoveNpcTowards(NpcInputModule npcInput, GameObject target)
        {
            this.npcInput = npcInput;
            source = npcInput.GetComponent<Collider2D>();
            this.target = target.GetComponent<Collider2D>();
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            npcInput.Direction = target.Distance(source).normal;

            return new WaitForCondition(
                () => target.Distance(source).distance <= .2f,
                () => npcInput.Direction = Vector2.zero
            );
        }
    }
}