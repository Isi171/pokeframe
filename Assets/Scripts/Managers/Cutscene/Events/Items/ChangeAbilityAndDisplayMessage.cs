using Managers.Cutscene.Waiting;
using Managers.Data;
using Models.Pokemon;

namespace Managers.Cutscene.Events.Items
{
    public class ChangeAbilityAndDisplayMessage : CutsceneEvent
    {
        private readonly Pokemon target;
        private readonly string[] possibleAbilities;

        public ChangeAbilityAndDisplayMessage(Pokemon target, string[] possibleAbilities)
        {
            this.target = target;
            this.possibleAbilities = possibleAbilities;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            string currentAbility = target.Ability.Name;

            // Get the index of the next one, circularly
            int i;
            for (i = 0; i < possibleAbilities.Length; i++)
            {
                if (currentAbility == possibleAbilities[i])
                {
                    break;
                }
            }

            i++;
            if (i >= possibleAbilities.Length)
            {
                i = 0;
            }

            string newAbility = possibleAbilities[i];

            // Assign new ability
            target.Ability = DataAccess.Abilities.Get(newAbility);
            manager.PushToFront(
                manager.DialogueManager.StartSingletonNode(manager.Strings.AbilityChanged(target, newAbility)));
            return null;
        }
    }
}