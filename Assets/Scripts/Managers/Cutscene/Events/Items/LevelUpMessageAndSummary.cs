using Managers.Cutscene.Waiting;
using Models.Pokemon;

namespace Managers.Cutscene.Events.Items
{
    public class LevelUpMessageAndSummary : CutsceneEvent
    {
        private readonly Pokemon pokemon;

        public LevelUpMessageAndSummary(Pokemon pokemon)
        {
            this.pokemon = pokemon;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            manager.LevelUpSummary.Display(pokemon, manager.OldStats, true);
            manager.OldStats = null;
            manager.PushToFront(
                manager.DialogueManager.StartSingletonNode(manager.Strings.LevelUp(pokemon),
                    manager.LevelUpSummary.Hide));
            return null;
        }
    }
}