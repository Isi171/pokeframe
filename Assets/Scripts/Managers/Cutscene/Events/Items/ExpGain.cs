using Managers.Cutscene.Waiting;
using Models.Pokemon;

namespace Managers.Cutscene.Events.Items
{
    public class ExpGain : CutsceneEvent
    {
        private readonly Pokemon pokemon;
        private readonly int value;

        public ExpGain(Pokemon pokemon, int value)
        {
            this.pokemon = pokemon;
            this.value = value;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            pokemon.Experience.TotalExp += value;
            return null;
        }
    }
}