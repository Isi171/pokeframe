using Managers.Cutscene.Waiting;
using Models.Pokemon;

namespace Managers.Cutscene.Events.Items
{
    public class NatureChangeMessage : CutsceneEvent
    {
        private readonly string nature;
        private readonly Pokemon target;

        public NatureChangeMessage(string nature, Pokemon target)
        {
            this.nature = nature;
            this.target = target;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            manager.PushToFront(
                manager.DialogueManager.StartSingletonNode(manager.Strings.NatureChanged(target, nature)));
            return null;
        }
    }
}