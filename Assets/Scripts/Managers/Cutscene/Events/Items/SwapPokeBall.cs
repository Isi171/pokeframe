using Managers.Cutscene.Waiting;
using Models.Pokemon;

namespace Managers.Cutscene.Events.Items
{
    public class SwapPokeBall : CutsceneEvent
    {
        private readonly string ball;
        private readonly Pokemon target;

        public SwapPokeBall(string ball, Pokemon target)
        {
            this.ball = ball;
            this.target = target;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            target.Ball = ball;
            return null;
        }
    }
}