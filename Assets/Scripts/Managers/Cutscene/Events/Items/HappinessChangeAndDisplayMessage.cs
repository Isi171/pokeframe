using Managers.Cutscene.Waiting;
using Models.Pokemon;

namespace Managers.Cutscene.Events.Items
{
    public class HappinessChangeAndDisplayMessage : CutsceneEvent
    {
        private readonly Pokemon pokemon;
        private readonly int amount1;
        private readonly int amount2;
        private readonly int amount3;

        public HappinessChangeAndDisplayMessage(Pokemon pokemon, int amount1, int amount2, int amount3)
        {
            this.pokemon = pokemon;
            this.amount1 = amount1;
            this.amount2 = amount2;
            this.amount3 = amount3;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            int usedAmount = pokemon.ChangeHappiness(amount1, amount2, amount3);
            manager.PushToFront(
                manager.DialogueManager.StartSingletonNode(manager.Strings.HappinessChange(pokemon, usedAmount)));
            return null;
        }
    }
}