using Managers.Cutscene.Waiting;
using Models.Pokemon;

namespace Managers.Cutscene.Events.Items
{
    public class ItemHealHealth : CutsceneEvent
    {
        private readonly Pokemon target;
        private readonly int value;

        public ItemHealHealth(Pokemon target, int value)
        {
            this.target = target;
            this.value = value;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            target.CurrentHp += value;
            return null;
        }
    }
}