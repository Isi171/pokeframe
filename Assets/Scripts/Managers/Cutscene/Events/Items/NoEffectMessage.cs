using Managers.Cutscene.Waiting;

namespace Managers.Cutscene.Events.Items
{
    public class NoEffectMessage : CutsceneEvent
    {
        public override WaitMode Resolve(CutsceneManager manager)
        {
            manager.PushToFront(
                manager.DialogueManager.StartSingletonNode(manager.Strings.ItemNoEffect()));
            return null;
        }
    }
}