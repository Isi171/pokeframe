using Managers.Cutscene.Waiting;
using Models.Pokemon;

namespace Managers.Cutscene.Events.Items
{
    public class LevelUp : CutsceneEvent
    {
        private readonly Pokemon pokemon;

        public LevelUp(Pokemon pokemon)
        {
            this.pokemon = pokemon;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            manager.OldStats = new[]
            {
                pokemon.Hp.Derived,
                pokemon.Attack.Derived,
                pokemon.Defense.Derived,
                pokemon.SpecialAttack.Derived,
                pokemon.SpecialDefense.Derived,
                pokemon.Speed.Derived
            };

            int oldHp = pokemon.Hp.Derived;
            pokemon.Level++;
            pokemon.CalculateDerivedStats();
            int newHp = pokemon.Hp.Derived;
            if (oldHp < newHp)
            {
                pokemon.CurrentHp += newHp - oldHp;
            }

            return null;
        }
    }
}