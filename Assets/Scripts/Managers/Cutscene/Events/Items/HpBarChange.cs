using Managers.Cutscene.Waiting;
using Models.Pokemon;

namespace Managers.Cutscene.Events.Items
{
    public class HpBarChange : CutsceneEvent
    {
        private readonly Pokemon pokemon;

        public HpBarChange(Pokemon pokemon)
        {
            this.pokemon = pokemon;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            // TODO hp bar animation
            manager.BagMenu.RefreshList(pokemon);
            return null;
        }
    }
}