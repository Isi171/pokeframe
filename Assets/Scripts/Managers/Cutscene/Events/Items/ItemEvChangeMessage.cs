using System;
using Managers.Cutscene.Waiting;
using Models;
using Models.Pokemon;

namespace Managers.Cutscene.Events.Items
{
    public class ItemEvChangeMessage : CutsceneEvent
    {
        private readonly Pokemon target;
        private readonly StatName stat;
        private readonly int value;

        public ItemEvChangeMessage(Pokemon target, StatName stat, int value)
        {
            this.target = target;
            this.stat = stat;
            this.value = value;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            int actualChange = stat switch
            {
                StatName.Hp => CalculateChange(target.Hp),
                StatName.Attack => CalculateChange(target.Attack),
                StatName.Defense => CalculateChange(target.Defense),
                StatName.SpecialAttack => CalculateChange(target.SpecialAttack),
                StatName.SpecialDefense => CalculateChange(target.SpecialDefense),
                StatName.Speed => CalculateChange(target.Speed),
                _ => throw new ArgumentOutOfRangeException()
            };

            manager.PushToFront(
                manager.DialogueManager.StartSingletonNode(manager.Strings.ItemEvChange(target, stat, actualChange)));
            return null;
        }

        private int CalculateChange(Stat stat)
        {
            if (value > 0)
            {
                return Math.Min(510 - target.GetEvTotal(), Math.Min(252 - stat.Ev, value));
            }

            return -value > stat.Ev ? -stat.Ev : value;
        }
    }
}