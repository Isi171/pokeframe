using Managers.Cutscene.Waiting;
using Managers.Data;
using Models.Dialogue;
using Models.Items;
using Models.State;

namespace Managers.Cutscene.Events.Items
{
    public class ItemsFound : CutsceneEvent
    {
        private readonly string lootId;
        private readonly bool singletonNode;

        public ItemsFound(string lootId, bool singletonNode)
        {
            this.lootId = lootId;
            this.singletonNode = singletonNode;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            LootTable loot = DataAccess.Loot.Get(lootId);
            GlobalState.Instance.PlayerTrainer.Bag.Add(loot);
            GraphNode node = manager.Strings.GotItemsNode(loot);
            manager.PushToFront(
                singletonNode
                    ? manager.DialogueManager.StartSingletonNode(node)
                    : manager.DialogueManager.StartNode(node));
            return null;
        }
    }
}