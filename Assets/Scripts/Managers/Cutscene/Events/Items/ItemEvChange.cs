using Managers.Cutscene.Waiting;
using Models;
using Models.Pokemon;

namespace Managers.Cutscene.Events.Items
{
    public class ItemEvChange : CutsceneEvent
    {
        private readonly Pokemon target;
        private readonly StatName stat;
        private readonly int value;

        public ItemEvChange(Pokemon target, StatName stat, int value)
        {
            this.target = target;
            this.stat = stat;
            this.value = value;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            int oldHp = target.Hp.Derived;
            target.ChangeEvCheckingLimit(stat, value);
            target.CalculateDerivedStats();
            int newHp = target.Hp.Derived;
            if (oldHp != newHp)
            {
                target.CurrentHp += newHp - oldHp;

                if (oldHp > 0 && target.CurrentHp == 0)
                {
                    target.CurrentHp = 1;
                }
                else if (oldHp == 0 && target.CurrentHp > 0)
                {
                    target.CurrentHp = 0;
                }

                manager.BagMenu.RefreshList(target);
            }

            return null;
        }
    }
}