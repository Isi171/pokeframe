using Managers.Cutscene.Waiting;
using Models;
using Models.Pokemon;

namespace Managers.Cutscene.Events.Items
{
    public class ItemHealStatus : CutsceneEvent
    {
        private readonly Pokemon target;

        public ItemHealStatus(Pokemon target)
        {
            this.target = target;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            target.Status = Status.None;
            manager.BagMenu.RefreshList(target);
            return null;
        }
    }
}