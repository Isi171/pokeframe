using Managers.Cutscene.Waiting;
using Models.Pokemon;

namespace Managers.Cutscene.Events.Items
{
    public class ExpGainMessage : CutsceneEvent
    {
        private readonly Pokemon pokemon;
        private readonly int gain;

        public ExpGainMessage(Pokemon pokemon, int gain)
        {
            this.pokemon = pokemon;
            this.gain = gain;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            manager.PushToFront(
                manager.DialogueManager.StartSingletonNode(manager.Strings.ExpGain(pokemon, gain)));
            return null;
        }
    }
}