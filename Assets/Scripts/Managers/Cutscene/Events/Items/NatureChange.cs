using Managers.Cutscene.Waiting;
using Models.Pokemon;

namespace Managers.Cutscene.Events.Items
{
    public class NatureChange : CutsceneEvent
    {
        private readonly string nature;
        private readonly Pokemon target;

        public NatureChange(string nature, Pokemon target)
        {
            this.nature = nature;
            this.target = target;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            target.Nature = Nature.ForName(nature);
            target.CalculateDerivedStats();
            return null;
        }
    }
}