using Managers.Cutscene.Waiting;

namespace Managers.Cutscene.Events.Boxes
{
    public class PartyIsFullMessage : CutsceneEvent
    {
        public override WaitMode Resolve(CutsceneManager manager)
        {
            manager.PushToFront(
                manager.DialogueManager.StartSingletonNode(manager.Strings.PartyIsFull()));
            return null;
        }
    }
}