using Managers.Cutscene.Waiting;

namespace Managers.Cutscene.Events.Boxes
{
    public class CantDepositLastHealthyPokemonMessage : CutsceneEvent
    {
        public override WaitMode Resolve(CutsceneManager manager)
        {
            manager.PushToFront(
                manager.DialogueManager.StartSingletonNode(manager.Strings.CantDepositLastHealthyPokemon()));
            return null;
        }
    }
}