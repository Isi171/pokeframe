using Managers.Cutscene.Waiting;
using Managers.Data;
using Models.Pokemon;
using Models.State;

namespace Managers.Cutscene.Events.Boxes
{
    public class SentToBoxMessage : CutsceneEvent
    {
        private readonly string staticEncounterId;

        public SentToBoxMessage(string staticEncounterId)
        {
            this.staticEncounterId = staticEncounterId;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            Pokemon pokemon = DataAccess.StaticEncounters.Get(staticEncounterId);
            if (!GlobalState.Instance.PlayerTrainer.Party.Contains(pokemon))
            {
                manager.PushToFront(manager.DialogueManager.StartSingletonNode(manager.Strings.SentToBox(pokemon)));
            }

            return null;
        }
    }
}