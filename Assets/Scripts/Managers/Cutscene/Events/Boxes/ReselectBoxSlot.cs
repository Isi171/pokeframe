using Managers.Cutscene.Waiting;
using Models.Pokemon;
using View.Menus.Boxes;

namespace Managers.Cutscene.Events.Boxes
{
    public class ReselectBoxSlot : CutsceneEvent
    {
        private readonly BoxMenuController boxMenu;
        private readonly Pokemon pokemon;

        public ReselectBoxSlot(BoxMenuController boxMenu, Pokemon pokemon)
        {
            this.boxMenu = boxMenu;
            this.pokemon = pokemon;
        }

        public override WaitMode Resolve(CutsceneManager manager)
        {
            boxMenu.Select(pokemon);
            return null;
        }
    }
}