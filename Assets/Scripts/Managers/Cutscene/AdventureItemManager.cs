using System;
using System.Collections.Generic;
using System.Linq;
using Managers.Cutscene.Events;
using Managers.Cutscene.Events.Evolutions;
using Managers.Cutscene.Events.Items;
using Managers.Cutscene.Events.Learning;
using Managers.Cutscene.Events.Menus;
using Managers.Data;
using Managers.Shared;
using Models;
using Models.Items;
using Models.Pokemon;
using Utils;

namespace Managers.Cutscene
{
    public class AdventureItemManager : ItemManager
    {
        public EvolutionManager EvolutionManager { private get; set; }

        public bool CanLearnTm(Item item, Pokemon pokemon)
        {
            string move = item.ExtractTmMove();
            bool alreadyLearned = pokemon.Moves
                .Where(slot => slot.IsAssigned())
                .Any(slot => slot.Move.Name == move);

            if (alreadyLearned)
            {
                return false;
            }

            // For sanity's sake, a TM is compatible if the pokémon could learn the move by the declared lists of
            // TM, Tutor, Evolution or Level (but not as an Egg move) 
            Learnset learnset = DataAccess.Learnsets.Get(pokemon.Species);

            if (learnset.Machine.Contains(move))
            {
                return true;
            }

            if (learnset.Tutor.Contains(move))
            {
                return true;
            }

            if (learnset.Evolution.Contains(move))
            {
                return true;
            }

            if (learnset.Level.Select(pair => pair.Move).Contains(move))
            {
                return true;
            }

            return false;
        }

        public bool ItemIsEffective(Item item, Pokemon target)
        {
            return HandleItem(item, target).Count > 0;
        }

        public Queue<CutsceneEvent> HandleItem(Item item, Pokemon target)
        {
            Queue<CutsceneEvent> events = new Queue<CutsceneEvent>();

            if (!target.Fainted
                && (item.Category == ItemCategory.MedicineRevive
                    || item.Category == ItemCategory.MedicineHerbalRevive
                    || item.Category == ItemCategory.MedicineSacredAsh))
            {
                return events;
            }

            if (item.Category == ItemCategory.MedicineHealth
                || item.Category == ItemCategory.MedicineDrink
                || item.Category == ItemCategory.MedicineHerbalHealth
                || item.Category == ItemCategory.HealthBerry)
            {
                if (!target.Fainted && target.CurrentHp < target.Hp.Derived)
                {
                    return HandleHealthItem(item, target);
                }

                return events;
            }

            if (item.Category == ItemCategory.Mint)
            {
                return HandleMint(item, target);
            }

            if (item.Category == ItemCategory.Vitamin)
            {
                return HandleVitamin(item, target);
            }

            if (item.Category == ItemCategory.Experience)
            {
                return HandleExperience(item, target);
            }

            if (item.Category == ItemCategory.EvBerry)
            {
                return HandleEvBerry(item, target);
            }

            if (item.Category == ItemCategory.EvolutionStone)
            {
                return EvolutionManager.HandleEvolutionUsedItem(target, item.Name);
            }

            if (item.Category == ItemCategory.PokeBall)
            {
                return HandleSwapPokeBall(item, target);
            }

            switch (item.Name)
            {
                case "Antidote":
                case "Pecha Berry":
                    if (target.Status == Status.Poisoned || target.Status == Status.BadlyPoisoned)
                    {
                        events.Enqueue(new ItemHealStatus(target));
                    }

                    break;
                case "Paralyze Heal":
                case "Cheri Berry":
                    if (target.Status == Status.Paralyzed)
                    {
                        events.Enqueue(new ItemHealStatus(target));
                    }

                    break;
                case "Awakening":
                case "Chesto Berry":
                    if (target.Status == Status.Asleep)
                    {
                        events.Enqueue(new ItemHealStatus(target));
                    }

                    break;
                case "Burn Heal":
                case "Rawst Berry":
                    if (target.Status == Status.Burned)
                    {
                        events.Enqueue(new ItemHealStatus(target));
                    }

                    break;
                case "Ice Heal":
                case "Aspear Berry":
                    if (target.Status == Status.Frozen)
                    {
                        events.Enqueue(new ItemHealStatus(target));
                    }

                    break;
                case "Persim Berry":
                    // Persim Berry cannot be used out of battle, as confusion is a volatile status

                    break;
                case "Full Heal":
                case "Lum Berry":
                case "Pewter Crunchies":
                case "Rage Candy Bar":
                    if (target.Status != Status.None && target.Status != Status.Fainted)
                    {
                        events.Enqueue(new ItemHealStatus(target));
                    }

                    break;
                case "Heal Powder":

                    if (target.Status != Status.None && target.Status != Status.Fainted)
                    {
                        events.Enqueue(new ItemHealStatus(target));
                        events.Enqueue(new HappinessChangeAndDisplayMessage(target, -5, -5, -10));
                    }

                    break;
                case "Revive":
                    if (target.Fainted)
                    {
                        events.Enqueue(new ItemHealHealth(target, CustomMath.Div(target.Hp.Derived, 2)));
                        events.Enqueue(new ItemHealStatus(target));
                    }

                    break;
                case "Max Revive":
                    if (target.Fainted)
                    {
                        events.Enqueue(new ItemHealHealth(target, target.Hp.Derived));
                        events.Enqueue(new ItemHealStatus(target));
                    }

                    break;
                case "Revival Herb":
                    if (target.Fainted)
                    {
                        events.Enqueue(new ItemHealHealth(target, target.Hp.Derived));
                        events.Enqueue(new ItemHealStatus(target));
                        events.Enqueue(new HappinessChangeAndDisplayMessage(target, -15, -15, -20));
                    }

                    break;
                case "Full Restore":
                    if (target.CurrentHp < target.Hp.Derived)
                    {
                        events.Enqueue(new ItemHealHealth(target, target.Hp.Derived));
                        events.Enqueue(new HpBarChange(target));
                    }

                    if (target.Status != Status.None && target.Status != Status.Fainted)
                    {
                        events.Enqueue(new ItemHealStatus(target));
                    }

                    break;
                case "Leppa Berry":

                    break;
                case "Ether":

                    break;
                case "Max Ether":

                    break;
                case "Elixir":

                    break;
                case "Max Elixir":

                    break;
                case "Sacred Ash":

                    break;
                case "Ability Capsule":
                    PokemonBaseData data = DataAccess.PokemonBaseData.Get(target.Species);
                    string[] possibleAbilities = new[] { data.Ability1, data.Ability2, data.HiddenAbility }
                        .Where(ability => ability != null)
                        .Where(ability => ability != "")
                        .ToArray();
                    if (possibleAbilities.Length > 1)
                    {
                        events.Enqueue(new ChangeAbilityAndDisplayMessage(target, possibleAbilities));
                    }

                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(item), $"Item {item.Name} is not implemented");
            }

            return events;
        }

        private Queue<CutsceneEvent> HandleHealthItem(Item item, Pokemon target)
        {
            Queue<CutsceneEvent> events = new Queue<CutsceneEvent>();
            events.Enqueue(new ItemHealHealth(target, GetHealingMagnitude(item, target)));
            events.Enqueue(new HpBarChange(target));

            if (item.Name == "Energy Root")
            {
                events.Enqueue(new HappinessChangeAndDisplayMessage(target, -10, -10, -15));
            }
            else if (item.Name == "Energy Powder")
            {
                events.Enqueue(new HappinessChangeAndDisplayMessage(target, -5, -5, -10));
            }

            return events;
        }

        private Queue<CutsceneEvent> HandleMint(Item item, Pokemon target)
        {
            Queue<CutsceneEvent> events = new Queue<CutsceneEvent>();
            int space = item.Name.IndexOf(" ", StringComparison.Ordinal);
            string nature = item.Name.Substring(0, space);

            if (target.Nature.Name != nature)
            {
                events.Enqueue(new NatureChange(nature, target));
                events.Enqueue(new NatureChangeMessage(nature, target));
            }

            return events;
        }

        private Queue<CutsceneEvent> HandleVitamin(Item item, Pokemon target)
        {
            const int amount = 63;
            Queue<CutsceneEvent> events = new Queue<CutsceneEvent>();

            switch (item.Name)
            {
                case "HP Up" when target.Hp.Ev < 252 && target.GetEvTotal() < 510:
                    events.Enqueue(new ItemEvChangeMessage(target, StatName.Hp, amount));
                    events.Enqueue(new ItemEvChange(target, StatName.Hp, amount));
                    break;
                case "Protein" when target.Attack.Ev < 252 && target.GetEvTotal() < 510:
                    events.Enqueue(new ItemEvChangeMessage(target, StatName.Attack, amount));
                    events.Enqueue(new ItemEvChange(target, StatName.Attack, amount));
                    break;
                case "Iron" when target.Defense.Ev < 252 && target.GetEvTotal() < 510:
                    events.Enqueue(new ItemEvChangeMessage(target, StatName.Defense, amount));
                    events.Enqueue(new ItemEvChange(target, StatName.Defense, amount));
                    break;
                case "Calcium" when target.SpecialAttack.Ev < 252 && target.GetEvTotal() < 510:
                    events.Enqueue(new ItemEvChangeMessage(target, StatName.SpecialAttack, amount));
                    events.Enqueue(new ItemEvChange(target, StatName.SpecialAttack, amount));
                    break;
                case "Zinc" when target.SpecialDefense.Ev < 252 && target.GetEvTotal() < 510:
                    events.Enqueue(new ItemEvChangeMessage(target, StatName.SpecialDefense, amount));
                    events.Enqueue(new ItemEvChange(target, StatName.SpecialDefense, amount));
                    break;
                case "Carbos" when target.Speed.Ev < 252 && target.GetEvTotal() < 510:
                    events.Enqueue(new ItemEvChangeMessage(target, StatName.Speed, amount));
                    events.Enqueue(new ItemEvChange(target, StatName.Speed, amount));
                    break;
            }

            if (events.Count > 0)
            {
                events.Enqueue(new HappinessChangeAndDisplayMessage(target, 5, 3, 2));
            }

            return events;
        }

        private Queue<CutsceneEvent> HandleEvBerry(Item item, Pokemon target)
        {
            const int amount = -10;
            Queue<CutsceneEvent> events = new Queue<CutsceneEvent>();

            switch (item.Name)
            {
                case "Pomeg Berry" when target.Hp.Ev > 0 || target.Happiness < 255:
                    events.Enqueue(new ItemEvChangeMessage(target, StatName.Hp, amount));
                    events.Enqueue(new ItemEvChange(target, StatName.Hp, amount));
                    break;
                case "Kelpsy Berry" when target.Attack.Ev > 0 || target.Happiness < 255:
                    events.Enqueue(new ItemEvChangeMessage(target, StatName.Attack, amount));
                    events.Enqueue(new ItemEvChange(target, StatName.Attack, amount));
                    break;
                case "Qualot Berry" when target.Defense.Ev > 0 || target.Happiness < 255:
                    events.Enqueue(new ItemEvChangeMessage(target, StatName.Defense, amount));
                    events.Enqueue(new ItemEvChange(target, StatName.Defense, amount));
                    break;
                case "Hondew Berry" when target.SpecialAttack.Ev > 0 || target.Happiness < 255:
                    events.Enqueue(new ItemEvChangeMessage(target, StatName.SpecialAttack, amount));
                    events.Enqueue(new ItemEvChange(target, StatName.SpecialAttack, amount));
                    break;
                case "Grepa Berry" when target.SpecialDefense.Ev > 0 || target.Happiness < 255:
                    events.Enqueue(new ItemEvChangeMessage(target, StatName.SpecialDefense, amount));
                    events.Enqueue(new ItemEvChange(target, StatName.SpecialDefense, amount));
                    break;
                case "Tamato Berry" when target.Speed.Ev > 0 || target.Happiness < 255:
                    events.Enqueue(new ItemEvChangeMessage(target, StatName.Speed, amount));
                    events.Enqueue(new ItemEvChange(target, StatName.Speed, amount));
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (events.Count > 0)
            {
                events.Enqueue(new HappinessChangeAndDisplayMessage(target, 10, 5, 2));
            }

            return events;
        }

        private Queue<CutsceneEvent> HandleExperience(Item item, Pokemon target)
        {
            Queue<CutsceneEvent> events = new Queue<CutsceneEvent>();

            if (target.Experience.CurrentToNextLevel == -1)
            {
                return events;
            }

            // From using a vitamin
            events.Enqueue(new HappinessChange(target, 5, 3, 2));

            if (item.Name == "Rare Candy")
            {
                events.Enqueue(new LevelUp(target));
                // From leveling up
                events.Enqueue(new HappinessChange(target, 5, 4, 3));
                events.Enqueue(new RefreshPokemonList(target));
                events.Enqueue(new LevelUpMessageAndSummary(target));
                events.Enqueue(new EvaluateLearnMove(target));
                events.Enqueue(new EvaluateEvolution(target));
                return events;
            }

            int gain = item.Name switch
            {
                "Exp. Candy XS" => 100,
                "Exp. Candy S" => 800,
                "Exp. Candy M" => 3_000,
                "Exp. Candy L" => 10_000,
                "Exp. Candy XL" => 30_000,
                _ => throw new ArgumentOutOfRangeException()
            };

            // TODO use partial gain up to level cap, not full listed value
            events.Enqueue(new ExpGainMessage(target, gain));

            int levelUps = 0;
            while (true)
            {
                if (target.Experience.CurrentToNextLevelAdjusted(levelUps) == -1)
                {
                    break;
                }

                if (gain < target.Experience.CurrentToNextLevelAdjusted(levelUps))
                {
                    events.Enqueue(new ExpGain(target, gain));
                    break;
                }

                int partialGainUpToLevel = target.Experience.CurrentToNextLevelAdjusted(levelUps);
                gain -= partialGainUpToLevel;
                levelUps++;
                events.Enqueue(new ExpGain(target, partialGainUpToLevel));
                events.Enqueue(new LevelUp(target));
                events.Enqueue(new HappinessChange(target, 5, 4, 3));
                events.Enqueue(new RefreshPokemonList(target));
                events.Enqueue(new LevelUpMessageAndSummary(target));
                events.Enqueue(new EvaluateLearnMove(target));
            }

            if (levelUps > 0)
            {
                events.Enqueue(new EvaluateEvolution(target));
            }

            return events;
        }

        private Queue<CutsceneEvent> HandleSwapPokeBall(Item item, Pokemon target)
        {
            Queue<CutsceneEvent> events = new Queue<CutsceneEvent>();

            if (target.Ball == item.Name)
            {
                return events;
            }

            events.Enqueue(new SwapPokeBall(item.Name, target));

            return events;
        }
    }
}