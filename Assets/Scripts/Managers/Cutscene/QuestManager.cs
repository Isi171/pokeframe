using System.Collections.Generic;
using System.Linq;
using Managers.Cutscene.Events;
using Managers.Cutscene.Events.Quests;
using Managers.Data;
using Models.Quests;
using Models.State;

namespace Managers.Cutscene
{
    public class QuestManager
    {
        public Queue<CutsceneEvent> AdvanceQuestStage(string questId, int stage)
        {
            Queue<CutsceneEvent> events = new Queue<CutsceneEvent>();
            Journal journal = GlobalState.Instance.Journal;

            // Already done
            if (journal.Complete.Any(progress => progress.Quest == questId))
            {
                return events;
            }

            // Create new
            bool isNew = false;
            if (journal.Active.All(progress => progress.Quest != questId))
            {
                isNew = true;
                journal.Active.Add(new QuestProgress
                {
                    Quest = questId
                });
            }

            // Update existing
            QuestProgress progress = journal.Active.First(progress => progress.Quest == questId);

            if (progress.Stage >= stage)
            {
                return events;
            }

            progress.Stage = stage;
            GlobalState.Instance.Variables.Set(BuiltIns.QuestStage(questId), stage);

            Quest quest = DataAccess.Quests.Get(questId);
            if (quest.Stages[stage].FinishesQuest)
            {
                journal.Active.Remove(progress);
                journal.Complete.Add(progress);
                events.Enqueue(new NotifyQuest("Quest completed", quest.Name));
            }
            else if (isNew)
            {
                events.Enqueue(new NotifyQuest("New quest", quest.Name));
            }
            else
            {
                events.Enqueue(new NotifyQuest("Quest updated", quest.Name));
            }

            return events;
        }
    }
}