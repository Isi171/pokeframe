using System;
using System.Collections.Generic;
using System.Linq;
using Managers.Cutscene.Events;
using Managers.Cutscene.Events.Dialogue;
using Managers.Data;
using Models;
using Models.Dialogue;
using Models.State;

namespace Managers.Cutscene
{
    public class DialogueManager
    {
        public Queue<CutsceneEvent> StartDialogue(string dialogueId)
        {
            GraphRoot graphRoot = DataAccess.DialogueRoots.Get(dialogueId);

            foreach (GraphRootEntry entry in graphRoot.OrderedEntries)
            {
                if (GlobalState.Instance.Variables.And(entry.ConditionsAnd))
                {
                    Queue<CutsceneEvent> events = StartNode(entry.Node);
                    events.Enqueue(new EndDialogue());
                    return events;
                }
            }

            return new Queue<CutsceneEvent>();
        }

        public Queue<CutsceneEvent> StartSingletonNode(GraphNode node, Action cleanupCallback = null)
        {
            Queue<CutsceneEvent> events = new Queue<CutsceneEvent>();

            // Temporary disable of all "exit menu" buttons because otherwise pressing "Cancel" while in a
            // item-induced bag dialogue would close the bag leaving the dialogue on
            events.Enqueue(new UpdateKeyActivatedButtons(false));

            foreach (CutsceneEvent nodeEvent in StartNode(node))
            {
                events.Enqueue(nodeEvent);
            }

            if (cleanupCallback != null)
            {
                events.Enqueue(new PerformAction(cleanupCallback));
            }

            events.Enqueue(new EndDialogue());
            events.Enqueue(new UpdateKeyActivatedButtons(true));

            return events;
        }

        public Queue<CutsceneEvent> StartNode(string nodeId)
        {
            GraphNode node = DataAccess.DialogueNodes.Get(nodeId);
            return StartNode(node);
        }

        public Queue<CutsceneEvent> StartNode(GraphNode node)
        {
            Queue<CutsceneEvent> events = new Queue<CutsceneEvent>();

            if (node.VariableChanges.Any())
            {
                events.Enqueue(new ChangeVariables(node.VariableChanges));
            }

            for (int i = 0; i < node.Lines.Length; i++)
            {
                Line line = node.Lines[i];
                bool moreTextWillCome = i < node.Lines.Length - 1;

                events.Enqueue(new DisplayLine(line, moreTextWillCome));

                if (line.Effect != DialogueEffect.None)
                {
                    events.Enqueue(new PerformDialogueEffect(line.Effect, line.EffectData));
                }
            }

            GraphLink[] availableLinks = node.Links
                .Where(link => GlobalState.Instance.Variables.And(link.ConditionsAnd))
                .ToArray();

            if (availableLinks.Length > 0)
            {
                events.Enqueue(new DisplayLinks(availableLinks));
            }

            return events;
        }
    }
}