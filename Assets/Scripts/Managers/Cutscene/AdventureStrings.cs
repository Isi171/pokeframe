using System;
using Managers.Shared;
using Models;
using Models.Dialogue;
using Models.Items;
using Models.Pokemon;

namespace Managers.Cutscene
{
    public class AdventureStrings : SharedStrings
    {
        private GraphNode BuildNode(string line)
        {
            return new GraphNode
            {
                Lines = new[]
                {
                    new Line
                    {
                        Text = line
                    }
                }
            };
        }

        public GraphNode GotItemsNode(LootTable loot)
        {
            return BuildNode($"You got {DisplayLoot(loot)}.");
        }

        public GraphNode EvolutionStart(Pokemon pokemon)
        {
            return BuildNode($"What? {pokemon.Name} is evolving!");
        }

        public GraphNode EvolutionEnd(string oldName, string newSpecies)
        {
            return BuildNode($"Congratulations! Your {oldName} evolved into {newSpecies}!");
        }

        public GraphNode MoveLearned(Pokemon target, string move)
        {
            return BuildNode(MoveLearnedInternal(target, move));
        }

        public GraphNode WantsToLearnMove(Pokemon target, string move)
        {
            return BuildNode(WantsToLearnMoveInternal(target, move));
        }

        public GraphNode ItemNoEffect()
        {
            return BuildNode("It would have no effect.");
        }

        public GraphNode NatureChanged(Pokemon target, string nature)
        {
            return BuildNode($"{target.Name}'s nature was changed to {nature}!");
        }

        public GraphNode AbilityChanged(Pokemon target, string ability)
        {
            return BuildNode($"{target.Name}'s ability was changed to {ability}!");
        }

        public GraphNode ItemEvChange(Pokemon target, StatName stat, int gain)
        {
            string change;
            if (gain > 0)
            {
                change = $"increased by {gain} to {GetEv(target, stat) + gain}!";
            }
            else if (gain < 0)
            {
                change = $"decreased by {-gain} to {GetEv(target, stat) + gain}!";
            }
            else
            {
                change = "did not change.";
            }

            return BuildNode($"{target.Name}'s base {Stat(stat)} {change}");
        }

        private static int GetEv(Pokemon pokemon, StatName stat)
        {
            return stat switch
            {
                StatName.Hp => pokemon.Hp.Ev,
                StatName.Attack => pokemon.Attack.Ev,
                StatName.Defense => pokemon.Defense.Ev,
                StatName.SpecialAttack => pokemon.SpecialAttack.Ev,
                StatName.SpecialDefense => pokemon.SpecialDefense.Ev,
                StatName.Speed => pokemon.Speed.Ev,
                _ => throw new ArgumentOutOfRangeException(nameof(stat), stat, null)
            };
        }

        public GraphNode HappinessChange(Pokemon pokemon, int amount)
        {
            string verb;
            if (amount >= 10)
            {
                verb = "loved";
            }
            else if (amount > 0)
            {
                verb = "liked";
            }
            else if (amount <= -10)
            {
                verb = "hated";
            }
            else if (amount < 0)
            {
                verb = "disliked";
            }
            else
            {
                // Meme case: this should never happen
                verb = "does not think much of";
            }

            string maxValue = "";
            if (pokemon.Happiness == 255)
            {
                maxValue = $"{It(pokemon)} adores you!";
            }
            else if (pokemon.Happiness == 0)
            {
                maxValue = $"{It(pokemon)} despises you!";
            }

            return BuildNode($"{pokemon.Name} {verb} that! {maxValue}");
        }

        public GraphNode GotGiftPokemon(Pokemon pokemon)
        {
            string nickname = pokemon.Name == pokemon.Species
                ? ""
                : $"{pokemon.Name}, ";
            return BuildNode($"You got {nickname}a {pokemon.Species}!");
        }

        public GraphNode SentToBox(Pokemon pokemon)
        {
            return BuildNode(SentToBoxInternal(pokemon));
        }

        public GraphNode CantDepositLastHealthyPokemon()
        {
            return BuildNode("It's your last healthy Pokémon!");
        }

        public GraphNode PartyIsFull()
        {
            return BuildNode("Your party is full! Deposit someone first.");
        }

        public GraphNode LevelUp(Pokemon pokemon)
        {
            return BuildNode(LevelUpInternal(pokemon));
        }

        public GraphNode ExpGain(Pokemon pokemon, int experience)
        {
            return BuildNode(ExpGainInternal(pokemon, experience));
        }
    }
}