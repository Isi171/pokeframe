using System;

namespace Managers.Cutscene.Waiting
{
    public abstract class WaitMode
    {
        public Action AllowNextEvent { protected get; set; }
        public Action<Func<bool>, Action> WaitForCondition { protected get; set; }

        public abstract void Await();
    }
}