using System;
using Managers.Battle.Waiting;
using Managers.Shared;

namespace Managers.Cutscene.Waiting
{
    public class StartAnimation : WaitMode
    {
        private readonly IAnimationDoneSource source;
        private readonly Action animationStart;

        public StartAnimation(IAnimationDoneSource source, Action animationStart)
        {
            this.source = source;
            this.animationStart = animationStart;
        }

        public override void Await()
        {
            source.SetAnimationDoneCallback(AllowNextEvent);
            animationStart();
        }
    }
}