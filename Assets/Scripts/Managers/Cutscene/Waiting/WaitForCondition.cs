using System;

namespace Managers.Cutscene.Waiting
{
    public class WaitForCondition : WaitMode
    {
        private readonly Func<bool> condition;
        private readonly Action cleanup;

        public WaitForCondition(Func<bool> condition, Action cleanup = null)
        {
            this.condition = condition;
            this.cleanup = cleanup;
        }

        public override void Await()
        {
            WaitForCondition(condition, cleanup);
        }
    }
}