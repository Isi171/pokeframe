using System;
using Daybreak.Log;
using Models.Audio;
using UnityEngine;

namespace Managers.Audio
{
    public class AudioManager : MonoBehaviour
    {
        public static AudioManager Instance { get; private set; }

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
            }
            else
            {
                DontDestroyOnLoad(gameObject);
                Instance = this;
            }
        }

        [SerializeField] private AudioSource music;
        [SerializeField] private AudioLibrary musicLibrary;
        [SerializeField] private AudioSource effects;
        [SerializeField] private AudioLibrary effectsLibrary;

        public float MusicVolume
        {
            set => music.volume = value;
        }

        public float EffectsVolume
        {
            set => effects.volume = value;
        }

        public void PlayMusic(string track)
        {
            Play(musicLibrary, music, track);
        }

        public void PlaySoundEffect(string track)
        {
            Play(effectsLibrary, effects, track);
        }

        private void Play(AudioLibrary library, AudioSource source, string track)
        {
            try
            {
                string[] parts = track.Split('#');

                AudioClip clip = library.Get(parts[0], parts[1]);

                if (clip == null)
                {
                    Log.Error($"No such audio clip: {track}");
                    return;
                }

                source.clip = clip;
                source.Play();
            }
            catch (Exception)
            {
                Log.Error($"Bad audio clip reference: {track ?? "null"}");
            }
        }
    }
}