using System;
using System.Collections;
using System.Collections.Generic;
using Managers.Cutscene;
using Managers.Cutscene.Events;
using Managers.Cutscene.Events.Audio;
using Managers.Cutscene.Events.Dialogue;
using Managers.Cutscene.Events.Dialogue.Effects;
using Managers.Cutscene.Events.Transition;
using Managers.Cutscene.Events.Transition.Screen;
using Managers.Data;
using Managers.Shared;
using Models.Battle;
using Models.Pokemon;
using Models.State;
using UnityEngine;
using UnityEngine.SceneManagement;
using View.State;

namespace Managers.State
{
    public class TransitionManager : MonoBehaviour
    {
        public static TransitionManager Instance { get; private set; }

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
            }
            else
            {
                DontDestroyOnLoad(gameObject);
                Instance = this;
            }
        }

        public void StartGame(AbsolutePosition spawnPoint, bool doTransition)
        {
            StartCoroutine(StartGameCoroutine(spawnPoint, doTransition));
        }

        private IEnumerator StartGameCoroutine(AbsolutePosition spawnPoint, bool doTransition)
        {
            // MainMenu.scene
            GlobalState.Instance.Options.ApplyStatic();

            if (doTransition)
            {
                TransitionEffectController battleTransitionEffect = GameObject.FindGameObjectWithTag("TransitionEffect")
                    .GetComponent<TransitionEffectController>();
                battleTransitionEffect.CoverTheScreen();
                yield return new WaitUntil(() => battleTransitionEffect.TransitionDone);
            }

            AsyncOperation loadCore = SceneManager.LoadSceneAsync("Core");
            while (!loadCore.isDone)
            {
                yield return null;
            }

            // Core.scene
            Queue<CutsceneEvent> events = new Queue<CutsceneEvent>();

            events.Enqueue(new LoadMap(spawnPoint.Map));
            events.Enqueue(new UpdateMapState(spawnPoint.Map));
            events.Enqueue(new WarpPlayer(spawnPoint.Position, spawnPoint.Direction));
            events.Enqueue(new StartBackgroundMusic());
            events.Enqueue(new UncoverTheScreen());

            GameObject.FindGameObjectWithTag("Managers")
                .GetComponent<CutsceneManager>()
                .Enqueue(events);
        }

        public static Queue<CutsceneEvent> Warp(string rawIdentifier)
        {
            string[] parts = rawIdentifier.Split('#');
            return Warp(parts[0], parts[1]);
        }

        public static Queue<CutsceneEvent> Warp(string targetScene, string targetWarpId)
        {
            Queue<CutsceneEvent> events = new Queue<CutsceneEvent>();

            bool changesMap = targetScene != "" && !targetScene.Equals(GlobalState.Instance.CurrentWorldSceneName,
                StringComparison.OrdinalIgnoreCase);

            if (changesMap)
            {
                events.Enqueue(new ForceHideLocationViewer());
                events.Enqueue(new SaveMapState());
                events.Enqueue(new CoverTheScreen());
                events.Enqueue(new ChangeMap(targetScene));
                events.Enqueue(new UpdateMapState(targetScene));
            }

            events.Enqueue(new WarpPlayer(targetWarpId));

            if (changesMap)
            {
                events.Enqueue(new StartBackgroundMusic());
                events.Enqueue(new UncoverTheScreen());
                events.Enqueue(new ShowCurrentLocation());
            }

            events.Enqueue(new UnpauseWarpTrigger());

            return events;
        }

        public static Queue<CutsceneEvent> StartBattle()
        {
            Queue<CutsceneEvent> events = new Queue<CutsceneEvent>();

            events.Enqueue(new SaveMapState(true));
            events.Enqueue(new StartBattleMusic());
            events.Enqueue(new CoverTheScreenBattle());
            events.Enqueue(new LoadBattle());

            return events;
        }

        // An instance is required here in order to run the coroutine on this object, which persists across scene changes
        public void ReturnFromBattle(bool victory)
        {
            StartCoroutine(ReturnFromBattleCoroutine(victory));
        }

        private IEnumerator ReturnFromBattleCoroutine(bool victory)
        {
            // Battle.scene
            TransitionEffectController battleTransitionEffect = GameObject.FindGameObjectWithTag("TransitionEffect")
                .GetComponent<TransitionEffectController>();
            battleTransitionEffect.CoverTheScreen();
            yield return new WaitUntil(() => battleTransitionEffect.TransitionDone);

            AsyncOperation loadCore = SceneManager.LoadSceneAsync("Core");
            while (!loadCore.isDone)
            {
                yield return null;
            }

            // Core.scene
            GlobalState globalState = GlobalState.Instance;
            Queue<CutsceneEvent> events = new Queue<CutsceneEvent>();
            CutsceneManager cutsceneManager =
                GameObject.FindGameObjectWithTag("Managers").GetComponent<CutsceneManager>();

            if (!victory)
            {
                globalState.CurrentBattleId = null;
                globalState.CurrentWildEncounter = null;

                AbsolutePosition safeSpot = globalState.LastSafeSpot;
                events.Enqueue(new LoadMap(safeSpot.Map));
                events.Enqueue(new UpdateMapState(safeSpot.Map));
                events.Enqueue(new WarpPlayer(safeSpot.Position, safeSpot.Direction));
                events.Enqueue(new StartBackgroundMusic());
                events.Enqueue(new HealParty());
                events.Enqueue(new UncoverTheScreen());
            }
            else
            {
                events.Enqueue(new LoadMap(globalState.CurrentWorldSceneName));
                events.Enqueue(new UpdateMapState(globalState.CurrentWorldSceneName, true));
                events.Enqueue(new StartBackgroundMusic());
                events.Enqueue(new UncoverTheScreen());

                AddEvolutionEvents(events, globalState, cutsceneManager.EvolutionManager);
                AddPostBattleDialogueEvents(events, globalState);

                globalState.CurrentBattleId = null;
                globalState.CurrentWildEncounter = null;
            }

            cutsceneManager.Enqueue(events);
        }

        private void AddEvolutionEvents(Queue<CutsceneEvent> events, GlobalState globalState,
            EvolutionManager evolutionManager)
        {
            foreach (EvolutionRequest request in globalState.EvolutionRequests)
            {
                Queue<CutsceneEvent> evolutionEvents = evolutionManager.HandleEvolution(request.ToEvolve,
                    request.EvolutionSpecies, false, request.DestroyHeldItem);
                foreach (CutsceneEvent evolutionEvent in evolutionEvents)
                {
                    events.Enqueue(evolutionEvent);
                }
            }

            globalState.EvolutionRequests.Clear();
        }

        private void AddPostBattleDialogueEvents(Queue<CutsceneEvent> events, GlobalState globalState)
        {
            if (globalState.CurrentBattleId != null)
            {
                TrainerBattleReference battle = DataAccess.Battles.Get(globalState.CurrentBattleId);
                globalState.Variables.Mod(BuiltIns.Victory(battle.Id), 1);

                if (battle.AfterBattleDialogue != null)
                {
                    events.Enqueue(new StartDialogue(battle.AfterBattleDialogue));
                }
            }
        }
    }
}