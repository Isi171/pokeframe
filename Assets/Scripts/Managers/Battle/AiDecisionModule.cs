using System.Collections.Generic;
using System.Linq;
using Daybreak.Log;
using Managers.Battle.Events.Moves.Damage;
using Managers.Battle.Events.Moves.Targeting;
using Models;
using Models.Battle;
using Models.Pokemon;
using UnityEngine;
using Utils;
using View.Battle;
using Event = Managers.Battle.Events.Event;

namespace Managers.Battle
{
    public class AiDecisionModule : DecisionModule
    {
        private BattleManager battleManager;
        private MoveManager moveManager;
        private Field field;

        public void Initialize(BattleManager battleManager)
        {
            this.battleManager = battleManager;
            field = battleManager.Field;
            moveManager = battleManager.MoveManager;
        }

        public override void Decide()
        {
            if (IsStruggling())
            {
                SelectStruggle();
                return;
            }

            DecideCorrectly();
            Ready = true;
        }

        public override void Substitute()
        {
            if (DidIRunOutOfSubstitutions())
            {
                Substitution = null;
                SubstitutionReady = true;
                return;
            }

            // TODO improve choice here with something smarter
            Substitution = Self.Trainer.Party.FirstAvailable(Ally != null ? Ally.Pokemon : null);
            SubstitutionReady = true;
        }

        private void RandomMove()
        {
            SelectedMove = RandomUtils.Element(Self.Pokemon.Moves
                    .Where(slot => slot.IsAssigned())
                    .Where(slot => slot.CurrentPp > 0)
                    .ToArray())
                .Move;

            if (SelectedMove.Target == MoveTarget.Target)
            {
                Target = RandomEnemy();
            }
        }

        private void DecideCorrectly()
        {
            // evaluate items

            // evaluate switch out

            // rank moves
            RankedMove[] rankedMoves = Self.Pokemon.Moves
                .Where(slot => slot.IsAssigned())
                .Where(slot => slot.CurrentPp > 0)
                .Select(slot => slot.Move)
                .Select(Rank)
                .OrderByDescending(tuple => tuple.Rank)
                .ThenByDescending(tuple => tuple.GuaranteedKo)
                .ToArray();

            foreach (RankedMove rankedMove in rankedMoves)
            {
                Log.Trace(Format(rankedMove));
            }

            (SelectedMove, Target) = rankedMoves.First();
        }

        private string Format(RankedMove rankedMove)
        {
            string target = rankedMove.Target == null
                ? ""
                : $" on {rankedMove.Target.Pokemon.Name}";
            string ko = rankedMove.GuaranteedKo == 0
                ? ""
                : " (guaranteed KO)";
            return $"AI {Self.Pokemon.Name}: {rankedMove.Move.Name}{target}: {rankedMove.Rank}{ko}";
        }

        private class RankedMove
        {
            public BattlerController Target { get; }
            public Move Move { get; }
            public int Rank { get; }
            public int GuaranteedKo { get; }

            public RankedMove(Move move, BattlerController target, int rank, bool guaranteedKo = false)
            {
                Move = move;
                Target = move.Target == MoveTarget.Target ? target : null;
                Rank = rank;
                GuaranteedKo = guaranteedKo ? 1 : 0;
            }

            public void Deconstruct(out Move move, out BattlerController target)
            {
                move = Move;
                target = Target;
            }
        }

        private RankedMove Rank(Move move)
        {
            // TODO correctly evaluate both targets in a double battle
            // TODO also boost move if it hits both
            BattlerController target = RandomEnemy();

            Queue<Event> events = moveManager.HandleMove(move, target.Pokemon, Self.Pokemon,
                BattleUtils.IfPresentAndNotFainted(Enemy),
                BattleUtils.IfPresentAndNotFainted(Ally),
                BattleUtils.IfPresentAndNotFainted(Enemy2));

            if (events.Any(e => e is Fail || e is Immune))
            {
                return new RankedMove(move, target, -1);
            }


            // if you have a weather move, chances are you want to keep it up
            if (move.SetWeather != Weather.Clear)
            {
                if (field.Weather != move.SetWeather)
                {
                    if (Ally == null)
                    {
                        return new RankedMove(move, null, 95);
                    }

                    if (Ally.DecisionModule.SelectedMove == null)
                    {
                        return new RankedMove(move, null, 95);
                    }

                    if (Ally.DecisionModule.SelectedMove.SetWeather == Weather.Clear)
                    {
                        return new RankedMove(move, null, 95);
                    }
                }

                return new RankedMove(move, null, -1);
            }


            if (move.BasePower > 0
                || move.SetDamage > 0
                || MoveManager.SetDamageEffects.Contains(move.SpecialEffect)
                || MoveManager.VariablePowerEffects.Contains(move.SpecialEffect))
            {
                // A killing blow is ranked 100
                // All others are ranked according to how much max health percentage they take
                int damage = GetDamage(move, target.Pokemon);
                int rank = damage >= target.Pokemon.CurrentHp
                    ? 100
                    : (int)100.0d * damage / target.Pokemon.Hp.Derived;
                bool guaranteedKo = rank == 100;

                // Adjust rank with modifiers
                rank = CustomMath.Mul(rank, 1.2d,
                    move.Priority > 0
                    && Self.Pokemon.CurrentHp < Self.Pokemon.Hp.Derived / 3
                    && battleManager.GetSpeedModifiedByContext(Self.Pokemon) <=
                    battleManager.GetSpeedModifiedByContext(target.Pokemon));
                if (guaranteedKo && move.Priority > 0)
                {
                    rank += 50;
                }

                rank = CustomMath.Mul(rank, 1.1d, move.Leech && Self.Pokemon.CurrentHp < Self.Pokemon.Hp.Derived);

                if (move.TargetStatChangesChance == 100)
                {
                    // It's actually a debuff move with some damage mixed in
                    rank = 30 + rank / 2;
                }
                else if (move.TargetStatChangesChance > 0)
                {
                    rank = CustomMath.Mul(rank, 1.1d);
                }
                // TODO rank status, confusion, flinch

                // Weigh damage and special effect vis-a-vis accuracy
                // TODO use modified accuracy in weather
                double accuracyScale = CustomMath.Div(move.Accuracy, 100);
                rank = CustomMath.Mul(rank, accuracyScale, move.Accuracy > 0);

                return new RankedMove(move, target, rank, guaranteedKo);
            }

            // Status moves
            if (move.SpecialEffect == SpecialEffect.FailIfTargetHasStatus)
            {
                if (moveManager.StatusImmunityCheck(move, Self.Pokemon, target.Pokemon))
                {
                    return new RankedMove(move, target, -1);
                }

                int rank = 40;
                return new RankedMove(move, target, rank);
            }

            if (move.SpecialEffect == SpecialEffect.FailIfTargetConfused || move.ConfusionChance == 100)
            {
                if (target.Pokemon.VolatilePokemon.Confused)
                {
                    return new RankedMove(move, target, -1);
                }

                int rank = 40;
                return new RankedMove(move, target, rank);
            }

            // Setup moves
            if (move.UserStatChangesChance == 100)
            {
                int rank = 30;
                if (Self.Pokemon.CurrentHp < Self.Pokemon.Hp.Derived / 3)
                {
                    rank = 0;
                }

                foreach (StatName stat in move.UserStatChanges.Keys)
                {
                    if (move.UserStatChanges[stat] > 0
                        && Self.Pokemon.VolatilePokemon.GetStatStageFromStatName(stat).Value < 6)
                    {
                        rank += 10;
                    }
                }

                return new RankedMove(move, target, rank);
            }

            // Debuff moves
            if (move.TargetStatChangesChance == 100)
            {
                int rank = 20;
                if (Self.Pokemon.CurrentHp < Self.Pokemon.Hp.Derived / 3)
                {
                    rank = 0;
                }

                foreach (StatName stat in move.TargetStatChanges.Keys)
                {
                    if (move.TargetStatChanges[stat] < 0
                        && target.Pokemon.VolatilePokemon.GetStatStageFromStatName(stat).Value > -6)
                    {
                        rank += 10;
                    }
                }

                return new RankedMove(move, target, rank);
            }

            // Special effects
            if (MoveManager.HealingEffects.Contains(move.SpecialEffect)
                && Self.Pokemon.CurrentHp <= Self.Pokemon.Hp.Derived / 2)
            {
                return new RankedMove(move, target, 50);
            }

            if (move.SpecialEffect == SpecialEffect.BellyDrum)
            {
                if (Self.Pokemon.CurrentHp == Self.Pokemon.Hp.Derived
                    && Self.Pokemon.VolatilePokemon.Attack.Stage.Value < 2)
                {
                    return new RankedMove(move, target, 95);
                }

                return new RankedMove(move, target, -1);
            }

            return new RankedMove(move, target, 0);
        }

        private int GetDamage(Move move, Pokemon target)
        {
            if (target == null)
            {
                return 0;
            }

            Pokemon enemy1 = BattleUtils.IfPresentAndNotFainted(Enemy);
            Pokemon enemy2 = BattleUtils.IfPresentAndNotFainted(Enemy2);
            bool multiTarget = enemy1 != null
                               && enemy2 != null
                               && (move.Target == MoveTarget.Enemies || move.Target == MoveTarget.AllNoSelf);
            TypeEffectiveness effectiveness = moveManager.ResolveTypeEffectiveness(move, Self.Pokemon, target);
            return Damage.GetDamageValue(move, Self.Pokemon, target, multiTarget, false, effectiveness, moveManager);
        }
    }
}