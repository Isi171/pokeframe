﻿using System;
using System.Collections.Generic;
using System.Linq;
using Managers.Battle.Events;
using Managers.Battle.Events.Abilities;
using Managers.Battle.Events.Faint;
using Managers.Battle.Events.Field;
using Managers.Battle.Events.Moves;
using Managers.Battle.Events.Moves.Damage;
using Managers.Battle.Events.Moves.Effects;
using Managers.Battle.Events.Moves.Effects.Special;
using Managers.Battle.Events.Moves.Targeting;
using Managers.Battle.Events.Statuses;
using Models;
using Models.Battle;
using Models.Pokemon;
using Utils;
using Type = Models.Type;

namespace Managers.Battle
{
    public class MoveManager
    {
        public static readonly SpecialEffect[] SetDamageEffects =
        {
            SpecialEffect.LevelBasedDamage,
            SpecialEffect.Psywave,
            SpecialEffect.SuperFang,
            SpecialEffect.OneHitKo,
            SpecialEffect.Endeavor,
        };

        public static readonly SpecialEffect[] VariablePowerEffects =
        {
            SpecialEffect.Return,
            SpecialEffect.Frustration,
            SpecialEffect.FlailLike,
            SpecialEffect.HpBasedPower
        };

        public static readonly SpecialEffect[] HealingEffects =
        {
            SpecialEffect.Healing,
            SpecialEffect.HealingSunny,
            SpecialEffect.HealingRoost
        };

        public Field Field { private get; set; }
        public BattleItemManager ItemManager { private get; set; }

        private Pokemon[] ResolveTargets(Move move, Pokemon user, Pokemon enemy, Pokemon ally,
            Pokemon secondaryEnemy)
        {
            switch (move.Target)
            {
                case MoveTarget.Self:
                case MoveTarget.AlliedSide:
                case MoveTarget.EnemySide:
                case MoveTarget.Field:
                    return new[] { user };
                case MoveTarget.Ally:
                    return new[] { ally };
                case MoveTarget.AllyAndSelf:
                    return new[] { user, ally };
                case MoveTarget.Enemies:
                    return new[] { enemy, secondaryEnemy };
                case MoveTarget.AllNoSelf:
                    return new[] { enemy, secondaryEnemy, ally };
                case MoveTarget.All:
                    return new[] { user, enemy, secondaryEnemy, ally };
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private bool AccuracyCheck(Move move, Pokemon user, Pokemon target, Weather weather)
        {
            if (move.HasFlag(MoveFlag.PerfectAccuracyIfUserSameType) && user.HasType(move.Type))
            {
                return true;
            }

            if (move.SpecialEffect == SpecialEffect.OneHitKo)
            {
                int oneHitKoBase = move.Name == "Sheer Cold" && !user.HasType(Type.Ice) ? 20 : 30;
                int oneHitKoThreshold = oneHitKoBase + user.Level - target.Level;
                return RandomUtils.RangeInclusive(1, 100) <= oneHitKoThreshold;
            }

            int baseAccuracy = move.ModifiedAccuracyInWeather.ContainsKey(weather)
                ? move.ModifiedAccuracyInWeather[weather]
                : move.Accuracy;

            // Moves that bypass accuracy checks
            if (baseAccuracy == 0)
            {
                return true;
            }

            StatStage adjustedStage = new StatStage
            {
                Value = user.VolatilePokemon.AccuracyStage.Value - target.VolatilePokemon.EvasionStage.Value
            };
            double accuracyAndEvasionModifier = Tables.AccuracyAndEvasionModifiers[adjustedStage.Value];

            int threshold = CustomMath.Mul(baseAccuracy, accuracyAndEvasionModifier);

            threshold = CustomMath.Mul(threshold, 0.9d, target.HeldItem.Is("Bright Powder", "Lax Incense"));
            threshold = CustomMath.Mul(threshold, 0.8d, weather == Weather.Sandstorm && target.Ability.Is("Sand Veil"));
            threshold = CustomMath.Mul(threshold, 0.8d, weather == Weather.Hail && target.Ability.Is("Snow Cloak"));
            threshold = CustomMath.Mul(threshold, 1.3d, user.Ability.Is("Compound Eyes"));
            threshold = CustomMath.Mul(threshold, 1.1d, user.HeldItem.Is("Wide Lens"));

            return RandomUtils.RangeInclusive(1, 100) <= threshold;
        }

        private bool ImmunityCheck(Move move, Pokemon user, Pokemon target)
        {
            if (move.SpecialEffect == SpecialEffect.OneHitKo)
            {
                if (move.Name == "Sheer Cold" && target.HasType(Type.Ice))
                {
                    return true;
                }

                // TODO Sturdy
                return user.Level < target.Level;
            }

            // Thunder Wave is a special exception for some reason
            if (move.MoveType != MoveType.Status || move.Name == "Thunder Wave")
            {
                return ResolveTypeEffectiveness(move, user, target) == TypeEffectiveness.Immune;
            }

            return move.SpecialEffect == SpecialEffect.FailIfTargetHasStatus && StatusImmunityCheck(move, user, target);
        }

        public bool StatusImmunityCheck(Move move, Pokemon user, Pokemon target)
        {
            if (move.BurnChance > 0)
            {
                return StatusImmunityCheck(Status.Burned, user, target);
            }

            if (move.FreezeChance > 0)
            {
                return StatusImmunityCheck(Status.Frozen, user, target);
            }

            if (move.ParalysisChance > 0 && target.HasType(Type.Electric))
            {
                return StatusImmunityCheck(Status.Paralyzed, user, target);
            }

            if (move.PoisonChance > 0)
            {
                return StatusImmunityCheck(Status.Poisoned, user, target);
            }

            if (move.BadPoisonChance > 0)
            {
                return StatusImmunityCheck(Status.BadlyPoisoned, user, target);
            }

            return false;
        }

        private bool StatusImmunityCheck(Status status, Pokemon user, Pokemon target)
        {
            return status switch
            {
                Status.Burned when target.HasType(Type.Fire) => true,
                Status.Frozen when target.HasType(Type.Ice) => true,
                Status.Paralyzed => true,
                Status.Poisoned when target.HasType(Type.Poison) || target.HasType(Type.Steel) => true,
                Status.BadlyPoisoned when target.HasType(Type.Poison) || target.HasType(Type.Steel) => true,
                _ => false
            };
        }

        private bool ShouldAutoFail(Move move, Pokemon user, Pokemon[] targets)
        {
            // Status
            if (move.SpecialEffect == SpecialEffect.FailIfTargetHasStatus && targets.All(target =>
                target.Status != Status.None))
            {
                return true;
            }

            // Volatile status
            if (move.SpecialEffect == SpecialEffect.FailIfTargetConfused && targets.All(target =>
                target.VolatilePokemon.Confused))
            {
                return true;
            }

            // Weather
            if (move.SetWeather != Weather.Clear && move.SetWeather == Field.Weather)
            {
                return true;
            }

            // Healing
            if (HealingEffects.Contains(move.SpecialEffect)
                && targets.All(target => target.CurrentHp == target.Hp.Derived))
            {
                return true;
            }

            // Belly drum
            if (move.SpecialEffect == SpecialEffect.BellyDrum && targets.All(target =>
                target.CurrentHp <= target.Hp.Derived / 2 || target.VolatilePokemon.Attack.Stage.Value == 6))
            {
                return true;
            }

            // Refresh
            if (move.SpecialEffect == SpecialEffect.Refresh && targets.All(target =>
                target.Status != Status.Paralyzed
                && target.Status != Status.Burned
                && target.Status != Status.Poisoned
                && target.Status != Status.BadlyPoisoned))
            {
                return true;
            }

            // Endeavor
            if (move.SpecialEffect == SpecialEffect.Endeavor && targets.All(target =>
                target.CurrentHp <= user.CurrentHp))
            {
                return true;
            }

            return false;
        }

        private bool RollCritical(Move move, Pokemon user, Pokemon target)
        {
            if (target.Ability.Is("Battle Armor", "Shell Armor"))
            {
                return false;
            }

            int criticalStage = move.CriticalStage;

            if (user.HeldItem.Is("Scope Lens", "Razor Claw"))
            {
                criticalStage += 1;
            }
            else if (user.HeldItem.Is("Leek") && user.Species == "Farfetch'd")
            {
                criticalStage += 2;
            }
            else if (user.HeldItem.Is("Lucky Punch") && user.Species == "Chansey")
            {
                criticalStage += 2;
            }

            if (user.Ability.Is("Super Luck"))
            {
                criticalStage += 1;
            }

            /* TODO has volatile status from Lansat Berry or Dire Hit or Focus Energy*/

            switch (criticalStage)
            {
                case 0:
                    return RandomUtils.ChanceOneInMany(24);
                case 1:
                    return RandomUtils.ChanceOneInMany(8);
                case 2:
                    return RandomUtils.ChanceOneInMany(2);
                default:
                    return true;
            }
        }

        public TypeEffectiveness ResolveTypeEffectiveness(Move move, Pokemon user, Pokemon target)
        {
            // Scrappy, Levitate, etc
            return TypeChart.Get(move.Type, target.Type1, target.Type2);
        }

        private int CalculatePower(Move move, Pokemon user, Pokemon target)
        {
            int power = CalculateVariableBasePower(move, user, target);

            power = CustomMath.Mul(power, 1.5d, user.Ability.Is("Technician") && power <= 60);
            power = CustomMath.Mul(power, 1.2d,
                user.Ability.Is("Reckless")
                && (move.RecoilDivisor > 0 || move.SpecialEffect == SpecialEffect.CrashDamageOnMiss));

            power = CustomMath.Mul(power, 1.2d, ItemManager.ItemBoostsPower(move.Type, user.HeldItem));
            power = CustomMath.Mul(power, 1.1d,
                user.HeldItem.Is("Muscle Band") && move.MoveType == MoveType.Physical);
            power = CustomMath.Mul(power, 1.1d,
                user.HeldItem.Is("Wise Glasses") && move.MoveType == MoveType.Special);

            return power;
        }

        private int CalculateVariableBasePower(Move move, Pokemon user, Pokemon target)
        {
            switch (move.SpecialEffect)
            {
                case SpecialEffect.Return:
                    return Math.Max(1, user.Happiness * 2 / 5);
                case SpecialEffect.Frustration:
                    return Math.Max(1, (255 - user.Happiness) * 2 / 5);
                case SpecialEffect.FlailLike:
                    return user.CurrentHp >= CustomMath.Mul(user.Hp.Derived, 0.6875) ? 20
                        : user.CurrentHp >= CustomMath.Mul(user.Hp.Derived, 0.3542) ? 40
                        : user.CurrentHp >= CustomMath.Mul(user.Hp.Derived, 0.2083d) ? 80
                        : user.CurrentHp >= CustomMath.Mul(user.Hp.Derived, 0.1042d) ? 100
                        : user.CurrentHp >= CustomMath.Mul(user.Hp.Derived, 0.0417d) ? 150
                        : 200;
                    ;
                case SpecialEffect.HpBasedPower:
                    return Math.Max(1, user.CurrentHp * 150 / user.Hp.Derived);
                case SpecialEffect.Facade:
                    return user.Status == Status.Burned
                           || user.Status == Status.Paralyzed
                           || user.Status == Status.Poisoned
                           || user.Status == Status.BadlyPoisoned
                        ? move.BasePower * 2
                        : move.BasePower;
                default:
                    return move.BasePower;
            }
        }

        public int CalculateDamage(Move move, Pokemon user, Pokemon target, bool multiTarget, bool critical,
            TypeEffectiveness effectiveness)
        {
            // See:
            // https://bulbapedia.bulbagarden.net/wiki/Damage, accessed 2021-02-28
            // https://www.smogon.com/bw/articles/bw_complete_damage_formula, accessed 2021-02-28
            // However, this was explicitly implemented simplifying the whole integer division vs decimal modifier mess
            // All modifiers beyond the base damage formula are applied as double-precision multiplications with rounding

            VolatileStat userAttack = move.MoveType == MoveType.Physical
                ? user.VolatilePokemon.Attack
                : user.VolatilePokemon.SpecialAttack;

            int attack = critical && userAttack.Stage.Value < 0
                ? userAttack.Base
                : userAttack.Modified;

            attack = CustomMath.Mul(attack, 0.5d,
                target.Ability.Is("Thick Fat") && (move.Type == Type.Fire || move.Type == Type.Ice));
            attack = CustomMath.Mul(attack, 1.5d,
                move.MoveType == MoveType.Physical && user.Ability.Is("Guts") && user.Status != Status.None);
            attack = CustomMath.Mul(attack, 2d,
                move.MoveType == MoveType.Physical && user.Ability.Is("Huge Power", "Pure Power"));
            attack = CustomMath.Mul(attack, 1.5d,
                user.Ability.Is("Blaze") && move.Type == Type.Fire && user.CurrentHp <= user.Hp.Derived / 3);
            attack = CustomMath.Mul(attack, 1.5d,
                user.Ability.Is("Overgrow") && move.Type == Type.Grass && user.CurrentHp <= user.Hp.Derived / 3);
            attack = CustomMath.Mul(attack, 1.5d,
                user.Ability.Is("Swarm") && move.Type == Type.Bug && user.CurrentHp <= user.Hp.Derived / 3);
            attack = CustomMath.Mul(attack, 1.5d,
                user.Ability.Is("Torrent") && move.Type == Type.Water && user.CurrentHp <= user.Hp.Derived / 3);

            VolatileStat targetDefense = move.MoveType == MoveType.Physical
                ? target.VolatilePokemon.Defense
                : target.VolatilePokemon.SpecialDefense;

            int defense = critical && targetDefense.Stage.Value > 0
                ? targetDefense.Base
                : targetDefense.Modified;

            int power = CalculatePower(move, user, target);

            // Integer division enforced in this part
            int baseDamage = (2 * user.Level / 5 + 2) * power * attack / defense / 50 + 2;

            int damage = baseDamage;
            damage = CustomMath.Mul(damage, 0.75d, multiTarget);

            bool weatherBoost = false;
            bool weatherReduction = false;
            if (Field.Weather == Weather.Rain && move.Type == Type.Water ||
                Field.Weather == Weather.Sun && move.Type == Type.Fire)
            {
                weatherBoost = true;
            }
            else if (Field.Weather == Weather.Rain && move.Type == Type.Fire ||
                     Field.Weather == Weather.Sun && move.Type == Type.Water)
            {
                weatherReduction = true;
            }
            else if (move.SpecialEffect == SpecialEffect.ChargeTurnSunny
                     && (Field.Weather == Weather.Rain
                         || Field.Weather == Weather.Hail
                         || Field.Weather == Weather.Sandstorm))
            {
                weatherReduction = true;
            }

            damage = CustomMath.Mul(damage, 1.5d, weatherBoost);
            damage = CustomMath.Mul(damage, 0.5d, weatherReduction);
            damage = CustomMath.Mul(damage, user.Ability.Is("Sniper") ? 2.25d : 1.5d, critical);
            damage = CustomMath.Mul(damage, RandomUtils.RangeInclusive(85, 100) / 100.0d);
            bool stab = move.Type != Type.Typeless && user.HasType(move.Type);
            damage = CustomMath.Mul(damage, user.Ability.Is("Adaptability") ? 2.0d : 1.5d, stab);
            damage = CustomMath.Mul(damage, Tables.TypeEffectivenessModifiers[effectiveness]);
            bool burnReduction = user.Status == Status.Burned
                                 && move.MoveType == MoveType.Physical
                                 && move.SpecialEffect != SpecialEffect.Facade
                                 && !user.Ability.Is("Guts");
            damage = CustomMath.Mul(damage, 0.5d, burnReduction);

            damage = CustomMath.Mul(damage, 1.2d,
                user.HeldItem.Is("Expert Belt") && (effectiveness == TypeEffectiveness.SuperEffective ||
                                                    effectiveness == TypeEffectiveness.QuadrupleEffective));

            damage = CustomMath.Mul(damage, 2,
                user.Ability.Is("Tinted Lens") && (effectiveness == TypeEffectiveness.NotVeryEffective
                                                   || effectiveness == TypeEffectiveness.QuarterEffective));
            damage = CustomMath.Mul(damage, 0.75d,
                target.Ability.Is("Filter", "Solid Rock")
                && (effectiveness == TypeEffectiveness.SuperEffective
                    || effectiveness == TypeEffectiveness.QuadrupleEffective));

            return Math.Max(1, damage);
        }

        public int CalculateConfusionSelfDamage(Pokemon target)
        {
            int attack = target.VolatilePokemon.Attack.Modified;
            int defense = target.VolatilePokemon.Defense.Modified;

            // Integer division enforced in this part
            int baseDamage = (2 * target.Level / 5 + 2) * 40 * attack / defense / 50 + 2;

            int damage = baseDamage;
            damage = CustomMath.Mul(damage, RandomUtils.RangeInclusive(85, 100) / 100.0d);

            return Math.Max(1, damage);
        }

        public Queue<Event> HandleMove(Move move, Pokemon target, Pokemon user, Pokemon enemy, Pokemon ally,
            Pokemon secondaryEnemy)
        {
            Pokemon[] targets;
            if (target != null)
            {
                targets = new[] { target };
            }
            else if (move.Target == MoveTarget.Target)
            {
                targets = enemy != null
                    ? new[] { enemy }
                    : new[] { secondaryEnemy };
            }
            else
            {
                targets = ResolveTargets(move, user, enemy, ally, secondaryEnemy);
            }

            targets = targets.Where(t => t != null).ToArray();

            return GenerateBattleEvents(move, user, targets);
        }

        private Queue<Event> GenerateBattleEvents(Move move, Pokemon user, Pokemon[] targets)
        {
            Queue<Event> events = new Queue<Event>();

            // Illegal targeting
            if (targets.Length == 0)
            {
                if (move.Target == MoveTarget.Ally)
                {
                    events.Enqueue(new MoveAnnouncement(move, user));
                    events.Enqueue(new PpDeduction(move, user));
                    events.Enqueue(new TimePaddingForMoveAnnouncement());
                    events.Enqueue(new Fail());
                    return events;
                }

                if (move.Target == MoveTarget.Target || move.Target == MoveTarget.Enemies)
                {
                    events.Enqueue(new MoveAnnouncement(move, user));
                    events.Enqueue(new PpDeduction(move, user));
                    events.Enqueue(new TimePaddingForMoveAnnouncement());
                    events.Enqueue(new NoTarget());
                    return events;
                }
            }

            // Auto fail
            if (ShouldAutoFail(move, user, targets))
            {
                events.Enqueue(new MoveAnnouncement(move, user));
                events.Enqueue(new PpDeduction(move, user));
                events.Enqueue(new TimePaddingForMoveAnnouncement());
                events.Enqueue(new Fail());
                return events;
            }

            // Execution
            if (move.SpecialEffect == SpecialEffect.Struggle)
            {
                events.Enqueue(new StruggleMessage(user));
            }

            events.Enqueue(new MoveAnnouncement(move, user));

            if (move.SpecialEffect != SpecialEffect.Struggle)
            {
                events.Enqueue(new PpDeduction(move, user));
            }

            foreach (Pokemon target in targets)
            {
                // Immunity
                if (ImmunityCheck(move, user, target))
                {
                    if (NoneOfType(events, nameof(TimePaddingForMoveAnnouncement)))
                    {
                        events.Enqueue(new TimePaddingForMoveAnnouncement());
                    }

                    events.Enqueue(new Immune(target));
                    if (move.SpecialEffect == SpecialEffect.CrashDamageOnMiss)
                    {
                        events.Enqueue(new SpecialMoveMessage(move, user, target));
                        events.Enqueue(new CrashSelfDamage(user));
                        events.Enqueue(new HpBarChange(user));
                        events.Enqueue(new FaintCheck(user));
                    }

                    continue;
                }

                // Accuracy
                if (!AccuracyCheck(move, user, target, Field.Weather))
                {
                    if (NoneOfType(events, nameof(TimePaddingForMoveAnnouncement)))
                    {
                        events.Enqueue(new TimePaddingForMoveAnnouncement());
                    }

                    events.Enqueue(new Miss(target));
                    if (move.SpecialEffect == SpecialEffect.CrashDamageOnMiss)
                    {
                        events.Enqueue(new SpecialMoveMessage(move, user, target));
                        events.Enqueue(new CrashSelfDamage(user));
                        events.Enqueue(new HpBarChange(user));
                        events.Enqueue(new FaintCheck(user));
                    }

                    continue;
                }

                // Move animation (only for the first good target)
                if (NoneOfType(events, nameof(MoveAnimation)))
                {
                    events.Enqueue(new MoveAnimation(move, user, target));
                }

                // Damage
                if (move.BasePower > 0 || VariablePowerEffects.Contains(move.SpecialEffect))
                {
                    int strikes;
                    if (move.FixedMultiStrike > 0)
                    {
                        strikes = move.FixedMultiStrike;
                    }
                    else if (move.MultiStrike)
                    {
                        strikes = user.Ability.Is("Skill Link") ? 5 : RandomUtils.MultiStrikesTwoToFive();
                    }
                    else
                    {
                        strikes = 1;
                    }

                    for (int i = 0; i < strikes; i++)
                    {
                        bool critical = RollCritical(move, user, target);
                        if (strikes > 1 && i == 0)
                        {
                            events.Enqueue(new MultiStrikeBegin());
                        }

                        TypeEffectiveness effectiveness = ResolveTypeEffectiveness(move, user, target);
                        events.Enqueue(new Damage(move, user, target, critical, targets.Length > 1,
                            effectiveness));
                        events.Enqueue(new HpBarChange(target));
                        if (critical)
                        {
                            events.Enqueue(new CriticalMessage());
                        }

                        if (effectiveness != TypeEffectiveness.Normal
                            && NoneOfType(events, nameof(EffectivenessMessage)))
                        {
                            events.Enqueue(new EffectivenessMessage(effectiveness));
                        }

                        if (move.Leech)
                        {
                            events.Enqueue(new Leech(user));
                            events.Enqueue(new HpBarChange(user));
                            events.Enqueue(new LeechMessage(target));
                        }

                        events.Enqueue(new FaintCheck(target, i + 1));

                        if (move.RecoilDivisor > 0 && !user.Ability.Is("Rock Head"))
                        {
                            events.Enqueue(new Recoil(user, move.RecoilDivisor));
                            events.Enqueue(new HpBarChange(user));
                            events.Enqueue(new RecoilMessage(user));
                            events.Enqueue(new FaintCheck(user));
                        }

                        if (move.Type == Type.Fire
                            && target.Status == Status.Frozen
                            && NoneOfType(events, nameof(HealStatus)))
                        {
                            events.Enqueue(new HealStatus(target, Status.Frozen));
                            events.Enqueue(new TurnLostStatusHealedMessage(target, Status.Frozen));
                        }

                        if (CheckFlinchChance(move, user, target))
                        {
                            events.Enqueue(new SetVolatileStatus(target, VolatileStatus.Flinch));
                        }

                        if (move.HasFlag(MoveFlag.Contact))
                        {
                            if (target.Ability.Is("Poison Point")
                                && user.Status == Status.None
                                && !StatusImmunityCheck(Status.Poisoned, target, user)
                                && RandomUtils.Chance(30))
                            {
                                events.Enqueue(new StatusAnimation(user, Status.Poisoned));
                                events.Enqueue(new SetStatus(user, Status.Poisoned));
                                events.Enqueue(new AbilityTriggeredMessage(target, target.Ability, user));
                            }
                            
                            if (target.Ability.Is("Static")
                                && user.Status == Status.None
                                && !StatusImmunityCheck(Status.Paralyzed, target, user)
                                && RandomUtils.Chance(30))
                            {
                                events.Enqueue(new StatusAnimation(user, Status.Paralyzed));
                                events.Enqueue(new SetStatus(user, Status.Paralyzed));
                                events.Enqueue(new AbilityTriggeredMessage(target, target.Ability, user));
                            }
                            
                            if (target.Ability.Is("Flame Body")
                                && user.Status == Status.None
                                && !StatusImmunityCheck(Status.Burned, target, user)
                                && RandomUtils.Chance(30))
                            {
                                events.Enqueue(new StatusAnimation(user, Status.Burned));
                                events.Enqueue(new SetStatus(user, Status.Burned));
                                events.Enqueue(new AbilityTriggeredMessage(target, target.Ability, user));
                            }
                        }
                    }

                    if (strikes > 1)
                    {
                        events.Enqueue(new MultiStrikeEnd(strikes));
                    }
                }
                else if (move.SetDamage > 0 || SetDamageEffects.Contains(move.SpecialEffect))
                {
                    events.Enqueue(new Damage(move, user, target, false, false, TypeEffectiveness.Normal));
                    events.Enqueue(new HpBarChange(target));
                    if (move.SpecialEffect == SpecialEffect.OneHitKo)
                    {
                        events.Enqueue(new SpecialMoveMessage(move, user, target));
                    }

                    events.Enqueue(new FaintCheck(target));
                }

                // Target stat changes
                if (move.TargetStatChangesChance > 0 && RandomUtils.Chance(move.TargetStatChangesChance))
                {
                    foreach (KeyValuePair<StatName, int> change in move.TargetStatChanges)
                    {
                        if (NoneOfType(events, nameof(StatChangeAnimation)))
                        {
                            events.Enqueue(new StatChangeAnimation(target, change.Key, change.Value));
                        }

                        events.Enqueue(new StatChange(target, change.Key, change.Value));
                        events.Enqueue(new StatChangeMessage(target, change.Key, change.Value));
                    }
                }

                // Statuses
                if (target.Status == Status.None && !StatusImmunityCheck(move, user, target))
                {
                    if (move.BurnChance > 0 && RandomUtils.Chance(move.BurnChance))
                    {
                        events.Enqueue(new StatusAnimation(target, Status.Burned));
                        events.Enqueue(new SetStatus(target, Status.Burned));
                        events.Enqueue(new StatusMessage(target, Status.Burned));
                    }
                    else if (move.FreezeChance > 0 && RandomUtils.Chance(move.FreezeChance))
                    {
                        events.Enqueue(new StatusAnimation(target, Status.Frozen));
                        events.Enqueue(new SetStatus(target, Status.Frozen));
                        events.Enqueue(new StatusMessage(target, Status.Frozen));
                    }
                    else if (move.ParalysisChance > 0 && RandomUtils.Chance(move.ParalysisChance))
                    {
                        events.Enqueue(new StatusAnimation(target, Status.Paralyzed));
                        events.Enqueue(new SetStatus(target, Status.Paralyzed));
                        events.Enqueue(new StatusMessage(target, Status.Paralyzed));
                    }
                    else if (move.PoisonChance > 0 && RandomUtils.Chance(move.PoisonChance))
                    {
                        events.Enqueue(new StatusAnimation(target, Status.Poisoned));
                        events.Enqueue(new SetStatus(target, Status.Poisoned));
                        events.Enqueue(new StatusMessage(target, Status.Poisoned));
                    }
                    else if (move.BadPoisonChance > 0 && RandomUtils.Chance(move.BadPoisonChance))
                    {
                        events.Enqueue(new StatusAnimation(target, Status.BadlyPoisoned));
                        events.Enqueue(new SetStatus(target, Status.BadlyPoisoned));
                        events.Enqueue(new StatusMessage(target, Status.BadlyPoisoned));
                    }
                    else if (move.SleepChance > 0 && RandomUtils.Chance(move.SleepChance))
                    {
                        events.Enqueue(new StatusAnimation(target, Status.Asleep));
                        events.Enqueue(new SetStatus(target, Status.Asleep));
                        events.Enqueue(new StatusMessage(target, Status.Asleep));
                    }
                }

                // Volatile statuses
                if (move.ConfusionChance > 0 && RandomUtils.Chance(move.ConfusionChance)
                                             && !target.VolatilePokemon.Confused)
                {
                    events.Enqueue(new VolatileStatusAnimation(target, VolatileStatus.Confusion));
                    events.Enqueue(new SetVolatileStatus(target, VolatileStatus.Confusion));
                    events.Enqueue(new VolatileStatusMessage(target, VolatileStatus.Confusion));
                }

                // User stat changes
                if (move.UserStatChangesChance > 0 && RandomUtils.Chance(move.UserStatChangesChance))
                {
                    foreach (KeyValuePair<StatName, int> change in move.UserStatChanges)
                    {
                        if (NoneOfType(events, nameof(StatChangeAnimation)))
                        {
                            events.Enqueue(new StatChangeAnimation(user, change.Key, change.Value));
                        }

                        events.Enqueue(new StatChange(user, change.Key, change.Value));
                        events.Enqueue(new StatChangeMessage(user, change.Key, change.Value));
                    }
                }

                // Weather
                if (move.SetWeather != Weather.Clear)
                {
                    // TODO padding is temporary as long as we have no animations
                    events.Enqueue(new TimePaddingForMoveAnnouncement());
                    events.Enqueue(new SetWeather(move.SetWeather, user));
                    events.Enqueue(new WeatherMessage(move.SetWeather));
                }

                // Assorted special effects
                if (move.SpecialEffect == SpecialEffect.Splash)
                {
                    events.Enqueue(new SpecialMoveMessage(move, user, target));
                }
                else if (move.SpecialEffect == SpecialEffect.Healing)
                {
                    events.Enqueue(new Heal(target));
                    events.Enqueue(new HpBarChange(target));
                    events.Enqueue(new SpecialMoveMessage(move, user, target));
                }
                else if (move.SpecialEffect == SpecialEffect.HealingSunny)
                {
                    events.Enqueue(new HealSunny(target));
                    events.Enqueue(new HpBarChange(target));
                    events.Enqueue(new SpecialMoveMessage(move, user, target));
                }
                else if (move.SpecialEffect == SpecialEffect.HealingRoost)
                {
                    // TODO remove flying type until end of turn
                    events.Enqueue(new Heal(target));
                    events.Enqueue(new HpBarChange(target));
                    events.Enqueue(new SpecialMoveMessage(move, user, target));
                }
                else if (move.SpecialEffect == SpecialEffect.Haze)
                {
                    events.Enqueue(new Haze());
                    events.Enqueue(new SpecialMoveMessage(move, user, target));
                    // Forced return since it targets multiple
                    return events;
                }
                else if (move.SpecialEffect == SpecialEffect.PsychUp)
                {
                    events.Enqueue(new PsychUp(user, target));
                    events.Enqueue(new SpecialMoveMessage(move, user, target));
                }
                else if (move.SpecialEffect == SpecialEffect.BellyDrum)
                {
                    events.Enqueue(new LoseHalfMaxHp(user));
                    events.Enqueue(new HpBarChange(user));
                    events.Enqueue(new StatChangeAnimation(user, StatName.Attack, 12));
                    events.Enqueue(new StatChange(user, StatName.Attack, 12));
                    events.Enqueue(new SpecialMoveMessage(move, user, target));
                }
                else if (move.SpecialEffect == SpecialEffect.Refresh)
                {
                    events.Enqueue(new HealStatus(user, user.Status));
                    events.Enqueue(new SpecialMoveMessage(move, user, target));
                }
                else if (move.SpecialEffect == SpecialEffect.HealPartyStatus)
                {
                    events.Enqueue(new HealPartyStatus(user));
                    events.Enqueue(new SpecialMoveMessage(move, user, target));
                    // Forced return since it targets multiple
                    return events;
                }
                else if (move.SpecialEffect == SpecialEffect.Struggle)
                {
                    events.Enqueue(new LoseOneQuarterMaxHp(user));
                    events.Enqueue(new HpBarChange(user));
                    events.Enqueue(new FaintCheck(user));
                }
            }

            return events;
        }

        private bool NoneOfType(IEnumerable<Event> events, string type)
        {
            return events.All(@event => @event.Type != type);
        }

        private bool CheckFlinchChance(Move move, Pokemon user, Pokemon target)
        {
            if (target.VolatilePokemon.Flinched)
            {
                return false;
            }

            if (target.Ability.Is("Inner Focus") && !user.Ability.Is("Mold Breaker", "Teravolt", "Turboblaze"))
            {
                return false;
            }

            if (move.FlinchChance > 0)
            {
                return RandomUtils.Chance(move.FlinchChance);
            }

            if (user.HeldItem.Is("King's Rock", "Razor Fang") || user.Ability.Is("Stench"))
            {
                return RandomUtils.Chance(10);
            }

            return false;
        }
    }
}