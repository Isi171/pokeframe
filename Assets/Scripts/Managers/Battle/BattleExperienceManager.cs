using System;
using System.Collections.Generic;
using System.Linq;
using Managers.Battle.Events;
using Managers.Battle.Events.Experience;
using Managers.Battle.Events.Relationship;
using Managers.Data;
using Managers.Shared;
using Models;
using Models.Pokemon;

namespace Managers.Battle
{
    public class BattleExperienceManager
    {
        public MoveLearningManager MoveLearningManager { private get; set; }

        private static int CalculateExpGain(int baseYield, int defeatedLevel, int victorLevel, int participants,
            bool trainer, bool luckyEgg, bool traded)
        {
            double trainerMod = trainer ? 1.5 : 1;
            double luckyEggMod = luckyEgg ? 1.5 : 1;
            double tradedMod = traded ? 1.5 : 1;

            double factor1 = baseYield * defeatedLevel * trainerMod / (5 * participants);
            double factor2 = (double)(2 * defeatedLevel + 10) / (defeatedLevel + victorLevel + 10);
            double result = (factor1 * Math.Pow(factor2, 2.5) + 1) * luckyEggMod * tradedMod;

            return (int)result;
        }

        public Queue<Event> HandleExperienceAssignment(Pokemon defeated, Pokemon[] participants, bool trainer)
        {
            Queue<Event> events = new Queue<Event>();
            PokemonBaseData baseData = DataAccess.PokemonBaseData.Get(defeated.Species);

            foreach (Pokemon participant in participants)
            {
                // Effort values
                foreach (EvGain gainEvent in baseData.Effort.Split(',')
                    .Select(rawEv => rawEv.Trim())
                    .Where(rawEv => !string.IsNullOrEmpty(rawEv))
                    .Select(ParseEvYield)
                    .Select(tuple => new EvGain(participant, tuple.Item2, tuple.Item1)))
                {
                    events.Enqueue(gainEvent);
                }

                // Experience
                if (participant.Experience.CurrentToNextLevel == -1)
                {
                    continue;
                }

                int gain = CalculateExpGain(baseData.BaseExperience, defeated.Level, participant.Level,
                    participants.Length,
                    trainer, participant.HeldItem.Is("Lucky Egg"), false);

                events.Enqueue(new ExpGainMessage(participant, gain));

                int levelUps = 0;
                while (true)
                {
                    if (participant.Experience.CurrentToNextLevelAdjusted(levelUps) == -1)
                    {
                        break;
                    }

                    if (gain < participant.Experience.CurrentToNextLevelAdjusted(levelUps))
                    {
                        events.Enqueue(new ExpGain(participant, gain));
                        events.Enqueue(new ExpBarChange(participant));
                        break;
                    }

                    int partialGainUpToLevel = participant.Experience.CurrentToNextLevelAdjusted(levelUps);
                    gain -= partialGainUpToLevel;
                    levelUps++;
                    events.Enqueue(new ExpGain(participant, partialGainUpToLevel));
                    events.Enqueue(new ExpBarChange(participant));
                    events.Enqueue(new LevelUp(participant));
                    events.Enqueue(new HappinessChange(participant, 5, 4, 3));
                    events.Enqueue(new LevelUpMessageAndSummary(participant));

                    foreach (string move in MoveLearningManager.GetByLevelUp(participant,
                        participant.Level + levelUps))
                    {
                        events.Enqueue(new LearnMoveMessage(participant, move));
                        events.Enqueue(new LearnMove(participant, move));
                    }
                }
            }

            return events;
        }

        private static Tuple<int, StatName> ParseEvYield(string rawEv)
        {
            int value = int.Parse(rawEv.Substring(0, 1));
            string rawStat = rawEv.Remove(0, 1);
            switch (rawStat)
            {
                case "HP": return new Tuple<int, StatName>(value, StatName.Hp);
                case "Att": return new Tuple<int, StatName>(value, StatName.Attack);
                case "Def": return new Tuple<int, StatName>(value, StatName.Defense);
                case "SpA": return new Tuple<int, StatName>(value, StatName.SpecialAttack);
                case "SpD": return new Tuple<int, StatName>(value, StatName.SpecialDefense);
                case "Spe": return new Tuple<int, StatName>(value, StatName.Speed);
                default: throw new ArgumentOutOfRangeException();
            }
        }
    }
}