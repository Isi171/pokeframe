using System.Collections.Generic;
using Managers.Battle.Events;
using Managers.Battle.Events.Abilities;
using Managers.Battle.Events.Field;
using Managers.Battle.Events.Moves.Effects;
using Models;
using Models.Abilities;
using Models.Battle;
using Models.Pokemon;

namespace Managers.Battle
{
    public class AbilityManager
    {
        public Field Field { private get; set; }

        public Queue<Event> HandleSwitchInAbility(Ability ability, Pokemon source, Pokemon[] enemies)
        {
            if (ability.Is("Drizzle", "Drought", "Sand Stream", "Snow Warning"))
            {
                return HandleWeatherAbility(ability, source);
            }

            Queue<Event> events = new Queue<Event>();
            if (ability.Is("Mold Breaker", "Teravolt", "Turboblaze"))
            {
                events.Enqueue(new AbilityTriggeredMessage(source, ability));
                return events;
            }

            if (ability.Is("Intimidate"))
            {
                foreach (Pokemon enemy in enemies)
                {
                    if (enemy.VolatilePokemon.Attack.Stage.Value > -6)
                    {
                        events.Enqueue(new StatChangeAnimation(enemy, StatName.Attack, -1));
                        events.Enqueue(new StatChange(enemy, StatName.Attack, -1));
                        events.Enqueue(new AbilityTriggeredMessage(source, ability, enemy));
                    }
                }
            }

            return events;
        }

        private Queue<Event> HandleWeatherAbility(Ability ability, Pokemon source)
        {
            Queue<Event> events = new Queue<Event>();
            Weather weather = ability.Name switch
            {
                "Drizzle" when Field.Weather != Weather.Rain => Weather.Rain,
                "Drought" when Field.Weather != Weather.Sun => Weather.Sun,
                "Sand Stream" when Field.Weather != Weather.Sandstorm => Weather.Sandstorm,
                "Snow Warning" when Field.Weather != Weather.Hail => Weather.Hail,
                _ => Weather.Clear
            };

            if (weather != Weather.Clear)
            {
                events.Enqueue(new SetWeather(weather, source));
                events.Enqueue(new AbilityTriggeredMessage(source, ability));
            }

            return events;
        }
    }
}