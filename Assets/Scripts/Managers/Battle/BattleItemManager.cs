using System;
using System.Collections.Generic;
using System.Linq;
using Managers.Battle.Events;
using Managers.Battle.Events.Audio;
using Managers.Battle.Events.Items;
using Managers.Battle.Events.Moves.Damage;
using Managers.Battle.Events.Moves.Effects;
using Managers.Battle.Events.Relationship;
using Managers.Battle.Events.Transition;
using Managers.Data;
using Managers.Shared;
using Models;
using Models.Items;
using Models.Pokemon;
using Utils;
using Type = Models.Type;

namespace Managers.Battle
{
    public class BattleItemManager : ItemManager
    {
        public bool ItemIsEffective(Item item, Pokemon target, bool targetIsCurrentBattler, bool trainerBattle)
        {
            return HandleItem(item, target, targetIsCurrentBattler, trainerBattle).Count > 0;
        }

        public bool ItemBoostsPower(Type type, HeldItem item)
        {
            return type switch
            {
                Type.Normal when item.Is("Silk Scarf") => true,
                Type.Fighting when item.Is("Black Belt") => true,
                Type.Flying when item.Is("Sharp Beak") => true,
                Type.Poison when item.Is("Poison Barb") => true,
                Type.Ground when item.Is("Soft Sand") => true,
                Type.Rock when item.Is("Hard Stone", "Rock Incense") => true,
                Type.Bug when item.Is("Silver Powder") => true,
                Type.Ghost when item.Is("Spell Tag") => true,
                Type.Steel when item.Is("Metal Coat") => true,
                Type.Fire when item.Is("Charcoal") => true,
                Type.Water when item.Is("Mystic Water", "Sea Incense", "Wave Incense") => true,
                Type.Grass when item.Is("Miracle Seed", "Rose Incense") => true,
                Type.Electric when item.Is("Magnet") => true,
                Type.Psychic when item.Is("Twisted Spoon", "Odd Incense") => true,
                Type.Ice when item.Is("Never-Melt Ice") => true,
                Type.Dragon when item.Is("Dragon Fang") => true,
                Type.Dark when item.Is("Black Glasses") => true,
                Type.Fairy when item.Is("Pink Bow") => true,
                _ => false
            };
        }

        public Queue<Event> HandleItem(Item item, Pokemon target, bool targetIsCurrentBattler, bool trainerBattle)
        {
            Queue<Event> events = new Queue<Event>();
            if (target.Fainted
                && item.Category != ItemCategory.MedicineRevive
                && item.Category != ItemCategory.MedicineHerbalRevive)
            {
                return events;
            }

            if (item.Category == ItemCategory.MedicineHealth
                || item.Category == ItemCategory.MedicineDrink
                || item.Category == ItemCategory.MedicineHerbalHealth
                || item.Category == ItemCategory.HealthBerry)
            {
                if (target.CurrentHp < target.Hp.Derived)
                {
                    return HandleHealthItem(item, target, targetIsCurrentBattler);
                }

                return events;
            }

            if (item.Category == ItemCategory.PokeBall)
            {
                return HandlePokeBallItem(item, target, trainerBattle);
            }

            switch (item.Name)
            {
                case "Antidote":
                case "Pecha Berry":
                    if (target.Status == Status.Poisoned || target.Status == Status.BadlyPoisoned)
                    {
                        events.Enqueue(new ItemHealStatus(target));
                    }

                    break;
                case "Paralyze Heal":
                case "Cheri Berry":
                    if (target.Status == Status.Paralyzed)
                    {
                        events.Enqueue(new ItemHealStatus(target));
                    }

                    break;
                case "Awakening":
                case "Chesto Berry":
                    if (target.Status == Status.Asleep)
                    {
                        events.Enqueue(new ItemHealStatus(target));
                    }

                    break;
                case "Burn Heal":
                case "Rawst Berry":
                    if (target.Status == Status.Burned)
                    {
                        events.Enqueue(new ItemHealStatus(target));
                    }

                    break;
                case "Ice Heal":
                case "Aspear Berry":
                    if (target.Status == Status.Frozen)
                    {
                        events.Enqueue(new ItemHealStatus(target));
                    }

                    break;
                case "Persim Berry":
                    if (targetIsCurrentBattler && target.VolatilePokemon.Confused)
                    {
                        events.Enqueue(new ItemHealConfusion(target));
                    }

                    break;
                case "Full Heal":
                case "Lum Berry":
                case "Pewter Crunchies":
                case "Rage Candy Bar":
                    if (target.Status != Status.None && target.Status != Status.Fainted)
                    {
                        events.Enqueue(new ItemHealStatus(target));
                    }

                    if (targetIsCurrentBattler && target.VolatilePokemon.Confused)
                    {
                        events.Enqueue(new ItemHealConfusion(target));
                    }

                    break;
                case "Heal Powder":

                    if (target.Status != Status.None && target.Status != Status.Fainted)
                    {
                        events.Enqueue(new ItemHealStatus(target));
                        events.Enqueue(new HappinessChange(target, -5, -5, -10));
                    }

                    if (targetIsCurrentBattler && target.VolatilePokemon.Confused)
                    {
                        events.Enqueue(new ItemHealConfusion(target));
                        if (events.All(@event => @event.Type != nameof(HappinessChange)))
                        {
                            events.Enqueue(new HappinessChange(target, -5, -5, -10));
                        }
                    }

                    break;
                case "Revive":
                    if (target.Fainted)
                    {
                        events.Enqueue(new ItemHealHealth(target, CustomMath.Div(target.Hp.Derived, 2)));
                        events.Enqueue(new ItemHealStatus(target));
                    }

                    break;
                case "Max Revive":
                    if (target.Fainted)
                    {
                        events.Enqueue(new ItemHealHealth(target, target.Hp.Derived));
                        events.Enqueue(new ItemHealStatus(target));
                    }

                    break;
                case "Revival Herb":
                    if (target.Fainted)
                    {
                        events.Enqueue(new ItemHealHealth(target, target.Hp.Derived));
                        events.Enqueue(new ItemHealStatus(target));
                        events.Enqueue(new HappinessChange(target, -15, -15, -20));
                    }

                    break;
                case "Full Restore":
                    if (target.CurrentHp < target.Hp.Derived)
                    {
                        events.Enqueue(new ItemHealHealth(target, target.Hp.Derived));
                        if (targetIsCurrentBattler)
                        {
                            events.Enqueue(new HpBarChange(target));
                        }
                    }

                    if (target.Status != Status.None && target.Status != Status.Fainted)
                    {
                        events.Enqueue(new ItemHealStatus(target));
                    }

                    if (targetIsCurrentBattler && target.VolatilePokemon.Confused)
                    {
                        events.Enqueue(new ItemHealConfusion(target));
                    }

                    break;
                case "Leppa Berry":

                    break;
                case "Ether":

                    break;
                case "Max Ether":

                    break;
                case "Elixir":

                    break;
                case "Max Elixir":

                    break;
                case "Guard Spec.":

                    events.Enqueue(new HappinessChange(target, 1, 1, 0));
                    break;
                case "Dire Hit":

                    events.Enqueue(new HappinessChange(target, 1, 1, 0));
                    break;
                case "X Attack":
                    events.Enqueue(new StatChangeAnimation(target, StatName.Attack, 2));
                    events.Enqueue(new StatChange(target, StatName.Attack, 2));
                    events.Enqueue(new StatChangeMessage(target, StatName.Attack, 2));
                    events.Enqueue(new HappinessChange(target, 1, 1, 0));
                    break;
                case "X Defense":
                    events.Enqueue(new StatChangeAnimation(target, StatName.Defense, 2));
                    events.Enqueue(new StatChange(target, StatName.Defense, 2));
                    events.Enqueue(new StatChangeMessage(target, StatName.Defense, 2));
                    events.Enqueue(new HappinessChange(target, 1, 1, 0));
                    break;
                case "X Speed":
                    events.Enqueue(new StatChangeAnimation(target, StatName.Speed, 2));
                    events.Enqueue(new StatChange(target, StatName.Speed, 2));
                    events.Enqueue(new StatChangeMessage(target, StatName.Speed, 2));
                    events.Enqueue(new HappinessChange(target, 1, 1, 0));
                    break;
                case "X Accuracy":
                    events.Enqueue(new StatChangeAnimation(target, StatName.Accuracy, 2));
                    events.Enqueue(new StatChange(target, StatName.Accuracy, 2));
                    events.Enqueue(new StatChangeMessage(target, StatName.Accuracy, 2));
                    events.Enqueue(new HappinessChange(target, 1, 1, 0));
                    break;
                case "X Sp. Atk":
                    events.Enqueue(new StatChangeAnimation(target, StatName.SpecialAttack, 2));
                    events.Enqueue(new StatChange(target, StatName.SpecialAttack, 2));
                    events.Enqueue(new StatChangeMessage(target, StatName.SpecialAttack, 2));
                    events.Enqueue(new HappinessChange(target, 1, 1, 0));
                    break;
                case "X Sp. Def":
                    events.Enqueue(new StatChangeAnimation(target, StatName.SpecialDefense, 2));
                    events.Enqueue(new StatChange(target, StatName.SpecialDefense, 2));
                    events.Enqueue(new StatChangeMessage(target, StatName.SpecialDefense, 2));
                    events.Enqueue(new HappinessChange(target, 1, 1, 0));
                    break;
                case "Poké Doll":

                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(item), $"Item {item.Name} is not implemented");
            }

            return events;
        }

        private Queue<Event> HandleHealthItem(Item item, Pokemon target, bool targetIsCurrentBattler)
        {
            Queue<Event> events = new Queue<Event>();
            events.Enqueue(new ItemHealHealth(target, GetHealingMagnitude(item, target)));

            if (item.Name == "Energy Root")
            {
                events.Enqueue(new HappinessChange(target, -10, -10, -15));
            }
            else if (item.Name == "Energy Powder")
            {
                events.Enqueue(new HappinessChange(target, -5, -5, -10));
            }

            if (targetIsCurrentBattler)
            {
                events.Enqueue(new HpBarChange(target));
            }

            return events;
        }

        private Queue<Event> HandlePokeBallItem(Item item, Pokemon target, bool trainerBattle)
        {
            // Based on
            // https://bulbapedia.bulbagarden.net/wiki/Catch_rate
            // and on
            // https://www.dragonflycave.com/mechanics/gen-viii-capturing
            // accessed 2021-09-06
            Queue<Event> events = new Queue<Event>();

            if (trainerBattle)
            {
                events.Enqueue(new BallBlockedAnimation());
                events.Enqueue(new BallBlockedMessage());
                return events;
            }

            // TODO critical capture changes animation
            events.Enqueue(new BallThrowAnimation(target, item.Name));

            if (item.Name == "Master Ball")
            {
                events.Enqueue(new BallCapture(target, item));
                events.Enqueue(new BallCaptureAnimation());
                events.Enqueue(new StartVictoryMusic());
                events.Enqueue(new BallCaptureMessage(target));
                events.Enqueue(new SentToBoxMessage(target));
                events.Enqueue(new BattleEnd(true));
                return events;
            }

            int baseRate = GetBaseCaptureRate(target.Species, item.Name);
            double ball = GetBallModifier(target, item.Name);
            double status = GetStatusModifier(target.Status);
            int maxHp = target.Hp.Derived;
            int currHp = target.CurrentHp;
            double finalCaptureRate = ((3 * maxHp - 2 * currHp) * baseRate * ball / (3 * maxHp)) * status;

            if (finalCaptureRate >= 255)
            {
                events.Enqueue(new BallCapture(target, item));
                events.Enqueue(new BallCaptureAnimation());
                events.Enqueue(new StartVictoryMusic());
                events.Enqueue(new BallCaptureMessage(target));
                events.Enqueue(new SentToBoxMessage(target));
                events.Enqueue(new BattleEnd(true));
                return events;
            }

            // TODO critical capture sets attempts to 1
            int attempts = 4;
            int breakoutThreshold = (int)(65536d / Math.Pow(255d / finalCaptureRate, 3d / 16d));

            for (int i = 1; i <= attempts; i++)
            {
                int breakout = RandomUtils.RangeInclusive(0, 65535);

                if (breakout >= breakoutThreshold)
                {
                    events.Enqueue(new BallBreakAnimation(target, item.Name));
                    events.Enqueue(new BallBreakMessage(i));
                    break;
                }

                if (i == attempts)
                {
                    events.Enqueue(new BallCapture(target, item));
                    events.Enqueue(new BallCaptureAnimation());
                    events.Enqueue(new StartVictoryMusic());
                    events.Enqueue(new BallCaptureMessage(target));
                    events.Enqueue(new SentToBoxMessage(target));
                    events.Enqueue(new BattleEnd(true));
                }
                else
                {
                    events.Enqueue(new BallShakeAnimation(item.Name));
                }
            }

            return events;
        }

        private int GetBaseCaptureRate(string species, string ball)
        {
            PokemonBaseData baseData = DataAccess.PokemonBaseData.Get(species);
            int baseRate = baseData.CaptureRate;

            if (ball != "Heavy Ball")
            {
                return baseRate;
            }

            string weightString = baseData.Weight;
            double weight = double.Parse(weightString.Remove(weightString.Length - 3));
            int mod;
            if (weight >= 300)
            {
                mod = 30;
            }
            else if (weight >= 200)
            {
                mod = 20;
            }
            else if (weight >= 100)
            {
                mod = 0;
            }
            else
            {
                mod = -20;
            }

            return Math.Min(1, baseRate + mod);
        }

        private double GetBallModifier(Pokemon target, string ball)
        {
            return ball switch
            {
                "Great Ball" => 1.5,
                "Safari Ball" => 1.5,
                "Ultra Ball" => 2,
                "Net Ball" => target.HasType(Type.Water) || target.HasType(Type.Bug) ? 3.5 : 1,
                "Nest Ball" => target.Level < 31 ? (41 - target.Level) / 10d : 1,
                "Dive Ball" =>
                    // TODO when on or in water, 1 otherwise
                    3.5,
                "Repeat Ball" =>
                    // TODO when pokemon is registered in pokédex, 1 otherwise
                    3.5,
                "Timer Ball" =>
                    // TODO 1 + (number of turns passed in battle * 0.3), maximum 4
                    Math.Max(4, 1 + 0.3),
                "Quick Ball" =>
                    // TODO on first turn, 1 otherwise
                    5,
                "Dusk Ball" =>
                    // TODO at night and inside caves, 1 otherwise
                    3,
                "Fast Ball" => DataAccess.PokemonBaseData.Get(target.Species).Speed >= 100 ? 4 : 1,
                "Level Ball" =>
                    // TODO 8 if your Pokémon's level divided by four and rounded down is greater than the target
                    // Pokémon's level; otherwise, B = 4 if your Pokémon's level divided by two and rounded down is
                    // greater than the target Pokémon's level; otherwise, B = 2 if your Pokémon's level is greater
                    // than that of the target Pokémon; B = 1 otherwise
                    8,
                "Love Ball" =>
                    // TODO 8 if the Pokémon is of the same species as your Pokémon but the opposite gender; 1 otherwise
                    8,
                "Lure Ball" =>
                    // TODO 4 when fishing, 1 otherwise
                    4,
                "Moon Ball" =>
                    // TODO 4 if the Pokémon belongs to a family that evolves by Moon Stone (Nidoran, Clefairy,
                    // Jigglypuff, Skitty, Munna); B = 1 otherwise
                    4,
                _ => 1
            };
        }

        private double GetStatusModifier(Status status)
        {
            return status switch
            {
                Status.Frozen => 2.5,
                Status.Asleep => 2.5,
                Status.Burned => 1.5,
                Status.Paralyzed => 1.5,
                Status.Poisoned => 1.5,
                Status.BadlyPoisoned => 1.5,
                _ => 1
            };
        }
    }
}