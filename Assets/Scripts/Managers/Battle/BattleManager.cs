﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daybreak.Log;
using Managers.Audio;
using Managers.Battle.Events.Abilities;
using Managers.Battle.Events.Experience;
using Managers.Battle.Events.Switch;
using Managers.Battle.Events.Transition;
using Managers.Battle.Events.Turn;
using Managers.Battle.Waiting;
using Managers.Shared;
using Managers.State;
using Models;
using Models.Battle;
using Models.Items;
using Models.Pokemon;
using Models.State;
using Models.Trainer;
using UnityEngine;
using Utils;
using View.Battle;
using View.State;
using Event = Managers.Battle.Events.Event;

namespace Managers.Battle
{
    public class BattleManager : EventProcessor<Event>
    {
        [SerializeField] private MessageBoxController messageBox;
        [SerializeField] private WeatherIndicatorController weatherIndicator;
        [SerializeField] private BattleMenuController battleMenu;
        [SerializeField] private PlayerMenuController playerMenu;
        [SerializeField] private LevelUpSummaryController levelUpSummary;

        [SerializeField] private BattlerController playerSingleBattler;
        [SerializeField] private BattlerController playerDoubleBattler1;
        [SerializeField] private BattlerController playerDoubleBattler2;
        [SerializeField] private BattlerController allyDoubleBattler;
        [SerializeField] private BattlerController enemySingleBattler;
        [SerializeField] private BattlerController enemyDoubleBattler1;
        [SerializeField] private BattlerController enemyDoubleBattler2;

        [SerializeField] private EnemyTrainerSliderController enemyTrainerSlider;
        [SerializeField] private PlayerTrainerSliderController playerTrainerSlider;

        [SerializeField] private PokemonCounterController enemyPokemonCounter1;
        [SerializeField] private PokemonCounterController enemyPokemonCounter2;
        [SerializeField] private PokemonCounterController playerPokemonCounter1;
        [SerializeField] private PokemonCounterController playerPokemonCounter2;

        [SerializeField] private CatchingBallController catchingBall;

        [SerializeField] private TransitionEffectController transitionEffect;

        public BattleData BattleData { get; private set; }
        public BattleStrings Strings { get; private set; }
        public Field Field { get; private set; }
        public MoveManager MoveManager { get; private set; }
        public AbilityManager AbilityManager { get; private set; }
        public BattleItemManager ItemManager { get; private set; }
        public MoveLearningManager MoveLearningManager { get; private set; }
        public BattleExperienceManager BattleExperienceManager { get; private set; }
        public EvolutionManager EvolutionManager { get; private set; }
        public MoveOverwriteDecider MoveOverwriteDecider { get; private set; }
        public BattlerController[] Battlers { get; private set; }
        public BattlerController[] UnsortedBattlers { get; private set; }
        public AudioManager AudioManager => AudioManager.Instance;
        public LevelUpSummaryController LevelUpSummary => levelUpSummary;
        public EnemyTrainerSliderController EnemyTrainerSlider => enemyTrainerSlider;
        public PlayerTrainerSliderController PlayerTrainerSlider => playerTrainerSlider;
        public PokemonCounterController EnemyPokemonCounter1 => enemyPokemonCounter1;
        public PokemonCounterController EnemyPokemonCounter2 => enemyPokemonCounter2;
        public PokemonCounterController PlayerPokemonCounter1 => playerPokemonCounter1;
        public PokemonCounterController PlayerPokemonCounter2 => playerPokemonCounter2;
        public TransitionEffectController TransitionEffect => transitionEffect;
        public PlayerMenuController PlayerMenu => playerMenu;
        public CatchingBallController CatchingBall => catchingBall;
        public int LastDamageDealt { get; set; }
        public bool StatWontChange { get; set; }
        public bool MultiStrikeInProgress { get; set; }
        public int[] OldStats { get; set; }
        public int EscapeAttempts { get; set; }

        private void Start()
        {
            Field = new Field();
            ItemManager = new BattleItemManager();
            MoveManager = new MoveManager
            {
                Field = Field,
                ItemManager = ItemManager
            };
            AbilityManager = new AbilityManager
            {
                Field = Field
            };
            MoveLearningManager = new MoveLearningManager();
            BattleExperienceManager = new BattleExperienceManager
            {
                MoveLearningManager = MoveLearningManager
            };
            EvolutionManager = new EvolutionManager();
            MoveOverwriteDecider = new MoveOverwriteDecider();

            BattleData = GlobalState.Instance.CurrentBattleData;

            // Since trainers are mutable, we must ensure the AI ones are reset
            // This also has implications on their bag (items used carry over to the next battle with that instance)
            BattleData.Enemy1?.Heal();
            BattleData.Enemy2?.Heal();
            BattleData.Ally?.Heal();

            ClearMessageBox();

            InitBattlersAndStrings();
            battleMenu.Strings = Strings;
            UnsortedBattlers = new BattlerController[Battlers.Length];
            Battlers.CopyTo(UnsortedBattlers, 0);
            foreach (BattlerController battler in Battlers)
            {
                battler.Initialize(AllowNextEvent, this);
            }

            List<Lead> leads = CalculateLeads();

            Enqueue(new BattleStart());
            Enqueue(new BattleStartAnimation());
            if (BattleData.TrainerBattle)
            {
                Enqueue(new ShowPokemonCount());
                Enqueue(new BattleStartMessage());
                Enqueue(new TrainerSlideOutAnimation());
                Enqueue(new HidePokemonCount());
                foreach (Lead lead in leads)
                {
                    Enqueue(new SwitchInMessage(lead.Trainer, lead.Pokemon));
                    Enqueue(new SwitchIn(lead.Pokemon, lead.Battler));
                    Enqueue(new SwitchInAnimation(lead.Trainer, lead.Pokemon, lead.Battler));
                }
            }
            else
            {
                Lead encounter = leads.First(lead => !lead.ShouldBeAnnounced(BattleData));
                Enqueue(new SwitchIn(encounter.Pokemon, encounter.Battler));
                Enqueue(new SwitchInAnimation(encounter.Trainer, encounter.Pokemon, encounter.Battler));
                Enqueue(new BattleStartMessage());
                Enqueue(new TrainerSlideOutAnimation());
                foreach (Lead lead in leads)
                {
                    if (lead.ShouldBeAnnounced(BattleData))
                    {
                        Enqueue(new SwitchInMessage(lead.Trainer, lead.Pokemon));
                        Enqueue(new SwitchIn(lead.Pokemon, lead.Battler));
                        Enqueue(new SwitchInAnimation(lead.Trainer, lead.Pokemon, lead.Battler));
                    }
                }
            }

            Enqueue(new UpdateOpponentsForExp());
            Enqueue(new SpeedSortForInitialAbilities());
            Enqueue(new TriggerSwitchInAbilities(Battlers));
            Enqueue(new MoveSelection());

            AllowNextEvent();
        }

        private void Update()
        {
            if (ShouldWait())
            {
                return;
            }

            DisallowNextEvent();

            Event @event = Dequeue();
            if (@event.Loggable)
            {
                Log.Info(@event);
            }

            WaitMode waitMode = @event.Resolve(this);
            if (waitMode == null)
            {
                AllowNextEvent();
            }
            else
            {
                waitMode.MessageBox = messageBox;
                waitMode.AllowNextEvent = AllowNextEvent;
                waitMode.Wait = time => StartCoroutine(WaitCoroutine(time));
                waitMode.Await();
            }
        }

        public void ClearMessageBox()
        {
            messageBox.Clear();
        }

        public void UpdateWeatherIndicator(Weather weather)
        {
            weatherIndicator.SetWeather(weather);
        }

        public void EndBattle(bool victory)
        {
            TransitionManager.Instance.ReturnFromBattle(victory);
        }

        public bool PokemonIsOut(Pokemon pokemon)
        {
            foreach (BattlerController battler in Battlers)
            {
                if (battler.Pokemon == pokemon)
                {
                    return true;
                }
            }

            return false;
        }

        public BattlerController BattlerFromModel(Pokemon pokemon)
        {
            foreach (BattlerController battler in Battlers)
            {
                if (battler.Pokemon == pokemon)
                {
                    return battler;
                }
            }

            throw new ArgumentOutOfRangeException();
        }

        public bool ItemIsEffective(Item item, Pokemon target, bool targetIsCurrentBattler)
        {
            return ItemManager.ItemIsEffective(item, target, targetIsCurrentBattler, BattleData.TrainerBattle);
        }

        public void SortOnSpeedAndPriority()
        {
            Battlers.Shuffle();
            Battlers.StableSort((x, y) =>
                GetSpeedModifiedByContext(y.Pokemon).CompareTo(GetSpeedModifiedByContext(x.Pokemon)));
            Battlers.StableSort((x, y) =>
                (y.DecisionModule.SelectedMove?.Priority ?? 0).CompareTo(x.DecisionModule.SelectedMove?.Priority ?? 0));
        }

        public int GetSpeedModifiedByContext(Pokemon pokemon)
        {
            int speed = pokemon.VolatilePokemon.Speed.Modified;

            speed = CustomMath.Mul(speed, 2.0d, Field.Weather == Weather.Sun && pokemon.Ability.Is("Chlorophyll"));
            speed = CustomMath.Mul(speed, 2.0d, Field.Weather == Weather.Rain && pokemon.Ability.Is("Swift Swim"));
            speed = CustomMath.Mul(speed, 2.0d, Field.Weather == Weather.Sandstorm && pokemon.Ability.Is("Sand Rush"));
            speed = CustomMath.Mul(speed, 2.0d, Field.Weather == Weather.Hail && pokemon.Ability.Is("Slush Rush"));

            speed = CustomMath.Mul(speed, 1.5d, pokemon.Status != Status.None && pokemon.Ability.Is("Quick Feet"));
            speed = CustomMath.Mul(speed, 0.5d,
                pokemon.Status == Status.Paralyzed && !pokemon.Ability.Is("Quick Feet"));

            return speed;
        }

        private List<Lead> CalculateLeads()
        {
            List<Lead> leads = new List<Lead>(Battlers.Length);
            Pokemon last = null;
            foreach (BattlerController battler in Battlers)
            {
                Trainer trainer = battler.Trainer;
                Pokemon lead = trainer.Party.FirstAvailable(last);
                last = lead;
                if (lead != null)
                {
                    leads.Add(new Lead
                    {
                        Battler = battler,
                        Pokemon = lead,
                        Trainer = trainer
                    });
                }
            }

            return leads;
        }

        private void InitBattlersAndStrings()
        {
            switch (BattleData.Type)
            {
                case BattleType.Single:
                    enemySingleBattler.Trainer = BattleData.Enemy1;
                    playerSingleBattler.Trainer = BattleData.Player;
                    Battlers = new[]
                    {
                        enemySingleBattler,
                        playerSingleBattler
                    };
                    Strings = new BattleStrings
                    {
                        Enemy = enemySingleBattler,
                        Player = playerSingleBattler,
                        TrainerBattle = BattleData.TrainerBattle
                    };
                    enemyPokemonCounter1.Trainer = BattleData.Enemy1;
                    playerPokemonCounter1.Trainer = BattleData.Player;
                    break;
                case BattleType.DoubleOneEnemy:
                    enemyDoubleBattler1.Trainer = BattleData.Enemy1;
                    enemyDoubleBattler2.Trainer = BattleData.Enemy1;
                    playerDoubleBattler1.Trainer = BattleData.Player;
                    playerDoubleBattler2.Trainer = BattleData.Player;
                    Battlers = new[]
                    {
                        enemyDoubleBattler1,
                        enemyDoubleBattler2,
                        playerDoubleBattler1,
                        playerDoubleBattler2,
                    };
                    Strings = new BattleStrings
                    {
                        Enemy = enemyDoubleBattler1,
                        Enemy2 = enemyDoubleBattler2,
                        Player = playerDoubleBattler1,
                        Player2 = playerDoubleBattler2,
                        TrainerBattle = BattleData.TrainerBattle
                    };
                    enemyPokemonCounter1.Trainer = BattleData.Enemy1;
                    playerPokemonCounter1.Trainer = BattleData.Player;
                    break;
                case BattleType.DoubleTwoEnemies:
                    enemyDoubleBattler1.Trainer = BattleData.Enemy1;
                    enemyDoubleBattler2.Trainer = BattleData.Enemy2;
                    playerDoubleBattler1.Trainer = BattleData.Player;
                    playerDoubleBattler2.Trainer = BattleData.Player;
                    Battlers = new[]
                    {
                        enemyDoubleBattler1,
                        enemyDoubleBattler2,
                        playerDoubleBattler1,
                        playerDoubleBattler2,
                    };
                    Strings = new BattleStrings
                    {
                        Enemy = enemyDoubleBattler1,
                        Enemy2 = enemyDoubleBattler2,
                        Player = playerDoubleBattler1,
                        Player2 = playerDoubleBattler2,
                        TrainerBattle = BattleData.TrainerBattle
                    };
                    enemyPokemonCounter1.Trainer = BattleData.Enemy1;
                    enemyPokemonCounter2.Trainer = BattleData.Enemy2;
                    playerPokemonCounter1.Trainer = BattleData.Player;
                    break;
                case BattleType.Multi:
                    enemyDoubleBattler1.Trainer = BattleData.Enemy1;
                    enemyDoubleBattler2.Trainer = BattleData.Enemy2;
                    playerDoubleBattler1.Trainer = BattleData.Player;
                    allyDoubleBattler.Trainer = BattleData.Ally;

                    enemyDoubleBattler1.DecisionModule.Enemy2 = allyDoubleBattler;
                    enemyDoubleBattler2.DecisionModule.Enemy2 = allyDoubleBattler;
                    playerDoubleBattler1.DecisionModule.Ally = allyDoubleBattler;

                    Battlers = new[]
                    {
                        enemyDoubleBattler1,
                        enemyDoubleBattler2,
                        allyDoubleBattler,
                        playerDoubleBattler1
                    };
                    Strings = new BattleStrings
                    {
                        Enemy = enemyDoubleBattler1,
                        Enemy2 = enemyDoubleBattler2,
                        Player = playerDoubleBattler1,
                        Ally = allyDoubleBattler,
                        TrainerBattle = BattleData.TrainerBattle
                    };
                    enemyPokemonCounter1.Trainer = BattleData.Enemy1;
                    enemyPokemonCounter2.Trainer = BattleData.Enemy2;
                    playerPokemonCounter1.Trainer = BattleData.Player;
                    playerPokemonCounter2.Trainer = BattleData.Ally;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}