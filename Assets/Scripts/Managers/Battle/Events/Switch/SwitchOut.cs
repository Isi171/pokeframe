using Managers.Battle.Waiting;
using Models.Pokemon;
using Models.Trainer;

namespace Managers.Battle.Events.Switch
{
    public class SwitchOut : Event
    {
        private readonly Trainer trainer;
        private readonly Pokemon target;

        public SwitchOut(Trainer trainer, Pokemon target)
        {
            this.trainer = trainer;
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            // TODO ???
            return null;
        }
    }
}