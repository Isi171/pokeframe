using Managers.Battle.Waiting;
using Models.Pokemon;
using Models.Trainer;
using View.Battle;

namespace Managers.Battle.Events.Switch
{
    public class SwitchInAnimation : Event
    {
        private readonly Trainer trainer;
        private readonly Pokemon target;
        private readonly BattlerController battler;

        public SwitchInAnimation(Trainer trainer, Pokemon target, BattlerController battler)
        {
            this.trainer = trainer;
            this.target = target;
            this.battler = battler;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            // TODO animation
            battler.gameObject.SetActive(true);
            return null;
        }
    }
}