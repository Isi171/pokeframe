using Managers.Battle.Waiting;
using Models.Pokemon;
using Models.Trainer;

namespace Managers.Battle.Events.Switch
{
    public class SwitchOutMessage : Event
    {
        private readonly Trainer trainer;
        private readonly Pokemon target;

        public SwitchOutMessage(Trainer trainer, Pokemon target)
        {
            this.trainer = trainer;
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndWait(manager.Strings.SwitchOut(trainer, target));
        }
    }
}