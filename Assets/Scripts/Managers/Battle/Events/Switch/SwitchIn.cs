using Managers.Battle.Waiting;
using Models.Battle;
using Models.Pokemon;
using View.Battle;

namespace Managers.Battle.Events.Switch
{
    public class SwitchIn : Event
    {
        private readonly Pokemon target;
        private readonly BattlerController battler;

        public SwitchIn(Pokemon target, BattlerController battler)
        {
            this.target = target;
            this.battler = battler;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            target.VolatilePokemon = new VolatilePokemon(target);
            battler.SetPokemon(target);
            battler.UpdateStatIndicators();
            battler.UpdateExperienceBarInstant();
            return null;
        }
    }
}