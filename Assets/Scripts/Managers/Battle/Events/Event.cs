using Managers.Battle.Waiting;
using Utils;

namespace Managers.Battle.Events
{
    public abstract class Event
    {
        public string Type => GetType().Name;
        public bool Loggable { get; protected set; } = true;
        public abstract WaitMode Resolve(BattleManager manager);

        public override string ToString()
        {
            string fields = LoggingUtils.FormatFields(this);
            return fields.Length == 0
                ? Type
                : $"{Type} ({fields})";
        }
    }
}