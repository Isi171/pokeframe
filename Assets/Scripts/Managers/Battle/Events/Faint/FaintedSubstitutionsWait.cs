using System.Linq;
using Managers.Battle.Events.Abilities;
using Managers.Battle.Events.Experience;
using Managers.Battle.Events.Switch;
using Managers.Battle.Events.Turn;
using Managers.Battle.Waiting;
using View.Battle;

namespace Managers.Battle.Events.Faint
{
    public class FaintedSubstitutionsWait : Event
    {
        public FaintedSubstitutionsWait()
        {
            Loggable = false;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            if (manager.UnsortedBattlers
                .Select(battler => battler.DecisionModule)
                .Where(decisionModule => decisionModule.ShouldSubstitute)
                .Any(decisionModule => !decisionModule.SubstitutionReady))
            {
                manager.Enqueue(new FaintedSubstitutionsWait());
                return null;
            }

            BattlerController[] switches = manager.UnsortedBattlers
                .Where(b => b.DecisionModule.ShouldSubstitute)
                .ToArray();
            foreach (BattlerController battler in switches)
            {
                // In doubles, when only one pokémon remains, one of the two cannot substitute
                if (battler.DecisionModule.CannotSubstitute)
                {
                    battler.DecisionModule.ShouldSubstitute = false;
                    continue;
                }

                manager.Enqueue(new SwitchInMessage(
                    battler.Trainer,
                    battler.DecisionModule.Substitution));
                manager.Enqueue(new SwitchIn(battler.DecisionModule.Substitution,
                    battler));
                manager.Enqueue(new SwitchInAnimation(
                    battler.Trainer,
                    battler.DecisionModule.Substitution,
                    battler));
                battler.DecisionModule.ResetSubstitution();
            }

            manager.Enqueue(new UpdateOpponentsForExp());
            manager.Enqueue(new TriggerSwitchInAbilities(switches));
            manager.Enqueue(new MoveSelection());
            return null;
        }
    }
}