using System.Linq;
using Managers.Battle.Waiting;
using View.Battle;

namespace Managers.Battle.Events.Faint
{
    public class FaintedSubstitutions : Event
    {
        public override WaitMode Resolve(BattleManager manager)
        {
            foreach (BattlerController battler in manager.UnsortedBattlers
                .Where(b => b.Pokemon.Fainted)
                .Where(b => b.Trainer.Party.CountAvailable > 0)
                .Where(b => !b.DecisionModule.CannotSubstitute)
                .Reverse())
            {
                battler.DecisionModule.ShouldSubstitute = true;
                battler.DecisionModule.Substitute();
            }

            manager.Enqueue(new FaintedSubstitutionsWait());
            return null;
        }
    }
}