using System.Collections.Generic;
using System.Linq;
using Managers.Battle.Events.Moves.Effects;
using Managers.Battle.Events.Relationship;
using Managers.Battle.Events.Transition;
using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Faint
{
    public class FaintCheck : Event
    {
        private readonly Pokemon target;
        private readonly int strikes;

        public FaintCheck(Pokemon target, int strikes = 0)
        {
            this.target = target;
            this.strikes = strikes;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            if (!target.Fainted)
            {
                return null;
            }

            Queue<Event> toPush = new Queue<Event>();
            if (strikes > 0)
            {
                toPush.Enqueue(new MultiStrikeEnd(strikes));
            }

            toPush.Enqueue(new FaintMessage(target));
            toPush.Enqueue(new FaintAnimation(target));
            toPush.Enqueue(new Faint(target));
            toPush.Enqueue(new HappinessChange(target, -1, -1, -1));


            // Assign experience and EV, but only to player's party
            Pokemon[] participants = target.VolatilePokemon.Opponents
                .Where(opponent => manager.BattleData.Player.Party.Contains(opponent))
                .Where(opponent => !opponent.Fainted)
                .ToArray();
            if (manager.BattleData.YieldsExperience && participants.Length > 0)
            {
                Queue<Event> expEvents = manager.BattleExperienceManager.HandleExperienceAssignment(target,
                    participants, manager.BattleData.TrainerBattle);
                while (expEvents.Count > 0)
                {
                    toPush.Enqueue(expEvents.Dequeue());
                }
            }

            toPush.Enqueue(new BattleEndCheck());
            manager.PushToFront(toPush);

            return null;
        }
    }
}