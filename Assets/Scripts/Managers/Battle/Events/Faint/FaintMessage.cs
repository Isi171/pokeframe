using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Faint
{
    public class FaintMessage : Event
    {
        private readonly Pokemon target;

        public FaintMessage(Pokemon target)
        {
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndWait(manager.Strings.Faint(target));
        }
    }
}