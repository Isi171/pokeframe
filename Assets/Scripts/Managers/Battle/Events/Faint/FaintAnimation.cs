using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Faint
{
    public class FaintAnimation : Event
    {
        private readonly Pokemon target;

        public FaintAnimation(Pokemon target)
        {
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            // TODO animation
            manager.BattlerFromModel(target).HideBattlerAndInfo();
            manager.ClearMessageBox();
            return null;
        }
    }
}