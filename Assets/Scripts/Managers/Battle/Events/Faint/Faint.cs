using Managers.Battle.Waiting;
using Models;
using Models.Pokemon;

namespace Managers.Battle.Events.Faint
{
    public class Faint : Event
    {
        private readonly Pokemon target;

        public Faint(Pokemon target)
        {
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            target.Status = Status.Fainted;
            return null;
        }
    }
}