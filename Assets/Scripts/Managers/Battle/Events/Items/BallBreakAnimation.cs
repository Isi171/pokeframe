using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Items
{
    public class BallBreakAnimation : Event
    {
        private readonly Pokemon pokemon;
        private readonly string ball;

        public BallBreakAnimation(Pokemon pokemon, string ball)
        {
            this.pokemon = pokemon;
            this.ball = ball;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new StartAnimation(manager.CatchingBall,
                () => manager.CatchingBall.BreakAnimation(ball, manager.BattlerFromModel(pokemon)));
        }
    }
}