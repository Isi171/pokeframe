using Managers.Battle.Waiting;
using Models;
using Models.Pokemon;

namespace Managers.Battle.Events.Items
{
    public class ItemHealStatus : Event
    {
        private readonly Pokemon target;

        public ItemHealStatus(Pokemon target)
        {
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            if (target.Status == Status.Fainted)
            {
                target.Status = Status.None;
            }
            else
            {
                if (target.Status == Status.BadlyPoisoned && manager.PokemonIsOut(target))
                {
                    target.VolatilePokemon.BadPoisonCounter = 0;
                }

                target.Status = Status.None;
                manager.BattlerFromModel(target).UpdateStatusIndicator();
            }

            return null;
        }
    }
}