using System;
using Managers.Battle.Waiting;
using Models.Items;
using Models.Pokemon;

namespace Managers.Battle.Events.Items
{
    public class HeldItemHealHealth : Event
    {
        private readonly Pokemon pokemon;
        private readonly Item item;

        public HeldItemHealHealth(Pokemon pokemon, Item item)
        {
            this.pokemon = pokemon;
            this.item = item;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            pokemon.CurrentHp += item.Name switch
            {
                "Leftovers" => Math.Max(1, pokemon.Hp.Derived / 16),
                "Black Sludge" => Math.Max(1, pokemon.Hp.Derived / 16),
                _ => throw new ArgumentOutOfRangeException()
            };

            return null;
        }
    }
}