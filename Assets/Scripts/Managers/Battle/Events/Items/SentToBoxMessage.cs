using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Items
{
    public class SentToBoxMessage : Event
    {
        private readonly Pokemon target;

        public SentToBoxMessage(Pokemon target)
        {
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return manager.BattleData.Player.Party.Contains(target)
                ? null
                : new DisplayTextAndWait(manager.Strings.SentToBox(target));
        }
    }
}