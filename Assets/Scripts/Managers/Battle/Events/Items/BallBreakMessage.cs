using Managers.Battle.Waiting;

namespace Managers.Battle.Events.Items
{
    public class BallBreakMessage : Event
    {
        private readonly int currentShakeCheck;

        public BallBreakMessage(int currentShakeCheck)
        {
            this.currentShakeCheck = currentShakeCheck;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndWait(manager.Strings.BallBreak(currentShakeCheck));
        }
    }
}