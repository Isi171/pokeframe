using Managers.Battle.Waiting;
using Models.Items;
using Models.Pokemon;

namespace Managers.Battle.Events.Items
{
    public class HeldItemDamageMessage : Event
    {
        private readonly Pokemon pokemon;
        private readonly Item item;

        public HeldItemDamageMessage(Pokemon pokemon, Item item)
        {
            this.pokemon = pokemon;
            this.item = item;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndWait(manager.Strings.HeldItemDamage(pokemon, item));
        }
    }
}