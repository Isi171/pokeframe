using System;
using Managers.Battle.Waiting;
using Models.Items;
using Models.Pokemon;

namespace Managers.Battle.Events.Items
{
    public class HeldItemDamage : Event
    {
        private readonly Pokemon pokemon;
        private readonly Item item;

        public HeldItemDamage(Pokemon pokemon, Item item)
        {
            this.pokemon = pokemon;
            this.item = item;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            // TODO Magic Guard is immune to this damage

            pokemon.CurrentHp -= item.Name switch
            {
                "Black Sludge" => Math.Max(1, pokemon.Hp.Derived / 8),
                _ => throw new ArgumentOutOfRangeException()
            };

            return new StartAnimation(manager.BattlerFromModel(pokemon).StartReceiveDamageAnimation);
        }
    }
}