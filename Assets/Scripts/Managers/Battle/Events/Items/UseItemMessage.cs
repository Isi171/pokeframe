using Managers.Battle.Waiting;
using Models;
using Models.Items;
using Models.Pokemon;
using Models.Trainer;

namespace Managers.Battle.Events.Items
{
    public class UseItemMessage : Event
    {
        private readonly Trainer trainer;
        private readonly Pokemon target;
        private readonly Item item;

        public UseItemMessage(Trainer trainer, Pokemon target, Item item)
        {
            this.trainer = trainer;
            this.target = target;
            this.item = item;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            if (item.Category == ItemCategory.PokeBall)
            {
                return new DisplayTextAndAllowNextEvent(manager.Strings.UseItem(trainer, target, item));
            }
            
            return new DisplayTextAndWait(manager.Strings.UseItem(trainer, target, item));
        }
    }
}