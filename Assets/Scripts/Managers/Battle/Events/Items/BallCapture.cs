using Managers.Battle.Waiting;
using Models.Items;
using Models.Pokemon;

namespace Managers.Battle.Events.Items
{
    public class BallCapture : Event
    {
        private readonly Pokemon target;
        private readonly Item ball;

        public BallCapture(Pokemon target, Item ball)
        {
            this.target = target;
            this.ball = ball;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            target.Ball = ball.Name;

            if (ball.Name == "Heal Ball")
            {
                target.Heal();
            }
            else if (ball.Name == "Friend Ball")
            {
                target.Happiness = 200;
            }

            manager.BattleData.Player.AddNew(target);

            return null;
        }
    }
}