using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Items
{
    public class ItemHealHealth : Event
    {
        private readonly Pokemon target;
        private readonly int value;

        public ItemHealHealth(Pokemon target, int value)
        {
            this.target = target;
            this.value = value;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            target.CurrentHp += value;
            return null;
        }
    }
}