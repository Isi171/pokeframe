using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Items
{
    public class BallThrowAnimation : Event
    {
        private readonly Pokemon pokemon;
        private readonly string ball;

        public BallThrowAnimation(Pokemon pokemon, string ball)
        {
            this.pokemon = pokemon;
            this.ball = ball;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new StartAnimation(manager.CatchingBall,
                () => manager.CatchingBall.FlyAnimation(ball, manager.BattlerFromModel(pokemon)));
        }
    }
}