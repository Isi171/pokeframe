using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Items
{
    public class HeldItemActivatedAnimation : Event
    {
        private readonly Pokemon pokemon;

        public HeldItemActivatedAnimation(Pokemon pokemon)
        {
            this.pokemon = pokemon;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            // TODO animation
            return null;
        }
    }
}