using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Items
{
    public class ItemHealConfusion : Event
    {
        private readonly Pokemon target;

        public ItemHealConfusion(Pokemon target)
        {
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            target.VolatilePokemon.ConfusionCounter = 0;
            // TODO update confusion indicator
            return null;
        }
    }
}