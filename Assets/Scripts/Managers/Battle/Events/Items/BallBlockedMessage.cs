using Managers.Battle.Waiting;

namespace Managers.Battle.Events.Items
{
    public class BallBlockedMessage : Event
    {
        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndWait(manager.Strings.BallBlocked());
        }
    }
}