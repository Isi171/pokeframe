using Managers.Battle.Waiting;

namespace Managers.Battle.Events.Items
{
    public class BallShakeAnimation : Event
    {
        private readonly string ball;

        public BallShakeAnimation(string ball)
        {
            this.ball = ball;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new StartAnimation(manager.CatchingBall, () => manager.CatchingBall.ShakeAnimation(ball));
        }
    }
}