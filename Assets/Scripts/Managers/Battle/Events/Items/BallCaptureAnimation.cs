using Managers.Battle.Waiting;

namespace Managers.Battle.Events.Items
{
    public class BallCaptureAnimation : Event
    {
        public override WaitMode Resolve(BattleManager manager)
        {
            return new StartAnimation(manager.CatchingBall, () => manager.CatchingBall.CaptureAnimation());
        }
    }
}