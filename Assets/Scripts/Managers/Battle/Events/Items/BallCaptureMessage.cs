using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Items
{
    public class BallCaptureMessage : Event
    {
        private readonly Pokemon target;

        public BallCaptureMessage(Pokemon target)
        {
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndAwaitInput(manager.Strings.BallCapture(target));
        }
    }
}