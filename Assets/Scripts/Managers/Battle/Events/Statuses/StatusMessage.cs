using Managers.Battle.Waiting;
using Models;
using Models.Pokemon;

namespace Managers.Battle.Events.Statuses
{
    public class StatusMessage : Event
    {
        private readonly Pokemon target;
        private readonly Status status;

        public StatusMessage(Pokemon target, Status status)
        {
            this.target = target;
            this.status = status;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            if (target.Fainted)
            {
                return null;
            }

            return new DisplayTextAndWait(manager.Strings.StatusWasApplied(target, status));
        }
    }
}