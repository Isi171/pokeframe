using Managers.Battle.Waiting;
using Models;
using Models.Pokemon;

namespace Managers.Battle.Events.Statuses
{
    public class VolatileStatusHealedMessage : Event
    {
        private readonly Pokemon target;
        private readonly VolatileStatus volatileStatus;

        public VolatileStatusHealedMessage(Pokemon target, VolatileStatus volatileStatus)
        {
            this.target = target;
            this.volatileStatus = volatileStatus;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndWait(manager.Strings.VolatileStatusWasHealed(target, volatileStatus));
        }
    }
}