using Managers.Battle.Waiting;
using Models;
using Models.Pokemon;

namespace Managers.Battle.Events.Statuses
{
    public class VolatileStatusIsPresentMessage : Event
    {
        private readonly Pokemon target;
        private readonly VolatileStatus volatileStatus;

        public VolatileStatusIsPresentMessage(Pokemon target, VolatileStatus volatileStatus)
        {
            this.target = target;
            this.volatileStatus = volatileStatus;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndWait(manager.Strings.VolatileStatusIsPresent(target, volatileStatus));
        }
    }
}