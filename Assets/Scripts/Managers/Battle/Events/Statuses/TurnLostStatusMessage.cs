using Managers.Battle.Waiting;
using Models;
using Models.Pokemon;

namespace Managers.Battle.Events.Statuses
{
    public class TurnLostStatusMessage : Event
    {
        private readonly Pokemon target;
        private readonly Status status;

        public TurnLostStatusMessage(Pokemon target, Status status)
        {
            this.target = target;
            this.status = status;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndWait(manager.Strings.LostTurnDueToStatus(target, status));
        }
    }
}