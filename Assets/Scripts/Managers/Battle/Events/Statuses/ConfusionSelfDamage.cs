using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Statuses
{
    public class ConfusionSelfDamage : Event
    {
        private readonly Pokemon target;

        public ConfusionSelfDamage(Pokemon target)
        {
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            int damage = manager.MoveManager.CalculateConfusionSelfDamage(target);
            target.CurrentHp -= damage;
            return new StartAnimation(manager.BattlerFromModel(target).StartReceiveDamageAnimation);
        }
    }
}