using Managers.Battle.Waiting;
using Models;
using Models.Pokemon;
using Utils;

namespace Managers.Battle.Events.Statuses
{
    public class SetStatus : Event
    {
        private readonly Pokemon target;
        private readonly Status status;

        public SetStatus(Pokemon target, Status status)
        {
            this.target = target;
            this.status = status;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            if (target.Fainted)
            {
                return null;
            }

            target.Status = status;
            if (status == Status.Asleep)
            {
                target.SleepCounter = RandomUtils.RangeInclusive(1, 3);
            }

            manager.BattlerFromModel(target).UpdateStatusIndicator();
            return null;
        }
    }
}