using Managers.Battle.Waiting;
using Models;
using Models.Pokemon;

namespace Managers.Battle.Events.Statuses
{
    public class TurnLostVolatileStatusMessage : Event
    {
        private readonly Pokemon target;
        private readonly VolatileStatus volatileStatus;

        public TurnLostVolatileStatusMessage(Pokemon target, VolatileStatus volatileStatus)
        {
            this.target = target;
            this.volatileStatus = volatileStatus;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndWait(manager.Strings.LostTurnDueToVolatileStatus(target, volatileStatus));
        }
    }
}