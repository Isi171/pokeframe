using System;
using Managers.Battle.Waiting;
using Models;
using Models.Pokemon;
using Utils;

namespace Managers.Battle.Events.Statuses
{
    public class SetVolatileStatus : Event
    {
        private readonly Pokemon target;
        private readonly VolatileStatus volatileStatus;

        public SetVolatileStatus(Pokemon target, VolatileStatus volatileStatus)
        {
            this.target = target;
            this.volatileStatus = volatileStatus;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            if (target.Fainted)
            {
                return null;
            }

            switch (volatileStatus)
            {
                case VolatileStatus.Confusion:
                    target.VolatilePokemon.ConfusionCounter = RandomUtils.RangeInclusive(2, 5);
                    break;
                case VolatileStatus.Flinch:
                    target.VolatilePokemon.Flinched = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return null;
        }
    }
}