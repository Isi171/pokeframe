using Managers.Battle.Waiting;
using Models;
using Models.Pokemon;

namespace Managers.Battle.Events.Statuses
{
    public class VolatileStatusMessage : Event
    {
        private readonly Pokemon target;
        private readonly VolatileStatus volatileStatus;

        public VolatileStatusMessage(Pokemon target, VolatileStatus volatileStatus)
        {
            this.target = target;
            this.volatileStatus = volatileStatus;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            if (target.Fainted)
            {
                return null;
            }

            return new DisplayTextAndWait(manager.Strings.VolatileStatusWasApplied(target, volatileStatus));
        }
    }
}