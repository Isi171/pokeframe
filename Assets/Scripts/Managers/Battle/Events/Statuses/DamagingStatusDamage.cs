using System;
using Managers.Battle.Waiting;
using Models;
using Models.Pokemon;

namespace Managers.Battle.Events.Statuses
{
    public class DamagingStatusDamage : Event
    {
        private readonly Pokemon target;
        private readonly Status status;

        public DamagingStatusDamage(Pokemon target, Status status)
        {
            this.target = target;
            this.status = status;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            switch (status)
            {
                case Status.Burned:
                {
                    target.CurrentHp -= Math.Max(1, target.Hp.Derived / 16);
                    break;
                }
                case Status.Poisoned:
                {
                    target.CurrentHp -= Math.Max(1, target.Hp.Derived / 8);
                    break;
                }
                case Status.BadlyPoisoned:
                {
                    int baseDamage = target.Hp.Derived / 16;
                    target.VolatilePokemon.BadPoisonCounter++;
                    int multiplier = Math.Min(15, target.VolatilePokemon.BadPoisonCounter);
                    int damage = baseDamage * multiplier;
                    target.CurrentHp -= Math.Max(1, damage);
                    break;
                }
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return null;
        }
    }
}