using System;
using Managers.Battle.Waiting;
using Models;
using Models.Pokemon;

namespace Managers.Battle.Events.Statuses
{
    public class DamagingStatusAnimation : Event
    {
        private readonly Pokemon target;
        private readonly Status status;

        public DamagingStatusAnimation(Pokemon target, Status status)
        {
            this.target = target;
            this.status = status;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            // TODO animations, specific
            switch (status)
            {
                case Status.Burned:
                case Status.Poisoned:
                case Status.BadlyPoisoned:
                    return new StartAnimation(manager.BattlerFromModel(target).StartStatusAnimation);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}