using Managers.Battle.Waiting;
using Models;
using Models.Pokemon;

namespace Managers.Battle.Events.Statuses
{
    public class HealStatus : Event
    {
        private readonly Pokemon target;
        private readonly Status status;

        public HealStatus(Pokemon target, Status status)
        {
            this.target = target;
            this.status = status;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            if (target.Fainted)
            {
                return null;
            }

            if (status == Status.BadlyPoisoned && manager.PokemonIsOut(target))
            {
                target.VolatilePokemon.BadPoisonCounter = 0;
            }

            target.Status = Status.None;

            if (manager.PokemonIsOut(target))
            {
                manager.BattlerFromModel(target).UpdateStatusIndicator();
            }

            return null;
        }
    }
}