using System;
using Managers.Battle.Waiting;
using Models;
using Models.Pokemon;

namespace Managers.Battle.Events.Statuses
{
    public class TurnLostStatusAnimation : Event
    {
        private readonly Pokemon target;
        private readonly Status status;

        public TurnLostStatusAnimation(Pokemon target, Status status)
        {
            this.target = target;
            this.status = status;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            // TODO animations, specific
            switch (status)
            {
                case Status.Frozen:
                case Status.Paralyzed:
                case Status.Asleep:
                    return new StartAnimation(manager.BattlerFromModel(target).StartStatusAnimation);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}