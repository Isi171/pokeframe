using System.Linq;
using Managers.Battle.Events.Faint;
using Managers.Battle.Events.Field;
using Managers.Battle.Events.Items;
using Managers.Battle.Events.Moves.Damage;
using Managers.Battle.Events.Statuses;
using Managers.Battle.Waiting;
using Models;
using Models.Pokemon;
using View.Battle;

namespace Managers.Battle.Events.Turn
{
    public class RoundEnd : Event
    {
        public override WaitMode Resolve(BattleManager manager)
        {
            // 0. Flinch resetting
            foreach (BattlerController battler in manager.Battlers)
            {
                battler.Pokemon.VolatilePokemon.Flinched = false;
            }

            // https://bulbapedia.bulbagarden.net/wiki/User:SnorlaxMonster/End-turn_resolution_order
            // Events with the same number are either mutually exclusive and/or apply to each relevant Pokémon
            // from fastest to slowest. Unlike movement order, the order in which Pokémon are selected is based
            // on their current Speed rather than their Speed at the start of the turn. The order it is applied
            // to individual Pokémon is reversed by Trick Room.
            //
            //     Weather subsiding
            //     Hail damage/sandstorm damage/Rain Dish/Dry Skin/Solar Power/Ice Body
            //     Self-curing a status condition due to high Affection
            //     Future Sight/Doom Desire
            //     Wish
            //     Block A
            //     Aqua Ring
            //     Ingrain
            //     Leech Seed
            //     Poison/Poison Heal
            //     Burn
            //     Nightmare
            //     Curse
            //     Bind/Clamp/Fire Spin/Infestation/Magma Storm/Sand Tomb/Whirlpool/Wrap
            //     Taunt fading
            //     Encore fading
            //     Disable fading
            //     Magnet Rise fading
            //     Telekinesis fading
            //     Heal Block fading
            //     Embargo fading
            //     Yawn
            //     Perish count
            //     Roost fading
            //     Reflect dissipating
            //     Light Screen dissipating
            //     Safeguard dissipating
            //     Mist dissipating
            //     Tailwind dissipating
            //     Lucky Chant dissipating
            //     Rainbow (Water Pledge + Fire Pledge) dissipating
            //     Sea of fire (Fire Pledge + Grass Pledge) dissipating
            //     Swamp (Grass Pledge + Water Pledge) dissipating
            //     Trick Room dissipating
            //     Water Sport dissipating
            //     Mud Sport dissipating
            //     Wonder Room dissipating
            //     Magic Room dissipating
            //     Gravity dissipating
            //     Terrain dissipating
            //     Block B
            //     Zen Mode
            //
            // Block A
            // Each individual Pokémon performs these effects in this order (all applicable ones are done before
            // the next Pokémon does), with faster Pokémon doing them first. Unlike movement order, the order in
            // which Pokémon are selected is based on their current Speed rather than their Speed at the start
            // of the turn. Once a Pokémon has started a block, it finishes the block even if its Speed changes
            // within the block (e.g. being cured of paralysis). The order it is applied to individual Pokémon
            // is reversed by Trick Room.
            //
            //     Sea of fire (Fire Pledge + Grass Pledge)
            //     Grassy Terrain
            //     Hydration/Shed Skin
            //     Leftovers/Black Sludge
            //     Healer
            //
            // Block B
            //
            // Each individual Pokémon performs these effects in this order (all applicable ones are done before
            // the next Pokémon does), with faster Pokémon doing them first. Unlike movement order, the order in
            // which Pokémon are selected is based on their current Speed rather than their Speed at the start
            // of the turn. Once a Pokémon has started a block, it finishes the block even if its Speed changes
            // within the block (e.g. Speed Boost). The order it is applied to individual Pokémon is reversed by
            // Trick Room, including during the turn Trick Room wore off.
            //
            //     Uproar ending
            //     Speed Boost/Moody/Bad Dreams*
            //     Flame Orb/Toxic Orb/Sticky Barb
            //     Harvest/Pickup

            // 1. Weather subsiding
            if (manager.Field.WeatherCounter > 0)
            {
                manager.Field.WeatherCounter--;
                if (manager.Field.WeatherCounter == 0)
                {
                    manager.Enqueue(new WeatherStop());
                    manager.Enqueue(new WeatherStopMessage(manager.Field.Weather));
                }
                else
                {
                    // TODO weather animation?
                    // 2. Hail damage/sandstorm damage/Rain Dish/Dry Skin/Solar Power/Ice Body
                    foreach (Pokemon pokemon in manager.Battlers
                        .Select(battler => battler.Pokemon)
                        .Where(pokemon => !pokemon.Fainted))
                    {
                        if (IsHealedByWeather(pokemon, manager.Field.Weather))
                        {
                            manager.Enqueue(new WeatherHealMessage(manager.Field.Weather, pokemon));
                            manager.Enqueue(new WeatherHeal(manager.Field.Weather, pokemon));
                            manager.Enqueue(new HpBarChange(pokemon));
                        }
                        else if (IsDamagedByWeather(pokemon, manager.Field.Weather))
                        {
                            manager.Enqueue(new WeatherDamageMessage(manager.Field.Weather, pokemon));
                            manager.Enqueue(new WeatherDamage(pokemon));
                            manager.Enqueue(new HpBarChange(pokemon));
                            manager.Enqueue(new FaintCheck(pokemon));
                        }
                    }
                }
            }

            // 6. Block A
            foreach (Pokemon pokemon in manager.Battlers
                .Select(battler => battler.Pokemon)
                .Where(pokemon => !pokemon.Fainted))
            {
                // Sea of fire (Fire Pledge + Grass Pledge)
                // Grassy Terrain
                // Hydration/Shed Skin
                // Leftovers/Black Sludge
                if (pokemon.CurrentHp < pokemon.Hp.Derived
                    && (pokemon.HeldItem.Is("Leftovers") ||
                        (pokemon.HeldItem.Is("Black Sludge") && pokemon.HasType(Models.Type.Poison))))
                {
                    manager.Enqueue(new HeldItemActivatedAnimation(pokemon));
                    manager.Enqueue(new HeldItemHealHealth(pokemon, pokemon.HeldItem.Item));
                    manager.Enqueue(new HpBarChange(pokemon));
                    manager.Enqueue(new HeldItemHealHealthMessage(pokemon, pokemon.HeldItem.Item));
                }

                if (pokemon.HeldItem.Is("Black Sludge") && !pokemon.HasType(Models.Type.Poison))
                {
                    manager.Enqueue(new HeldItemActivatedAnimation(pokemon));
                    manager.Enqueue(new HeldItemDamage(pokemon, pokemon.HeldItem.Item));
                    manager.Enqueue(new HpBarChange(pokemon));
                    manager.Enqueue(new HeldItemDamageMessage(pokemon, pokemon.HeldItem.Item));
                    manager.Enqueue(new FaintCheck(pokemon));
                }

                // Healer
            }

            // 10. Poison/Poison Heal
            // 11. Burn
            foreach (Pokemon pokemon in manager.Battlers
                .Select(battler => battler.Pokemon)
                .Where(pokemon => !pokemon.Fainted))
            {
                if (pokemon.Status == Status.Poisoned
                    || pokemon.Status == Status.BadlyPoisoned
                    || pokemon.Status == Status.Burned)
                {
                    manager.Enqueue(new DamagingStatusMessage(pokemon, pokemon.Status));
                    manager.Enqueue(new DamagingStatusAnimation(pokemon, pokemon.Status));
                    manager.Enqueue(new DamagingStatusDamage(pokemon, pokemon.Status));
                    manager.Enqueue(new HpBarChange(pokemon));
                    manager.Enqueue(new FaintCheck(pokemon));
                }
            }

            manager.Enqueue(new FaintedSubstitutions());

            // TODO this is quite awkward, no?
            return new Wait();
        }

        private static bool IsDamagedByWeather(Pokemon pokemon, Weather weather)
        {
            if (weather == Weather.Hail
                && !pokemon.HasType(Models.Type.Ice)
                && !pokemon.Ability.Is("Snow Cloak")
                /*TODO doesn't have Ice Body, Magic Guard, or Overcoat; or is not holding Safety Goggles.*/)
            {
                return true;
            }

            if (weather == Weather.Sandstorm
                && !pokemon.HasType(Models.Type.Rock)
                && !pokemon.HasType(Models.Type.Ground)
                && !pokemon.HasType(Models.Type.Steel)
                && !pokemon.Ability.Is("Sand Veil")
                && !pokemon.Ability.Is("Sand Rush")
                /*TODO doesn't have Sand Force, Magic Guard, or Overcoat; or is not holding Safety Goggles*/
            )
            {
                return true;
            }

            // TODO Dry skin

            return false;
        }

        private static bool IsHealedByWeather(Pokemon pokemon, Weather weather)
        {
            // TODO Ice Body
            // TODO Dry skin
            // TODO Rain Dish

            return false;
        }
    }
}