using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Turn
{
    public class TurnEnd : Event
    {
        private readonly Pokemon target;

        public TurnEnd(Pokemon target)
        {
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            // TODO ???
            manager.Enqueue(new NextBattlerTurn());
            return null;
        }
    }
}