using System.Collections.Generic;
using System.Linq;
using Managers.Battle.Events.Abilities;
using Managers.Battle.Events.Experience;
using Managers.Battle.Events.Items;
using Managers.Battle.Events.Moves.Damage;
using Managers.Battle.Events.Statuses;
using Managers.Battle.Events.Switch;
using Managers.Battle.Events.Transition;
using Managers.Battle.Waiting;
using Models;
using Models.Items;
using Models.Pokemon;
using Models.Trainer;
using Utils;
using View.Battle;

namespace Managers.Battle.Events.Turn
{
    public class MoveSelectionWait : Event
    {
        public MoveSelectionWait()
        {
            Loggable = false;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            if (manager.Battlers
                .Where(battler => !battler.Pokemon.Fainted)
                .Any(battler => !battler.DecisionModule.Ready))
            {
                manager.Enqueue(new MoveSelectionWait());
                return null;
            }

            // Both player and some wild encounters can forfeit/run away
            foreach (BattlerController battler in manager.Battlers
                .Where(battler => !battler.Pokemon.Fainted)
                .Where(battler => battler.DecisionModule.Forfeit))
            {
                if (manager.BattleData.TrainerBattle)
                {
                    manager.Enqueue(new BattleEndMessage(false, true));
                    manager.Enqueue(new BattleEndAnimation(false));
                    manager.Enqueue(new BattleEnd(false));
                    return null;
                }

                if (battler.Trainer != manager.BattleData.Player)
                {
                    manager.Enqueue(new WildBattlePokemonFlewMessage(battler.Pokemon));
                    manager.Enqueue(new BattleEnd(true));
                }
                else
                {
                    bool success = EscapeCheck(manager, battler.Pokemon, battler.DecisionModule.Enemy.Pokemon);
                    manager.Enqueue(new WildBattlePlayerRunAwayMessage(success));

                    if (success)
                    {
                        manager.Enqueue(new BattleEnd(true));
                    }
                }
            }

            foreach (BattlerController battler in manager.Battlers
                .Where(battler => !battler.Pokemon.Fainted)
                .Where(b => b.DecisionModule.Item != null))
            {
                battler.DecisionModule.HasActed = true;
                Trainer trainer = battler.Trainer;
                Pokemon target = battler.DecisionModule.ItemTarget;
                Item item = battler.DecisionModule.Item;
                manager.Enqueue(new UseItemMessage(trainer, target, item));
                Queue<Event> itemEvents = manager.ItemManager.HandleItem(item, target, battler.Pokemon == target,
                    manager.BattleData.TrainerBattle);
                manager.Enqueue(itemEvents);
            }

            BattlerController[] switches = manager.Battlers
                .Where(battler => !battler.Pokemon.Fainted)
                .Where(b => b.DecisionModule.Switch != null)
                .ToArray();
            foreach (BattlerController battler in switches)
            {
                battler.DecisionModule.HasActed = true;
                Trainer trainer = battler.Trainer;
                Pokemon switchOut = battler.Pokemon;
                Pokemon switchIn = battler.DecisionModule.Switch;
                manager.Enqueue(new SwitchOutMessage(trainer, switchOut));

                if (switchOut.Ability.Is("Regenerator") && switchOut.CurrentHp < switchOut.Hp.Derived)
                {
                    manager.Enqueue(new TriggerRegenerator(switchOut));
                    manager.Enqueue(new HpBarChange(switchOut));
                    manager.Enqueue(new AbilityTriggeredMessage(switchOut, switchOut.Ability));
                }

                if (switchOut.Ability.Is("Natural Cure") && switchOut.Status != Status.None)
                {
                    manager.Enqueue(new HealStatus(switchOut, switchOut.Status));
                    manager.Enqueue(new AbilityTriggeredMessage(switchOut, switchOut.Ability));
                }

                manager.Enqueue(new SwitchOut(trainer, switchOut));
                manager.Enqueue(new SwitchOutAnimation(trainer, switchOut, battler));
                manager.Enqueue(new SwitchInMessage(trainer, switchIn));
                manager.Enqueue(new SwitchIn(switchIn, battler));
                manager.Enqueue(new SwitchInAnimation(trainer, switchIn, battler));
            }

            manager.Enqueue(new UpdateOpponentsForExp());
            manager.Enqueue(new TriggerSwitchInAbilities(switches));
            manager.Enqueue(new NextBattlerTurn());
            return null;
        }

        private static bool EscapeCheck(BattleManager manager, Pokemon self, Pokemon enemy)
        {
            // Based on https://bulbapedia.bulbagarden.net/wiki/Escape (accessed 2021-09-09)
            // It reports formulas just up to gen IV and I can't seem to find anything else online

            if (self.HasType(Models.Type.Ghost))
            {
                return true;
            }

            int ownSpeed = self.VolatilePokemon.Speed.Base;
            int enemySpeed = enemy.VolatilePokemon.Speed.Base;

            if (ownSpeed >= enemySpeed)
            {
                return true;
            }

            manager.EscapeAttempts += 1;
            int odds = (ownSpeed * 128 / enemySpeed + 30 * manager.EscapeAttempts) % 256;
            int attempt = RandomUtils.RangeInclusive(0, 255);
            return attempt < odds;
        }
    }
}