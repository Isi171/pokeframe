using System.Collections.Generic;
using System.Linq;
using Managers.Battle.Events.Faint;
using Managers.Battle.Events.Moves.Damage;
using Managers.Battle.Events.Statuses;
using Managers.Battle.Waiting;
using Models;
using Utils;
using View.Battle;

namespace Managers.Battle.Events.Turn
{
    public class NextBattlerTurn : Event
    {
        public override WaitMode Resolve(BattleManager manager)
        {
            manager.SortOnSpeedAndPriority();
            BattlerController battler = manager.Battlers
                .FirstOrDefault(b => !b.DecisionModule.HasActed && !b.Pokemon.Fainted);

            if (battler == null)
            {
                manager.Enqueue(new RoundEnd());
                return null;
            }

            DecisionModule decisionModule = battler.DecisionModule;
            decisionModule.HasActed = true;

            // Turn-losing statuses and volatile statuses
            if (battler.Pokemon.Status == Status.Frozen)
            {
                bool thaws = decisionModule.SelectedMove.HasFlag(MoveFlag.ThawsUser) || RandomUtils.ChanceOneInMany(5);
                if (!thaws)
                {
                    manager.Enqueue(new TurnLostStatusMessage(battler.Pokemon, battler.Pokemon.Status));
                    manager.Enqueue(new TurnLostStatusAnimation(battler.Pokemon, battler.Pokemon.Status));
                    manager.Enqueue(new TurnEnd(battler.Pokemon));
                    return null;
                }

                manager.Enqueue(new HealStatus(battler.Pokemon, battler.Pokemon.Status));
                manager.Enqueue(new TurnLostStatusHealedMessage(battler.Pokemon, battler.Pokemon.Status));
            }

            if (battler.Pokemon.Status == Status.Asleep)
            {
                if (battler.Pokemon.SleepCounter > 0)
                {
                    battler.Pokemon.SleepCounter -= battler.Pokemon.Ability.Is("Early Bird") ? 2 : 1;
                    manager.Enqueue(new TurnLostStatusMessage(battler.Pokemon, battler.Pokemon.Status));
                    manager.Enqueue(new TurnLostStatusAnimation(battler.Pokemon, battler.Pokemon.Status));
                    manager.Enqueue(new TurnEnd(battler.Pokemon));
                    return null;
                }

                manager.Enqueue(new HealStatus(battler.Pokemon, battler.Pokemon.Status));
                manager.Enqueue(new TurnLostStatusHealedMessage(battler.Pokemon, battler.Pokemon.Status));
            }

            if (battler.Pokemon.VolatilePokemon.Confused)
            {
                battler.Pokemon.VolatilePokemon.ConfusionCounter--;
                if (battler.Pokemon.VolatilePokemon.ConfusionCounter > 0)
                {
                    manager.Enqueue(new VolatileStatusIsPresentMessage(battler.Pokemon, VolatileStatus.Confusion));
                    manager.Enqueue(new VolatileStatusAnimation(battler.Pokemon, VolatileStatus.Confusion));

                    bool selfHarm = RandomUtils.ChanceOneInMany(3);
                    if (selfHarm)
                    {
                        manager.Enqueue(new TurnLostVolatileStatusMessage(battler.Pokemon, VolatileStatus.Confusion));
                        manager.Enqueue(new ConfusionSelfDamage(battler.Pokemon));
                        manager.Enqueue(new HpBarChange(battler.Pokemon));
                        manager.Enqueue(new FaintCheck(battler.Pokemon));
                        manager.Enqueue(new TurnEnd(battler.Pokemon));
                        return null;
                    }
                }
                else
                {
                    manager.Enqueue(new VolatileStatusHealedMessage(battler.Pokemon, VolatileStatus.Confusion));
                }
            }

            if (battler.Pokemon.VolatilePokemon.Flinched)
            {
                manager.Enqueue(new TurnLostVolatileStatusMessage(battler.Pokemon, VolatileStatus.Flinch));
                manager.Enqueue(new TurnEnd(battler.Pokemon));
                return null;
            }

            // TODO infatuation

            if (battler.Pokemon.Status == Status.Paralyzed)
            {
                bool paralyzed = RandomUtils.ChanceOneInMany(4);
                if (paralyzed)
                {
                    manager.Enqueue(new TurnLostStatusMessage(battler.Pokemon, battler.Pokemon.Status));
                    manager.Enqueue(new TurnLostStatusAnimation(battler.Pokemon, battler.Pokemon.Status));
                    manager.Enqueue(new TurnEnd(battler.Pokemon));
                    return null;
                }
            }


            // Execute move
            Queue<Event> moveEvents = manager.MoveManager.HandleMove(
                decisionModule.SelectedMove,
                BattleUtils.IfPresentAndNotFainted(decisionModule.Target),
                decisionModule.Self.Pokemon,
                BattleUtils.IfPresentAndNotFainted(decisionModule.Enemy),
                BattleUtils.IfPresentAndNotFainted(decisionModule.Ally),
                BattleUtils.IfPresentAndNotFainted(decisionModule.Enemy2));

            manager.Enqueue(moveEvents);

            manager.Enqueue(new TurnEnd(battler.Pokemon));

            return null;
        }
    }
}