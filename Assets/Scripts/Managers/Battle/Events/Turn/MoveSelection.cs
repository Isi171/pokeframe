using System.Linq;
using Managers.Battle.Waiting;
using View.Battle;

namespace Managers.Battle.Events.Turn
{
    public class MoveSelection : Event
    {
        public override WaitMode Resolve(BattleManager manager)
        {
            foreach (BattlerController battler in manager.UnsortedBattlers
                .Where(battler => !battler.Pokemon.Fainted)
                .Reverse())
            {
                battler.DecisionModule.ResetDecision();
                battler.DecisionModule.Decide();
            }

            manager.Enqueue(new MoveSelectionWait());
            return null;
        }
    }
}