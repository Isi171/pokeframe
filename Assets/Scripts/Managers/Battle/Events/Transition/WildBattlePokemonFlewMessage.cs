using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Transition
{
    public class WildBattlePokemonFlewMessage : Event
    {
        private readonly Pokemon pokemon;

        public WildBattlePokemonFlewMessage(Pokemon pokemon)
        {
            this.pokemon = pokemon;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndAwaitInput(manager.Strings.PokemonRunAwayWildBattle(pokemon));
        }
    }
}