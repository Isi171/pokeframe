using Managers.Battle.Waiting;

namespace Managers.Battle.Events.Transition
{
    public class BattleEnd : Event
    {
        private readonly bool victory;

        public BattleEnd(bool victory)
        {
            this.victory = victory;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            manager.EndBattle(victory);
            return new Stop();
        }
    }
}