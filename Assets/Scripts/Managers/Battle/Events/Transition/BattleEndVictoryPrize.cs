using Managers.Battle.Waiting;
using Models.Items;

namespace Managers.Battle.Events.Transition
{
    public class BattleEndVictoryPrize : Event
    {
        public override WaitMode Resolve(BattleManager manager)
        {
            LootTable loot = manager.BattleData.Prize;
            manager.BattleData.Player.Bag.Add(loot);
            return new DisplayTextAndAwaitInput(manager.Strings.GotItemsVictory(loot));
        }
    }
}