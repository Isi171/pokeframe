using Managers.Battle.Waiting;

namespace Managers.Battle.Events.Transition
{
    public class BattleEndAnimation : Event
    {
        private readonly bool victory;

        public BattleEndAnimation(bool victory)
        {
            this.victory = victory;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return victory
                ? new StartAnimation(manager.EnemyTrainerSlider, manager.EnemyTrainerSlider.SlideBackIn)
                : null;
        }
    }
}