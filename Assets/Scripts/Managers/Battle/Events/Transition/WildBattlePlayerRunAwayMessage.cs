using Managers.Battle.Waiting;

namespace Managers.Battle.Events.Transition
{
    public class WildBattlePlayerRunAwayMessage : Event
    {
        private readonly bool success;

        public WildBattlePlayerRunAwayMessage(bool success)
        {
            this.success = success;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndAwaitInput(manager.Strings.PlayerRunAwayWildBattle(success));
        }
    }
}