using Managers.Battle.Waiting;

namespace Managers.Battle.Events.Transition
{
    public class HidePokemonCount : Event
    {
        public override WaitMode Resolve(BattleManager manager)
        {
            return new StartAnimationWithoutCallback(() =>
            {
                manager.EnemyPokemonCounter1.Hide();
                manager.EnemyPokemonCounter2.Hide();
                manager.PlayerPokemonCounter1.Hide();
                manager.PlayerPokemonCounter2.Hide();
            });
        }
    }
}