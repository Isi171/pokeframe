using Managers.Battle.Waiting;

namespace Managers.Battle.Events.Transition
{
    public class ShowPokemonCount : Event
    {
        public override WaitMode Resolve(BattleManager manager)
        {
            return new StartAnimationWithoutCallback(() =>
            {
                manager.EnemyPokemonCounter1.Show();
                manager.EnemyPokemonCounter2.Show();
                manager.PlayerPokemonCounter1.Show();
                manager.PlayerPokemonCounter2.Show();
            });
        }
    }
}