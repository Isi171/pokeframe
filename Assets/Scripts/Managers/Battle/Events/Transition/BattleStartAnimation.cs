using Managers.Battle.Waiting;

namespace Managers.Battle.Events.Transition
{
    public class BattleStartAnimation : Event
    {
        public override WaitMode Resolve(BattleManager manager)
        {
            return new StartAnimation(manager.EnemyTrainerSlider,
                () => manager.EnemyTrainerSlider.SlideIn(manager.BattleData),
                () => manager.PlayerTrainerSlider.SlideIn(manager.BattleData),
                () => manager.TransitionEffect.UncoverTheScreenBattle());
        }
    }
}