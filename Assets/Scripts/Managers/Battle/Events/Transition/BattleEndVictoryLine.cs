using Managers.Battle.Waiting;

namespace Managers.Battle.Events.Transition
{
    public class BattleEndVictoryLine : Event
    {
        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndAwaitInput(manager.BattleData.VictoryLine);
        }
    }
}