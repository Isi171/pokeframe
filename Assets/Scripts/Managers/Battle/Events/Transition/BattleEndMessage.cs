using Managers.Battle.Waiting;

namespace Managers.Battle.Events.Transition
{
    public class BattleEndMessage : Event
    {
        private readonly bool victory;
        private readonly bool forfeit;

        public BattleEndMessage(bool victory, bool forfeit = false)
        {
            this.victory = victory;
            this.forfeit = forfeit;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndAwaitInput(victory
                ? manager.Strings.Victory(manager.BattleData.Enemy1, manager.BattleData.Enemy2, manager.BattleData.Ally)
                : manager.Strings.Loss(manager.BattleData.Enemy1, manager.BattleData.Enemy2, manager.BattleData.Ally,
                    forfeit));
        }
    }
}