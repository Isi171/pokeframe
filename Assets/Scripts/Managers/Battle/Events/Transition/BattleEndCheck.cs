using Managers.Battle.Events.Audio;
using Managers.Battle.Waiting;

namespace Managers.Battle.Events.Transition
{
    public class BattleEndCheck : Event
    {
        public override WaitMode Resolve(BattleManager manager)
        {
            if (manager.BattleData.Player.Party.CountAvailable == 0)
            {
                manager.Enqueue(new BattleEndMessage(false));

                if (manager.BattleData.TrainerBattle)
                {
                    manager.Enqueue(new BattleEndAnimation(false));
                }

                manager.Enqueue(new BattleEnd(false));
            }
            else if (manager.BattleData.Enemy1.Party.CountAvailable == 0
                     && (manager.BattleData.Enemy2 == null || manager.BattleData.Enemy2.Party.CountAvailable == 0))
            {
                manager.Enqueue(new StartVictoryMusic());

                if (manager.BattleData.TrainerBattle)
                {
                    manager.Enqueue(new BattleEndMessage(true));
                    if (manager.BattleData.VictoryLine != null)
                    {
                        manager.Enqueue(new BattleEndAnimation(true));
                        manager.Enqueue(new BattleEndVictoryLine());
                    }

                    if (manager.BattleData.Prize != null)
                    {
                        manager.Enqueue(new BattleEndVictoryPrize());
                    }
                }

                manager.Enqueue(new BattleEnd(true));
            }

            return null;
        }
    }
}