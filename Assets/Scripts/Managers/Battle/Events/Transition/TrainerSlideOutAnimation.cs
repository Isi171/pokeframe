using Managers.Battle.Waiting;

namespace Managers.Battle.Events.Transition
{
    public class TrainerSlideOutAnimation : Event
    {
        public override WaitMode Resolve(BattleManager manager)
        {
            return new StartAnimation(manager.PlayerTrainerSlider,
                manager.PlayerTrainerSlider.SlideOut,
                manager.EnemyTrainerSlider.SlideOut);
        }
    }
}