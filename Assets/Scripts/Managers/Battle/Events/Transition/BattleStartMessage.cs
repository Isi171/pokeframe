using Managers.Battle.Waiting;

namespace Managers.Battle.Events.Transition
{
    public class BattleStartMessage : Event
    {
        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndWait(manager.Strings.BattleStart(
                manager.BattleData.Enemy1,
                manager.BattleData.Enemy2,
                manager.BattleData.Ally,
                manager.BattleData.Type));
        }
    }
}