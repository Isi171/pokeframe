using Managers.Battle.Waiting;
using Models;
using Models.Pokemon;

namespace Managers.Battle.Events.Field
{
    public class WeatherDamageMessage : Event
    {
        private readonly Weather weather;
        private readonly Pokemon target;

        public WeatherDamageMessage(Weather weather, Pokemon target)
        {
            this.weather = weather;
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndWait(manager.Strings.WeatherDamage(weather, target));
        }
    }
}