using System;
using Managers.Battle.Waiting;
using Models;
using Models.Pokemon;

namespace Managers.Battle.Events.Field
{
    public class WeatherHeal : Event
    {
        private readonly Weather weather;
        private readonly Pokemon target;

        public WeatherHeal(Weather weather, Pokemon target)
        {
            this.weather = weather;
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            int heal = Math.Max(1, target.Hp.Derived / 16);
            target.CurrentHp += Math.Max(1, heal);
            return null;
        }
    }
}