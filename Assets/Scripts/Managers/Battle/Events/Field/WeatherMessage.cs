using Managers.Battle.Waiting;
using Models;

namespace Managers.Battle.Events.Field
{
    public class WeatherMessage : Event
    {
        private readonly Weather weather;

        public WeatherMessage(Weather weather)
        {
            this.weather = weather;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndWait(manager.Strings.WeatherStart(weather));
        }
    }
}