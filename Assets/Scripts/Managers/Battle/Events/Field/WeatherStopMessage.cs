using Managers.Battle.Waiting;
using Models;

namespace Managers.Battle.Events.Field
{
    public class WeatherStopMessage : Event
    {
        private readonly Weather weather;

        public WeatherStopMessage(Weather weather)
        {
            this.weather = weather;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndWait(manager.Strings.WeatherEnd(weather));
        }
    }
}