using Managers.Battle.Waiting;
using Models;

namespace Managers.Battle.Events.Field
{
    public class WeatherStop : Event
    {
        public override WaitMode Resolve(BattleManager manager)
        {
            manager.Field.Weather = Weather.Clear;
            manager.UpdateWeatherIndicator(Weather.Clear);
            return null;
        }
    }
}