using System;
using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Field
{
    public class WeatherDamage : Event
    {
        private readonly Pokemon target;

        public WeatherDamage(Pokemon target)
        {
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            target.CurrentHp -= Math.Max(1, target.Hp.Derived / 16);
            return new StartAnimation(manager.BattlerFromModel(target).StartReceiveDamageAnimation);
        }
    }
}