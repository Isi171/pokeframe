using Managers.Battle.Waiting;
using Models;
using Models.Pokemon;

namespace Managers.Battle.Events.Field
{
    public class SetWeather : Event
    {
        private readonly Weather weather;
        private readonly Pokemon user;

        public SetWeather(Weather weather, Pokemon user)
        {
            this.weather = weather;
            this.user = user;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            manager.Field.Weather = weather;

            switch (weather)
            {
                case Weather.Rain when user.HeldItem.Is("Damp Rock"):
                case Weather.Sun when user.HeldItem.Is("Heat Rock"):
                case Weather.Hail when user.HeldItem.Is("Icy Rock"):
                case Weather.Sandstorm when user.HeldItem.Is("Smooth Rock"):
                    manager.Field.WeatherCounter = 8;
                    break;
                default:
                    manager.Field.WeatherCounter = 5;
                    break;
            }

            manager.UpdateWeatherIndicator(weather);
            return null;
        }
    }
}