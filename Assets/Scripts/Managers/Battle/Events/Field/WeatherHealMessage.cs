using Managers.Battle.Waiting;
using Models;
using Models.Pokemon;

namespace Managers.Battle.Events.Field
{
    public class WeatherHealMessage : Event
    {
        private readonly Weather weather;
        private readonly Pokemon target;

        public WeatherHealMessage(Weather weather, Pokemon target)
        {
            this.weather = weather;
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndWait(manager.Strings.WeatherHeal(weather, target));
        }
    }
}