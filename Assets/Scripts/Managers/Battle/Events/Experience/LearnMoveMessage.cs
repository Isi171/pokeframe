using System.Linq;
using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Experience
{
    public class LearnMoveMessage : Event
    {
        private readonly Pokemon target;
        private readonly string move;

        public LearnMoveMessage(Pokemon target, string move)
        {
            this.target = target;
            this.move = move;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            bool hasFreeSlots = target.Moves.Any(slot => !slot.IsAssigned());
            return new DisplayTextAndAwaitInput(hasFreeSlots
                ? manager.Strings.MoveLearned(target, move)
                : manager.Strings.WantsToLearnMove(target, move));
        }
    }
}