using Managers.Battle.Waiting;
using View.Battle;

namespace Managers.Battle.Events.Experience
{
    public class UpdateOpponentsForExp : Event
    {
        public override WaitMode Resolve(BattleManager manager)
        {
            foreach (BattlerController battler in manager.Battlers)
            {
                battler.Pokemon.VolatilePokemon.Opponents.Add(battler.DecisionModule.Enemy.Pokemon);
                if (battler.DecisionModule.Enemy2 != null)
                {
                    battler.Pokemon.VolatilePokemon.Opponents.Add(battler.DecisionModule.Enemy2.Pokemon);
                }
            }

            return null;
        }
    }
}