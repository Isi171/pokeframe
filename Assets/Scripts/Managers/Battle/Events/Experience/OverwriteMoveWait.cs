using Managers.Battle.Waiting;
using Managers.Shared;
using Models.Pokemon;

namespace Managers.Battle.Events.Experience
{
    public class OverwriteMoveWait : Event
    {
        private readonly Pokemon pokemon;
        private readonly string move;

        public OverwriteMoveWait(Pokemon pokemon, string move)
        {
            Loggable = false;

            this.pokemon = pokemon;
            this.move = move;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            MoveOverwriteDecider moveOverwriteDecider = manager.MoveOverwriteDecider;

            if (!moveOverwriteDecider.MoveOverwriteReady)
            {
                manager.PushToFront(new OverwriteMoveWait(pokemon, move));
                return null;
            }

            // If outside the range, new move got declined
            if (moveOverwriteDecider.MoveSlotToOverwrite >= 0 && moveOverwriteDecider.MoveSlotToOverwrite <= 3)
            {
                pokemon.Moves[moveOverwriteDecider.MoveSlotToOverwrite] = new MoveSlot(move);
            }

            moveOverwriteDecider.ResetMoveOverwrite();
            manager.ClearMessageBox();
            manager.PlayerMenu.Hide();
            manager.PlayerMenu.SelectionDone();
            return null;
        }
    }
}