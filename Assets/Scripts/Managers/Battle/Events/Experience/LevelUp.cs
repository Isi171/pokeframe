using Managers.Battle.Waiting;
using Models.Pokemon;
using View.Battle;

namespace Managers.Battle.Events.Experience
{
    public class LevelUp : Event
    {
        private readonly Pokemon target;

        public LevelUp(Pokemon target)
        {
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            manager.OldStats = new[]
            {
                target.Hp.Derived,
                target.Attack.Derived,
                target.Defense.Derived,
                target.SpecialAttack.Derived,
                target.SpecialDefense.Derived,
                target.Speed.Derived
            };

            int oldHp = target.Hp.Derived;
            target.Level++;
            target.CalculateDerivedStats();
            int newHp = target.Hp.Derived;
            if (oldHp < newHp)
            {
                target.CurrentHp += newHp - oldHp;
            }

            if (manager.PokemonIsOut(target))
            {
                target.VolatilePokemon.UpdateStats(target);
                BattlerController battler = manager.BattlerFromModel(target);
                battler.UpdateLevel();
                battler.UpdateExperienceBarInstant();
                // TODO just update the hp bar instead of re-setting everything
                battler.SetPokemon(target);
            }

            manager.EvolutionManager.EvaluatePostBattleEvolution(target);

            return null;
        }
    }
}