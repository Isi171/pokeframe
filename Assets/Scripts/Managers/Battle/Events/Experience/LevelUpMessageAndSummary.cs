using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Experience
{
    public class LevelUpMessageAndSummary : Event
    {
        private readonly Pokemon target;

        public LevelUpMessageAndSummary(Pokemon target)
        {
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            manager.LevelUpSummary.Display(target, manager.OldStats, true);
            manager.OldStats = null;
            return new DisplayTextAndAwaitInput(manager.Strings.LevelUp(target), manager.LevelUpSummary.Hide);
        }
    }
}