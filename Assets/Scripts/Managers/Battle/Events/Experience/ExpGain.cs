using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Experience
{
    public class ExpGain : Event
    {
        private readonly Pokemon target;
        private readonly int value;

        public ExpGain(Pokemon target, int value)
        {
            this.target = target;
            this.value = value;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            target.Experience.TotalExp += value;
            return null;
        }
    }
}