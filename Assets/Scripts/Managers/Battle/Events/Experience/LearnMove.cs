using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Experience
{
    public class LearnMove : Event
    {
        private readonly Pokemon target;
        private readonly string move;

        public LearnMove(Pokemon target, string move)
        {
            this.target = target;
            this.move = move;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            for (int i = 0; i < 4; i++)
            {
                if (!target.Moves[i].IsAssigned())
                {
                    target.Moves[i] = new MoveSlot(move);
                    return null;
                }
            }

            manager.PlayerMenu.RequestOverwriteMove(manager.MoveOverwriteDecider, target, move);
            manager.PushToFront(new OverwriteMoveWait(target, move));
            return null;
        }
    }
}