using Managers.Battle.Waiting;
using Models;
using Models.Pokemon;

namespace Managers.Battle.Events.Experience
{
    public class EvGain : Event
    {
        private readonly Pokemon target;
        private readonly StatName stat;
        private readonly int value;

        public EvGain(Pokemon target, StatName stat, int value)
        {
            this.target = target;
            this.stat = stat;
            this.value = value;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            int oldHp = target.Hp.Derived;
            target.ChangeEvCheckingLimit(stat, value);
            target.CalculateDerivedStats();
            int newHp = target.Hp.Derived;
            if (oldHp < newHp)
            {
                target.CurrentHp += newHp - oldHp;
            }

            if (manager.PokemonIsOut(target))
            {
                target.VolatilePokemon.UpdateStats(target);
                // TODO just update the hp bar instead of re-setting everything
                manager.BattlerFromModel(target).SetPokemon(target);
            }

            return null;
        }
    }
}