using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Experience
{
    public class ExpBarChange : Event
    {
        private readonly Pokemon target;

        public ExpBarChange(Pokemon target)
        {
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            if (manager.PokemonIsOut(target))
            {
                return new StartAnimation(manager.BattlerFromModel(target).UpdateExperienceBarAnimated);
            }

            return null;
        }
    }
}