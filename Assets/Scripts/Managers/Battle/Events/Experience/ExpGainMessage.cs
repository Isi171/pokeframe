using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Experience
{
    public class ExpGainMessage : Event
    {
        private readonly Pokemon target;
        private readonly int value;

        public ExpGainMessage(Pokemon target, int value)
        {
            this.target = target;
            this.value = value;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndWait(manager.Strings.ExpGain(target, value));
        }
    }
}