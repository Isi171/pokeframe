using Managers.Battle.Waiting;

namespace Managers.Battle.Events.Audio
{
    public class StartVictoryMusic : Event
    {
        public override WaitMode Resolve(BattleManager manager)
        {
            if (manager.BattleData.TrainerBattle)
            {
                manager.AudioManager.PlayMusic(manager.BattleData.VictoryMusic);
            }
            else
            {
                // TODO properly define wild battle music somewhere
                manager.AudioManager.PlayMusic("Victory#Wild");
            }
            
            return null;
        }
    }
}