using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Abilities
{
    public class TriggerRegenerator : Event
    {
        private readonly Pokemon pokemon;

        public TriggerRegenerator(Pokemon pokemon)
        {
            this.pokemon = pokemon;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            pokemon.CurrentHp += pokemon.Hp.Derived / 3;
            return null;
        }
    }
}