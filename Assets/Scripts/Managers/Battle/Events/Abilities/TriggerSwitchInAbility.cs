using Managers.Battle.Waiting;
using Models.Abilities;
using Models.Pokemon;

namespace Managers.Battle.Events.Abilities
{
    public class TriggerSwitchInAbility : Event
    {
        private readonly Ability ability;
        private readonly Pokemon source;
        private readonly Pokemon[] enemies;

        public TriggerSwitchInAbility(Ability ability, Pokemon source, Pokemon[] enemies)
        {
            this.ability = ability;
            this.source = source;
            this.enemies = enemies;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            manager.PushToFront(manager.AbilityManager.HandleSwitchInAbility(ability, source, enemies));
            return null;
        }
    }
}