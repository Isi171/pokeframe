using System.Collections.Generic;
using System.Linq;
using Managers.Battle.Waiting;
using Models.Pokemon;
using Utils;
using View.Battle;

namespace Managers.Battle.Events.Abilities
{
    public class TriggerSwitchInAbilities : Event
    {
        private readonly BattlerController[] switchedInBattlers;

        public TriggerSwitchInAbilities(BattlerController[] switchedInBattlers)
        {
            this.switchedInBattlers = switchedInBattlers;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            Queue<Event> events = new Queue<Event>();
            foreach (BattlerController source in switchedInBattlers)
            {
                Pokemon[] enemies = new[]
                    {
                        BattleUtils.IfPresentAndNotFainted(source.DecisionModule.Enemy),
                        BattleUtils.IfPresentAndNotFainted(source.DecisionModule.Enemy2)
                    }.Where(pokemon => pokemon != null)
                    .ToArray();

                events.Enqueue(new TriggerSwitchInAbility(source.Pokemon.Ability, source.Pokemon, enemies));
            }

            manager.PushToFront(events);
            return null;
        }
    }
}