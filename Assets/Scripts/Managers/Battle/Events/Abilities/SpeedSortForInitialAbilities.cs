using Managers.Battle.Waiting;

namespace Managers.Battle.Events.Abilities
{
    public class SpeedSortForInitialAbilities : Event
    {
        public override WaitMode Resolve(BattleManager manager)
        {
            manager.SortOnSpeedAndPriority();
            return null;
        }
    }
}