using Managers.Battle.Waiting;
using Models.Abilities;
using Models.Pokemon;

namespace Managers.Battle.Events.Abilities
{
    public class AbilityTriggeredMessage : Event
    {
        private readonly Pokemon user;
        private readonly Ability ability;
        private readonly Pokemon target;
        private readonly Pokemon target2;

        public AbilityTriggeredMessage(Pokemon user, Ability ability, Pokemon target = null, Pokemon target2 = null)
        {
            this.user = user;
            this.ability = ability;
            this.target = target;
            this.target2 = target2;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndWait(manager.Strings.AbilityTriggered(ability, user, target, target2));
        }
    }
}