using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Moves
{
    public class MoveAnimation : Event
    {
        private readonly Move move;
        private readonly Pokemon user;
        private readonly Pokemon target;

        public MoveAnimation(Move move, Pokemon user, Pokemon target)
        {
            this.move = move;
            this.user = user;
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            // TODO animation
            return new StartAnimation(manager.BattlerFromModel(user).StartMoveAnimation);
        }
    }
}