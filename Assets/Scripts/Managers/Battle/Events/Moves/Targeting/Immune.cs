using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Moves.Targeting
{
    public class Immune : Event
    {
        private readonly Pokemon target;

        public Immune(Pokemon target)
        {
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndWait(manager.Strings.Immune(target));
        }
    }
}