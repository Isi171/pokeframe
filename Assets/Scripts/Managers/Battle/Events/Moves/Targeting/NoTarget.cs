using Managers.Battle.Waiting;

namespace Managers.Battle.Events.Moves.Targeting
{
    public class NoTarget : Event
    {
        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndWait(manager.Strings.NoTarget());
        }
    }
}