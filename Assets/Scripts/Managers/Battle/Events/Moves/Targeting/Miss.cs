using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Moves.Targeting
{
    public class Miss : Event
    {
        private readonly Pokemon target;

        public Miss(Pokemon target)
        {
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndWait(manager.Strings.Miss(target));
        }
    }
}