using Managers.Battle.Waiting;

namespace Managers.Battle.Events.Moves
{
    public class TimePaddingForMoveAnnouncement : Event
    {
        public override WaitMode Resolve(BattleManager manager)
        {
            return new Wait();
        }
    }
}