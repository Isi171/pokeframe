using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Moves.Damage
{
    public class HpBarChange : Event
    {
        private readonly Pokemon target;

        public HpBarChange(Pokemon target)
        {
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new StartAnimation(manager.BattlerFromModel(target).UpdateHpBar);
        }
    }
}