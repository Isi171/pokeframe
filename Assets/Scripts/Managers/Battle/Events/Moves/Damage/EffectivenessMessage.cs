using Managers.Battle.Waiting;
using Models;

namespace Managers.Battle.Events.Moves.Damage
{
    public class EffectivenessMessage : Event
    {
        private readonly TypeEffectiveness effectiveness;

        public EffectivenessMessage(TypeEffectiveness effectiveness)
        {
            this.effectiveness = effectiveness;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndWait(manager.Strings.Effectiveness(effectiveness));
        }
    }
}