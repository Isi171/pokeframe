using Managers.Battle.Waiting;

namespace Managers.Battle.Events.Moves.Damage
{
    public class CriticalMessage : Event
    {
        public override WaitMode Resolve(BattleManager manager)
        {
            // TODO crits during multistrikes clear the textbox https://youtu.be/kKpMw73E3MM?t=1124
            return new DisplayTextAndWait(manager.Strings.Critical());
        }
    }
}