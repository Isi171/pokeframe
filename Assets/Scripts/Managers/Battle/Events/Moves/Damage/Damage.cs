using System;
using Managers.Battle.Waiting;
using Models;
using Models.Pokemon;
using Utils;

namespace Managers.Battle.Events.Moves.Damage
{
    public class Damage : Event
    {
        private readonly Move move;
        private readonly Pokemon user;
        private readonly Pokemon target;
        private readonly bool critical;
        private readonly bool multiTarget;
        private readonly TypeEffectiveness effectiveness;

        public Damage(Move move, Pokemon user, Pokemon target, bool critical, bool multiTarget,
            TypeEffectiveness effectiveness)
        {
            this.move = move;
            this.user = user;
            this.target = target;
            this.critical = critical;
            this.multiTarget = multiTarget;
            this.effectiveness = effectiveness;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            if (target.Fainted)
            {
                return null;
            }

            int damage = GetDamageValue(move, user, target, critical, multiTarget, effectiveness, manager.MoveManager);
            int prevCurrentHp = target.CurrentHp;
            target.CurrentHp -= damage;
            manager.LastDamageDealt = prevCurrentHp - target.CurrentHp;
            return new StartAnimation(manager.BattlerFromModel(target).StartReceiveDamageAnimation);
        }

        public static int GetDamageValue(Move move, Pokemon user, Pokemon target, bool critical, bool multiTarget,
            TypeEffectiveness effectiveness, MoveManager moveManager)
        {
            if (move.SetDamage > 0)
            {
                return move.SetDamage;
            }

            return move.SpecialEffect switch
            {
                SpecialEffect.LevelBasedDamage => user.Level,
                SpecialEffect.Psywave => Math.Max(1, user.Level * (50 + RandomUtils.RangeInclusive(0, 100)) / 100),
                SpecialEffect.SuperFang => Math.Max(1, target.CurrentHp / 2),
                SpecialEffect.OneHitKo => target.CurrentHp,
                SpecialEffect.Endeavor => target.CurrentHp - user.CurrentHp,
                _ => moveManager.CalculateDamage(move, user, target, multiTarget, critical, effectiveness)
            };
        }
    }
}