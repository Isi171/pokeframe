using System.Linq;
using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Moves
{
    public class PpDeduction : Event
    {
        private readonly Move move;
        private readonly Pokemon user;

        public PpDeduction(Move move, Pokemon user)
        {
            this.move = move;
            this.user = user;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            MoveSlot slot = user.Moves.Where(slot => slot.IsAssigned()).First(slot => slot.Move == move);
            slot.CurrentPp--;
            return null;
        }
    }
}