using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Moves
{
    public class MoveAnnouncement : Event
    {
        private readonly Move move;
        private readonly Pokemon user;

        public MoveAnnouncement(Move move, Pokemon user)
        {
            this.move = move;
            this.user = user;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndAllowNextEvent(manager.Strings.MoveAnnouncement(user, move));
        }
    }
}