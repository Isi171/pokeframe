using Managers.Battle.Waiting;
using Models.Pokemon;
using Utils;

namespace Managers.Battle.Events.Moves.Effects.Special
{
    public class LoseOneQuarterMaxHp : Event
    {
        private readonly Pokemon target;

        public LoseOneQuarterMaxHp(Pokemon target)
        {
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            target.CurrentHp -= CustomMath.Div(target.Hp.Derived, 4);
            return null;
        }
    }
}