using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Moves.Effects.Special
{
    public class CrashSelfDamage : Event
    {
        private readonly Pokemon user;

        public CrashSelfDamage(Pokemon user)
        {
            this.user = user;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            int damage = user.Hp.Derived / 2;
            user.CurrentHp -= damage;
            return new StartAnimation(manager.BattlerFromModel(user).StartReceiveDamageAnimation);
        }
    }
}