using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Moves.Effects.Special
{
    public class LoseHalfMaxHp : Event
    {
        private readonly Pokemon target;

        public LoseHalfMaxHp(Pokemon target)
        {
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            target.CurrentHp -= target.Hp.Derived / 2;
            return null;
        }
    }
}