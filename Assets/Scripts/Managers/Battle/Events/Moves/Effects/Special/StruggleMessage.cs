using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Moves.Effects.Special
{
    public class StruggleMessage : Event
    {
        private readonly Pokemon user;

        public StruggleMessage(Pokemon user)
        {
            this.user = user;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndWait(manager.Strings.Struggle(user));
        }
    }
}