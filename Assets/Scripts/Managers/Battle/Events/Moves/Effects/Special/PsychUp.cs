using Managers.Battle.Waiting;
using Models.Battle;
using Models.Pokemon;

namespace Managers.Battle.Events.Moves.Effects.Special
{
    public class PsychUp : Event
    {
        private readonly Pokemon user;
        private readonly Pokemon target;

        public PsychUp(Pokemon user, Pokemon target)
        {
            this.user = user;
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            VolatilePokemon volatileUser = user.VolatilePokemon;
            VolatilePokemon volatileTarget = target.VolatilePokemon;
            volatileUser.Attack.Stage.Value = volatileTarget.Attack.Stage.Value;
            volatileUser.Defense.Stage.Value = volatileTarget.Defense.Stage.Value;
            volatileUser.SpecialAttack.Stage.Value = volatileTarget.SpecialAttack.Stage.Value;
            volatileUser.SpecialDefense.Stage.Value = volatileTarget.SpecialDefense.Stage.Value;
            volatileUser.Speed.Stage.Value = volatileTarget.Speed.Stage.Value;
            volatileUser.AccuracyStage.Value = volatileTarget.AccuracyStage.Value;
            volatileUser.EvasionStage.Value = volatileTarget.EvasionStage.Value;
            // TODO should also copy critical stage changes such as Focus Energy
            manager.BattlerFromModel(user).UpdateStatIndicators();

            return null;
        }
    }
}