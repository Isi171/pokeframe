using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Moves.Effects.Special
{
    public class SpecialMoveMessage : Event
    {
        private readonly Move move;
        private readonly Pokemon user;
        private readonly Pokemon target;
        private readonly string data;
        private readonly int value;

        public SpecialMoveMessage(Move move, Pokemon user, Pokemon target, string data = "", int value = 0)
        {
            this.move = move;
            this.user = user;
            this.target = target;
            this.data = data;
            this.value = value;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndWait(manager.Strings.SpecialMoveEffect(move, user, target, data, value));
        }
    }
}