using Managers.Battle.Waiting;
using Models;
using Models.Pokemon;

namespace Managers.Battle.Events.Moves.Effects.Special
{
    public class HealSunny : Event
    {
        private readonly Pokemon target;
    
        public HealSunny(Pokemon target)
        {
            this.target = target;
        }
    
        public override WaitMode Resolve(BattleManager manager)
        {
            int amount;
            if (manager.Field.Weather == Weather.Clear)
            {
                amount = target.Hp.Derived / 2;
            }
            else if (manager.Field.Weather == Weather.Sun)
            {
                amount = target.Hp.Derived * 2 / 3;
            }
            else
            {
                amount = target.Hp.Derived / 4;
            }

            target.CurrentHp += amount;
            return null;
        }
    }
}