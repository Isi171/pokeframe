using System.Linq;
using Managers.Battle.Events.Statuses;
using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Moves.Effects.Special
{
    public class HealPartyStatus : Event
    {
        private readonly Pokemon user;

        public HealPartyStatus(Pokemon user)
        {
            this.user = user;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            manager.PushToFront(manager.BattlerFromModel(user)
                .Trainer
                .Party
                .All
                .Select(member => new HealStatus(member, member.Status)));

            return null;
        }
    }
}