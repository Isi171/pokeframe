using Managers.Battle.Waiting;
using View.Battle;

namespace Managers.Battle.Events.Moves.Effects.Special
{
    public class Haze : Event
    {
        public override WaitMode Resolve(BattleManager manager)
        {
            foreach (BattlerController battler in manager.Battlers)
            {
                battler.Pokemon.VolatilePokemon.Attack.Stage.Value = 0;
                battler.Pokemon.VolatilePokemon.Defense.Stage.Value = 0;
                battler.Pokemon.VolatilePokemon.SpecialAttack.Stage.Value = 0;
                battler.Pokemon.VolatilePokemon.SpecialDefense.Stage.Value = 0;
                battler.Pokemon.VolatilePokemon.Speed.Stage.Value = 0;
                battler.Pokemon.VolatilePokemon.AccuracyStage.Value = 0;
                battler.Pokemon.VolatilePokemon.EvasionStage.Value = 0;
                battler.UpdateStatIndicators();
            }

            return null;
        }
    }
}