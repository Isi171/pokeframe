using Managers.Battle.Waiting;
using Models;
using Models.Battle;
using Models.Pokemon;

namespace Managers.Battle.Events.Moves.Effects
{
    public class StatChange : Event
    {
        private readonly Pokemon target;
        private readonly StatName stat;
        private readonly int stage;

        public StatChange(Pokemon target, StatName stat, int stage)
        {
            this.target = target;
            this.stat = stat;
            this.stage = stage;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            if (target.Fainted)
            {
                return null;
            }

            VolatilePokemon pokemon = target.VolatilePokemon;
            StatStage statStage = pokemon.GetStatStageFromStatName(stat);
            statStage.Value += stage;
            manager.BattlerFromModel(target).UpdateStatIndicators();
            return null;
        }
    }
}