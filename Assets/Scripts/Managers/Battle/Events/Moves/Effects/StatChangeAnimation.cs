using Managers.Battle.Waiting;
using Models;
using Models.Battle;
using Models.Pokemon;

namespace Managers.Battle.Events.Moves.Effects
{
    public class StatChangeAnimation : Event
    {
        private readonly Pokemon target;
        private readonly StatName stat;
        private readonly int stage;

        public StatChangeAnimation(Pokemon target, StatName stat, int stage)
        {
            this.target = target;
            this.stat = stat;
            this.stage = stage;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            if (target.Fainted)
            {
                return null;
            }

            VolatilePokemon volatilePokemon = target.VolatilePokemon;
            StatStage statStage = volatilePokemon.GetStatStageFromStatName(stat);
            if (statStage.Value == 6 || statStage.Value == -6)
            {
                manager.StatWontChange = true;
                return null;
            }

            return new StartAnimation(manager.BattlerFromModel(target).StartStatChangeAnimation);
        }
    }
}