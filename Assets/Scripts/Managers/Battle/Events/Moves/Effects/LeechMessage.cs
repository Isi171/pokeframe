using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Moves.Effects
{
    public class LeechMessage : Event
    {
        private readonly Pokemon target;

        public LeechMessage(Pokemon target)
        {
            this.target = target;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndWait(manager.Strings.Leech(target));
        }
    }
}