using Managers.Battle.Waiting;

namespace Managers.Battle.Events.Moves.Effects
{
    public class MultiStrikeBegin : Event
    {
        public override WaitMode Resolve(BattleManager manager)
        {
            manager.MultiStrikeInProgress = true;
            return null;
        }
    }
}