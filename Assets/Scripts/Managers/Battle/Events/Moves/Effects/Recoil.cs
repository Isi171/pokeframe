using System;
using Managers.Battle.Waiting;
using Models.Pokemon;
using Utils;

namespace Managers.Battle.Events.Moves.Effects
{
    public class Recoil : Event
    {
        private readonly Pokemon user;
        private readonly int recoilDivisor;

        public Recoil(Pokemon user, int recoilDivisor)
        {
            this.user = user;
            this.recoilDivisor = recoilDivisor;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            user.CurrentHp -= Math.Max(1, CustomMath.Div(manager.LastDamageDealt, recoilDivisor));
            manager.LastDamageDealt = 0;
            return null;
        }
    }
}