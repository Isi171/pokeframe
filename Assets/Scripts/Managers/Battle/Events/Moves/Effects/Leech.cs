using Managers.Battle.Waiting;
using Models.Pokemon;
using Utils;

namespace Managers.Battle.Events.Moves.Effects
{
    public class Leech : Event
    {
        private readonly Pokemon user;

        public Leech(Pokemon user)
        {
            this.user = user;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            int leech = CustomMath.Mul(manager.LastDamageDealt, 0.5d);
            leech = CustomMath.Mul(leech, 1.3d, user.HeldItem.Is("Big Root"));
            user.CurrentHp += leech;
            manager.LastDamageDealt = 0;
            return null;
        }
    }
}