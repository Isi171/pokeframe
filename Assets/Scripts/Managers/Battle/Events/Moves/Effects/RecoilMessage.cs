using Managers.Battle.Waiting;
using Models.Pokemon;

namespace Managers.Battle.Events.Moves.Effects
{
    public class RecoilMessage : Event
    {
        private readonly Pokemon user;

        public RecoilMessage(Pokemon user)
        {
            this.user = user;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            return new DisplayTextAndWait(manager.Strings.Recoil(user));
        }
    }
}