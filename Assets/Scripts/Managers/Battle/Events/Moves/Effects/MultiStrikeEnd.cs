using Managers.Battle.Waiting;

namespace Managers.Battle.Events.Moves.Effects
{
    public class MultiStrikeEnd : Event
    {
        private readonly int strikes;

        public MultiStrikeEnd(int strikes)
        {
            this.strikes = strikes;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            if (manager.MultiStrikeInProgress)
            {
                manager.MultiStrikeInProgress = false;
                return new DisplayTextAndWait(manager.Strings.MultiStrike(strikes));
            }
            else
            {
                return null;
            }
        }
    }
}