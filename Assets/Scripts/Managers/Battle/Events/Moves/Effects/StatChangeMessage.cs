using Managers.Battle.Waiting;
using Models;
using Models.Pokemon;

namespace Managers.Battle.Events.Moves.Effects
{
    public class StatChangeMessage : Event
    {
        private readonly Pokemon target;
        private readonly StatName stat;
        private readonly int stage;

        public StatChangeMessage(Pokemon target, StatName stat, int stage)
        {
            this.target = target;
            this.stat = stat;
            this.stage = stage;
        }

        public override WaitMode Resolve(BattleManager manager)
        {
            if (target.Fainted)
            {
                return null;
            }

            if (manager.StatWontChange)
            {
                manager.StatWontChange = false;
                return new DisplayTextAndWait(manager.Strings.StatWontChange(target, stat, stage));
            }
            else
            {
                return new DisplayTextAndWait(manager.Strings.StatChanged(target, stat, stage));
            }
        }
    }
}