using System.Linq;
using Models;
using Models.Items;
using Models.Pokemon;
using UnityEngine;
using View.Battle;

namespace Managers.Battle
{
    public class PlayerDecisionModule : DecisionModule
    {
        private static readonly int Highlighted = Animator.StringToHash("Highlighted");
        [SerializeField] private PlayerMenuController playerMenu;

        public override void Decide()
        {
            playerMenu.RequestMenu(Self);
        }

        public override void Substitute()
        {
            playerMenu.RequestSubstitution(Self);
        }

        public void Run()
        {
            playerMenu.Hide();
            playerMenu.SelectionDone();
            Forfeit = true;
            Ready = true;
        }

        public void ChooseItem(Item item, Pokemon target)
        {
            playerMenu.Hide();
            playerMenu.SelectionDone();
            Item = item;
            ItemTarget = target;
            Ready = true;
        }

        public void SwitchIn(Pokemon pokemon)
        {
            playerMenu.Hide();
            playerMenu.SelectionDone();
            Switch = pokemon;
            Ready = true;
        }

        public void SwitchInSubstitution(Pokemon pokemon)
        {
            playerMenu.Hide();
            playerMenu.SelectionDone();
            Substitution = pokemon;
            SubstitutionReady = true;
        }

        public override void SelectStruggle()
        {
            playerMenu.Hide();
            playerMenu.SelectionDone();
            base.SelectStruggle();
        }

        public void Select(Move move)
        {
            playerMenu.Hide();
            SelectedMove = move;

            // Auto targets
            if (move.Target != MoveTarget.Target)
            {
                playerMenu.SelectionDone();
                Ready = true;
                return;
            }

            // Single battle
            if (Ally == null && Enemy2 == null)
            {
                playerMenu.SelectionDone();
                Target = Enemy;
                Ready = true;
                return;
            }

            BattlerController[] others = GetOthers();
            foreach (BattlerController battler in others)
            {
                battler.SelectionButton.interactable = true;
                battler.SelectionButton.onClick.RemoveAllListeners();
                battler.SelectionButton.onClick.AddListener(() => TargetSelected(battler));
            }

            others.First().SelectionButton.Select();
            others.First().GetComponent<Animator>().SetTrigger(Highlighted);
        }

        private void TargetSelected(BattlerController target)
        {
            playerMenu.SelectionDone();

            BattlerController[] others = GetOthers();
            foreach (BattlerController battler in others)
            {
                battler.SelectionButton.interactable = false;
                battler.SelectionButton.onClick.RemoveAllListeners();
            }

            Target = target;
            Ready = true;
        }

        private BattlerController[] GetOthers()
        {
            return new[] { Enemy, Enemy2, Ally }
                .Where(battler => !battler.Pokemon.Fainted)
                .ToArray();
        }
    }
}