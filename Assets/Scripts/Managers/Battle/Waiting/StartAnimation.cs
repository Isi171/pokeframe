using System;
using Managers.Cutscene.Waiting;
using Managers.Shared;

namespace Managers.Battle.Waiting
{
    public class StartAnimation : WaitMode
    {
        private readonly IAnimationDoneSource callbackSource;
        private readonly Action startWithCallback;
        private readonly Action[] startWithoutCallback;

        public StartAnimation(Action startWithCallback)
        {
            this.startWithCallback = startWithCallback;
        }

        public StartAnimation(IAnimationDoneSource callbackSource, Action startWithCallback, params Action[] startWithoutCallback)
        {
            this.callbackSource = callbackSource;
            this.startWithCallback = startWithCallback;
            this.startWithoutCallback = startWithoutCallback;
        }

        public override void Await()
        {
            callbackSource?.SetAnimationDoneCallback(AllowNextEvent);

            startWithCallback();
            if (startWithoutCallback != null)
            {
                foreach (Action action in startWithoutCallback)
                {
                    action.Invoke();
                }
            }
        }
    }
}