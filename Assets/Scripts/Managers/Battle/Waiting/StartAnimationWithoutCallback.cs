using System;

namespace Managers.Battle.Waiting
{
    public class StartAnimationWithoutCallback : WaitMode
    {
        private readonly Action animationStart;

        public StartAnimationWithoutCallback(Action animationStart)
        {
            this.animationStart = animationStart;
        }

        public override void Await()
        {
            animationStart();
            AllowNextEvent();
        }
    }
}