using System;

namespace Managers.Battle.Waiting
{
    public class DisplayTextAndAwaitInput : WaitMode
    {
        private readonly string text;
        private readonly Action extraCallback;

        public DisplayTextAndAwaitInput(string text, Action extraCallback = null)
        {
            this.text = text;
            this.extraCallback = extraCallback;
        }

        public override void Await()
        {
            MessageBox.Display(text);
            if (extraCallback == null)
            {
                MessageBox.WaitForInput(AllowNextEvent);
            }
            else
            {
                MessageBox.WaitForInput(() =>
                {
                    extraCallback();
                    AllowNextEvent();
                });
            }
        }
    }
}