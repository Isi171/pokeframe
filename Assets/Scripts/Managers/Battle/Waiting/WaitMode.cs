using System;
using View.Battle;

namespace Managers.Battle.Waiting
{
    public abstract class WaitMode
    {
        public MessageBoxController MessageBox { protected get; set; }
        public Action AllowNextEvent { protected get; set; }
        public Action<float> Wait { protected get; set; }

        public abstract void Await();
    }
}