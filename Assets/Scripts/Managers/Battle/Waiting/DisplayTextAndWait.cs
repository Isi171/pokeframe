namespace Managers.Battle.Waiting
{
    public class DisplayTextAndWait : WaitMode
    {
        private readonly string text;

        public DisplayTextAndWait(string text)
        {
            this.text = text;
        }

        public override void Await()
        {
            MessageBox.Display(text);
            Wait(2f);
        }
    }
}