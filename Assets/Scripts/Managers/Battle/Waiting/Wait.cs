namespace Managers.Battle.Waiting
{
    public class Wait : WaitMode
    {
        private readonly float seconds;

        public Wait(float seconds = 2f)
        {
            this.seconds = seconds;
        }

        public override void Await()
        {
            Wait(seconds);
        }
    }
}