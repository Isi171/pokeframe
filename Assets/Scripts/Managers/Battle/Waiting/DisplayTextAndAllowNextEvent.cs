namespace Managers.Battle.Waiting
{
    public class DisplayTextAndAllowNextEvent : WaitMode
    {
        private readonly string text;

        public DisplayTextAndAllowNextEvent(string text)
        {
            this.text = text;
        }

        public override void Await()
        {
            MessageBox.Display(text);
            AllowNextEvent();
        }
    }
}