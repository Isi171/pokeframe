using System.Linq;
using Managers.Data;
using Models.Items;
using Models.Pokemon;
using UnityEngine;
using Utils;
using View.Battle;

namespace Managers.Battle
{
    public abstract class DecisionModule : MonoBehaviour
    {
        [SerializeField] private BattlerController self;
        [SerializeField] private BattlerController enemy;
        [SerializeField] private BattlerController enemy2;
        [SerializeField] private BattlerController ally;
        public BattlerController Self => self;
        public BattlerController Enemy => enemy;

        public BattlerController Ally
        {
            get => ally;
            set => ally = value;
        }

        public BattlerController Enemy2
        {
            get => enemy2;
            set => enemy2 = value;
        }

        public bool HasActed { get; set; }
        public Move SelectedMove { get; protected set; }
        public BattlerController Target { get; protected set; }
        public Item Item { get; protected set; }
        public Pokemon ItemTarget { get; protected set; }
        public Pokemon Switch { get; protected set; }
        public bool Ready { get; protected set; }
        public bool ShouldSubstitute { get; set; }
        public Pokemon Substitution { get; protected set; }
        public bool SubstitutionReady { get; protected set; }
        public bool CannotSubstitute => Substitution == null && SubstitutionReady;
        public bool Forfeit { get; protected set; }

        public void ResetDecision()
        {
            HasActed = false;
            SelectedMove = null;
            Target = null;
            Item = null;
            ItemTarget = null;
            Switch = null;
            Forfeit = false;
            Ready = false;
        }

        public void ResetSubstitution()
        {
            ShouldSubstitute = false;
            Substitution = null;
            SubstitutionReady = false;
        }

        public bool DidIRunOutOfSubstitutions()
        {
            return Ally != null
                   && Self.Trainer == Ally.Trainer
                   && Self.Trainer.Party.CountAvailable == 1
                   && (
                       Ally.Pokemon == Self.Trainer.Party.FirstAvailable()
                       || (Ally.DecisionModule.SubstitutionReady && Ally.DecisionModule.Substitution != null)
                   );
        }

        public abstract void Decide();
        public abstract void Substitute();

        public bool IsStruggling()
        {
            return Self.Pokemon.Moves.All(slot => !slot.IsAssigned())
                   || Self.Pokemon.Moves.Where(slot => slot.IsAssigned()).All(slot => slot.CurrentPp == 0);
        }

        public virtual void SelectStruggle()
        {
            SelectedMove = DataAccess.Moves.Get("Struggle");
            Target = RandomEnemy();
            Ready = true;
        }

        protected BattlerController RandomEnemy()
        {
            return RandomUtils.ChanceOneInMany(2)
                ? Enemy
                : Enemy2
                    ? Enemy2
                    : Enemy;
        }
    }
}