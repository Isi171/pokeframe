using System;
using Managers.Shared;
using Models;
using Models.Abilities;
using Models.Items;
using Models.Pokemon;
using Models.Trainer;
using View.Battle;

namespace Managers.Battle
{
    public class BattleStrings : SharedStrings
    {
        public BattlerController Player { private get; set; }
        public BattlerController Player2 { private get; set; }
        public BattlerController Enemy { private get; set; }
        public BattlerController Enemy2 { private get; set; }
        public BattlerController Ally { private get; set; }
        public bool TrainerBattle { private get; set; }

        private string QualifiedName(Pokemon pokemon, bool lowercase = false)
        {
            if (pokemon == Player.Pokemon || pokemon == (Player2 != null ? Player2.Pokemon : null))
            {
                return pokemon.Name;
            }

            if (pokemon == Enemy.Pokemon || pokemon == Enemy2.Pokemon)
            {
                string adjective = TrainerBattle ? "opposing" : "wild";
                return lowercase ? $"the {adjective} {pokemon.Name}" : $"The {adjective} {pokemon.Name}";
            }

            return lowercase ? $"the allied {pokemon.Name}" : $"The allied {pokemon.Name}";
        }

        private string QualifiedTeam(Pokemon pokemon, bool lowercase = false)
        {
            if (pokemon == Enemy.Pokemon || pokemon == Enemy2.Pokemon)
            {
                return lowercase ? "the opposite team" : "The opposite team";
            }

            return lowercase ? "your team" : "Your team";
        }

        public string BattleStart(Trainer trainer, Trainer trainer2, Trainer ally, BattleType type)
        {
            if (TrainerBattle)
            {
                string extraTrainer = trainer2 != null
                    ? $" and {trainer2.Class} {trainer2.Name}"
                    : "";
                string andAlly = ally != null
                    ? $" and {ally.Class} {ally.Name}"
                    : "";

                return
                    $"You{andAlly} are challenged by {trainer.Class} {trainer.Name}{extraTrainer} to a {(type == BattleType.Single ? "single" : "double")} battle!";
            }

            return $"A wild {trainer.Party.FirstAvailable().Species} appeared!";
        }

        public string SwitchIn(Trainer trainer, Pokemon pokemon)
        {
            if (trainer == Player.Trainer)
            {
                return $"Go! {pokemon.Name}!";
            }

            if (trainer == Enemy.Trainer || trainer == Enemy2.Trainer)
            {
                return $"{trainer.Class} {trainer.Name} sent out {pokemon.Name}!";
            }

            return $"Your ally {trainer.Class} {trainer.Name} sent out {pokemon.Name}!";
        }

        public string SwitchOut(Trainer trainer, Pokemon pokemon)
        {
            if (trainer == Player.Trainer)
            {
                return $"{pokemon.Name}, that's enough, come back!";
            }

            if (trainer == Enemy.Trainer || trainer == Enemy2.Trainer)
            {
                return $"{trainer.Class} {trainer.Name} withdrew {pokemon.Name}!";
            }

            return $"Your ally {trainer.Class} {trainer.Name} withdrew {pokemon.Name}!";
        }

        public string StatusWasApplied(Pokemon pokemon, Status status)
        {
            return status switch
            {
                Status.Burned => $"{QualifiedName(pokemon)} was burned!",
                Status.Frozen => $"{QualifiedName(pokemon)} was frozen solid!",
                Status.Paralyzed => $"{QualifiedName(pokemon)} was paralyzed! {It(pokemon)} might be unable to move!",
                Status.Poisoned => $"{QualifiedName(pokemon)} was poisoned!",
                Status.BadlyPoisoned => $"{QualifiedName(pokemon)} was badly poisoned!",
                Status.Asleep => $"{QualifiedName(pokemon)} fell asleep!",
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        public string StatusWasHealed(Pokemon pokemon, Status status)
        {
            return status switch
            {
                Status.Frozen => $"{QualifiedName(pokemon)} was thawed out!",
                Status.Asleep => $"{QualifiedName(pokemon)} woke up!",
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        public string VolatileStatusWasApplied(Pokemon pokemon, VolatileStatus status)
        {
            return status switch
            {
                VolatileStatus.Confusion => $"{QualifiedName(pokemon)} was confused!",
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        public string VolatileStatusWasHealed(Pokemon pokemon, VolatileStatus status)
        {
            return status switch
            {
                VolatileStatus.Confusion => $"{QualifiedName(pokemon)} snapped out of confusion!",
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        public string VolatileStatusIsPresent(Pokemon pokemon, VolatileStatus status)
        {
            return status switch
            {
                VolatileStatus.Confusion => $"{QualifiedName(pokemon)} is confused!",
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        public string LostTurnDueToVolatileStatus(Pokemon pokemon, VolatileStatus status)
        {
            return status switch
            {
                VolatileStatus.Flinch => $"{QualifiedName(pokemon)} flinched and couldn't move!",
                VolatileStatus.Confusion =>
                    $"{It(pokemon)} hurt {Itself(pokemon, true)} in {Its(pokemon, true)} confusion!",
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        public string LostTurnDueToStatus(Pokemon pokemon, Status status)
        {
            return status switch
            {
                Status.Frozen => $"{QualifiedName(pokemon)} is frozen solid!",
                Status.Paralyzed => $"{QualifiedName(pokemon)} is paralyzed! {It(pokemon)} can't move!",
                Status.Asleep => $"{QualifiedName(pokemon)} is fast asleep!",
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        public string DamagedByStatus(Pokemon pokemon, Status status)
        {
            return status switch
            {
                Status.Burned => $"{QualifiedName(pokemon)} is hurt by {Its(pokemon, true)} burn!",
                Status.Poisoned => $"{QualifiedName(pokemon)} is hurt by poison!",
                Status.BadlyPoisoned => $"{QualifiedName(pokemon)} is hurt by poison!",
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        public string MultiStrike(int times)
        {
            return $"Hit {times} time{(times == 1 ? "" : "s")}!";
        }

        public string StatWontChange(Pokemon pokemon, StatName statName, int stage)
        {
            return $"{QualifiedName(pokemon)}'s {Stat(statName)} won't go any {(stage > 0 ? "higher" : "lower")}!";
        }

        public string StatChanged(Pokemon pokemon, StatName statName, int stage)
        {
            return string.Format("{0}'s {1} {2}{3}!",
                QualifiedName(pokemon),
                Stat(statName),
                stage > 0 ? "rose" : "fell",
                Math.Abs(stage) == 1 ? "" : Math.Abs(stage) == 2 ? " sharply" : " drastically");
        }

        public string WeatherStart(Weather weather)
        {
            return weather switch
            {
                Weather.Rain => "It started to rain!",
                Weather.Sun => "The sunlight turned harsh!",
                Weather.Hail => "It started to hail!",
                Weather.Sandstorm => "A sandstorm kicked up!",
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        public string WeatherEnd(Weather weather)
        {
            return weather switch
            {
                Weather.Rain => "The rain stopped.",
                Weather.Sun => "The sunlight returned to normal.",
                Weather.Hail => "The hail stopped.",
                Weather.Sandstorm => "The sandstorm subsided.",
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        public string WeatherDamage(Weather weather, Pokemon pokemon)
        {
            return weather switch
            {
                Weather.Sun => "" // TODO Dry Skin TODO Solar Power
                ,
                Weather.Hail => $"{QualifiedName(pokemon)} is buffeted by the hail!",
                Weather.Sandstorm => $"{QualifiedName(pokemon)} is buffeted by the sandstorm!",
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        public string WeatherHeal(Weather weather, Pokemon pokemon)
        {
            return weather switch
            {
                Weather.Rain => "" // TODO Rain Dish TODO Dry Skin
                ,
                Weather.Hail => "" // TODO Ice Body
                ,
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        public string HeldItemDamage(Pokemon pokemon, Item item)
        {
            return $"{QualifiedName(pokemon)} was damaged by {Its(pokemon, true)} {item.Name}!";
        }

        public string HeldItemHealHealth(Pokemon pokemon, Item item)
        {
            return $"{QualifiedName(pokemon)} restored HP using {Its(pokemon, true)} {item.Name}!";
        }

        public string Immune(Pokemon pokemon)
        {
            return $"It doesn't affect {QualifiedName(pokemon, true)}...";
        }

        public string Miss(Pokemon pokemon)
        {
            return $"{QualifiedName(pokemon)} avoided the attack!";
        }

        public string Fail()
        {
            return "But it failed!";
        }

        public string NoTarget()
        {
            return "But there was no target...";
        }

        public string MoveAnnouncement(Pokemon pokemon, Move move)
        {
            return $"{QualifiedName(pokemon)} used {move.Name}!";
        }

        public string Critical()
        {
            return "A critical hit!";
        }

        public string Effectiveness(TypeEffectiveness effectiveness)
        {
            return effectiveness == TypeEffectiveness.SuperEffective
                   || effectiveness == TypeEffectiveness.QuadrupleEffective
                ? "It's super effective!"
                : "It's not very effective...";
        }

        public string Leech(Pokemon pokemon)
        {
            return $"{QualifiedName(pokemon)} had its energy drained!";
        }

        public string Recoil(Pokemon pokemon)
        {
            return $"{QualifiedName(pokemon)} was hit with recoil!";
        }

        public string Faint(Pokemon pokemon)
        {
            return $"{QualifiedName(pokemon)} fainted!";
        }

        public string Victory(Trainer trainer, Trainer trainer2, Trainer ally)
        {
            string extra = trainer2 != null
                ? $" and {trainer2.Class} {trainer2.Name}"
                : "";
            string andAlly = ally != null
                ? $" and {ally.Class} {ally.Name}"
                : "";

            return $"You{andAlly} won against {trainer.Class} {trainer.Name}{extra}!";
        }

        public string Loss(Trainer trainer, Trainer trainer2, Trainer ally, bool forfeit)
        {
            if (!TrainerBattle)
            {
                return "You are out of usable Pokémon!\nYou panicked and ran away!";
            }

            string extra = trainer2 != null
                ? $" and {trainer2.Class} {trainer2.Name}"
                : "";
            string andAlly = ally != null
                ? $" and {ally.Class} {ally.Name}"
                : "";

            string beginning = forfeit ? "You forfeited the match!" : $"You{andAlly} are out of usable Pokémon!";

            return
                $"{beginning}\nYou{andAlly} lost against {trainer.Class} {trainer.Name}{extra}!";
        }

        public string MovePrompt(Pokemon pokemon)
        {
            return $"What will {pokemon.Name} do?";
        }

        public string UseItem(Trainer trainer, Pokemon pokemon, Item item)
        {
            if (item.Category == ItemCategory.PokeBall)
            {
                return $"You threw one {item.Name}!";
            }

            if (trainer == Player.Trainer)
            {
                return $"You used the {item.Name} on {pokemon.Name}!";
            }

            if (trainer == Enemy.Trainer || trainer == Enemy2.Trainer)
            {
                return $"{trainer.Name} used the {item.Name} on {pokemon.Name}!";
            }

            return $"Your ally {trainer.Name} used the {item.Name} on {pokemon.Name}!";
        }

        public string ExpGain(Pokemon pokemon, int experience)
        {
            return ExpGainInternal(pokemon, experience);
        }

        public string LevelUp(Pokemon pokemon)
        {
            return LevelUpInternal(pokemon);
        }

        public string GotItemsVictory(LootTable loot)
        {
            return $"You got {DisplayLoot(loot)} from winning!";
        }

        public string BallCapture(Pokemon pokemon)
        {
            return $"Gotcha! {pokemon.Name} was caught!";
        }

        public string SentToBox(Pokemon pokemon)
        {
            return SentToBoxInternal(pokemon);
        }

        public string BallBreak(int check)
        {
            return check switch
            {
                0 => "Oh no! The Pokémon broke free!",
                1 => "Aww! It appeared to be caught!",
                2 => "Aargh! Almost had it!",
                _ => "Gah! It was so close, too!"
            };
        }

        public string BallBlocked()
        {
            return "The trainer blocked the Ball!";
        }

        public string PlayerRunAwayWildBattle(bool success)
        {
            return success
                ? "Got away safely!"
                : "Can't escape!";
        }

        public string PokemonRunAwayWildBattle(Pokemon pokemon)
        {
            return $"{QualifiedName(pokemon)} flew!";
        }

        public string WantsToLearnMove(Pokemon pokemon, string move)
        {
            return WantsToLearnMoveInternal(pokemon, move);
        }

        public string MoveLearned(Pokemon pokemon, string move)
        {
            return MoveLearnedInternal(pokemon, move);
        }

        public string AbilityTriggered(Ability ability, Pokemon user, Pokemon target, Pokemon target2)
        {
            return ability.Name switch
            {
                "Regenerator" => $"{QualifiedName(user)}'s {ability.Name} restored some HP!",
                "Natural Cure" => $"{QualifiedName(user)}'s {ability.Name} healed {Its(user, true)} status problem!",
                "Drizzle" => $"{QualifiedName(user)}'s {ability.Name} made it rain!",
                "Drought" => $"{QualifiedName(user)}'s {ability.Name} intensified the sun's rays!",
                "Sand Stream" => $"{QualifiedName(user)}'s {ability.Name} whipped up a sandstorm!",
                "Snow Warning" => $"{QualifiedName(user)}'s {ability.Name} made it hail!",
                "Mold Breaker" => $"{QualifiedName(user)} breaks the mold!",
                "Teravolt" => $"{QualifiedName(user)} is radiating a bursting aura!",
                "Turboblaze" => $"{QualifiedName(user)} is radiating a blazing aura!",
                "Intimidate" => $"{QualifiedName(user)}'s {ability.Name} cuts {QualifiedName(target, true)}'s Attack!",
                "Poison Point" => $"{QualifiedName(user)}'s {ability.Name} poisoned {QualifiedName(target, true)}!",
                "Static" => $"{QualifiedName(user)}'s {ability.Name} paralyzed {QualifiedName(target, true)}!",
                "Flame Body" => $"{QualifiedName(user)}'s {ability.Name} burned {QualifiedName(target, true)}!",
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        public string Struggle(Pokemon user)
        {
            return $"{QualifiedName(user)} has no moves left!";
        }

        public string SpecialMoveEffect(Move move, Pokemon user, Pokemon target, string data, int value)
        {
            return move.SpecialEffect switch
            {
                SpecialEffect.Splash => "But nothing happened!",
                SpecialEffect.OneHitKo => "It's a one-hit KO!",
                SpecialEffect.CrashDamageOnMiss => $"{QualifiedName(user)} kept going and crashed!",
                SpecialEffect.Trap => $"{QualifiedName(target)} can't escape now!",
                SpecialEffect.TrapAndDamage => move.Name switch
                {
                    "Bind" => $"{QualifiedName(target)} was squeezed by {QualifiedName(user, true)}!",
                    "Wrap" => $"{QualifiedName(target)} was wrapped by {QualifiedName(user, true)}!",
                    "Clamp" => $"{QualifiedName(user)} clamped {QualifiedName(target, true)}!",
                    "Fire Spin" => $"{QualifiedName(target)} became trapped in the vortex!",
                    "Whirlpool" => $"{QualifiedName(target)} became trapped in the vortex!",
                    "Sand Tomb" => $"{QualifiedName(target)} became trapped by Sand Tomb!",
                    _ => throw new ArgumentOutOfRangeException(nameof(move.SpecialEffect), move.SpecialEffect,
                        move.Name)
                },
                SpecialEffect.Reflect => $"Reflect raised {QualifiedTeam(user, true)}'s Defense!",
                SpecialEffect.LightScreen => $"Light Screen raised {QualifiedTeam(user, true)}'s Special Defense!",
                SpecialEffect.PayDay => "Coins scattered everywhere!",
                SpecialEffect.RequiresRecharge => $"{QualifiedName(user)} must recharge!",
                SpecialEffect.ChargeTurn => move.Name == "Sky Attack"
                    ? $"{QualifiedName(user)} became cloaked in a harsh light!"
                    : $"{QualifiedName(user)} whipped up a whirlwind!",
                SpecialEffect.ChargeTurnSunny => $"{QualifiedName(user)} absorbed light!",
                SpecialEffect.ChargeTurnUpDefense => $"{QualifiedName(user)} tucked in {Its(user, true)} head!",
                SpecialEffect.ChargeTurnSemiInvulnerable => move.Name switch
                {
                    "Dig" => $"{QualifiedName(user)} burrowed {Its(user, true)} way under the ground!",
                    "Dive" => $"{QualifiedName(user)} hid underwater!",
                    "Fly" => $"{QualifiedName(user)} flew up high!",
                    "Bounce" => $"{QualifiedName(user)} sprang up!",
                    _ => throw new ArgumentOutOfRangeException(nameof(move.SpecialEffect), move.SpecialEffect,
                        move.Name)
                },
                SpecialEffect.Substitute => $"{QualifiedName(user)} put in a substitute!",
                SpecialEffect.Disable => $"{QualifiedName(target)}'s {data} was disabled!",
                SpecialEffect.Mist => $"{QualifiedTeam(user)} became shrouded in mist!",
                SpecialEffect.Haze => "All stat changes were eliminated!",
                SpecialEffect.Mimic => $"{QualifiedName(user)} learned {data}!",
                SpecialEffect.LeechSeed => $"{QualifiedName(target)} was seeded!",
                SpecialEffect.Rage => $"{QualifiedName(user)}'s Rage grows!",
                SpecialEffect.Bide => $"{QualifiedName(user)} is storing energy!",
                SpecialEffect.FocusEnergy => $"{QualifiedName(user)} is getting pumped!",
                SpecialEffect.Transform => $"{QualifiedName(user)} transformed into {QualifiedName(target, true)}!",
                SpecialEffect.Rest => $"{QualifiedName(user)} slept and became healthy!",
                SpecialEffect.SwitchOutUser => move.Name == "Teleport"
                    ? $"{QualifiedName(user)} fled from battle!"
                    : $"{QualifiedName(user)} went back to {data}!",
                SpecialEffect.Healing => $"{QualifiedName(target)}'s HP was restored!",
                SpecialEffect.HealingSunny => $"{QualifiedName(target)}'s HP was restored!",
                SpecialEffect.Sketch => $"{QualifiedName(user)} learned {data}!",
                SpecialEffect.StealsHeldItem => $"{QualifiedName(user)} stole {QualifiedName(target, true)}'s {data}!",
                SpecialEffect.NextMoveWillHit => $"{QualifiedName(user)} took aim at {QualifiedName(target, true)}!",
                SpecialEffect.Safeguard => $"{QualifiedTeam(user)} became cloaked in a mystical veil!",
                SpecialEffect.Curse =>
                    $"{QualifiedName(user)} cut {Its(user, true)} own HP and laid a curse on {QualifiedName(target, true)}!",
                SpecialEffect.Conversion => $"{QualifiedTeam(user)} transformed into the {data} type!",
                SpecialEffect.Conversion2 => $"{QualifiedTeam(user)} transformed into the {data} type!",
                SpecialEffect.Camouflage => $"{QualifiedTeam(user)} transformed into the {data} type!",
                SpecialEffect.Spite => $"It reduced {QualifiedName(target)}'s {data} PP by {value}!",
                SpecialEffect.BellyDrum =>
                    $"{QualifiedName(user)} cut {Its(user, true)} HP and maximized {Its(user, true)} attack!",
                SpecialEffect.Spikes => $"Spikes were scattered all around the feet of {QualifiedTeam(user, true)}!",
                SpecialEffect.ForesightLike => $"{QualifiedName(target)} was identified!",
                SpecialEffect.PerishSong => "All Pokémon hearing the song will faint in three turns!",
                SpecialEffect.DestinyBond =>
                    $"{QualifiedName(user)} is trying to take {Its(user, true)} foe with {ItPronoun(user, true)}!",
                SpecialEffect.Endure => $"{QualifiedName(user)} braced {Itself(user, true)}!",
                SpecialEffect.HealPartyStatus => move.Name == "Heal Bell"
                    ? "A bell chimed!"
                    : "A soothing aroma wafted through the area!",
                SpecialEffect.PainSplit => "The battlers shared their pain!",
                SpecialEffect.Magnitude => $"Magnitude {value}",
                SpecialEffect.Encore => $"{QualifiedName(target)} received an encore!",
                SpecialEffect.PsychUp => $"{QualifiedName(user)} copied {QualifiedName(target, true)} stat changes!",
                SpecialEffect.FutureAttack => move.Name == "Future Sight"
                    ? $"{QualifiedName(user)} foresaw an attack!"
                    : $"{QualifiedName(user)} choose Doom Desire as {Its(user, true)} destiny!",
                SpecialEffect.Uproar => $"{QualifiedName(user)} caused an uproar!",
                SpecialEffect.Stockpile => $"{QualifiedName(user)} stockpiled {value}!",
                SpecialEffect.FocusPunch => $"{QualifiedName(user)} is tightening {Its(user, true)} focus!",
                SpecialEffect.SmellingSalts => $"{QualifiedName(target)} was cured of paralysis!",
                SpecialEffect.FollowMe => $"{QualifiedName(user)} became the center of attention!",
                SpecialEffect.NaturePower => $"Nature Power turned into {data}!",
                SpecialEffect.Charge => $"{QualifiedName(user)} began charging power!",
                SpecialEffect.HelpingHand => $"{QualifiedName(user)} is ready to help {QualifiedName(target, true)}!",
                SpecialEffect.Trick => $"{QualifiedName(user)} switched items with {QualifiedName(target, true)}!",
                SpecialEffect.RolePlay => $"{QualifiedName(user)} copied {QualifiedName(target, true)}'s {data}!",
                SpecialEffect.Ingrain => $"{QualifiedName(user)} planted {Its(user, true)} roots!",
                SpecialEffect.MagicCoat => $"{QualifiedName(user)} shrouded {Itself(user, true)} with Magic Coat!",
                SpecialEffect.Recycle => $"{QualifiedName(user)} found one {data}!",
                SpecialEffect.KnockOff => $"{QualifiedName(user)} knocked off {QualifiedName(target, true)}'s {data}!",
                SpecialEffect.SkillSwap =>
                    $"{QualifiedName(user)} swapped abilities with {QualifiedName(target, true)}!",
                SpecialEffect.Imprison => $"{QualifiedName(user)} sealed the opponents moves!",
                SpecialEffect.Refresh => $"{QualifiedName(user)}'s status returned to normal!",
                SpecialEffect.Grudge => $"{QualifiedName(user)} wants {QualifiedName(target, true)} to bear a grudge!",
                SpecialEffect.Snatch => $"{QualifiedName(user)} waits for a target to make a move!",
                SpecialEffect.MudSport => "Electricity's power was weakened!",
                SpecialEffect.WaterSport => "Fire's power was weakened!",
                // TODO Gen 4 moves
                SpecialEffect.HealingRoost => $"{QualifiedName(target)}'s HP was restored!",
                _ => throw new ArgumentOutOfRangeException(nameof(move.SpecialEffect), move.SpecialEffect, null)
            };
        }
    }
}