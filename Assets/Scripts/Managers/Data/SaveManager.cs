using System.IO;
using Daybreak.Log;
using Models.Ext.Save;
using Models.State;
using Newtonsoft.Json;
using UnityEngine;

namespace Managers.Data
{
    public static class SaveManager
    {
        private static readonly string Path = $"{Application.persistentDataPath}/save.json";

        public static void Save(GlobalState state, AbsolutePosition playerPosition)
        {
            StateExt ext = DataConverter.Convert(state, playerPosition);
            string data = JsonConvert.SerializeObject(ext, Formatting.Indented,
                new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore });

            File.WriteAllText(Path, data);
            Log.Trace($"Saved to {Path}");
        }

        public static AbsolutePosition Load()
        {
            string data = File.ReadAllText(Path);
            StateExt ext = JsonConvert.DeserializeObject<StateExt>(data);

            AbsolutePosition currentPosition = DataConverter.Convert(ext.CurrentPosition);
            GlobalState.Instance.Options = DataConverter.Convert(ext.Options);
            GlobalState.Instance.PlayerTrainer = DataConverter.Convert(ext.Player);
            GlobalState.Instance.Journal = DataConverter.Convert(ext.Journal);
            GlobalState.Instance.Variables = DataConverter.Convert(ext.Variables);
            GlobalState.Instance.LastSafeSpot = DataConverter.Convert(ext.LastSafeSpot);
            GlobalState.Instance.CurrentWorldSceneName = currentPosition.Map;
            GlobalState.Instance.WorldScenePersistentStates = DataConverter.Convert(ext.MapStates);

            return currentPosition;
        }

        public static bool SaveExists()
        {
            return File.Exists(Path);
        }

        public static void DeleteSave()
        {
            File.Delete(Path);
        }
    }
}