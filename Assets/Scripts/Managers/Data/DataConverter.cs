using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Models;
using Models.Abilities;
using Models.Battle;
using Models.Dialogue;
using Models.Ext;
using Models.Ext.Save;
using Models.Items;
using Models.Pokemon;
using Models.Quests;
using Models.State;
using Models.Trainer;
using Models.World;
using UnityEngine;
using Type = Models.Type;

namespace Managers.Data
{
    public static class DataConverter
    {
        private static readonly Regex ConditionRegex = new Regex("\\s*(\\w+)\\s*([><=]+)\\s*(\\d+)");
        private static readonly Regex VariableChangeRegex = new Regex("\\s*(\\w+)\\s*([+-=:]+)\\s*(\\d+)");

        private static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        private static int Clamp(int value, int min, int max)
        {
            return Math.Max(min, Math.Min(max, value));
        }

        public static Trainer Convert(string key, TrainerExt ext)
        {
            Trainer trainer = new Trainer { Name = ext.Name, Class = ext.Class, Sprite = ext.Sprite };

            foreach (PokemonExt pokemonExt in ext.Party)
            {
                trainer.AddNew(Convert(pokemonExt.Species, pokemonExt));
            }

            if (ext.Bag != null)
            {
                foreach (KeyValuePair<string, int> itemAndQuantity in ext.Bag)
                {
                    trainer.Bag.AddOrRemove(itemAndQuantity.Key, itemAndQuantity.Value);
                }
            }

            return trainer;
        }

        public static Pokemon Convert(string key, PokemonExt ext)
        {
            Pokemon pokemon = Pokemon.Get(ext.Species, Clamp(ext.Level, 1, 100));

            if (ext.Moves != null)
            {
                for (int i = 0; i < ext.Moves.Length && i < 4; i++)
                {
                    pokemon.Moves[i] = new MoveSlot(ext.Moves[i]);
                }
            }
            else
            {
                pokemon.SetMovesByLevel();
            }

            if (ext.Item != null)
            {
                pokemon.HeldItem.Item = DataAccess.Items.Get(ext.Item);
            }

            if (ext.Ability != null)
            {
                pokemon.Ability = DataAccess.Abilities.Get(ext.Ability);
            }

            if (ext.Nature != null)
            {
                pokemon.Nature = Nature.ForName(ext.Nature);
            }

            if (ext.Ivs != null && ext.Ivs.Length > 0)
            {
                pokemon.SetIvs(ext.Ivs[0], ext.Ivs[1], ext.Ivs[2], ext.Ivs[3], ext.Ivs[4], ext.Ivs[5]);
            }

            if (ext.Evs != null && ext.Evs.Length > 0)
            {
                pokemon.SetEvs(ext.Evs[0], ext.Evs[1], ext.Evs[2], ext.Evs[3], ext.Evs[4], ext.Evs[5]);
            }

            pokemon.CalculateDerivedStats();
            pokemon.CurrentHp = pokemon.Hp.Derived;

            if (ext.Gender != null)
            {
                pokemon.Gender = ParseEnum<Gender>(ext.Gender);
            }

            // 0 is a default value, so to set happiness to actual 0 we require it to be a negative number
            if (ext.Happiness > 0)
            {
                pokemon.Happiness = Math.Min(ext.Happiness, 255);
            }
            else
            {
                pokemon.Happiness = 0;
            }

            if (ext.Name != null)
            {
                pokemon.Name = ext.Name;
            }

            pokemon.Shiny = ext.Shiny;

            if (ext.Ball != null)
            {
                pokemon.Ball = ext.Ball;
            }

            return pokemon;
        }

        public static LootTable Convert(string key, LootTableExt ext)
        {
            LootTable loot = new LootTable { Money = ext.Money };
            if (ext.Items != null)
            {
                foreach (string item in ext.Items.Keys)
                {
                    loot.Items.Add(item, ext.Items[item]);
                }
            }

            return loot;
        }

        public static Learnset Convert(string key, LearnsetExt ext)
        {
            return new Learnset
            {
                Level = (
                    from level in ext.Level.Keys
                    from move in ext.Level[level]
                    select new MoveAtLevel { Level = level, Move = move }
                ).ToArray(),
                Egg = ext.Egg,
                Machine = ext.Machine,
                Tutor = ext.Tutor,
                Evolution = ext.Evolution
            };
        }

        public static PokemonBaseData Convert(string key, PokemonBaseDataExt ext)
        {
            return new PokemonBaseData
            {
                NationalDexNumber = ext.NationalDexNumber,
                MaleRatio = ext.MaleRatio,
                Type1 = ParseEnum<Type>(ext.Type1),
                Type2 = ParseEnum<Type>(ext.Type2),
                Ability1 = ext.Ability1,
                Ability2 = ext.Ability2,
                HiddenAbility = ext.HiddenAbility,
                Hp = ext.Hp,
                Attack = ext.Attack,
                Defense = ext.Defense,
                SpecialAttack = ext.SpecialAttack,
                SpecialDefense = ext.SpecialDefense,
                Speed = ext.Speed,
                EggGroup1 = ParseEnum<EggGroup>(ext.EggGroup1),
                EggGroup2 = ParseEnum<EggGroup>(ext.EggGroup2),
                HatchCounter = ext.HatchCounter,
                StepsToHatch = ext.StepsToHatch,
                Effort = ext.Effort,
                Evolutions = ext.Evolutions?.Select(Convert).ToArray() ?? Array.Empty<Evolution>(),
                ExpGroup = ParseEnum<ExpGroup>(ext.ExpGroup),
                BaseExperience = ext.BaseExperience,
                CaptureRate = ext.CaptureRate,
                BaseHappiness = ext.BaseHappiness,
                Height = ext.Height,
                Weight = ext.Weight,
                Genus = ext.Genus,
                Color = ext.Color,
                Habitat = ext.Habitat,
                Shape = ext.Shape
            };
        }

        public static Evolution Convert(EvolutionExt ext)
        {
            return new Evolution
            {
                EvolutionMethod = ParseEnum<EvolutionMethod>(ext.EvolutionMethod),
                Level = ext.Level,
                Item = ext.Item,
                Move = ext.Move,
                Party = ext.Party,
                Species = ext.Species
            };
        }

        public static Ability Convert(string key, AbilityExt ext)
        {
            return new Ability { Name = key, Description = ext.Description };
        }

        public static TrainerBattleReference Convert(string key, TrainerBattleReferenceExt ext)
        {
            return new TrainerBattleReference
            {
                Id = key,
                Type = ext.Type != null ? ParseEnum<BattleType>(ext.Type) : BattleType.Single,
                YieldsExperience = !ext.NoExperience,
                Enemy1 = ext.Enemy1 ?? ext.Enemy1Alt,
                Enemy2 = ext.Enemy2,
                Ally = ext.Ally,
                Prize = ext.Prize != null ? Convert(null, ext.Prize) : null,
                VictoryLine = ext.VictoryLine,
                AfterBattleDialogue = ext.AfterBattleDialogue,
                Music = ext.Music,
                VictoryMusic = ext.VictoryMusic
            };
        }

        public static SpawnTable Convert(string key, SpawnTableEntryExt[] ext)
        {
            return new SpawnTable
            {
                Entries = ext.Select(entry => Convert(null, entry)).ToArray()
            };
        }

        public static SpawnTableEntry Convert(string key, SpawnTableEntryExt ext)
        {
            return new SpawnTableEntry
            {
                Species = ext.Species,
                MaxLevel = ext.MaxLevel,
                MinLevel = ext.MinLevel
            };
        }

        public static Item Convert(string key, ItemExt ext)
        {
            return new Item
            {
                Name = key,
                Category = ParseEnum<ItemCategory>(ext.Category),
                Buy = ext.Buy,
                Sell = ext.Sell,
                Description = ext.Description
            };
        }

        public static GraphRoot Convert(string key, GraphRootEntryExt[] ext)
        {
            return new GraphRoot
            {
                OrderedEntries = ext.Select(entry => Convert(null, entry)).ToArray()
            };
        }

        public static GraphRootEntry Convert(string key, GraphRootEntryExt ext)
        {
            return new GraphRootEntry
            {
                Node = ext.Node,
                ConditionsAnd = ext.ConditionsAnd?.Select(ParseCondition).ToArray()
            };
        }

        public static Condition ParseCondition(string ext)
        {
            Match match = ConditionRegex.Match(ext);
            return new Condition
            {
                Name = match.Groups[1].Value,
                Type = ParseConditionType(match.Groups[2].Value),
                Value = int.Parse(match.Groups[3].Value)
            };
        }

        private static ConditionType ParseConditionType(string comparison)
        {
            return comparison switch
            {
                ">" => ConditionType.GreaterThan,
                ">=" => ConditionType.GreaterThanOrEqualTo,
                "<" => ConditionType.LesserThan,
                "<=" => ConditionType.LesserThanOrEqualTo,
                "=" => ConditionType.EqualTo,
                "==" => ConditionType.EqualTo,
                _ => throw new ArgumentOutOfRangeException(nameof(comparison),
                    $"{comparison} is not in {{>, >=, <, <=, =, ==}}")
            };
        }

        public static GraphNode Convert(string key, GraphNodeExt ext)
        {
            return new GraphNode
            {
                Lines = ext.Lines.Select(entry => Convert(null, entry)).ToArray(),
                Links = ext.Links?.Select(entry => Convert(null, entry)).ToArray()
                        ?? Array.Empty<GraphLink>(),
                VariableChanges = ext.VariableChanges?.Select(ParseVariableChange).ToArray()
                                  ?? Array.Empty<VariableChange>()
            };
        }

        public static Line Convert(string key, LineExt ext)
        {
            return new Line
            {
                Name = ext.Name,
                Portrait = ext.Portrait,
                Text = ext.Text,
                Effect = ext.Effect != null ? ParseEnum<DialogueEffect>(ext.Effect) : DialogueEffect.None,
                EffectData = ext.EffectData
            };
        }

        public static GraphLink Convert(string key, GraphLinkExt ext)
        {
            return new GraphLink
            {
                Line = ext.Line,
                LinksTo = ext.LinksTo,
                ConditionsAnd = ext.ConditionsAnd?.Select(ParseCondition).ToArray()
            };
        }

        private static VariableChange ParseVariableChange(string ext)
        {
            Match match = VariableChangeRegex.Match(ext);
            string rawType = match.Groups[2].Value;
            int value = int.Parse(match.Groups[3].Value);
            return new VariableChange
            {
                Name = match.Groups[1].Value,
                Type = ParseChangeType(rawType),
                Value = rawType == "-" ? -value : value
            };
        }

        private static ChangeType ParseChangeType(string operation)
        {
            return operation switch
            {
                "+" => ChangeType.Mod,
                "-" => ChangeType.Mod,
                "=" => ChangeType.Set,
                ":=" => ChangeType.Set,
                _ => throw new ArgumentOutOfRangeException(nameof(operation),
                    $"{operation} is not in {{+, -, =, :=}}")
            };
        }

        public static Move Convert(string key, MoveExt ext)
        {
            Move move = new Move
            {
                Name = key,
                Index = ext.Index,
                Description = ext.Description,
                Type = ParseEnum<Type>(ext.Type),
                MoveType = ParseEnum<MoveType>(ext.MoveType),
                Pp = ext.Pp,
                Accuracy = ext.Accuracy,
                Target = ParseEnum<MoveTarget>(ext.Target),
                Priority = ext.Priority,
                ModifiedAccuracyInWeather = ext.ModifiedAccuracyInWeather?.Keys.ToDictionary(ParseEnum<Weather>,
                                                innerKey => ext.ModifiedAccuracyInWeather[innerKey])
                                            ?? new Dictionary<Weather, int>(),
                BasePower = ext.BasePower,
                CriticalStage = ext.CriticalStage,
                RecoilDivisor = ext.RecoilDivisor,
                MultiStrike = ext.MultiStrike,
                FixedMultiStrike = ext.FixedMultiStrike,
                Leech = ext.Leech,
                SetDamage = ext.SetDamage,
                BurnChance = ext.BurnChance,
                FreezeChance = ext.FreezeChance,
                ParalysisChance = ext.ParalysisChance,
                PoisonChance = ext.PoisonChance,
                BadPoisonChance = ext.BadPoisonChance,
                SleepChance = ext.SleepChance,
                FlinchChance = ext.FlinchChance,
                ConfusionChance = ext.ConfusionChance,
                UserStatChangesChance = ext.UserStatChangesChance,
                UserStatChanges = ext.UserStatChanges?.Keys.ToDictionary(ParseEnum<StatName>,
                                      innerKey => ext.UserStatChanges[innerKey])
                                  ?? new Dictionary<StatName, int>(),
                TargetStatChangesChance = ext.TargetStatChangesChance,
                TargetStatChanges = ext.TargetStatChanges?.Keys.ToDictionary(ParseEnum<StatName>,
                                        innerKey => ext.TargetStatChanges[innerKey])
                                    ?? new Dictionary<StatName, int>(),
                SetWeather = ext.Weather != null
                    ? ParseEnum<Weather>(ext.Weather)
                    : Weather.Clear,
                SpecialEffect = ext.SpecialEffect != null
                    ? ParseEnum<SpecialEffect>(ext.SpecialEffect)
                    : SpecialEffect.None,
                Flags = ext.Flags?.Select(ParseEnum<MoveFlag>).ToArray() ?? Array.Empty<MoveFlag>()
            };
            return move;
        }

        public static Merchant Convert(string key, MerchantExt ext)
        {
            return new Merchant
            {
                Stock = ext.Stock.Select(item => DataAccess.Items.Get(item)).ToArray()
            };
        }

        public static Quest Convert(string key, QuestExt ext)
        {
            return new Quest
            {
                Name = ext.Name,
                Stages = ext.Stages.ToDictionary(pair => int.Parse(pair.Key), pair => Convert(pair.Value))
            };
        }

        private static Stage Convert(StageExt ext)
        {
            return new Stage
            {
                Description = ext.Description,
                FinishesQuest = ext.FinishesQuest
            };
        }

        public static StateExt Convert(GlobalState state, AbsolutePosition playerPosition)
        {
            return new StateExt
            {
                Options = Convert(state.Options),
                Player = Convert(state.PlayerTrainer),
                LastSafeSpot = Convert(state.LastSafeSpot),
                CurrentPosition = Convert(playerPosition),
                Journal = Convert(state.Journal),
                Variables = Convert(state.Variables),
                MapStates = state.WorldScenePersistentStates.ToDictionary(pair => pair.Key, pair => Convert(pair.Value))
            };
        }

        private static OptionsExt Convert(Options model)
        {
            return new OptionsExt
            {
                MusicVolume = model.MusicVolume,
                EffectsVolume = model.EffectsVolume,
                Timescale = model.Timescale
            };
        }

        private static PlayerTrainerExt Convert(Trainer model)
        {
            return new PlayerTrainerExt
            {
                Name = model.Name,
                Sprite = model.Sprite,
                Party = model.Party.All.Select(Convert).ToArray(),
                Box = model.Box.All.Select(Convert).ToArray(),
                Bag = model.Bag.All.ToDictionary(slot => slot.Item.Name, slot => slot.Quantity),
                Money = model.Bag.Money
            };
        }

        private static AbsolutePositionExt Convert(AbsolutePosition model)
        {
            return new AbsolutePositionExt
            {
                Map = model.Map,
                Position = new System.Numerics.Vector2(model.Position.x, model.Position.y),
                Direction = Enum.GetName(typeof(Direction), model.Direction)
            };
        }

        private static JournalExt Convert(Journal model)
        {
            return new JournalExt
            {
                Active = model.Active.Select(Convert).ToArray(),
                Complete = model.Complete.Select(Convert).ToArray()
            };
        }

        private static QuestProgressExt Convert(QuestProgress model)
        {
            return new QuestProgressExt
            {
                Quest = model.Quest,
                Stage = model.Stage
            };
        }

        private static Dictionary<string, int> Convert(Variables model)
        {
            return model.AllKeys.ToDictionary(key => key, model.Get);
        }

        private static WorldScenePersistentStateExt Convert(WorldScenePersistentState model)
        {
            return new WorldScenePersistentStateExt
            {
                Pickups = model.Pickups.ToDictionary(element => element.Name, element => element.Spent)
            };
        }

        private static PersistedPokemonExt Convert(Pokemon model)
        {
            PersistedPokemonExt ext = new PersistedPokemonExt
            {
                Species = model.Species,
                Level = model.Level,
                ExperienceInExcessOfLevel = model.Experience.CurrentInExcessOfLevel,
                Moves = model.Moves.Where(slot => slot.IsAssigned()).Select(Convert).ToArray(),
                Item = model.HeldItem.Item?.Name,
                Ability = model.Ability.Name,
                Nature = model.Nature.Name,
                Ivs = new[]
                {
                    model.Hp.Iv, model.Attack.Iv, model.Defense.Iv, model.SpecialAttack.Iv, model.SpecialDefense.Iv,
                    model.Speed.Iv
                },
                Evs = new[]
                {
                    model.Hp.Ev, model.Attack.Ev, model.Defense.Ev, model.SpecialAttack.Ev, model.SpecialDefense.Ev,
                    model.Speed.Ev
                },
                Gender = Enum.GetName(typeof(Gender), model.Gender),
                Happiness = model.Happiness,
                Name = model.Name,
                Shiny = model.Shiny,
                Ball = model.Ball,
                CurrentHp = model.CurrentHp,
                Status = Enum.GetName(typeof(Status), model.Status),
                SleepCounter = model.SleepCounter,
                Ownership = Convert(model.Ownership)
            };
            return ext;
        }

        private static MoveSlotExt Convert(MoveSlot model)
        {
            return new MoveSlotExt
            {
                Move = model.Move.Name,
                CurrentPp = model.CurrentPp
            };
        }

        private static OwnershipExt Convert(Ownership model)
        {
            return new OwnershipExt
            {
                MetLocation = model.MetLocation,
                MetLevel = model.MetLevel,
                OriginalTrainerName = model.OriginalTrainerName,
                OriginalTrainerId = model.OriginalTrainerId
            };
        }

        public static AbsolutePosition Convert(AbsolutePositionExt ext)
        {
            return new AbsolutePosition(ext.Map, Convert(ext.Position), ParseEnum<Direction>(ext.Direction));
        }

        private static Vector2 Convert(System.Numerics.Vector2 ext)
        {
            return new Vector2(ext.X, ext.Y);
        }

        public static Options Convert(OptionsExt ext)
        {
            return new Options
            {
                MusicVolume = Clamp(ext.MusicVolume, Options.MinMusicVolume, Options.MaxMusicVolume),
                EffectsVolume = Clamp(ext.EffectsVolume, Options.MinEffectsVolume, Options.MaxEffectsVolume),
                Timescale = Clamp(ext.Timescale, Options.MinTimescale, Options.MaxTimescale)
            };
        }

        public static Journal Convert(JournalExt ext)
        {
            Journal model = new Journal();

            foreach (QuestProgressExt progressExt in ext.Active)
            {
                model.Active.Add(Convert(progressExt));
            }

            foreach (QuestProgressExt progressExt in ext.Complete)
            {
                model.Complete.Add(Convert(progressExt));
            }

            return model;
        }

        private static QuestProgress Convert(QuestProgressExt ext)
        {
            return new QuestProgress
            {
                Quest = ext.Quest,
                Stage = ext.Stage
            };
        }

        public static Variables Convert(Dictionary<string, int> ext)
        {
            Variables model = new Variables();
            foreach (KeyValuePair<string, int> pair in ext)
            {
                model.Set(pair.Key, pair.Value);
            }

            return model;
        }

        public static Dictionary<string, WorldScenePersistentState> Convert(
            Dictionary<string, WorldScenePersistentStateExt> ext)
        {
            return ext.ToDictionary(pair => pair.Key, pair => Convert(pair.Value));
        }

        private static WorldScenePersistentState Convert(WorldScenePersistentStateExt ext)
        {
            return new WorldScenePersistentState
            {
                Pickups = ext.Pickups.Select(pair => new PickupState { Name = pair.Key, Spent = pair.Value }).ToArray()
            };
        }

        public static Trainer Convert(PlayerTrainerExt ext)
        {
            Trainer model = new Trainer
            {
                Name = ext.Name,
                Class = "Player",
                Sprite = ext.Sprite
            };
            foreach (Pokemon pokemon in ext.Party.Select(Convert))
            {
                model.Party.AddLast(pokemon);
            }

            foreach (Pokemon pokemon in ext.Box.Select(Convert))
            {
                model.Box.Add(pokemon);
            }

            foreach (KeyValuePair<string, int> pair in ext.Bag)
            {
                model.Bag.AddOrRemove(pair.Key, pair.Value);
            }

            model.Bag.Money = ext.Money;

            return model;
        }

        private static Pokemon Convert(PersistedPokemonExt ext)
        {
            Pokemon model = Pokemon.Get(ext.Species, ext.Level);
            model.Experience.TotalExp += ext.ExperienceInExcessOfLevel;
            model.Ability = DataAccess.Abilities.Get(ext.Ability);
            model.Nature = Nature.ForName(ext.Nature);
            model.SetIvs(ext.Ivs[0], ext.Ivs[1], ext.Ivs[2], ext.Ivs[3], ext.Ivs[4], ext.Ivs[5]);
            model.SetEvs(ext.Evs[0], ext.Evs[1], ext.Evs[2], ext.Evs[3], ext.Evs[4], ext.Evs[5]);
            model.CalculateDerivedStats();
            model.CurrentHp = ext.CurrentHp;
            model.Gender = ParseEnum<Gender>(ext.Gender);
            model.Happiness = ext.Happiness;
            model.Name = ext.Name;
            model.Shiny = ext.Shiny;
            model.Ball = ext.Ball;
            model.Status = ParseEnum<Status>(ext.Status);
            model.SleepCounter = ext.SleepCounter;
            model.Ownership.MetLocation = ext.Ownership.MetLocation;
            model.Ownership.MetLevel = ext.Ownership.MetLevel;
            model.Ownership.OriginalTrainerName = ext.Ownership.OriginalTrainerName;
            model.Ownership.OriginalTrainerId = ext.Ownership.OriginalTrainerId;

            for (int i = 0; i < ext.Moves.Length && i < 4; i++)
            {
                model.Moves[i] = Convert(ext.Moves[i]);
            }

            if (ext.Item != null)
            {
                model.HeldItem.Item = DataAccess.Items.Get(ext.Item);
            }

            return model;
        }

        private static MoveSlot Convert(MoveSlotExt ext)
        {
            return new MoveSlot(ext.Move)
            {
                CurrentPp = ext.CurrentPp
            };
        }

        public static NewGame Convert(NewGameExt ext)
        {
            Trainer player = Convert(null, ext.Player);
            player.Bag.Money = ext.Money;
            return new NewGame
            {
                SpawnPoint = Convert(ext.SpawnPoint),
                Player = player
            };
        }
    }
}