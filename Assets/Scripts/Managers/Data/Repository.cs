using System;
using System.Collections.Generic;
using System.Linq;
using Utils;

namespace Managers.Data
{
    public class Repository<T>
    {
        private readonly Dictionary<string, T> table = new Dictionary<string, T>();
        public IEnumerable<string> All => table.Keys.AsEnumerable();
        public string Random => table.ElementAt(RandomUtils.RangeInclusive(0, table.Count - 1)).Key;

        public IEnumerable<string> Filter(Predicate<T> filter) =>
            table.Where(pair => filter(pair.Value)).Select(pair => pair.Key);

        public T Get(string key)
        {
            if (table.TryGetValue(key, out T value))
            {
                return value;
            }

            throw new KeyNotFoundException($"The {typeof(T).Name} {key} does not exist.");
        }

        public void Add(string key, T value)
        {
            if (table.ContainsKey(key))
            {
                throw new ArgumentException(
                    $"A {typeof(T).Name} with the same key has already been added. Key: {key}.");
            }

            table.Add(key, value);
        }

        public void Clear() => table.Clear();
    }
}