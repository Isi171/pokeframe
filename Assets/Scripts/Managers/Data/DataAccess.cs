using System;
using System.Collections.Generic;
using Daybreak.Log;
using Models.Abilities;
using Models.Battle;
using Models.Dialogue;
using Models.Ext;
using Models.Ext.Save;
using Models.Items;
using Models.Pokemon;
using Models.Quests;
using Models.State;
using Models.Trainer;
using Models.World;
using Newtonsoft.Json;
using UnityEngine;

namespace Managers.Data
{
    public static class DataAccess
    {
        public static readonly Repository<PokemonBaseData> PokemonBaseData = new Repository<PokemonBaseData>();
        public static readonly Repository<Ability> Abilities = new Repository<Ability>();
        public static readonly Repository<Move> Moves = new Repository<Move>();
        public static readonly Repository<Learnset> Learnsets = new Repository<Learnset>();
        public static readonly Repository<Item> Items = new Repository<Item>();
        public static readonly Repository<Trainer> Trainers = new Repository<Trainer>();
        public static readonly Repository<LootTable> Loot = new Repository<LootTable>();
        public static readonly Repository<Merchant> Merchants = new Repository<Merchant>();
        public static readonly Repository<Pokemon> StaticEncounters = new Repository<Pokemon>();
        public static readonly Repository<SpawnTable> Encounters = new Repository<SpawnTable>();
        public static readonly Repository<TrainerBattleReference> Battles = new Repository<TrainerBattleReference>();
        public static readonly Repository<GraphRoot> DialogueRoots = new Repository<GraphRoot>();
        public static readonly Repository<GraphNode> DialogueNodes = new Repository<GraphNode>();
        public static readonly Repository<Quest> Quests = new Repository<Quest>();

        public static NewGame NewGame { get; private set; }

        public static void LoadAll()
        {
            LoadFromResources(PokemonBaseData, "json/pokemon-base-data/",
                (string key, PokemonBaseDataExt ext) => DataConverter.Convert(key, ext));
            LoadFromResources(Abilities, "json/abilities/",
                (string key, AbilityExt ext) => DataConverter.Convert(key, ext));
            LoadFromResources(Moves, "json/moves/",
                (string key, MoveExt ext) => DataConverter.Convert(key, ext));
            LoadFromResources(Learnsets, "json/learnsets/",
                (string key, LearnsetExt ext) => DataConverter.Convert(key, ext));
            LoadFromResources(Items, "json/items/",
                (string key, ItemExt ext) => DataConverter.Convert(key, ext));
            LoadFromResources(Trainers, "json/trainers/",
                (string key, TrainerExt ext) => DataConverter.Convert(key, ext));
            LoadFromResources(Loot, "json/loot/",
                (string key, LootTableExt ext) => DataConverter.Convert(key, ext));
            LoadFromResources(Merchants, "json/merchants/",
                (string key, MerchantExt ext) => DataConverter.Convert(key, ext));
            LoadFromResources(StaticEncounters, "json/static-encounters/",
                (string key, PokemonExt ext) => DataConverter.Convert(key, ext));
            LoadFromResources(Encounters, "json/encounters/",
                (string key, SpawnTableEntryExt[] ext) => DataConverter.Convert(key, ext));
            LoadFromResources(Battles, "json/battles/",
                (string key, TrainerBattleReferenceExt ext) => DataConverter.Convert(key, ext));
            LoadFromResources(DialogueRoots, "json/dialogue/roots/",
                (string key, GraphRootEntryExt[] ext) => DataConverter.Convert(key, ext));
            LoadFromResources(DialogueNodes, "json/dialogue/nodes/",
                (string key, GraphNodeExt ext) => DataConverter.Convert(key, ext));
            LoadFromResources(Quests, "json/quests/",
                (string key, QuestExt ext) => DataConverter.Convert(key, ext));

            NewGame = LoadNewGameState();
        }

        private static void LoadFromResources<TExt, TInt>(Repository<TInt> repository, string folder,
            Func<string, TExt, TInt> mappingFunction)
        {
            repository.Clear();

            TextAsset[] assets = Resources.LoadAll<TextAsset>(folder);
            foreach (TextAsset asset in assets)
            {
                try
                {
                    Dictionary<string, TExt> ext = JsonConvert.DeserializeObject<Dictionary<string, TExt>>(asset.text);
                    Resources.UnloadAsset(asset);

                    foreach (string key in ext.Keys)
                    {
                        repository.Add(key, mappingFunction(key, ext[key]));
                    }
                }
                catch (Exception e)
                {
                    Log.Error(
                        $"{e.GetType().Name} while loading data in {folder}{asset.name}: {e.Message}");
                    Log.StackTrace(e);
                }
            }
        }

        private static NewGame LoadNewGameState()
        {
            try
            {
                TextAsset asset = Resources.Load<TextAsset>("json/new-game");
                NewGameExt ext = JsonConvert.DeserializeObject<NewGameExt>(asset.text);
                Resources.UnloadAsset(asset);
                return DataConverter.Convert(ext);
            }
            catch (Exception e)
            {
                Log.Error($"{e.GetType().Name} while loading new game state: {e.Message}");
                Log.StackTrace(e);
                return null;
            }
        }

        public static void EDITOR__LoadDataForTools()
        {
            LoadFromResources(PokemonBaseData, "json/pokemon-base-data/",
                (string key, PokemonBaseDataExt ext) => DataConverter.Convert(key, ext));
            LoadFromResources(Abilities, "json/abilities/",
                (string key, AbilityExt ext) => DataConverter.Convert(key, ext));
            LoadFromResources(Moves, "json/moves/",
                (string key, MoveExt ext) => DataConverter.Convert(key, ext));
            LoadFromResources(Learnsets, "json/learnsets/",
                (string key, LearnsetExt ext) => DataConverter.Convert(key, ext));
            LoadFromResources(Items, "json/items/",
                (string key, ItemExt ext) => DataConverter.Convert(key, ext));
        }
    }
}