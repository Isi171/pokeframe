using System;

namespace Managers.Shared
{
    public interface IAnimationDoneSource
    {
        public void SetAnimationDoneCallback(Action animationDoneCallback);
    }
}