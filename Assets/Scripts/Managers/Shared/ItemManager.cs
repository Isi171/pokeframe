using System;
using Models.Items;
using Models.Pokemon;
using Utils;

namespace Managers.Shared
{
    public class ItemManager
    {
        protected int GetHealingMagnitude(Item item, Pokemon target)
        {
            return item.Name switch
            {
                "Potion" => 20,
                "Super Potion" => 50,
                "Hyper Potion" => 120,
                "Max Potion" => target.Hp.Derived,
                "Fresh Water" => 30,
                "Soda Pop" => 50,
                "Lemonade" => 70,
                "Moomoo Milk" => 100,
                "Energy Powder" => 60,
                "Energy Root" => 120,
                "Berry Juice" => 20,
                "Oran Berry" => 10,
                "Sitrus Berry" => CustomMath.Div(target.Hp.Derived, 4),
                _ => throw new ArgumentOutOfRangeException(nameof(item), $"Item {item.Name} is not implemented")
            };
        }
    }
}