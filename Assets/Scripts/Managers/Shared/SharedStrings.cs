using System;
using System.Collections.Generic;
using System.Linq;
using Models;
using Models.Items;
using Models.Pokemon;

namespace Managers.Shared
{
    public class SharedStrings
    {
        private string IntToString(int i)
        {
            return i switch
            {
                1 => "a",
                2 => "two",
                3 => "three",
                4 => "four",
                5 => "five",
                6 => "six",
                7 => "seven",
                8 => "eight",
                9 => "nine",
                10 => "ten",
                11 => "eleven",
                12 => "twelve",
                _ => i.ToString()
            };
        }

        protected static string It(Pokemon pokemon, bool lowercase = false)
        {
            return pokemon.Gender switch
            {
                Gender.Male => lowercase ? "he" : "He",
                Gender.Female => lowercase ? "she" : "She",
                Gender.Unknown => lowercase ? "it" : "It",
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        protected static string Its(Pokemon pokemon, bool lowercase = false)
        {
            return pokemon.Gender switch
            {
                Gender.Male => lowercase ? "his" : "His",
                Gender.Female => lowercase ? "her" : "Her",
                Gender.Unknown => lowercase ? "its" : "Its",
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        protected static string ItPronoun(Pokemon pokemon, bool lowercase = false)
        {
            return pokemon.Gender switch
            {
                Gender.Male => lowercase ? "him" : "Him",
                Gender.Female => lowercase ? "her" : "Her",
                Gender.Unknown => lowercase ? "it" : "It",
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        protected static string Itself(Pokemon pokemon, bool lowercase = false)
        {
            return pokemon.Gender switch
            {
                Gender.Male => lowercase ? "himself" : "Himself",
                Gender.Female => lowercase ? "herself" : "Herself",
                Gender.Unknown => lowercase ? "itself" : "Itself",
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        protected static string Stat(StatName statName)
        {
            return statName switch
            {
                StatName.Hp => "HP",
                StatName.Attack => "Attack",
                StatName.Defense => "Defense",
                StatName.SpecialAttack => "Special Attack",
                StatName.SpecialDefense => "Special Defense",
                StatName.Speed => "Speed",
                StatName.Accuracy => "Accuracy",
                StatName.Evasion => "Evasion",
                _ => throw new ArgumentOutOfRangeException(nameof(statName), statName, null)
            };
        }

        protected string DisplayLoot(LootTable loot)
        {
            List<string> parts = loot.Items
                .Select(pair =>
                    $"{IntToString(pair.Value)} {pair.Key}{(pair.Value == 1 || pair.Key.EndsWith("s") ? "" : "s")}")
                .ToList();
            if (loot.Money > 0)
            {
                // TODO use proper poké-yen symbol
                parts.Add($"{loot.Money:n0}¥".Replace(",", "#"));
            }

            string joinedParts = string.Join(", ", parts);
            int lastComma = joinedParts.LastIndexOf(",", StringComparison.Ordinal);
            string result = lastComma == -1
                ? joinedParts
                : joinedParts.Remove(lastComma, 1).Insert(lastComma, " and");
            return result.Replace("#", ",");
        }

        protected string WantsToLearnMoveInternal(Pokemon pokemon, string move)
        {
            return
                $"{pokemon.Name} wants to learn the move {move}! However, {It(pokemon, true)} already knows four moves. Do you want to replace an old move?";
        }

        protected string MoveLearnedInternal(Pokemon pokemon, string move)
        {
            return $"{pokemon.Name} learned {move}!";
        }

        protected string SentToBoxInternal(Pokemon pokemon)
        {
            return $"You put {pokemon.Name} in the box.";
        }

        protected string LevelUpInternal(Pokemon pokemon)
        {
            return $"{pokemon.Name} grew to level {pokemon.Level}!";
        }

        protected string ExpGainInternal(Pokemon pokemon, int experience)
        {
            return $"{pokemon.Name} gained {experience:n0} Exp. Points!";
        }
    }
}