using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Managers.Shared
{
    public abstract class EventProcessor<T> : MonoBehaviour
    {
        private Queue<T> events = new Queue<T>();
        private bool nextEventCanFire;

        protected int EventCount => events.Count;

        protected T Dequeue() => events.Dequeue();

        public void Enqueue(T @event) => events.Enqueue(@event);

        public void Enqueue(IEnumerable<T> events)
        {
            foreach (T @event in events)
            {
                Enqueue(@event);
            }
        }

        public void PushToFront(T @event)
        {
            Queue<T> newQueue = new Queue<T>();

            newQueue.Enqueue(@event);

            foreach (T oldEvent in events)
            {
                newQueue.Enqueue(oldEvent);
            }

            events = newQueue;
        }

        public void PushToFront(IEnumerable<T> events)
        {
            Queue<T> newQueue = new Queue<T>();

            foreach (T @event in events)
            {
                newQueue.Enqueue(@event);
            }

            foreach (T oldEvent in this.events)
            {
                newQueue.Enqueue(oldEvent);
            }

            this.events = newQueue;
        }

        protected virtual void AllowNextEvent()
        {
            nextEventCanFire = true;
        }

        protected virtual void DisallowNextEvent()
        {
            nextEventCanFire = false;
        }

        protected bool ShouldWait()
        {
            if (events.Count == 0 && nextEventCanFire)
            {
                return true;
            }

            if (!nextEventCanFire)
            {
                return true;
            }

            return false;
        }

        protected IEnumerator WaitCoroutine(float time)
        {
            yield return new WaitForSeconds(time);
            AllowNextEvent();
        }

        protected IEnumerator WaitForConditionCoroutine(Func<bool> condition, Action cleanup)
        {
            while (!condition())
            {
                yield return null;
            }

            cleanup?.Invoke();
            AllowNextEvent();
        }
    }
}