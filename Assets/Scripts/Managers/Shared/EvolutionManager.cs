using System.Collections.Generic;
using System.Linq;
using Managers.Cutscene.Events;
using Managers.Cutscene.Events.Evolutions;
using Managers.Cutscene.Events.Items;
using Managers.Cutscene.Events.Learning;
using Managers.Cutscene.Events.Menus;
using Managers.Data;
using Models;
using Models.Pokemon;
using Models.State;

namespace Managers.Shared
{
    public class EvolutionManager
    {
        public MoveLearningManager MoveLearningManager { private get; set; }

        public Queue<CutsceneEvent> HandleEvolutionUsedItem(Pokemon pokemon, string item)
        {
            Models.Pokemon.Evolution evolution = GetEvolutionUsedItem(pokemon, item);

            if (evolution == null)
            {
                return new Queue<CutsceneEvent>();
            }

            return HandleEvolution(pokemon, evolution.Species, true, false);
        }

        private Models.Pokemon.Evolution GetEvolutionUsedItem(Pokemon pokemon, string item)
        {
            return DataAccess.PokemonBaseData.Get(pokemon.Species)
                .Evolutions
                .FirstOrDefault(evolution =>
                    evolution.EvolutionMethod == EvolutionMethod.UsedItem && evolution.Item == item);
        }

        public Queue<CutsceneEvent> HandleEvolution(Pokemon pokemon, string evoSpecies, bool fromBagMenu,
            bool destroyHeldItem)
        {
            Queue<CutsceneEvent> events = new Queue<CutsceneEvent>();

            string oldName = pokemon.Name;
            events.Enqueue(new EvolutionStartMessage(pokemon, fromBagMenu));
            events.Enqueue(new EvolutionAnimation(pokemon, evoSpecies));
            events.Enqueue(new EvolutionEffect(pokemon, evoSpecies, destroyHeldItem));

            if (fromBagMenu)
            {
                events.Enqueue(new RefreshPokemonList(pokemon));
            }

            events.Enqueue(new EvolutionEndMessage(oldName, evoSpecies, fromBagMenu));

            foreach (string move in MoveLearningManager.GetByEvolution(pokemon, evoSpecies))
            {
                events.Enqueue(new LearnMoveMessage(pokemon, move));
                events.Enqueue(new LearnMove(pokemon, move));
            }

            return events;
        }

        public void EvaluatePostBattleEvolution(Pokemon pokemon)
        {
            PokemonBaseData baseData = DataAccess.PokemonBaseData.Get(pokemon.Species);
            foreach (Models.Pokemon.Evolution evolution in baseData.Evolutions)
            {
                if (ShouldEvolveOnLevelUp(pokemon, evolution)
                    && GlobalState.Instance.EvolutionRequests.All(request => request.ToEvolve != pokemon))
                {
                    GlobalState.Instance.EvolutionRequests.Add(new EvolutionRequest
                    {
                        ToEvolve = pokemon,
                        EvolutionSpecies = evolution.Species,
                        DestroyHeldItem = evolution.EvolutionMethod == EvolutionMethod.HeldItem
                    });

                    break;
                }
            }
        }

        public Queue<CutsceneEvent> HandleEvolutionLevelUp(Pokemon pokemon)
        {
            Models.Pokemon.Evolution evolution = GetEvolutionLevelUp(pokemon);

            if (evolution == null)
            {
                return new Queue<CutsceneEvent>();
            }

            return HandleEvolution(pokemon, evolution.Species, true,
                evolution.EvolutionMethod == EvolutionMethod.HeldItem);
        }

        private Models.Pokemon.Evolution GetEvolutionLevelUp(Pokemon pokemon)
        {
            return DataAccess.PokemonBaseData.Get(pokemon.Species)
                .Evolutions
                .FirstOrDefault(evolution => ShouldEvolveOnLevelUp(pokemon, evolution));
        }

        private bool ShouldEvolveOnLevelUp(Pokemon pokemon, Models.Pokemon.Evolution evolution)
        {
            EvolutionMethod method = evolution.EvolutionMethod;
            if (method == EvolutionMethod.Level
                && pokemon.Level >= evolution.Level)
            {
                return true;
            }

            if (method == EvolutionMethod.Friendship
                && pokemon.Happiness > 220)
            {
                return true;
            }

            if (method == EvolutionMethod.FriendshipAndFairyMove
                && pokemon.Happiness > 220
                && pokemon.Moves.Where(slot => slot.IsAssigned()).Any(slot => slot.Move.Type == Type.Fairy))
            {
                return true;
            }

            if (method == EvolutionMethod.HeldItem
                && pokemon.HeldItem.Is(evolution.Item))
            {
                return true;
            }

            if (method == EvolutionMethod.KnownMove
                && pokemon.Moves.Where(slot => slot.IsAssigned()).Any(slot => slot.Move.Name == evolution.Move))
            {
                return true;
            }

            if (method == EvolutionMethod.PartyContains
                && GlobalState.Instance.PlayerTrainer.Party.All
                    .Select(p => p.Species)
                    .Any(p => p == evolution.Party))
            {
                return true;
            }

            if (method == EvolutionMethod.AttackGreater
                && pokemon.Level >= evolution.Level
                && pokemon.Attack.Derived > pokemon.Defense.Derived)
            {
                return true;
            }

            if (method == EvolutionMethod.DefenseGreater
                && pokemon.Level >= evolution.Level
                && pokemon.Attack.Derived < pokemon.Defense.Derived)
            {
                return true;
            }

            if (method == EvolutionMethod.AttackDefenseEqual
                && pokemon.Level >= evolution.Level
                && pokemon.Attack.Derived == pokemon.Defense.Derived)
            {
                return true;
            }

            return false;
        }
    }
}