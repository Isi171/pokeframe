using System.Collections.Generic;
using System.Linq;
using Daybreak.Log;
using Managers.Data;
using Models.Pokemon;

namespace Managers.Shared
{
    public class MoveLearningManager
    {
        public IEnumerable<string> GetByLevelUp(Pokemon pokemon, int level)
        {
            return DataAccess.Learnsets
                .Get(pokemon.Species)
                .Level
                .Where(set => set.Level == level)
                .Select(set => set.Move)
                .Where(move => !IsKnown(pokemon, move))

                // TODO temporary until all moves are implemented
                .Where(move => IsAvailable(move, $"{pokemon.Species} L{level}"));
        }

        public IEnumerable<string> GetByEvolution(Pokemon pokemon, string evolvedSpecies)
        {
            return DataAccess.Learnsets
                .Get(evolvedSpecies)
                .Evolution
                .Where(move => !IsKnown(pokemon, move))

                // TODO temporary until all moves are implemented
                .Where(move => IsAvailable(move, $"{evolvedSpecies} upon evolution"));
        }

        private bool IsKnown(Pokemon pokemon, string move)
        {
            return pokemon.Moves
                .Where(slot => slot.IsAssigned())
                .Any(slot => slot.Move.Name == move);
        }

        // TODO temporary until all moves are implemented
        private bool IsAvailable(string move, string pokemonIdentifier)
        {
            if (move.All(char.IsLower))
            {
                Log.Warn($"{pokemonIdentifier} wants the unimplemented {move}");
                return false;
            }

            return true;
        }
    }
}