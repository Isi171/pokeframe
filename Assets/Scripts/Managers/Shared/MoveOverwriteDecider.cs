namespace Managers.Shared
{
    public class MoveOverwriteDecider
    {
        public bool MoveOverwriteReady { get; private set; }
        public int MoveSlotToOverwrite { get; private set; }

        public void OverwriteMove(int slot)
        {
            MoveSlotToOverwrite = slot;
            MoveOverwriteReady = true;
        }

        public void ResetMoveOverwrite()
        {
            MoveSlotToOverwrite = -1;
            MoveOverwriteReady = false;
        }
    }
}