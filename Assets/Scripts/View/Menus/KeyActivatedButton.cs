using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace View.Menus
{
    public class KeyActivatedButton : MonoBehaviour
    {
        public static bool GlobalEnable = true;
        private static readonly Dictionary<string, Stack<int>> Hierarchy = new Dictionary<string, Stack<int>>();

        [SerializeField] private Button button;

        public string virtualKey;

        private void OnEnable()
        {
            if (!Hierarchy.ContainsKey(virtualKey))
            {
                Hierarchy[virtualKey] = new Stack<int>();
            }

            Hierarchy[virtualKey].Push(button.GetHashCode());
        }

        private void OnDisable()
        {
            Hierarchy[virtualKey].Pop();
        }

        private void Update()
        {
            if (!GlobalEnable)
            {
                return;
            }

            if (Input.GetButtonDown(virtualKey) && Hierarchy[virtualKey].Peek() == button.GetHashCode())
            {
                button.onClick.Invoke();
            }
        }
    }
}