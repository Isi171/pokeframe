using System;
using System.Linq;
using Models;
using Models.Pokemon;
using Models.Trainer;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;
using View.Menus.List;

namespace View.Menus.Boxes
{
    public class BoxMenuController : MonoBehaviour
    {
        [SerializeField] private Color highlightDuringSwapColor;

        [SerializeField] private PartyListSubmenuController partyList;
        [SerializeField] private Transform contentHolder;
        [SerializeField] private Selectable firstButton;
        [SerializeField] private TMP_Text pokemonName;
        [SerializeField] private Image sprite;
        [SerializeField] private Image ball;
        [SerializeField] private TMP_Text sortButtonText;
        [SerializeField] private Toggle reversedSortToggle;
        [SerializeField] private TMP_Text ownedText;
        [SerializeField] private TMP_Text uniqueSpeciesText;


        private Box box;
        private Models.Trainer.Party party;
        private BoxMenuCustomization customization;

        public void SetData(Box box, Models.Trainer.Party party, BoxMenuCustomization customization)
        {
            this.box = box;
            this.party = party;
            this.customization = customization;
            UpdateView();
            partyList.SetParty(party, FillOut, customization.OnClickPartyMember);
        }

        public void Show()
        {
            gameObject.SetActive(true);
            UpdateStats();
            SelectFirst();
        }

        public void SelectFirst()
        {
            if (box.All.Any())
            {
                firstButton.Select();
            }
            else
            {
                partyList.SelectFirst();
            }
        }

        public void Select(Pokemon pokemon)
        {
            for (int i = 0; i < contentHolder.childCount; i++)
            {
                PokemonInfoListController info = contentHolder.GetChild(i)
                    .GetComponent<PokemonInfoListController>();
                if (info.Pokemon == pokemon)
                {
                    info.Select();
                    return;
                }
            }

            if (party.Contains(pokemon))
            {
                partyList[pokemon].Select();
                return;
            }

            SelectFirst();
        }

        public void Deposit(Pokemon pokemon)
        {
            box.Add(pokemon);
            party.Remove(pokemon);
            UpdateView();
            partyList.UpdateView();
        }

        public void Withdraw(Pokemon pokemon)
        {
            box.Remove(pokemon);
            party.AddLast(pokemon);
            UpdateView();
            partyList.UpdateView();
        }

        public void ChangeSorting()
        {
            box.Sorting = box.Sorting switch
            {
                BoxSorting.Unsorted => BoxSorting.ByName,
                BoxSorting.ByName => BoxSorting.ByDexNo,
                BoxSorting.ByDexNo => BoxSorting.Unsorted,
                _ => throw new ArgumentOutOfRangeException()
            };

            UpdateView();
        }

        public void ToggleReverse()
        {
            if (box == null)
            {
                return;
            }

            box.ReversedSorting = reversedSortToggle.isOn;
            UpdateView();
        }

        public void RefreshList(Pokemon pokemon)
        {
            partyList[pokemon].Display();
        }

        public void UpdateView()
        {
            sortButtonText.text = box.Sorting switch
            {
                BoxSorting.Unsorted => "By deposit order",
                BoxSorting.ByName => "By name",
                BoxSorting.ByDexNo => "By dex no.",
                _ => throw new ArgumentOutOfRangeException(nameof(box.Sorting), box.Sorting, null)
            };
            reversedSortToggle.isOn = box.ReversedSorting;

            // TODO temporarily visualize only a limited number of pokemon in the box until I properly design its UI
            Pokemon[] contents = box.All.ToArray();
            for (int i = 0; i < contentHolder.childCount; i++)
            {
                PokemonInfoListController info = contentHolder.GetChild(i)
                    .GetComponent<PokemonInfoListController>();
                if (i < contents.Length)
                {
                    info.SetData(contents[i], FillOut, customization.OnClickBoxedPokemon);
                }
                else
                {
                    info.SetEmpty();
                }
            }
        }

        private void FillOut(Pokemon pokemon)
        {
            pokemonName.text = pokemon.Name;
            sprite.sprite = SpriteUtils.LoadSprite(pokemon.Species, pokemon.Gender, pokemon.Shiny, false);
            ball.sprite = SpriteUtils.LoadItem(pokemon.Ball);
        }

        private void UpdateStats()
        {
            ownedText.text = $"{box.All.Count() + party.Count:n0}";

            int uniqueCount = box.All.Concat(party.All)
                .Select(pokemon => pokemon.Species)
                .Distinct()
                .Count();

            uniqueSpeciesText.text = $"{uniqueCount:n0}";
        }
    }
}