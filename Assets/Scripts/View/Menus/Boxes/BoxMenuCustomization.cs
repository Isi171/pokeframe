using System;
using Models.Pokemon;

namespace View.Menus.Boxes
{
    public class BoxMenuCustomization
    {
        public Action<Pokemon> OnClickPartyMember { get; set; }
        public Action<Pokemon> OnClickBoxedPokemon { get; set; }
    }
}