using System.Collections.Generic;
using Models;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace View.Menus
{
    public class TypeIndicatorController : MonoBehaviour
    {
        private static readonly Dictionary<Type, Color> Table = new Dictionary<Type, Color>
        {
            {Type.Normal, new Color(0.6588235f, 0.6588235f, 0.4705882f)},
            {Type.Fighting, new Color(0.7529412f, 0.1882353f, 0.1568628f)},
            {Type.Flying, new Color(0.6588235f, 0.5647059f, 0.9411765f)},
            {Type.Poison, new Color(0.627451f, 0.2509804f, 0.627451f)},
            {Type.Ground, new Color(0.8784314f, 0.7529412f, 0.4078431f)},
            {Type.Rock, new Color(0.7215686f, 0.627451f, 0.2196078f)},
            {Type.Bug, new Color(0.6588235f, 0.7215686f, 0.1254902f)},
            {Type.Ghost, new Color(0.4392157f, 0.345098f, 0.5960785f)},
            {Type.Steel, new Color(0.7215686f, 0.7215686f, 0.8156863f)},
            {Type.Fire, new Color(0.9411765f, 0.5019608f, 0.1882353f)},
            {Type.Water, new Color(0.4078431f, 0.5647059f, 0.9411765f)},
            {Type.Grass, new Color(0.4705882f, 0.7843137f, 0.3137255f)},
            {Type.Electric, new Color(0.972549f, 0.8156863f, 0.1882353f)},
            {Type.Psychic, new Color(0.972549f, 0.345098f, 0.5333334f)},
            {Type.Ice, new Color(0.5960785f, 0.8470588f, 0.8470588f)},
            {Type.Dragon, new Color(0.4392157f, 0.2196078f, 0.972549f)},
            {Type.Dark, new Color(0.4392157f, 0.345098f, 0.282353f)},
            {Type.Fairy, new Color(0.9333333f, 0.6f, 0.6745098f)},
        };

        [SerializeField] private Image background;
        [SerializeField] private TMP_Text text;

        public void Set(Type type)
        {
            if (type == Type.Typeless)
            {
                background.gameObject.SetActive(false);
                return;
            }

            background.gameObject.SetActive(true);
            background.color = Table[type];
            text.text = type.ToString();
        }
    }
}