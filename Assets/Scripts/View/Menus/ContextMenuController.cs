using System;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace View.Menus
{
    public class ContextAction
    {
        public ContextAction(string name, Action action, Func<bool> condition = null)
        {
            Name = name;
            Action = action;
            Condition = condition;
        }

        public string Name { get; }
        public Action Action { get; }
        public Func<bool> Condition { get; }
    }

    public class ContextMenuController : MonoBehaviour
    {
        [SerializeField] private RectTransform menuPanel;
        [SerializeField] private Button[] buttons;

        private Action onClose;

        public void Show(Vector2 position, Action onClose, params ContextAction[] entries)
        {
            entries = entries.Where(entry => entry.Condition?.Invoke() ?? true).ToArray();

            if (entries.Length == 0)
            {
                throw new Exception("Empty context menu is not allowed");
            }

            if (entries.Length > buttons.Length)
            {
                throw new Exception($"Too many voices ({entries.Length} out of {buttons.Length}) in context menu");
            }

            gameObject.SetActive(true);
            menuPanel.anchoredPosition = position;
            this.onClose = onClose;

            for (int i = 0; i < buttons.Length; i++)
            {
                Button button = buttons[i];
                if (i < entries.Length)
                {
                    ContextAction contextAction = entries[i];
                    button.gameObject.SetActive(true);
                    button.GetComponentInChildren<TMP_Text>().text = contextAction.Name;
                    button.onClick.RemoveAllListeners();
                    button.onClick.AddListener(() =>
                    {
                        Hide();
                        contextAction.Action();
                    });
                }
                else
                {
                    button.gameObject.SetActive(false);
                }
            }

            menuPanel.sizeDelta = new Vector2(menuPanel.sizeDelta.x, 10 + 10 + 30 * entries.Length);

            buttons[0].Select();
        }

        private void Hide()
        {
            gameObject.SetActive(false);
            onClose();
        }

        public void HideSilently()
        {
            gameObject.SetActive(false);
        }
    }
}