using System;
using UnityEngine;

namespace View.Menus
{
    public class ExperienceBarController : MonoBehaviour
    {
        [SerializeField] private RectTransform barRectTransform;
        [SerializeField] private RectTransform barBackgroundRectTransform;

        private Action barUpdatedCallback;
        private bool callbackCalled = true;
        private int currentExp;
        private int targetExp;
        private int expStep;
        private int maxExp;

        public void UpdateExperienceBar(int current, int required)
        {
            float height = barRectTransform.sizeDelta.y;
            float maxWidth = barBackgroundRectTransform.sizeDelta.x;

            barRectTransform.sizeDelta = new Vector2(maxWidth * (current / (float) required), height);

            currentExp = current;
            targetExp = current;
            maxExp = required;
        }

        public void SetExpBarUpdatedCallback(Action animationDoneCallback)
        {
            barUpdatedCallback = animationDoneCallback;
        }

        private void Update()
        {
            if (currentExp == targetExp)
            {
                if (callbackCalled)
                {
                    return;
                }

                callbackCalled = true;
                barUpdatedCallback();
            }

            currentExp += expStep;
            if (currentExp > targetExp)
            {
                currentExp = targetExp;
            }

            UpdateExperienceBarForAnimation(currentExp, maxExp);
        }

        private void UpdateExperienceBarForAnimation(int current, int required)
        {
            float height = barRectTransform.sizeDelta.y;
            float maxWidth = barBackgroundRectTransform.sizeDelta.x;

            barRectTransform.sizeDelta = new Vector2(maxWidth * (current / (float) required), height);
        }

        public void UpdateExperienceBarAnimated(int current, int required)
        {
            targetExp = current;
            maxExp = required;
            callbackCalled = false;
            expStep = Math.Max(1, required / 100);
        }
    }
}