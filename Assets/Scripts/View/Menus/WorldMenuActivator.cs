using Models.State;
using UnityEngine;
using View.Actor;

namespace View.Menus
{
    public class WorldMenuActivator : MonoBehaviour
    {
        private GlobalState globalState;

        [SerializeField] private WorldMenuController worldMenuController;
        [SerializeField] private ActorInputModule playerInputModule;

        private void Start()
        {
            globalState = GlobalState.Instance;
        }

        private void Update()
        {
            if (playerInputModule.InputEnabled && Input.GetButtonDown("Cancel"))
            {
                Time.timeScale = 0f;
                playerInputModule.DisableInput();
                worldMenuController.Show(globalState.PlayerTrainer);
            }
        }

        public void MenuClosed()
        {
            Time.timeScale = GlobalState.Instance.Options.Timescale;
            playerInputModule.EnableInput();
        }
    }
}