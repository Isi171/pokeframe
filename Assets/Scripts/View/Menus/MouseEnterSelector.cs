using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace View.Menus
{
    public class MouseEnterSelector : MonoBehaviour, IPointerEnterHandler
    {
        [SerializeField] private Selectable selectable;

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (selectable != null && selectable.interactable)
            {
                selectable.Select();
            }
        }
    }
}