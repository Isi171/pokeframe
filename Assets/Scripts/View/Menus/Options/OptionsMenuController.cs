using Models.State;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace View.Menus.Options
{
    public class OptionsMenuController : MonoBehaviour
    {
        [SerializeField] private Selectable firstButton;
        [SerializeField] private Slider musicVolumeSlider;
        [SerializeField] private TMP_Text musicVolumeValue;
        [SerializeField] private Slider effectsVolumeSlider;
        [SerializeField] private TMP_Text effectsVolumeValue;
        [SerializeField] private Slider timescaleSlider;
        [SerializeField] private TMP_Text timescaleValue;

        public void Show()
        {
            gameObject.SetActive(true);
            firstButton.Select();
            Models.State.Options options = GlobalState.Instance.Options;

            InitSlider(musicVolumeSlider, musicVolumeValue, Models.State.Options.MaxMusicVolume,
                Models.State.Options.MinMusicVolume, options.MusicVolume);

            InitSlider(effectsVolumeSlider, effectsVolumeValue, Models.State.Options.MaxEffectsVolume,
                Models.State.Options.MinEffectsVolume, options.EffectsVolume);

            InitSlider(timescaleSlider, timescaleValue, Models.State.Options.MaxTimescale,
                Models.State.Options.MinTimescale, options.Timescale);
        }

        private void InitSlider(Slider slider, TMP_Text text, int max, int min, int value)
        {
            slider.maxValue = max;
            slider.minValue = min;
            slider.SetValueWithoutNotify(value);
            text.text = value.ToString();
        }

        public void MusicVolumeChanged()
        {
            int value = (int)musicVolumeSlider.value;
            musicVolumeValue.text = value.ToString();

            GlobalState.Instance.Options.MusicVolume = value;
            GlobalState.Instance.Options.ApplyStatic();
        }

        public void EffectsVolumeChanged()
        {
            int value = (int)effectsVolumeSlider.value;
            effectsVolumeValue.text = value.ToString();

            GlobalState.Instance.Options.EffectsVolume = value;
            GlobalState.Instance.Options.ApplyStatic();
        }

        public void TimescaleChanged()
        {
            int value = (int)timescaleSlider.value;
            timescaleValue.text = value.ToString();

            GlobalState.Instance.Options.Timescale = value;
            GlobalState.Instance.Options.ApplyStatic();
        }
    }
}