using System.Linq;
using Managers.Cutscene;
using Managers.Cutscene.Events.Menus;
using Models.Items;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using View.Menus.Items;

namespace View.Menus.Barter
{
    public class BarterMenuController : MonoBehaviour
    {
        [SerializeField] private CutsceneManager cutsceneManager;
        [SerializeField] private TMP_Text playerMoney;
        [SerializeField] private Selectable firstButton;
        [SerializeField] private Transform buyContainer;

        private Merchant merchant;
        private Bag bag;

        public void SetData(Merchant merchant, Bag bag)
        {
            this.merchant = merchant;
            this.bag = bag;
        }

        public void Show()
        {
            gameObject.SetActive(true);
            UpdateView();
            firstButton.Select();
        }

        public void Hide()
        {
            gameObject.SetActive(false);
            cutsceneManager.Enqueue(new CloseBarterMenu());
        }

        private void UpdateView()
        {
            playerMoney.text = $"{bag.Money:n0}";

            // Don't show countless items if already owned
            ItemSlot[] stock = merchant.Stock
                .Where(item => !item.Countless || !bag.Contains(item.Name))
                .Select(item => new ItemSlot(item, bag.Amount(item.Name)))
                .ToArray();

            for (int i = 0; i < buyContainer.childCount; i++)
            {
                if (i < stock.Length)
                {
                    var itemViewer = buyContainer.GetChild(i).gameObject.GetComponent<ItemSlotViewerController>();
                    itemViewer.SetDataAsMerchant(stock[i], Buy);
                    itemViewer.gameObject.SetActive(true);
                }
                else
                {
                    buyContainer.GetChild(i).gameObject.SetActive(false);
                }
            }
        }

        private void Buy(ItemSlot itemSlot)
        {
            if (itemSlot.Item.Buy > bag.Money)
            {
                return;
            }

            bag.Money -= itemSlot.Item.Buy;
            bag.AddOrRemove(itemSlot.Item.Name);
            UpdateView();
        }
    }
}