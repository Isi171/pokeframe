using Models.Items;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace View.Menus.Items
{
    public class PocketSelectionController : MonoBehaviour, ISelectHandler
    {
        [SerializeField] private Selectable selectable;
        [SerializeField] private TMP_Text label;
        [SerializeField] private BagMenuController bagMenu;
        [SerializeField] private string text;

        public Pocket Pocket { private get; set; }

        private void Start()
        {
            label.text = text;
        }

        public void OnSelect(BaseEventData eventData)
        {
            bagMenu.DisplayPocket(Pocket, text, selectable, this);
        }

        public void Select() => selectable.Select();

        public void SetInteractable(bool interactable) => selectable.interactable = interactable;
    }
}