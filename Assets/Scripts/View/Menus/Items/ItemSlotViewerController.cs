using System;
using Managers.Data;
using Models;
using Models.Items;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace View.Menus.Items
{
    public class ItemSlotViewerController : MonoBehaviour
    {
        [SerializeField] private Button button;
        [SerializeField] private TMP_Text quantity;
        [SerializeField] private TMP_Text itemName;
        [SerializeField] private TMP_Text description;
        [SerializeField] private TMP_Text sellPrice;
        [SerializeField] private Image image;
        [SerializeField] private GameObject currencySymbol;

        public Button Button => button;
        public ItemSlot ItemSlot { get; private set; }

        public void SetData(ItemSlot itemSlot, Action<ItemSlot> choiceCallback)
        {
            SetCommonData(itemSlot, choiceCallback);
            quantity.text = itemSlot.Item.Countless ? "" : $"×{itemSlot.Quantity}";
            if (itemSlot.Item.Sellable)
            {
                sellPrice.text = $"{itemSlot.Item.Sell:n0}";
                currencySymbol.SetActive(true);
            }
            else
            {
                sellPrice.text = "";
                currencySymbol.SetActive(false);
            }
        }

        public void SetDataAsMerchant(ItemSlot itemSlot, Action<ItemSlot> choiceCallback)
        {
            SetCommonData(itemSlot, choiceCallback);
            quantity.text = !itemSlot.Item.Countless
                ? $"×{itemSlot.Quantity}"
                : itemSlot.Quantity == 0
                    ? "×0"
                    : "×1";
            sellPrice.text = $"{itemSlot.Item.Buy:n0}";
            currencySymbol.SetActive(true);
        }

        private void SetCommonData(ItemSlot itemSlot, Action<ItemSlot> choiceCallback)
        {
            ItemSlot = itemSlot;
            image.sprite = SpriteUtils.LoadItem(itemSlot.Item.Name);
            itemName.text = itemSlot.Item.Name;
            description.text = itemSlot.Item.Category == ItemCategory.Tm
                ? DataAccess.Moves.Get(itemSlot.Item.ExtractTmMove()).Description
                : itemSlot.Item.Description;
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(() => choiceCallback(itemSlot));
        }
    }
}