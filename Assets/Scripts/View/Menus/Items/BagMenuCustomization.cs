using System;
using Models.Items;
using Models.Pokemon;

namespace View.Menus.Items
{
    public class BagMenuCustomization
    {
        public Action<ItemSlot> OnClickItem { get; set; }
        public Action<ItemSlot, Pokemon> OnClickTarget { get; set; }
    }
}