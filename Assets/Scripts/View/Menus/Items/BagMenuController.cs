using System;
using Models.Items;
using Models.Pokemon;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using View.Menus.List;

namespace View.Menus.Items
{
    public class BagMenuController : MonoBehaviour
    {
        [SerializeField] private GameObject itemSlotViewerPrefab;
        [SerializeField] private RectTransform viewportContent;
        [SerializeField] private PartyListSubmenuController partyList;
        [SerializeField] private TMP_Text money;
        [SerializeField] private TMP_Text currentPocketLabel;
        [SerializeField] private Button backButton;
        [SerializeField] private PocketSelectionController itemsSelector;
        [SerializeField] private PocketSelectionController medicineSelector;
        [SerializeField] private PocketSelectionController berriesSelector;
        [SerializeField] private PocketSelectionController battleSelector;
        [SerializeField] private PocketSelectionController pokeBallsSelector;
        [SerializeField] private PocketSelectionController vitaminsSelector;
        [SerializeField] private PocketSelectionController evolutionSelector;
        [SerializeField] private PocketSelectionController tmsSelector;
        [SerializeField] private PocketSelectionController valuablesSelector;
        [SerializeField] private PocketSelectionController keySelector;

        private Bag bag;
        private BagMenuCustomization customization;
        private PocketSelectionController currentSelectorController;
        private ItemSlot selected;

        public bool GiveMode { get; set; }

        public void SetData(Bag bag, Models.Trainer.Party party, BagMenuCustomization customization)
        {
            this.bag = bag;
            this.customization = customization;

            money.text = $"{bag.Money:n0}";

            itemsSelector.Pocket = bag.Items;
            medicineSelector.Pocket = bag.Medicine;
            berriesSelector.Pocket = bag.Berries;
            battleSelector.Pocket = bag.Battle;
            pokeBallsSelector.Pocket = bag.PokeBalls;
            vitaminsSelector.Pocket = bag.Vitamins;
            evolutionSelector.Pocket = bag.Evolution;
            tmsSelector.Pocket = bag.Tm;
            valuablesSelector.Pocket = bag.Valuables;
            keySelector.Pocket = bag.Key;

            partyList.SetParty(party, pokemon => { }, pokemon => { customization.OnClickTarget(selected, pokemon); });
            partyList.SetElementsInteractable(false);
        }

        public void Show()
        {
            gameObject.SetActive(true);
            DisableEmptyPockets();
            SelectFirstAvailablePocket();
        }

        public void Select(ItemSlot itemSlot)
        {
            // TODO improve: last item in list and last item in pocket deletion destroys the selection
            for (int i = 0; i < viewportContent.childCount; i++)
            {
                ItemSlotViewerController itemSlotViewer =
                    viewportContent.GetChild(i).GetComponent<ItemSlotViewerController>();
                if (itemSlotViewer.ItemSlot == itemSlot)
                {
                    itemSlotViewer.Button.Select();
                    return;
                }
            }
        }

        private void SelectFirstAvailablePocket()
        {
            if (bag.Items.Items.Count > 0)
            {
                itemsSelector.Select();
            }
            else if (bag.Medicine.Items.Count > 0)
            {
                medicineSelector.Select();
            }
            else if (bag.Berries.Items.Count > 0)
            {
                berriesSelector.Select();
            }
            else if (bag.Battle.Items.Count > 0)
            {
                battleSelector.Select();
            }
            else if (bag.PokeBalls.Items.Count > 0)
            {
                pokeBallsSelector.Select();
            }
            else if (bag.Vitamins.Items.Count > 0)
            {
                vitaminsSelector.Select();
            }
            else if (bag.Evolution.Items.Count > 0)
            {
                evolutionSelector.Select();
            }
            else if (bag.Tm.Items.Count > 0)
            {
                tmsSelector.Select();
            }
            else if (bag.Valuables.Items.Count > 0)
            {
                valuablesSelector.Select();
            }
            else if (bag.Key.Items.Count > 0)
            {
                keySelector.Select();
            }
            else
            {
                backButton.Select();
            }
        }

        private void DisableEmptyPockets()
        {
            itemsSelector.SetInteractable(bag.Items.Items.Count > 0);
            medicineSelector.SetInteractable(bag.Medicine.Items.Count > 0);
            berriesSelector.SetInteractable(bag.Berries.Items.Count > 0);
            battleSelector.SetInteractable(bag.Battle.Items.Count > 0);
            pokeBallsSelector.SetInteractable(bag.PokeBalls.Items.Count > 0);
            vitaminsSelector.SetInteractable(bag.Vitamins.Items.Count > 0);
            evolutionSelector.SetInteractable(bag.Evolution.Items.Count > 0);
            tmsSelector.SetInteractable(bag.Tm.Items.Count > 0);
            valuablesSelector.SetInteractable(bag.Valuables.Items.Count > 0);
            keySelector.SetInteractable(bag.Key.Items.Count > 0);
        }

        public void RefreshView()
        {
            currentSelectorController.OnSelect(null);
            DisableEmptyPockets();
        }

        public void RefreshList(Pokemon pokemon)
        {
            partyList[pokemon].Display();
        }

        public void UseItem(ItemSlot itemSlot, Pokemon target)
        {
            customization.OnClickTarget(itemSlot, target);
        }

        public void SetItemAndAllowTargetSelection(ItemSlot itemSlot)
        {
            selected = itemSlot;
            partyList.SetElementsInteractable(true);
            partyList.SelectFirst();
        }

        public void DisplayPocket(Pocket pocket, string label, Selectable pocketSelectable,
            PocketSelectionController selector)
        {
            if (pocket.Items.Count > viewportContent.childCount)
            {
                throw new Exception(
                    $"There are more unique items in this pocket ({pocket.Items.Count}) than available viewer slots ({viewportContent.childCount})!");
            }

            currentSelectorController = selector;
            currentPocketLabel.text = label;
            float height = itemSlotViewerPrefab.GetComponent<RectTransform>().sizeDelta.y;
            viewportContent.sizeDelta = new Vector2(viewportContent.sizeDelta.x, pocket.Items.Count * height);
            for (int i = 0; i < viewportContent.childCount; i++)
            {
                Transform child = viewportContent.GetChild(i);
                if (i < pocket.Items.Count)
                {
                    child.gameObject.SetActive(true);
                    ItemSlotViewerController itemSlotViewer = child.GetComponent<ItemSlotViewerController>();
                    itemSlotViewer.SetData(pocket.Items[i], customization.OnClickItem);

                    if (i == 0)
                    {
                        itemSlotViewer.Button.navigation = new Navigation
                        {
                            mode = itemSlotViewer.Button.navigation.mode,
                            selectOnDown = itemSlotViewer.Button.navigation.selectOnDown,
                            selectOnLeft = itemSlotViewer.Button.navigation.selectOnLeft,
                            selectOnRight = itemSlotViewer.Button.navigation.selectOnRight,
                            selectOnUp = pocketSelectable
                        };

                        pocketSelectable.navigation = new Navigation
                        {
                            mode = pocketSelectable.navigation.mode,
                            selectOnDown = itemSlotViewer.Button,
                            selectOnLeft = pocketSelectable.navigation.selectOnLeft,
                            selectOnRight = pocketSelectable.navigation.selectOnRight,
                            selectOnUp = pocketSelectable.navigation.selectOnUp
                        };
                    }
                }
                else
                {
                    child.gameObject.SetActive(false);
                }
            }
        }
    }
}