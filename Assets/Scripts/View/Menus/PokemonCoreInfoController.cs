using System;
using Models;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace View.Menus
{
    public class PokemonCoreInfoController : MonoBehaviour
    {
        private static readonly Color HealthyColor = new Color(0.07476866f, 1f, 0f);
        private static readonly Color WoundedColor = new Color(1f, 0.7640421f, 0f);
        private static readonly Color CriticalColor = new Color(1f, 0.06180752f, 0f);
        private static readonly Color PoisonColor = new Color(0.5611542f, 0f, 0.745283f);
        private static readonly Color BurnColor = new Color(0.8962264f, 0.1871442f, 0f);
        private static readonly Color ParalysisColor = new Color(0.990566f, 0.6808728f, 0f);
        private static readonly Color SleepColor = new Color(0.6024651f, 0.5831257f, 0.6792453f);
        private static readonly Color FreezeColor = new Color(0.3867925f, 0.9235445f, 1f);
        private static readonly Color FaintedColor = new Color(0.6f, 0f, 0f);

        [SerializeField] private bool showHpCount;
        [SerializeField] private TMP_Text battlerName;
        [SerializeField] private TMP_Text gender;
        [SerializeField] private TMP_Text level;
        [SerializeField] private TMP_Text hpCount;
        [SerializeField] private Image hpBar;
        [SerializeField] private RectTransform hpBarRectTransform;
        [SerializeField] private RectTransform hpBarBackgroundRectTransform;
        [SerializeField] private Image statusIndicator;
        [SerializeField] private TMP_Text statusIndicatorText;

        private float maxBarWidth;
        private int maxHp;
        private int currentHp;
        private int targetHp;
        private int yellowThreshold;
        private int redThreshold;

        private const int HpStep = 1;
        private Action hpBarUpdatedCallback;
        private bool callbackCalled = true;

        private void Update()
        {
            if (currentHp == targetHp)
            {
                if (callbackCalled)
                {
                    return;
                }

                callbackCalled = true;
                hpBarUpdatedCallback();
            }

            if (targetHp < currentHp)
            {
                currentHp -= HpStep;
                if (currentHp < targetHp)
                {
                    currentHp = targetHp;
                }
            }
            else
            {
                currentHp += HpStep;
                if (currentHp > targetHp)
                {
                    currentHp = targetHp;
                }
            }

            UpdateBarAndCount();
        }

        public void SetHpBarUpdatedCallback(Action hpBarUpdatedCallback)
        {
            this.hpBarUpdatedCallback = hpBarUpdatedCallback;
        }

        protected void SetInfo(string name, Gender gender, int level, int maxHp, int currentHp, Status status)
        {
            battlerName.text = name;

            maxBarWidth = hpBarBackgroundRectTransform.sizeDelta.x;
            (this.gender.text, this.gender.color) = ViewUtils.GetGender(gender);

            UpdateLevel(level);

            hpCount.enabled = showHpCount;
            this.maxHp = maxHp;
            yellowThreshold = maxHp / 2;
            redThreshold = maxHp / 5;
            this.currentHp = currentHp;
            targetHp = currentHp;
            UpdateBarAndCount();

            SetStatus(status);
        }

        public void SetHp(int hp)
        {
            targetHp = hp;
            callbackCalled = false;
        }

        public void SetStatus(Status status)
        {
            switch (status)
            {
                case Status.None:
                    statusIndicator.gameObject.SetActive(false);
                    break;
                case Status.Burned:
                    statusIndicator.gameObject.SetActive(true);
                    statusIndicatorText.text = "BRN";
                    statusIndicator.color = BurnColor;
                    break;
                case Status.Frozen:
                    statusIndicator.gameObject.SetActive(true);
                    statusIndicatorText.text = "FRZ";
                    statusIndicator.color = FreezeColor;
                    break;
                case Status.Paralyzed:
                    statusIndicator.gameObject.SetActive(true);
                    statusIndicatorText.text = "PAR";
                    statusIndicator.color = ParalysisColor;
                    break;
                case Status.Poisoned:
                    statusIndicator.gameObject.SetActive(true);
                    statusIndicatorText.text = "PSN";
                    statusIndicator.color = PoisonColor;
                    break;
                case Status.BadlyPoisoned:
                    statusIndicator.gameObject.SetActive(true);
                    statusIndicatorText.text = "TOX";
                    statusIndicator.color = PoisonColor;
                    break;
                case Status.Asleep:
                    statusIndicator.gameObject.SetActive(true);
                    statusIndicatorText.text = "SLP";
                    statusIndicator.color = SleepColor;
                    break;
                case Status.Fainted:
                    statusIndicator.gameObject.SetActive(true);
                    statusIndicatorText.text = "FNT";
                    statusIndicator.color = FaintedColor;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(status), status, null);
            }
        }

        private void UpdateBarAndCount()
        {
            hpCount.text = $"{currentHp}/{maxHp}";
            hpBarRectTransform.sizeDelta =
                new Vector2(maxBarWidth * (currentHp / (float) maxHp), hpBarRectTransform.sizeDelta.y);

            if (currentHp > yellowThreshold)
            {
                hpBar.color = HealthyColor;
            }
            else if (currentHp <= yellowThreshold && currentHp > redThreshold)
            {
                hpBar.color = WoundedColor;
            }
            else if (currentHp <= redThreshold)
            {
                hpBar.color = CriticalColor;
            }
        }

        public void UpdateLevel(int level)
        {
            this.level.text = $"Lv.{level}";
        }
    }
}