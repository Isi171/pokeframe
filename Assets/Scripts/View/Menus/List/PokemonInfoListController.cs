using System;
using Models.Pokemon;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Utils;

namespace View.Menus.List
{
    public class PokemonInfoListController : PokemonCoreInfoController, ISelectHandler
    {
        [SerializeField] private Button button;
        [SerializeField] private Image icon;
        [SerializeField] private Image itemIcon;

        private Action<Pokemon> fillOutCallback;
        private Action<Pokemon> clickCallback;

        public Pokemon Pokemon { get; private set; }
        public Selectable Selectable => button;

        public void SetData(Pokemon pokemon, Action<Pokemon> fillOutCallback, Action<Pokemon> clickCallback)
        {
            SetPokemonAndDisplay(pokemon);
            this.fillOutCallback = fillOutCallback;
            this.clickCallback = clickCallback;
            SetClickCallback(clickCallback);
        }

        public void SetPokemonAndDisplay(Pokemon pokemon)
        {
            gameObject.SetActive(true);
            Pokemon = pokemon;
            Display();
        }

        public void Display()
        {
            icon.sprite = SpriteUtils.LoadIcon(Pokemon.Species);
            if (Pokemon.HeldItem.Item == null)
            {
                itemIcon.gameObject.SetActive(false);
            }
            else
            {
                itemIcon.gameObject.SetActive(true);
                itemIcon.sprite = SpriteUtils.LoadItem(Pokemon.HeldItem.Item.Name);
            }

            SetInfo(Pokemon.Name, Pokemon.Gender, Pokemon.Level, Pokemon.Hp.Derived, Pokemon.CurrentHp, Pokemon.Status);
        }

        public void SetEmpty()
        {
            gameObject.SetActive(false);
        }

        private void SetClickCallback(Action<Pokemon> callback)
        {
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(() => callback(Pokemon));
        }

        public void SetTemporaryClickCallback(Action<Pokemon> callback)
        {
            SetClickCallback(callback);
        }

        public void RemoveTemporaryClickCallback()
        {
            SetClickCallback(clickCallback);
        }

        public void OnSelect(BaseEventData eventData)
        {
            fillOutCallback(Pokemon);
        }

        public void SetInteractable(bool interactable) => button.interactable = interactable;


        public void HighlightDuringSwap(Color color)
        {
            button.image.color = color;
        }

        public void ResetColor()
        {
            button.image.color = Color.white;
        }

        public void Select()
        {
            ViewUtils.ForceSelect(button);
        }
    }
}