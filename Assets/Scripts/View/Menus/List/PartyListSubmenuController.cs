using System;
using System.Collections.Generic;
using System.Linq;
using Models.Pokemon;
using UnityEngine;

namespace View.Menus.List
{
    public class PartyListSubmenuController : MonoBehaviour
    {
        [SerializeField] private PokemonInfoListController[] list;
        [SerializeField] private Color highlightDuringSwapColor;

        private Models.Trainer.Party party;
        private Action<Pokemon> fillOutCallback;
        private Action<Pokemon> clickCallback;

        private IEnumerable<PokemonInfoListController> SafeList()
        {
            return list.Where(info => info.gameObject.activeSelf);
        }

        public PokemonInfoListController this[Pokemon pokemon] => SafeList().First(info => info.Pokemon == pokemon);

        public void SetParty(Models.Trainer.Party party, Action<Pokemon> fillOutCallback, Action<Pokemon> clickCallback)
        {
            this.party = party;
            this.fillOutCallback = fillOutCallback;
            this.clickCallback = clickCallback;

            UpdateView();
        }

        public void UpdateView()
        {
            for (int i = 0; i < list.Length; i++)
            {
                if (i < party.Count)
                {
                    list[i].ResetColor();
                    list[i].SetData(party.Get(i), fillOutCallback, clickCallback);
                }
                else
                {
                    list[i].SetEmpty();
                }
            }
        }

        public void SetElementsInteractable(bool interactable)
        {
            foreach (PokemonInfoListController info in SafeList())
            {
                info.SetInteractable(interactable);
            }
        }

        public void SelectFirst()
        {
            list[0].Select();
        }

        public void BeginSwapPosition(Pokemon selected, Action<Pokemon> afterSwapCallback)
        {
            for (int i = 0; i < party.Count; i++)
            {
                list[i].SetTemporaryClickCallback(target => SwapPosition(selected, target, afterSwapCallback));

                if (list[i].Pokemon == selected)
                {
                    list[i].HighlightDuringSwap(highlightDuringSwapColor);
                    list[i].Select();
                }
            }
        }

        private void SwapPosition(Pokemon selected, Pokemon target, Action<Pokemon> afterSwapCallback)
        {
            party.SwapPositions(selected, target);

            for (int i = 0; i < party.Count; i++)
            {
                list[i].ResetColor();
                list[i].RemoveTemporaryClickCallback();
                list[i].SetPokemonAndDisplay(party.Get(i));
            }

            afterSwapCallback(selected);
        }
    }
}