using System.Collections.Generic;
using Managers.Cutscene;
using Managers.Cutscene.Events;
using Managers.Cutscene.Events.Boxes;
using Managers.Cutscene.Events.Items;
using Managers.Cutscene.Events.Learning;
using Managers.Cutscene.Events.Menus;
using Managers.Data;
using Models;
using Models.Items;
using Models.State;
using Models.Trainer;
using UnityEngine;
using UnityEngine.UI;
using Utils;
using View.Menus.Boxes;
using View.Menus.Items;
using View.Menus.Options;
using View.Menus.Party;
using View.Menus.Quests;

namespace View.Menus
{
    public class WorldMenuController : MonoBehaviour
    {
        [SerializeField] private Button firstButton;
        [SerializeField] private PokemonInfoMenuController pokemonInfoMenu;
        [SerializeField] private BagMenuController bagMenu;
        [SerializeField] private BoxMenuController boxMenu;
        [SerializeField] private QuestsMenuController questsMenu;
        [SerializeField] private OptionsMenuController optionsMenu;
        [SerializeField] private WorldMenuActivator worldMenuActivator;
        [SerializeField] private ContextMenuController contextMenu;
        [SerializeField] private CutsceneManager cutsceneManager;

        private Trainer trainer;

        public bool AnyOpen => gameObject.activeInHierarchy
                               || pokemonInfoMenu.gameObject.activeInHierarchy
                               || bagMenu.gameObject.activeInHierarchy
                               || boxMenu.gameObject.activeInHierarchy
                               || questsMenu.gameObject.activeInHierarchy
                               || optionsMenu.gameObject.activeInHierarchy;

        private void OnEnable()
        {
            ViewUtils.ForceSelect(firstButton);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
            HidePokemonMenu();
            HideBagMenu();
            HideBoxMenu();
            HideQuestsMenu();
            HideOptionsMenu();
            contextMenu.HideSilently();
            worldMenuActivator.MenuClosed();
        }

        public void Show(Trainer trainer)
        {
            this.trainer = trainer;

            gameObject.SetActive(true);
            HidePokemonMenu();
            HideBagMenu();
            HideBoxMenu();
            HideQuestsMenu();
            HideOptionsMenu();
            contextMenu.HideSilently();
        }

        public void ShowPokemonMenu()
        {
            pokemonInfoMenu.SetData(trainer.Party, GetPartyMenuCustomization());
            pokemonInfoMenu.Show();
            contextMenu.HideSilently();
        }

        private PartyMenuCustomization GetPartyMenuCustomization()
        {
            return new PartyMenuCustomization
            {
                OnClickPokemon = pokemon => contextMenu.Show(
                    new Vector2(300, -350), // TODO fix hardcoded?
                    () => pokemonInfoMenu.Select(pokemon),
                    new ContextAction("Swap position", () => pokemonInfoMenu.BeginSwapPosition(pokemon)),
                    new ContextAction("Swap moves", pokemonInfoMenu.SelectFirstMove),
                    new ContextAction("Rename", () => { }),
                    new ContextAction("Take item", () =>
                    {
                        trainer.Bag.AddOrRemove(pokemon.HeldItem.Item.Name);
                        pokemon.HeldItem.Item = null;
                        pokemonInfoMenu.FillOut(pokemon);
                        pokemonInfoMenu.RefreshList(pokemon);
                    }, () => pokemon.HeldItem.Item != null),
                    new ContextAction("Damage 10 HP", () =>
                    {
                        pokemon.CurrentHp -= 10;
                        if (pokemon.Fainted)
                        {
                            pokemon.Status = Status.Fainted;
                        }

                        pokemonInfoMenu.RefreshList(pokemon);
                    })
                )
            };
        }

        public void HidePokemonMenu()
        {
            if (boxMenu.gameObject.activeSelf)
            {
                boxMenu.SelectFirst();
            }
            else
            {
                firstButton.Select();
            }

            pokemonInfoMenu.gameObject.SetActive(false);
            contextMenu.HideSilently();
        }

        public void ShowBagMenu()
        {
            bagMenu.SetData(trainer.Bag, trainer.Party, new BagMenuCustomization
            {
                OnClickItem = slot =>
                {
                    contextMenu.Show(
                        new Vector2(300, -350), // TODO fix hardcoded?
                        () => bagMenu.Select(slot),
                        new ContextAction("Use", () =>
                        {
                            // TODO direct use (repel, escape rope)
                            trainer.Bag.AddOrRemove(slot.Item.Name, -1);
                            // TODO close bag instead of this actually
                            bagMenu.RefreshView();
                        }, () => slot.Item.UsableOutOfBattle && slot.Item.AutoTargets),
                        new ContextAction("Use", () => bagMenu.SetItemAndAllowTargetSelection(slot),
                            () => slot.Item.UsableOutOfBattle && !slot.Item.AutoTargets),
                        new ContextAction("Teach TM", () => bagMenu.SetItemAndAllowTargetSelection(slot),
                            () => slot.Item.Category == ItemCategory.Tm),
                        new ContextAction("Use key item", () =>
                        {
                            // TODO handle key items
                        }, () => slot.Item.Category == ItemCategory.Key),
                        new ContextAction("Give", () =>
                            {
                                bagMenu.GiveMode = true;
                                bagMenu.SetItemAndAllowTargetSelection(slot);
                            },
                            () => !slot.Item.CannotBeGivenOrThrownAway),
                        new ContextAction("Replace Ball", () => bagMenu.SetItemAndAllowTargetSelection(slot),
                            () => slot.Item.Category == ItemCategory.PokeBall),
                        new ContextAction("Throw away", () =>
                        {
                            trainer.Bag.AddOrRemove(slot.Item.Name, -1);
                            bagMenu.RefreshView();
                        }, () => !slot.Item.CannotBeGivenOrThrownAway)
                    );
                },
                OnClickTarget = (itemSlot, target) =>
                {
                    Item item = itemSlot.Item;
                    if (item.Category == ItemCategory.Tm)
                    {
                        if (cutsceneManager.ItemManager.CanLearnTm(item, target))
                        {
                            string move = item.ExtractTmMove();
                            cutsceneManager.Enqueue(new LearnMoveMessage(target, move));
                            cutsceneManager.Enqueue(new LearnMove(target, move));
                            cutsceneManager.Enqueue(new ReselectBagSlot(bagMenu, itemSlot));
                            return;
                        }

                        cutsceneManager.Enqueue(new NoEffectMessage());
                        cutsceneManager.Enqueue(new ReselectBagSlot(bagMenu, itemSlot));
                        return;
                    }

                    if (bagMenu.GiveMode)
                    {
                        if (target.HeldItem.Item != null)
                        {
                            trainer.Bag.AddOrRemove(target.HeldItem.Item.Name);
                        }

                        target.HeldItem.Item = item;
                        trainer.Bag.AddOrRemove(item.Name, -1);
                        bagMenu.Select(itemSlot);
                        bagMenu.RefreshView();
                        bagMenu.RefreshList(target);
                        bagMenu.GiveMode = false;
                        return;
                    }

                    if (!cutsceneManager.ItemManager.ItemIsEffective(item, target))
                    {
                        cutsceneManager.Enqueue(new NoEffectMessage());
                        cutsceneManager.Enqueue(new ReselectBagSlot(bagMenu, itemSlot));
                        return;
                    }

                    trainer.Bag.AddOrRemove(item.Name, -1);
                    bagMenu.Select(itemSlot);
                    bagMenu.RefreshView();
                    Queue<CutsceneEvent> events = cutsceneManager.ItemManager.HandleItem(item, target);
                    while (events.Count > 0)
                    {
                        cutsceneManager.Enqueue(events.Dequeue());
                    }

                    cutsceneManager.Enqueue(new ReselectBagSlot(bagMenu, itemSlot));
                }
            });
            bagMenu.Show();
            contextMenu.HideSilently();
        }

        public void HideBagMenu()
        {
            firstButton.Select();
            bagMenu.gameObject.SetActive(false);
            contextMenu.HideSilently();
        }

        public void ShowBoxMenu()
        {
            FreezeButtons();
            boxMenu.SetData(trainer.Box, trainer.Party, new BoxMenuCustomization
            {
                OnClickPartyMember = pokemon => contextMenu.Show(
                    new Vector2(300, -350), // TODO fix hardcoded?
                    () => boxMenu.Select(pokemon),
                    new ContextAction("Info", () =>
                    {
                        pokemonInfoMenu.SetData(new Models.Trainer.Party(pokemon), GetPartyMenuCustomization());
                        pokemonInfoMenu.Show();
                    }),
                    new ContextAction("Deposit", () =>
                    {
                        if (trainer.Party.CountAvailable == 1)
                        {
                            cutsceneManager.Enqueue(new CantDepositLastHealthyPokemonMessage());
                            cutsceneManager.Enqueue(new ReselectBoxSlot(boxMenu, pokemon));
                            return;
                        }

                        boxMenu.Deposit(pokemon);
                    }),
                    new ContextAction("Take item", () =>
                    {
                        trainer.Bag.AddOrRemove(pokemon.HeldItem.Item.Name);
                        pokemon.HeldItem.Item = null;
                        boxMenu.RefreshList(pokemon);
                    }, () => pokemon.HeldItem.Item != null)
                ),
                OnClickBoxedPokemon = pokemon => contextMenu.Show(
                    new Vector2(300, -350), // TODO fix hardcoded?
                    () => boxMenu.Select(pokemon),
                    new ContextAction("Info", () =>
                    {
                        pokemonInfoMenu.SetData(new Models.Trainer.Party(pokemon), GetPartyMenuCustomization());
                        pokemonInfoMenu.Show();
                    }),
                    new ContextAction("Withdraw", () =>
                    {
                        if (trainer.Party.Count == 6)
                        {
                            cutsceneManager.Enqueue(new PartyIsFullMessage());
                            cutsceneManager.Enqueue(new ReselectBoxSlot(boxMenu, pokemon));
                            return;
                        }

                        boxMenu.Withdraw(pokemon);
                    }),
                    new ContextAction("Take item", () =>
                    {
                        trainer.Bag.AddOrRemove(pokemon.HeldItem.Item.Name);
                        pokemon.HeldItem.Item = null;
                        boxMenu.UpdateView();
                    }, () => pokemon.HeldItem.Item != null)
                )
            });
            boxMenu.Show();
            contextMenu.HideSilently();
        }

        public void HideBoxMenu()
        {
            UnfreezeButtons();
            firstButton.Select();
            boxMenu.gameObject.SetActive(false);
            contextMenu.HideSilently();
        }


        public void ShowQuestsMenu()
        {
            FreezeButtons();
            questsMenu.Show();
            contextMenu.HideSilently();
        }

        public void HideQuestsMenu()
        {
            UnfreezeButtons();
            firstButton.Select();
            questsMenu.gameObject.SetActive(false);
            contextMenu.HideSilently();
        }

        public void ShowOptionsMenu()
        {
            FreezeButtons();
            optionsMenu.Show();
            contextMenu.HideSilently();
        }

        public void HideOptionsMenu()
        {
            UnfreezeButtons();
            firstButton.Select();
            optionsMenu.gameObject.SetActive(false);
            contextMenu.HideSilently();
        }

        /// This menu cannot be closed while its submenus are open and vice versa, because it would mess with
        /// KeyActivatedButton's internal tracking of what to invoke when the key is pressed. Some menus
        /// use automatic navigation, which still sees this menu's buttons unless I make them non-interactable.
        private void FreezeButtons()
        {
            // TODO improve
            foreach (Button button in GetComponentsInChildren<Button>())
            {
                button.interactable = false;
            }
        }

        private void UnfreezeButtons()
        {
            // TODO improve
            foreach (Button button in GetComponentsInChildren<Button>())
            {
                button.interactable = true;
            }
        }

        public void Save()
        {
            SaveManager.Save(GlobalState.Instance, cutsceneManager.PlayerAbsolutePosition);
        }
    }
}