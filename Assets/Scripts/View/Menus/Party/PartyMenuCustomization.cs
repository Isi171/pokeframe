using System;
using Models.Pokemon;
using UnityEngine.Events;

namespace View.Menus.Party
{
    public class PartyMenuCustomization
    {
        public UnityAction OnClose { get; set; }
        public Action<Pokemon> OnClickPokemon { get; set; }
        public Action<MoveSlot> OnClickMoveSlot { get; set; }
        public string NewMove { get; set; }
    }
}