using System;
using Models.Pokemon;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace View.Menus.Party
{
    public class MoveInfoButtonController : MonoBehaviour
    {
        [SerializeField] private Button button;
        [SerializeField] private TypeIndicatorController typeIndicator;
        [SerializeField] private TMP_Text category;
        [SerializeField] private TMP_Text moveName;
        [SerializeField] private TMP_Text pp;
        [SerializeField] private TMP_Text power;
        [SerializeField] private TMP_Text accuracy;
        [SerializeField] private TMP_Text description;

        private Action<MoveSlot> clickCallback;

        public MoveSlot MoveSlot { get; private set; }

        public void Initialize(MoveSlot moveSlot, Action<MoveSlot> clickCallback, Selectable selectable)
        {
            SetMoveSlotAndDisplay(moveSlot);
            this.clickCallback = clickCallback;
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(() => clickCallback(moveSlot));
            button.navigation = new Navigation
            {
                mode = button.navigation.mode,
                selectOnUp = button.navigation.selectOnUp,
                selectOnDown = button.navigation.selectOnDown,
                selectOnRight = button.navigation.selectOnRight,
                selectOnLeft = selectable
            };
        }

        public void SetMoveSlotAndDisplay(MoveSlot moveSlot)
        {
            MoveSlot = moveSlot;
            if (!moveSlot.IsAssigned())
            {
                gameObject.SetActive(false);
                return;
            }

            gameObject.SetActive(true);
            typeIndicator.Set(moveSlot.Move.Type);
            category.text = moveSlot.Move.MoveType.ToString();
            moveName.text = moveSlot.Move.Name;
            pp.text = $"{moveSlot.CurrentPp}/{moveSlot.Move.Pp} PP";
            power.text = moveSlot.Move.BasePower > 0 ? $"Pow: {moveSlot.Move.BasePower}" : "Pow: ---";
            accuracy.text = moveSlot.Move.Accuracy > 0 ? $"Acc: {moveSlot.Move.Accuracy}" : "Acc: ---";
            description.text = moveSlot.Move.Description;
        }

        public void SetTemporaryClickCallback(Action<MoveSlot> temporaryClickCallback)
        {
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(() => temporaryClickCallback(MoveSlot));
        }

        public void RemoveTemporaryClickCallback()
        {
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(() => clickCallback(MoveSlot));
        }

        public void HighlightDuringSwap(Color color)
        {
            button.image.color = color;
        }

        public void ResetColor()
        {
            button.image.color = Color.white;
        }

        public void Select() => button.Select();
    }
}