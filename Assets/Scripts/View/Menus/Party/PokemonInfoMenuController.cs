using System;
using Managers.Data;
using Models;
using Models.Pokemon;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Utils;
using View.Menus.List;

namespace View.Menus.Party
{
    public class PokemonInfoMenuController : MonoBehaviour
    {
        private static readonly Color Neutral = Color.black;
        private static readonly Color Lowered = new Color(0f, 0.25f, 1f);
        private static readonly Color Raised = new Color(1f, 0.2f, 0f);

        [SerializeField] private Color highlightDuringSwapColor;
        [SerializeField] private UnityEvent onClose;

        [SerializeField] private PartyListSubmenuController partyList;
        [SerializeField] private Button backButton;

        [SerializeField] private TMP_Text pokemonName;
        [SerializeField] private TMP_Text gender;
        [SerializeField] private Image sprite;
        [SerializeField] private Image ball;

        [SerializeField] private TypeIndicatorController typeIndicator1;
        [SerializeField] private TypeIndicatorController typeIndicator2;
        [SerializeField] private TMP_Text species;
        [SerializeField] private TMP_Text dexNo;
        [SerializeField] private TMP_Text originalTrainer;
        [SerializeField] private TMP_Text idNo;
        [SerializeField] private TMP_Text happiness;
        [SerializeField] private TMP_Text met;

        [SerializeField] private TMP_Text nature;
        [SerializeField] private TMP_Text hp;
        [SerializeField] private TMP_Text hpIv;
        [SerializeField] private TMP_Text hpEv;
        [SerializeField] private TMP_Text attackLabel;
        [SerializeField] private TMP_Text attack;
        [SerializeField] private TMP_Text attackIv;
        [SerializeField] private TMP_Text attackEv;
        [SerializeField] private TMP_Text defenseLabel;
        [SerializeField] private TMP_Text defense;
        [SerializeField] private TMP_Text defenseIv;
        [SerializeField] private TMP_Text defenseEv;
        [SerializeField] private TMP_Text specialAttackLabel;
        [SerializeField] private TMP_Text specialAttack;
        [SerializeField] private TMP_Text specialAttackIv;
        [SerializeField] private TMP_Text specialAttackEv;
        [SerializeField] private TMP_Text specialDefenseLabel;
        [SerializeField] private TMP_Text specialDefense;
        [SerializeField] private TMP_Text specialDefenseIv;
        [SerializeField] private TMP_Text specialDefenseEv;
        [SerializeField] private TMP_Text speedLabel;
        [SerializeField] private TMP_Text speed;
        [SerializeField] private TMP_Text speedIv;
        [SerializeField] private TMP_Text speedEv;

        [SerializeField] private TMP_Text level;
        [SerializeField] private TMP_Text totalExp;
        [SerializeField] private TMP_Text toNextLevel;
        [SerializeField] private ExperienceBarController experienceBar;

        [SerializeField] private TMP_Text abilityName;
        [SerializeField] private TMP_Text abilityDescription;

        [SerializeField] private TMP_Text itemName;
        [SerializeField] private TMP_Text itemDescription;
        [SerializeField] private Image itemSprite;

        [SerializeField] private MoveInfoButtonController newMoveInfoButton;
        [SerializeField] private MoveInfoButtonController[] moveInfoButtons;

        private PartyMenuCustomization defaults;
        private PartyMenuCustomization customization;
        private Pokemon currentPokemon;

        public void SetData(Models.Trainer.Party toDisplay, PartyMenuCustomization customization)
        {
            defaults = new PartyMenuCustomization
            {
                OnClose = onClose.Invoke,
                OnClickPokemon = _ => { },
                OnClickMoveSlot = BeginSwapMove
            };

            this.customization = customization;
            partyList.SetParty(toDisplay, FillOut, customization.OnClickPokemon ?? defaults.OnClickPokemon);
        }

        public void Show(bool closeable = true)
        {
            gameObject.SetActive(true);
            backButton.gameObject.SetActive(closeable);
            backButton.onClick.RemoveAllListeners();
            backButton.onClick.AddListener(customization.OnClose ?? defaults.OnClose);
            partyList.SelectFirst();
        }

        public void Select(Pokemon pokemon)
        {
            partyList[pokemon].Select();
        }

        public void RefreshList(Pokemon pokemon)
        {
            partyList[pokemon].Display();
        }

        public void SelectFirstMove()
        {
            moveInfoButtons[0].Select();
        }

        public void BeginSwapPosition(Pokemon selected)
        {
            partyList.BeginSwapPosition(selected, FillOut);
        }

        private void BeginSwapMove(MoveSlot selected)
        {
            for (int i = 0; i < 4; i++)
            {
                moveInfoButtons[i].SetTemporaryClickCallback(target => SwapMove(selected, target));

                if (moveInfoButtons[i].MoveSlot == selected)
                {
                    moveInfoButtons[i].HighlightDuringSwap(highlightDuringSwapColor);
                }
            }
        }

        private void SwapMove(MoveSlot selected, MoveSlot target)
        {
            currentPokemon.Moves.Swap(selected, target);

            for (int i = 0; i < 4; i++)
            {
                moveInfoButtons[i].ResetColor();
                moveInfoButtons[i].RemoveTemporaryClickCallback();
                moveInfoButtons[i].SetMoveSlotAndDisplay(currentPokemon.Moves[i]);
            }
        }

        public void FillOut(Pokemon pokemon)
        {
            currentPokemon = pokemon;

            int dexNumber = DataAccess.PokemonBaseData.Get(pokemon.Species).NationalDexNumber;

            pokemonName.text = pokemon.Name;
            sprite.sprite = SpriteUtils.LoadSprite(pokemon.Species, pokemon.Gender, pokemon.Shiny, false);
            ball.sprite = SpriteUtils.LoadItem(pokemon.Ball);

            typeIndicator1.Set(pokemon.Type1);
            typeIndicator2.Set(pokemon.Type2);
            species.text = pokemon.Species;
            (gender.text, gender.color) = ViewUtils.GetGender(pokemon.Gender);
            dexNo.text = dexNumber.ToString();
            dexNo.color = pokemon.Shiny ? Color.red : Color.black;
            originalTrainer.text = pokemon.Ownership.OriginalTrainerName;
            idNo.text = pokemon.Ownership.OriginalTrainerId.Length > 6
                ? pokemon.Ownership.OriginalTrainerId.Substring(0, 6)
                : pokemon.Ownership.OriginalTrainerId;
            happiness.text = $"{pokemon.Happiness}/255";
            met.text = $"Met in {pokemon.Ownership.MetLocation} at level {pokemon.Ownership.MetLevel}";

            nature.text = pokemon.Nature.Name;
            hp.text = pokemon.Hp.Derived.ToString();
            hpIv.text = pokemon.Hp.Iv.ToString();
            hpEv.text = pokemon.Hp.Ev.ToString();
            attack.text = pokemon.Attack.Derived.ToString();
            attackLabel.color = ColorFromNature(StatName.Attack, pokemon.Nature);
            attackIv.text = pokemon.Attack.Iv.ToString();
            attackEv.text = pokemon.Attack.Ev.ToString();
            defense.text = pokemon.Defense.Derived.ToString();
            defenseLabel.color = ColorFromNature(StatName.Defense, pokemon.Nature);
            defenseIv.text = pokemon.Defense.Iv.ToString();
            defenseEv.text = pokemon.Defense.Ev.ToString();
            specialAttack.text = pokemon.SpecialAttack.Derived.ToString();
            specialAttackLabel.color = ColorFromNature(StatName.SpecialAttack, pokemon.Nature);
            specialAttackIv.text = pokemon.SpecialAttack.Iv.ToString();
            specialAttackEv.text = pokemon.SpecialAttack.Ev.ToString();
            specialDefense.text = pokemon.SpecialDefense.Derived.ToString();
            specialDefenseLabel.color = ColorFromNature(StatName.SpecialDefense, pokemon.Nature);
            specialDefenseIv.text = pokemon.SpecialDefense.Iv.ToString();
            specialDefenseEv.text = pokemon.SpecialDefense.Ev.ToString();
            speed.text = pokemon.Speed.Derived.ToString();
            speedLabel.color = ColorFromNature(StatName.Speed, pokemon.Nature);
            speedIv.text = pokemon.Speed.Iv.ToString();
            speedEv.text = pokemon.Speed.Ev.ToString();

            level.text = pokemon.Level.ToString();
            totalExp.text = $"{pokemon.Experience.TotalExp:n0}";
            toNextLevel.text = pokemon.Experience.CurrentToNextLevel == -1
                ? "--"
                : $"{pokemon.Experience.CurrentToNextLevel:n0}";
            experienceBar.UpdateExperienceBar(pokemon.Experience.CurrentInExcessOfLevel,
                pokemon.Experience.TotalToNextLevel);

            abilityName.text = pokemon.Ability.Name;
            abilityDescription.text = pokemon.Ability.Description;

            itemName.text = pokemon.HeldItem.Item?.Name ?? "None";
            itemDescription.text = pokemon.HeldItem.Item?.Description ?? "--";
            if (pokemon.HeldItem.Item == null)
            {
                itemSprite.gameObject.SetActive(false);
            }
            else
            {
                itemSprite.gameObject.SetActive(true);
                itemSprite.sprite = SpriteUtils.LoadItem(pokemon.HeldItem.Item.Name);
            }

            for (int i = 0; i < 4; i++)
            {
                moveInfoButtons[i].Initialize(
                    pokemon.Moves[i],
                    customization.OnClickMoveSlot ?? defaults.OnClickMoveSlot,
                    partyList[pokemon].Selectable);
                moveInfoButtons[i].ResetColor();
            }

            if (customization.NewMove != null)
            {
                newMoveInfoButton.Initialize(new MoveSlot(customization.NewMove), slot => { }, null);
            }
            else
            {
                newMoveInfoButton.Initialize(new MoveSlot(), slot => { }, null);
            }
        }

        private Color ColorFromNature(StatName stat, Nature nature)
        {
            if (nature.Lowered == nature.Raised)
            {
                return Neutral;
            }

            if (nature.Lowered == stat)
            {
                return Lowered;
            }

            if (nature.Raised == stat)
            {
                return Raised;
            }

            return Neutral;
        }
    }
}