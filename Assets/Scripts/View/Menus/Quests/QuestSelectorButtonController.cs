using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace View.Menus.Quests
{
    public class QuestSelectorButtonController : MonoBehaviour
    {
        [SerializeField] private Button button;
        [SerializeField] private TMP_Text text;

        public void SetActive(string quest, UnityAction showQuest)
        {
            gameObject.SetActive(true);
            text.text = quest;
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(showQuest);
        }

        public void SetInactive()
        {
            gameObject.SetActive(false);
        }
    }
}