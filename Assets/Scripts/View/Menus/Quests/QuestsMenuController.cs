using System.Collections.Generic;
using System.Linq;
using Managers.Data;
using Models.Quests;
using Models.State;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace View.Menus.Quests
{
    public class QuestsMenuController : MonoBehaviour
    {
        [SerializeField] private Selectable firstButton;
        [SerializeField] private Transform selectorContainer;
        [SerializeField] private TMP_Text currentObjectiveText;

        public void Show()
        {
            gameObject.SetActive(true);
            ShowActiveQuests();
            SelectFirst();
        }

        private void SelectFirst()
        {
            if (selectorContainer.GetChild(0).gameObject.activeSelf)
            {
                selectorContainer.GetChild(0).GetComponent<Selectable>().Select();
            }
            else
            {
                firstButton.Select();
            }
        }

        public void ShowActiveQuests()
        {
            ShowQuests(GlobalState.Instance.Journal.Active);
        }

        public void ShowCompleteQuests()
        {
            ShowQuests(GlobalState.Instance.Journal.Complete);
        }

        private void ShowQuests(List<QuestProgress> progresses)
        {
            for (int i = 0; i < selectorContainer.childCount; i++)
            {
                QuestSelectorButtonController selector = selectorContainer.GetChild(i)
                    .GetComponent<QuestSelectorButtonController>();

                if (i >= progresses.Count)
                {
                    selector.SetInactive();
                }
                else
                {
                    QuestProgress progress = progresses[i];
                    Quest quest = DataAccess.Quests.Get(progress.Quest);
                    selector.SetActive(quest.Name, () => ShowQuestObjective(quest, progress));
                }
            }

            if (progresses.Count == 0)
            {
                currentObjectiveText.text = "";
            }
            else
            {
                QuestProgress progress = progresses.First();
                Quest quest = DataAccess.Quests.Get(progress.Quest);
                ShowQuestObjective(quest, progress);
            }
        }

        private void ShowQuestObjective(Quest quest, QuestProgress progress)
        {
            currentObjectiveText.text = quest.Stages[progress.Stage].Description;
        }
    }
}