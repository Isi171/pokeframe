using System.Collections;
using Managers.Data;
using Managers.State;
using Models.State;
using UnityEngine;
using UnityEngine.UI;

namespace View.State
{
    public class MainMenuController : MonoBehaviour
    {
        [SerializeField] private Button newGameButton;
        [SerializeField] private Button continueButton;
        [SerializeField] private Button deleteSaveButton;

        private void Start()
        {
            DataAccess.LoadAll();

            if (!SaveManager.SaveExists())
            {
                NewGameInternal(false);
                return;
            }

            continueButton.Select();
            StartCoroutine(ShowScreenCoroutine());
        }

        private IEnumerator ShowScreenCoroutine()
        {
            TransitionEffectController battleTransitionEffect = GameObject.FindGameObjectWithTag("TransitionEffect")
                .GetComponent<TransitionEffectController>();
            battleTransitionEffect.UncoverTheScreen();
            yield return new WaitUntil(() => battleTransitionEffect.TransitionDone);
        }

        public void NewGame()
        {
            NewGameInternal(true);
        }

        private void NewGameInternal(bool doTransition)
        {
            NewGame initialState = DataAccess.NewGame;
            GlobalState globalState = GlobalState.Instance;
            globalState.PlayerTrainer = initialState.Player;
            globalState.LastSafeSpot = initialState.SpawnPoint;

            TransitionManager.Instance.StartGame(initialState.SpawnPoint, doTransition);
        }

        public void Continue()
        {
            AbsolutePosition spawnPoint = SaveManager.Load();
            TransitionManager.Instance.StartGame(spawnPoint, true);
        }

        public void DeleteSave()
        {
            SaveManager.DeleteSave();
            newGameButton.Select();
            continueButton.interactable = false;
            deleteSaveButton.interactable = false;
        }
    }
}