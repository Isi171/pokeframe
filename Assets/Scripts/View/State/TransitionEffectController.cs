using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace View.State
{
    public class TransitionEffectController : MonoBehaviour
    {
        [SerializeField] private float fadeTimeSeconds;
        [SerializeField] private Image fadePanel;
        [SerializeField] private float battleTransitionTimeSeconds;
        [SerializeField] private RectTransform rising;
        [SerializeField] private RectTransform lowering;

        public bool TransitionDone { get; private set; } = false;

        public void UncoverTheScreen()
        {
            fadePanel.canvasRenderer.SetAlpha(1f);
            fadePanel.CrossFadeAlpha(0f, fadeTimeSeconds, false);
            StartCoroutine(TransitionTimeoutCoroutine(fadeTimeSeconds));
        }

        public void CoverTheScreen()
        {
            fadePanel.canvasRenderer.SetAlpha(0f);
            fadePanel.CrossFadeAlpha(1f, fadeTimeSeconds, false);
            StartCoroutine(TransitionTimeoutCoroutine(fadeTimeSeconds));
        }

        public void CoverTheScreenBattle()
        {
            StartCoroutine(MovePanelToCoverCoroutine(rising, rising.rect.height, battleTransitionTimeSeconds));
            StartCoroutine(MovePanelToCoverCoroutine(lowering, -lowering.rect.height, battleTransitionTimeSeconds));
            StartCoroutine(TransitionTimeoutCoroutine(battleTransitionTimeSeconds));
        }

        public void UncoverTheScreenBattle()
        {
            fadePanel.canvasRenderer.SetAlpha(0f);
            StartCoroutine(MovePanelToUncoverCoroutine(rising, rising.rect.height, battleTransitionTimeSeconds));
            StartCoroutine(MovePanelToUncoverCoroutine(lowering, -lowering.rect.height, battleTransitionTimeSeconds));
            StartCoroutine(TransitionTimeoutCoroutine(battleTransitionTimeSeconds));
        }

        private IEnumerator TransitionTimeoutCoroutine(float time)
        {
            TransitionDone = false;
            yield return new WaitForSeconds(time);
            TransitionDone = true;
        }

        private IEnumerator MovePanelToCoverCoroutine(RectTransform panel, float length, float time)
        {
            while (Math.Abs(panel.anchoredPosition.y) <= Math.Abs(length))
            {
                yield return null;
                float increment = Time.deltaTime * length / time;
                panel.anchoredPosition += new Vector2(0f, increment);
            }

            panel.anchoredPosition = new Vector2(0f, length);
        }

        private IEnumerator MovePanelToUncoverCoroutine(RectTransform panel, float length, float time)
        {
            while (Math.Abs(panel.anchoredPosition.y) >= 0)
            {
                yield return null;
                float increment = Time.deltaTime * length / time;
                panel.anchoredPosition -= new Vector2(0f, increment);
            }

            panel.anchoredPosition = new Vector2(0f, 0);
        }
    }
}