using System.Collections;
using System.Linq;
using Managers.Cutscene;
using Models.Dialogue;
using TMPro;
using UnityEngine;
using UnityEngine.Experimental.U2D.Animation;
using UnityEngine.UI;

namespace View.Dialogue
{
    public class DialogueBoxController : MonoBehaviour
    {
        [SerializeField] private SpriteLibraryAsset portraits;
        [SerializeField] private CutsceneManager cutsceneManager;
        [SerializeField] private TMP_Text text;
        [SerializeField] private GameObject awaitingInputIndicator;
        [SerializeField] private GameObject actorNamePanel;
        [SerializeField] private TMP_Text actorName;
        [SerializeField] private GameObject portraitPanel;
        [SerializeField] private Image portraitImage;
        [SerializeField] private AnswerButtonController[] answerButtons;
        [SerializeField] private Button invisibleAdvanceTextButton;

        public bool CanAdvance { get; set; }

        public void DisplayLine(string name, string portrait, string message, bool moreTextWillCome)
        {
            gameObject.SetActive(true);

            actorNamePanel.SetActive(!string.IsNullOrEmpty(name));
            actorName.text = name;
            portraitPanel.SetActive(!string.IsNullOrEmpty(portrait));
            if (!string.IsNullOrEmpty(portrait))
            {
                portraitImage.sprite = portraits.GetSprite("Portraits", portrait);
            }

            text.text = message;

            text.pageToDisplay = 1;

            invisibleAdvanceTextButton.Select();

            StartCoroutine(UpdateAwaitingInputIndicator(moreTextWillCome));
        }

        public void DisplayAnswers(GraphLink[] answers)
        {
            int startIndex = answerButtons.Length - answers.Length;
            for (int i = startIndex, currentAnswerIndex = 0; i < answerButtons.Length; i++, currentAnswerIndex++)
            {
                GraphLink answer = answers[currentAnswerIndex];
                answerButtons[i].Show(answer.Line, () =>
                {
                    foreach (AnswerButtonController answerButton in answerButtons)
                    {
                        answerButton.Hide();
                    }

                    cutsceneManager.PushToFront(cutsceneManager.DialogueManager.StartNode(answer.LinksTo));
                    CanAdvance = true;
                });
            }

            answerButtons[startIndex].Select();
        }

        public void Close()
        {
            text.text = "";
            gameObject.SetActive(false);
        }

        public void AdvancePage()
        {
            if (text.pageToDisplay < text.textInfo.pageCount)
            {
                text.pageToDisplay++;

                if (text.pageToDisplay == text.textInfo.pageCount)
                {
                    awaitingInputIndicator.SetActive(false);
                }
            }
            else if (text.pageToDisplay == text.textInfo.pageCount && !answerButtons.Last().isActiveAndEnabled)
            {
                CanAdvance = true;
            }
        }

        private IEnumerator UpdateAwaitingInputIndicator(bool moreTextWillCome)
        {
            awaitingInputIndicator.SetActive(moreTextWillCome);

            // This next part must be done in the next frame to allow
            // time for the text object to page the received string
            yield return null;

            if (text.textInfo.pageCount > 1)
            {
                awaitingInputIndicator.SetActive(true);
            }
        }
    }
}