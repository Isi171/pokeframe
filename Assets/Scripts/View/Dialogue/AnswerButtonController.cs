using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace View.Dialogue
{
    public class AnswerButtonController : MonoBehaviour
    {
        [SerializeField] private Button button;
        [SerializeField] private TMP_Text text;

        public void Show(string text, UnityAction lambda)
        {
            gameObject.SetActive(true);

            this.text.text = text;
            button.onClick.AddListener(lambda);
        }

        public void Hide()
        {
            button.onClick.RemoveAllListeners();

            gameObject.SetActive(false);
        }

        public void Select() => button.Select();
    }
}