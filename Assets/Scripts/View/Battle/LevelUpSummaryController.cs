﻿using Models.Pokemon;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace View.Battle
{
    public class LevelUpSummaryController : MonoBehaviour
    {
        [SerializeField] private Image icon;
        [SerializeField] private TMP_Text pokemonName;
        [SerializeField] private TMP_Text hpTotal;
        [SerializeField] private TMP_Text hpIncrease;
        [SerializeField] private TMP_Text attackTotal;
        [SerializeField] private TMP_Text attackIncrease;
        [SerializeField] private TMP_Text defenseTotal;
        [SerializeField] private TMP_Text defenseIncrease;
        [SerializeField] private TMP_Text specialAttackTotal;
        [SerializeField] private TMP_Text specialAttackIncrease;
        [SerializeField] private TMP_Text specialDefenseTotal;
        [SerializeField] private TMP_Text specialDefenseIncrease;
        [SerializeField] private TMP_Text speedTotal;
        [SerializeField] private TMP_Text speedIncrease;

        public void Display(Pokemon pokemon, int[] oldStats, bool rising)
        {
            gameObject.SetActive(true);

            icon.sprite = SpriteUtils.LoadIcon(pokemon.Species);
            pokemonName.text = pokemon.Name;

            FormatAndDisplay(rising, pokemon.Hp.Derived, oldStats[0], hpTotal, hpIncrease);
            FormatAndDisplay(rising, pokemon.Attack.Derived, oldStats[1], attackTotal, attackIncrease);
            FormatAndDisplay(rising, pokemon.Defense.Derived, oldStats[2], defenseTotal, defenseIncrease);
            FormatAndDisplay(rising, pokemon.SpecialAttack.Derived, oldStats[3], specialAttackTotal,
                specialAttackIncrease);
            FormatAndDisplay(rising, pokemon.SpecialDefense.Derived, oldStats[4], specialDefenseTotal,
                specialDefenseIncrease);
            FormatAndDisplay(rising, pokemon.Speed.Derived, oldStats[5], speedTotal, speedIncrease);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        private static void FormatAndDisplay(bool rising, int newValue, int oldValue, TMP_Text total, TMP_Text diff)
        {
            total.text = $"{newValue}";
            diff.text = (newValue - oldValue).ToString(rising ? "+#;-#;+0" : "+#;-#;-0");
        }
    }
}