using Managers.Battle;
using Models.Pokemon;
using UnityEngine;
using UnityEngine.UI;

namespace View.Battle
{
    public class MovesMenuController : MonoBehaviour
    {
        [SerializeField] private Button firstButton;
        [SerializeField] private MoveButtonController[] moves;

        private PlayerDecisionModule decisionModule;

        private void OnEnable()
        {
            firstButton.Select();
        }

        public void Initialize(PlayerDecisionModule decisionModule, MoveSlot[] moveSlots)
        {
            this.decisionModule = decisionModule;

            for (int i = 0; i < 4; i++)
            {
                moves[i].Initialize(decisionModule, moveSlots[i].IsAssigned() ? moveSlots[i] : null);
            }
        }

        public bool IsStruggling() => decisionModule.IsStruggling();
        public void SelectStruggle() => decisionModule.SelectStruggle();
    }
}