using Models.Trainer;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace View.Battle
{
    public class PokemonCounterController : MonoBehaviour
    {
        [SerializeField] private Sprite okSprite;
        [SerializeField] private Sprite koSprite;
        [SerializeField] private TMP_Text trainerName;
        [SerializeField] private Image[] balls;

        public Trainer Trainer { private get; set; }

        public void Show()
        {
            if (Trainer == null)
            {
                return;
            }

            gameObject.SetActive(true);
            UpdateIndicators();
        }

        public void Hide()
        {
            if (Trainer == null)
            {
                return;
            }

            gameObject.SetActive(false);
        }

        private void UpdateIndicators()
        {
            trainerName.text = Trainer.Name;

            for (int i = 0; i < 6; i++)
            {
                if (Trainer.Party.Get(i) == null)
                {
                    balls[i].enabled = false;
                }
                else if (Trainer.Party.Get(i).Fainted)
                {
                    balls[i].enabled = true;
                    balls[i].sprite = koSprite;
                }
                else
                {
                    balls[i].enabled = true;
                    balls[i].sprite = okSprite;
                }
            }
        }
    }
}