using System;
using Models;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace View.Battle
{
    public class WeatherIndicatorController : MonoBehaviour
    {
        private static readonly Color RainColor = new Color(0.1f, 0.4f, 1f);
        private static readonly Color SunColor = new Color(1f, 0.5f, 0.1f);
        private static readonly Color HailColor = new Color(0.3f, 0.9f, 1f);
        private static readonly Color SandstormColor = new Color(0.6f, 0.4f, 0f);
    
        [SerializeField] private Image background;
        [SerializeField] private TMP_Text label;

        public void SetWeather(Weather weather)
        {
            if (weather == Weather.Clear)
            {
                gameObject.SetActive(false);
                return;
            }
            
            gameObject.SetActive(true);
            
            switch (weather)
            {
                case Weather.Rain:
                    background.color = RainColor;
                    label.text = "Rain";
                    break;
                case Weather.Sun:
                    background.color = SunColor;
                    label.text = "Harsh sunlight";
                    break;
                case Weather.Hail:
                    background.color = HailColor;
                    label.text = "Hail";
                    break;
                case Weather.Sandstorm:
                    background.color = SandstormColor;
                    label.text = "Sand storm";
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(weather), weather, null);
            }
        }
    }
}