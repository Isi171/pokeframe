using System;
using Models;
using TMPro;
using UnityEngine;
using View.Menus;
using Type = Models.Type;

namespace View.Battle
{
    public class BattlerInfoController : PokemonCoreInfoController
    {
        private static readonly Color StatRisenColor = new Color(0.1288057f, 0.8396226f, 0f);
        private static readonly Color StatLoweredColor = new Color(1f, 0f, 0f);

        [SerializeField] private bool showExperienceBar;
        [SerializeField] private ExperienceBarController experienceBarController;
        [SerializeField] private GameObject shinyIndicator;
        [SerializeField] private GameObject ownedIndicator;
        [SerializeField] private TypeIndicatorController typeIndicator1;
        [SerializeField] private TypeIndicatorController typeIndicator2;
        [SerializeField] private TMP_Text atkIndicator;
        [SerializeField] private TMP_Text defIndicator;
        [SerializeField] private TMP_Text spaIndicator;
        [SerializeField] private TMP_Text spdIndicator;
        [SerializeField] private TMP_Text speIndicator;
        [SerializeField] private TMP_Text evaIndicator;
        [SerializeField] private TMP_Text accIndicator;

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void SetBattler(string name, Gender gender, int level, int maxHp, int currentHp, Status status,
            bool shiny, bool owned, Type type1, Type type2)
        {
            gameObject.SetActive(true);
            experienceBarController.gameObject.SetActive(showExperienceBar);
            shinyIndicator.SetActive(shiny);
            ownedIndicator.SetActive(owned);
            typeIndicator1.Set(type1);
            typeIndicator2.Set(type2);
            SetInfo(name, gender, level, maxHp, currentHp, status);
        }

        public void UpdateStatIndicators(int attack, int defense, int specialAttack, int specialDefense, int speed,
            int evasion, int accuracy)
        {
            SetStatIndicator(atkIndicator, "Att", attack);
            SetStatIndicator(defIndicator, "Def", defense);
            SetStatIndicator(spaIndicator, "SpA", specialAttack);
            SetStatIndicator(spdIndicator, "SpD", specialDefense);
            SetStatIndicator(speIndicator, "Spe", speed);
            SetStatIndicator(evaIndicator, "Eva", evasion);
            SetStatIndicator(accIndicator, "Acc", accuracy);
        }

        private static void SetStatIndicator(TMP_Text indicator, string stat, int stage)
        {
            if (stage == 0)
            {
                indicator.gameObject.SetActive(false);
                return;
            }

            indicator.gameObject.SetActive(true);
            string plus = stage > 0 ? "+" : "";
            indicator.text = $"{stat}{plus}{stage}";
            indicator.color = stage > 0 ? StatRisenColor : StatLoweredColor;
        }

        public void SetExpBarUpdatedCallback(Action animationDoneCallback)
        {
            experienceBarController.SetExpBarUpdatedCallback(animationDoneCallback);
        }

        public void UpdateExperienceBar(int currentExpToNextLevel, int totalExpToNextLevel)
        {
            experienceBarController.UpdateExperienceBar(currentExpToNextLevel, totalExpToNextLevel);
        }

        public void UpdateExperienceBarAnimated(int currentExpToNextLevel, int totalExpToNextLevel)
        {
            experienceBarController.UpdateExperienceBarAnimated(currentExpToNextLevel, totalExpToNextLevel);
        }
    }
}