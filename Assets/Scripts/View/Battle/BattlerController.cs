﻿using System;
using Managers.Battle;
using Models.Battle;
using Models.Pokemon;
using Models.Trainer;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace View.Battle
{
    public class BattlerController : MonoBehaviour
    {
        private static readonly int Move = Animator.StringToHash("move");
        private static readonly int Damage = Animator.StringToHash("damage");
        private static readonly int StatChange = Animator.StringToHash("statChange");
        private static readonly int Status = Animator.StringToHash("status");

        [SerializeField] private bool backSprite;
        [SerializeField] private Image image;
        [SerializeField] private Animator animator;
        [SerializeField] private BattlerInfoController battlerInfo;
        [SerializeField] private DecisionModule decisionModule;
        [SerializeField] private Button selectionButton;
        public DecisionModule DecisionModule => decisionModule;
        public Button SelectionButton => selectionButton;
        public Image Image => image;
        public Trainer Trainer { set; get; }
        public Pokemon Pokemon { get; private set; }

        private Action animationDoneCallback;

        public void Initialize(Action animationDoneCallback, BattleManager battleManager)
        {
            this.animationDoneCallback = animationDoneCallback;
            battlerInfo.SetHpBarUpdatedCallback(animationDoneCallback);
            battlerInfo.SetExpBarUpdatedCallback(animationDoneCallback);

            (decisionModule as AiDecisionModule)?.Initialize(battleManager);
        }

        public void HideBattlerAndInfo()
        {
            gameObject.SetActive(false);
            battlerInfo.Hide();
        }

        public void SetPokemon(Pokemon pokemon)
        {
            Pokemon = pokemon;
            battlerInfo.SetBattler(
                Pokemon.Name,
                Pokemon.Gender,
                Pokemon.Level,
                Pokemon.Hp.Derived,
                Pokemon.CurrentHp,
                Pokemon.Status,
                Pokemon.Shiny,
                false,
                Pokemon.Type1,
                Pokemon.Type2);
            battlerInfo.SetStatus(Pokemon.Status);
            image.sprite = SpriteUtils.LoadSprite(Pokemon.Species, Pokemon.Gender, pokemon.Shiny, backSprite);
        }

        public void UpdateHpBar()
        {
            battlerInfo.SetHp(Pokemon.CurrentHp);
        }

        public void UpdateStatusIndicator()
        {
            battlerInfo.SetStatus(Pokemon.Status);
        }

        public void UpdateStatIndicators()
        {
            battlerInfo.UpdateStatIndicators(
                Pokemon.VolatilePokemon.Attack.Stage.Value,
                Pokemon.VolatilePokemon.Defense.Stage.Value,
                Pokemon.VolatilePokemon.SpecialAttack.Stage.Value,
                Pokemon.VolatilePokemon.SpecialDefense.Stage.Value,
                Pokemon.VolatilePokemon.Speed.Stage.Value,
                Pokemon.VolatilePokemon.EvasionStage.Value,
                Pokemon.VolatilePokemon.AccuracyStage.Value);
        }

        public void UpdateLevel()
        {
            battlerInfo.UpdateLevel(Pokemon.Level);
        }

        public void UpdateExperienceBarInstant()
        {
            battlerInfo.UpdateExperienceBar(Pokemon.Experience.CurrentInExcessOfLevel,
                Pokemon.Experience.TotalToNextLevel);
        }

        public void UpdateExperienceBarAnimated()
        {
            battlerInfo.UpdateExperienceBarAnimated(Pokemon.Experience.CurrentInExcessOfLevel,
                Pokemon.Experience.TotalToNextLevel);
        }

        public void StartMoveAnimation()
        {
            animator.SetTrigger(Move);
        }

        public void StartReceiveDamageAnimation()
        {
            animator.SetTrigger(Damage);
        }

        public void StartStatusAnimation()
        {
            animator.SetTrigger(Status);
        }

        public void StartStatChangeAnimation()
        {
            animator.SetTrigger(StatChange);
        }

        /// Called by animator
        public void AnimationDone()
        {
            animationDoneCallback();
        }
    }
}