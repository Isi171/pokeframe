using System.Linq;
using Models.Battle;
using Models.Pokemon;
using UnityEngine;
using Utils;

namespace View.Battle
{
    public class EnemyTrainerSliderController : TrainerSliderController
    {
        protected override void ResolveImage()
        {
            if (BattleData.TrainerBattle)
            {
                SpriteName1 = BattleData.Enemy1.Sprite;
                SpriteName2 = BattleData.Enemy2?.Sprite;
                base.ResolveImage();
            }
            else
            {
                Pokemon pokemon = BattleData.Enemy1.Party.FirstAvailable();
                Image1 = wildPokemonImage;
                Image1.sprite = SpriteUtils.LoadSprite(pokemon.Species, pokemon.Gender, pokemon.Shiny, false);
                Image1.enabled = true;
                Image1.color = new Color(0.4f, 0.4f, 0.4f);
            }
        }

        public override void SlideIn(BattleData battleData)
        {
            base.SlideIn(battleData);
            Direction = Vector3.right;
        }

        public override void SlideOut()
        {
            if (!BattleData.TrainerBattle)
            {
                Image1.enabled = false;
                return;
            }

            base.SlideOut();
            Direction = Vector3.right;
        }

        public override void SlideBackIn()
        {
            base.SlideBackIn();
            Direction = Vector3.left;
        }
    }
}