using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace View.Battle
{
    public class MessageBoxController : MonoBehaviour
    {
        [SerializeField] private TMP_Text text;
        [SerializeField] private Image awaitingInputIndicator;

        private bool listening;
        private Action callback;

        public void Display(string message)
        {
            text.text = message;
        }

        public void Clear()
        {
            text.text = "";
        }

        public void WaitForInput(Action callback)
        {
            listening = true;
            awaitingInputIndicator.gameObject.SetActive(true);
            this.callback = callback;
        }

        private void InputReceived()
        {
            listening = false;
            awaitingInputIndicator.gameObject.SetActive(false);
            callback();
        }

        private void Update()
        {
            if (!listening)
            {
                return;
            }

            if (Input.GetMouseButtonDown(0) || Input.GetButtonDown("Submit"))
            {
                InputReceived();
            }
        }
    }
}