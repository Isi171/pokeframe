﻿using Managers.Battle;
using Models.Pokemon;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace View.Battle
{
    public class MoveButtonController : MonoBehaviour
    {
        [SerializeField] private Button button;
        [SerializeField] private TMP_Text moveName;
        [SerializeField] private TMP_Text pp;
        [SerializeField] private TMP_Text type;
        [SerializeField] private TMP_Text moveType;

        private PlayerDecisionModule decisionModule;
        private MoveSlot moveSlot;

        public void Initialize(PlayerDecisionModule decisionModule, MoveSlot moveSlot)
        {
            this.decisionModule = decisionModule;

            if (moveSlot != null)
            {
                this.moveSlot = moveSlot;
                button.interactable = true;

                moveName.text = moveSlot.Move.Name;
                pp.text = moveSlot.CurrentPp + "/" + moveSlot.Move.Pp;
                type.text = moveSlot.Move.Type.ToString();
                moveType.text = moveSlot.Move.MoveType.ToString();
            }
            else
            {
                button.interactable = false;

                moveName.text = "----";
                pp.text = "--/--";
                type.text = "--";
                moveType.text = "--";
            }
        }

        public void Select()
        {
            if (moveSlot.CurrentPp > 0)
            {
                decisionModule.Select(moveSlot.Move);
            }
        }
    }
}