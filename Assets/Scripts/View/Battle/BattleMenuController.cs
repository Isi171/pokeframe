using System;
using Managers.Battle;
using Models;
using Models.Pokemon;
using Models.Trainer;
using UnityEngine;
using UnityEngine.UI;
using Utils;
using View.Menus.Items;
using View.Menus.Party;

namespace View.Battle
{
    public class BattleMenuController : MonoBehaviour
    {
        [SerializeField] private Button firstButton;
        [SerializeField] private BattleManager battleManager;
        [SerializeField] private MovesMenuController movesMenu;
        [SerializeField] private MessageBoxController messageBox;
        [SerializeField] private PokemonInfoMenuController pokemonInfoMenu;
        [SerializeField] private BagMenuController bagMenu;

        private Action runCallback;
        public BattleStrings Strings { private get; set; }

        private void OnEnable()
        {
            firstButton.Select();
        }

        public void Hide()
        {
            gameObject.SetActive(false);
            movesMenu.gameObject.SetActive(false);
            pokemonInfoMenu.gameObject.SetActive(false);
            bagMenu.gameObject.SetActive(false);
        }

        public void Show(PlayerDecisionModule decisionModule, Pokemon pokemon, Trainer trainer)
        {
            movesMenu.Initialize(decisionModule, pokemon.Moves);
            gameObject.SetActive(true);
            movesMenu.gameObject.SetActive(false);
            pokemonInfoMenu.gameObject.SetActive(false);
            bagMenu.gameObject.SetActive(false);

            pokemonInfoMenu.SetData(trainer.Party, new PartyMenuCustomization
            {
                OnClickPokemon = choice =>
                {
                    if (!choice.Fainted
                        && choice != decisionModule.Self.Pokemon
                        && choice != (decisionModule.Ally != null ? decisionModule.Ally.Pokemon : null)
                        && choice != (decisionModule.Ally != null ? decisionModule.Ally.DecisionModule.Switch : null))
                    {
                        decisionModule.SwitchIn(choice);
                    }
                }
            });

            bagMenu.SetData(trainer.Bag, trainer.Party, new BagMenuCustomization
            {
                OnClickItem = itemSlot =>
                {
                    if (!itemSlot.Item.UsableInBattle)
                    {
                        // TODO "This item can't be used in battle."
                        return;
                    }

                    if (itemSlot.Item.AutoTargets)
                    {
                        bagMenu.UseItem(itemSlot, null);
                        return;
                    }

                    bagMenu.SetItemAndAllowTargetSelection(itemSlot);
                },
                OnClickTarget = (item, target) =>
                {
                    if (target == null)
                    {
                        target = item.Item.Category == ItemCategory.PokeBall
                            ? decisionModule.Enemy.Pokemon
                            : decisionModule.Self.Pokemon;
                    }

                    if (!battleManager.ItemIsEffective(item.Item, target, decisionModule.Self.Pokemon == target))
                    {
                        // TODO "It wouldn't have any effect."
                        return;
                    }

                    trainer.Bag.AddOrRemove(item.Item.Name, -1);
                    decisionModule.ChooseItem(item.Item, target);
                }
            });

            runCallback = decisionModule.Run;

            messageBox.Display(Strings.MovePrompt(pokemon));
        }

        public void ShowMovesMenu()
        {
            if (movesMenu.IsStruggling())
            {
                movesMenu.SelectStruggle();
                return;
            }

            gameObject.SetActive(false);
            movesMenu.gameObject.SetActive(true);
        }

        public void HideMovesMenu()
        {
            gameObject.SetActive(true);
            movesMenu.gameObject.SetActive(false);
        }

        public void ShowPokemonMenu()
        {
            gameObject.SetActive(false);
            pokemonInfoMenu.Show();
        }

        public void HidePokemonMenu()
        {
            gameObject.SetActive(true);
            pokemonInfoMenu.gameObject.SetActive(false);
        }

        public void ShowBagMenu()
        {
            gameObject.SetActive(false);
            bagMenu.Show();
        }

        public void HideBagMenu()
        {
            gameObject.SetActive(true);
            bagMenu.gameObject.SetActive(false);
        }

        public void Run()
        {
            // TODO improve with a confirmation
            runCallback();
        }
    }
}