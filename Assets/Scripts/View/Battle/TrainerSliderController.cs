using System;
using Managers;
using Managers.Cutscene.Waiting;
using Managers.Shared;
using Models.Battle;
using UnityEngine;
using UnityEngine.Experimental.U2D.Animation;
using UnityEngine.UI;

namespace View.Battle
{
    public class TrainerSliderController : MonoBehaviour, IAnimationDoneSource
    {
        [SerializeField] private float slideInTimeSeconds;
        [SerializeField] private float fadeTimeSeconds;
        [SerializeField] private float basePosition;
        [SerializeField] private float endPosition;
        [SerializeField] private float defeatedPosition;
        [SerializeField] private RectTransform rectTransform;
        [SerializeField] private RectTransform baseRectTransform;
        [SerializeField] private SpriteLibraryAsset spriteLibrary;
        [SerializeField] private Image singleTrainerImage;
        [SerializeField] private Image doubleTrainerImage1;
        [SerializeField] private Image doubleTrainerImage2;

        [SerializeField] protected Image wildPokemonImage;

        private float target;
        private bool targetSet;
        private bool baseFollows;
        private float speed;

        protected BattleData BattleData;
        protected Image Image1;
        protected Image Image2;
        protected string SpriteName1;
        protected string SpriteName2;
        protected Vector3 Direction;

        private Action animationDoneCallback;

        public void SetAnimationDoneCallback(Action animationDoneCallback)
        {
            this.animationDoneCallback = animationDoneCallback;
        }

        private void Start()
        {
            SetBasePositionToTrainer();
        }

        protected virtual void ResolveImage()
        {
            Image1 = SpriteName2 == null ? singleTrainerImage : doubleTrainerImage1;
            Image1.sprite = spriteLibrary.GetSprite("Trainers", SpriteName1);
            Image1.enabled = true;

            if (SpriteName2 != null)
            {
                Image2 = doubleTrainerImage2;
                Image2.sprite = spriteLibrary.GetSprite("Trainers", SpriteName2);
                Image2.enabled = true;
            }
        }

        public virtual void SlideIn(BattleData battleData)
        {
            this.BattleData = battleData;

            ResolveImage();

            target = basePosition;
            targetSet = true;
            baseFollows = true;

            // This formula confuses me (why the division by 100?), but it works in practice
            speed = (Math.Abs(target) + Math.Abs(rectTransform.anchoredPosition.x)) / (slideInTimeSeconds * 100);
        }

        public virtual void SlideOut()
        {
            target = endPosition;
            targetSet = true;
            baseFollows = false;

            if (fadeTimeSeconds > 0)
            {
                Image1.CrossFadeAlpha(0, fadeTimeSeconds, false);
                if (Image2 != null)
                {
                    Image2.CrossFadeAlpha(0, fadeTimeSeconds, false);
                }
            }
        }

        public virtual void SlideBackIn()
        {
            target = defeatedPosition;
            targetSet = true;
            baseFollows = false;

            if (fadeTimeSeconds > 0)
            {
                Image1.CrossFadeAlpha(1, fadeTimeSeconds, false);
                if (Image2 != null)
                {
                    Image2.CrossFadeAlpha(1, fadeTimeSeconds, false);
                }
            }
        }

        private void Update()
        {
            if (!targetSet)
            {
                return;
            }

            rectTransform.Translate(Direction * (speed * Time.deltaTime));

            if ((Direction == Vector3.right && rectTransform.anchoredPosition.x >= target)
                || (Direction == Vector3.left && rectTransform.anchoredPosition.x <= target))
            {
                rectTransform.anchoredPosition = new Vector2(target, rectTransform.anchoredPosition.y);
                targetSet = false;
                animationDoneCallback?.Invoke();
                animationDoneCallback = null;

                // Reset color tint for wild pokémon
                if (Image1.color.a > 0)
                {
                    Image1.color = Color.white;
                }
            }

            if (baseFollows)
            {
                SetBasePositionToTrainer();
            }
        }

        private void SetBasePositionToTrainer()
        {
            baseRectTransform.anchoredPosition =
                new Vector2(rectTransform.anchoredPosition.x, baseRectTransform.anchoredPosition.y);
        }
    }
}