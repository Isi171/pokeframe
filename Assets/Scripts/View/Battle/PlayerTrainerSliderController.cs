using Models.Battle;
using UnityEngine;

namespace View.Battle
{
    public class PlayerTrainerSliderController : TrainerSliderController
    {
        protected override void ResolveImage()
        {
            SpriteName1 = BattleData.Player.Sprite;
            SpriteName2 = BattleData.Ally?.Sprite;
            base.ResolveImage();
        }

        public override void SlideIn(BattleData battleData)
        {
            base.SlideIn(battleData);
            Direction = Vector3.left;
        }

        public override void SlideOut()
        {
            base.SlideOut();
            Direction = Vector3.left;
        }

        public override void SlideBackIn()
        {
            base.SlideBackIn();
            Direction = Vector3.right;
        }
    }
}