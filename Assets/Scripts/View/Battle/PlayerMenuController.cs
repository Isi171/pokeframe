using System;
using System.Collections.Generic;
using Managers;
using Managers.Battle;
using Managers.Shared;
using Models.Pokemon;
using Models.Trainer;
using UnityEngine;
using View.Menus.Party;

namespace View.Battle
{
    public class PlayerMenuController : MonoBehaviour
    {
        [SerializeField] private BattleMenuController battleMenu;
        [SerializeField] private PokemonInfoMenuController pokemonInfoMenu;

        private readonly Stack<Request> requests = new Stack<Request>();
        private bool menuWasShown = false;

        private void Update()
        {
            if (requests.Count == 0)
            {
                return;
            }

            Request currentRequest = requests.Peek();
            if (currentRequest.IsMenuActive())
            {
                return;
            }

            if (menuWasShown)
            {
                return;
            }

            menuWasShown = true;
            currentRequest.ShowMenu();
        }

        public void RequestMenu(BattlerController battler)
        {
            requests.Push(Request.BattleMenu(battler, battleMenu));
        }

        public void RequestSubstitution(BattlerController battler)
        {
            requests.Push(Request.SubstitutionMenu(battler, pokemonInfoMenu));
        }

        public void RequestOverwriteMove(MoveOverwriteDecider decider, Pokemon pokemon, string newMove)
        {
            requests.Push(Request.OverwriteMoveMenu(decider, pokemon, pokemonInfoMenu, newMove));
        }

        public void Hide()
        {
            battleMenu.Hide();
        }

        public void SelectionDone()
        {
            requests.Pop();
            menuWasShown = false;
        }

        private class Request
        {
            public Func<bool> IsMenuActive { get; private set; }
            public Action ShowMenu { get; private set; }

            public static Request BattleMenu(BattlerController battler, BattleMenuController battleMenu)
            {
                return new Request
                {
                    IsMenuActive = () => battleMenu.gameObject.activeSelf,
                    ShowMenu = () => battleMenu.Show(battler.DecisionModule as PlayerDecisionModule,
                        battler.Pokemon, battler.Trainer)
                };
            }

            public static Request SubstitutionMenu(BattlerController battler, PokemonInfoMenuController pokemonInfoMenu)
            {
                return new Request
                {
                    IsMenuActive = () => pokemonInfoMenu.gameObject.activeSelf,
                    ShowMenu = () =>
                    {
                        if (battler.DecisionModule.DidIRunOutOfSubstitutions())
                        {
                            ((PlayerDecisionModule)battler.DecisionModule).SwitchInSubstitution(null);
                            return;
                        }

                        pokemonInfoMenu.SetData(battler.Trainer.Party, new PartyMenuCustomization
                            {
                                OnClickPokemon = pokemon =>
                                {
                                    if (!pokemon.Fainted
                                        && pokemon != (battler.DecisionModule.Ally != null
                                            ? battler.DecisionModule.Ally.Pokemon
                                            : null)
                                        && pokemon != (battler.DecisionModule.Ally != null
                                            ? battler.DecisionModule.Ally.DecisionModule.Substitution
                                            : null))
                                    {
                                        ((PlayerDecisionModule)battler.DecisionModule).SwitchInSubstitution(pokemon);
                                    }
                                }
                            }
                        );
                        pokemonInfoMenu.Show(false);
                    }
                };
            }

            public static Request OverwriteMoveMenu(MoveOverwriteDecider decider, Pokemon pokemon,
                PokemonInfoMenuController pokemonInfoMenu, string newMove)
            {
                return new Request
                {
                    IsMenuActive = () => pokemonInfoMenu.gameObject.activeSelf,
                    ShowMenu = () =>
                    {
                        pokemonInfoMenu.SetData(new Party(pokemon), new PartyMenuCustomization
                        {
                            OnClose = () => decider.OverwriteMove(-1),
                            OnClickMoveSlot = slot =>
                            {
                                for (int i = 0; i < 4; i++)
                                {
                                    if (pokemon.Moves[i] == slot)
                                    {
                                        decider.OverwriteMove(i);
                                        return;
                                    }
                                }
                            },
                            NewMove = newMove
                        });
                        pokemonInfoMenu.Show();
                    }
                };
            }
        }
    }
}