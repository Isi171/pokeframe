using System;
using System.Collections;
using Managers.Shared;
using UnityEngine;
using UnityEngine.Experimental.U2D.Animation;
using UnityEngine.UI;

namespace View.Battle
{
    public class CatchingBallController : MonoBehaviour, IAnimationDoneSource
    {
        [SerializeField] private RectTransform rectTransform;
        [SerializeField] private Image image;
        [SerializeField] private SpriteLibrary spriteLibrary;
        [SerializeField] private RectTransform destination;
        [SerializeField] private float speed;

        private Vector2 startingPosition;
        private Action animationDoneCallback;

        public void SetAnimationDoneCallback(Action animationDoneCallback)
        {
            this.animationDoneCallback = animationDoneCallback;
        }

        private void Start()
        {
            startingPosition = rectTransform.anchoredPosition;
        }

        public void FlyAnimation(string ball, BattlerController battler)
        {
            StartCoroutine(FlyCoroutine(ball, battler));
        }

        public void ShakeAnimation(string ball)
        {
            StartCoroutine(ShakeCoroutine(ball));
        }

        public void BreakAnimation(string ball, BattlerController battler)
        {
            StartCoroutine(BreakCoroutine(ball, battler));
        }

        public void CaptureAnimation()
        {
            StartCoroutine(CaptureCoroutine());
        }

        private IEnumerator FlyCoroutine(string ball, BattlerController battler)
        {
            // Reset position, color and alpha
            rectTransform.anchoredPosition = startingPosition;
            image.color = Color.white;
            image.CrossFadeAlpha(1f, 0f, true);

            image.sprite = spriteLibrary.GetSprite(ball, "Rolling9");

            Vector2 destinationPosition = destination.anchoredPosition;
            Vector2 direction = (destinationPosition - startingPosition).normalized;

            while (destinationPosition.x - rectTransform.anchoredPosition.x > 3)
            {
                rectTransform.Translate(direction * (speed * Time.deltaTime));
                yield return null;
            }

            rectTransform.anchoredPosition = destinationPosition;
            yield return new WaitForSeconds(0.1f);

            image.sprite = spriteLibrary.GetSprite(ball, "Open");
            battler.Image.CrossFadeAlpha(0f, 0.5f, false);
            yield return new WaitForSeconds(0.6f);

            image.sprite = spriteLibrary.GetSprite(ball, "Rolling9");
            yield return new WaitForSeconds(0.4f);

            animationDoneCallback();
        }

        private IEnumerator ShakeCoroutine(string ball)
        {
            image.sprite = spriteLibrary.GetSprite(ball, "Shake1");
            yield return new WaitForSeconds(0.05f);

            image.sprite = spriteLibrary.GetSprite(ball, "Shake2");
            yield return new WaitForSeconds(0.05f);

            image.sprite = spriteLibrary.GetSprite(ball, "Shake3");
            yield return new WaitForSeconds(0.05f);

            image.sprite = spriteLibrary.GetSprite(ball, "Shake2");
            yield return new WaitForSeconds(0.05f);

            image.sprite = spriteLibrary.GetSprite(ball, "Shake1");
            yield return new WaitForSeconds(0.05f);

            image.sprite = spriteLibrary.GetSprite(ball, "Rolling9");
            yield return new WaitForSeconds(0.05f);

            image.sprite = spriteLibrary.GetSprite(ball, "Shake4");
            yield return new WaitForSeconds(0.05f);

            image.sprite = spriteLibrary.GetSprite(ball, "Shake5");
            yield return new WaitForSeconds(0.05f);

            image.sprite = spriteLibrary.GetSprite(ball, "Shake6");
            yield return new WaitForSeconds(0.05f);

            image.sprite = spriteLibrary.GetSprite(ball, "Shake5");
            yield return new WaitForSeconds(0.05f);

            image.sprite = spriteLibrary.GetSprite(ball, "Shake4");
            yield return new WaitForSeconds(0.05f);

            image.sprite = spriteLibrary.GetSprite(ball, "Rolling9");
            yield return new WaitForSeconds(0.2f);

            animationDoneCallback();
        }

        private IEnumerator BreakCoroutine(string ball, BattlerController battler)
        {
            image.sprite = spriteLibrary.GetSprite(ball, "Open");
            battler.Image.CrossFadeAlpha(1f, 0.5f, false);
            yield return new WaitForSeconds(0.5f);

            image.CrossFadeAlpha(0f, 0.5f, false);
            
            animationDoneCallback();
        }

        private IEnumerator CaptureCoroutine()
        {
            image.color = new Color(.5f, .5f, .5f);
            yield return new WaitForSeconds(0.1f);
            
            animationDoneCallback();
        }
    }
}