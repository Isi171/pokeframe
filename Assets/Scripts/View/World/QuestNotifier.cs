using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace View.World
{
    public class QuestNotifier : MonoBehaviour
    {
        // TODO merge in common superclass with LocationViewer

        [SerializeField] private float holdTime;
        [SerializeField] private float fadeOutTime;
        [SerializeField] private Image panel;
        [SerializeField] private TMP_Text header;
        [SerializeField] private TMP_Text questName;

        private float alpha;

        public void Start()
        {
            panel.color = new Color(panel.color.r, panel.color.g, panel.color.b, 0f);
            header.alpha = 0f;
            questName.alpha = 0f;
        }

        public void ForceHide()
        {
            StopAllCoroutines();
            panel.color = new Color(panel.color.r, panel.color.g, panel.color.b, 0f);
            header.alpha = 0f;
            questName.alpha = 0f;
        }

        public void Show(string state, string quest)
        {
            StopAllCoroutines();
            panel.color = new Color(panel.color.r, panel.color.g, panel.color.b, 1f);
            header.alpha = 1f;
            questName.alpha = 1f;

            header.text = state;
            questName.text = quest;

            StartCoroutine(FadeOut());
        }

        private IEnumerator FadeOut()
        {
            alpha = 1f;
            yield return new WaitForSeconds(holdTime);

            while (alpha > 0f)
            {
                panel.color = new Color(panel.color.r, panel.color.g, panel.color.b, alpha);
                header.alpha = alpha;
                questName.alpha = alpha;
                yield return null;

                alpha -= Time.deltaTime * fadeOutTime;
            }

            panel.color = new Color(panel.color.r, panel.color.g, panel.color.b, 0);
            header.alpha = 0;
            questName.alpha = 0;
        }
    }
}