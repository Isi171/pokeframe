using System.Linq;
using Managers.Data;
using Models.State;
using UnityEngine;

namespace View.World
{
    public class ConditionalPresence : MonoBehaviour
    {
        [SerializeField] private bool negate;
        [SerializeField] private string[] conditionsAnd;

        private void Start()
        {
            if (conditionsAnd.Length == 0)
            {
                return;
            }

            bool and = GlobalState.Instance.Variables.And(conditionsAnd.Select(DataConverter.ParseCondition), negate);
            gameObject.SetActive(and);
        }
    }
}