using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace View.World
{
    public class LocationViewer : MonoBehaviour
    {
        [SerializeField] private float holdTime;
        [SerializeField] private float fadeOutTime;
        [SerializeField] private Image panel;
        [SerializeField] private TMP_Text text;

        private float alpha;

        public void Start()
        {
            panel.color = new Color(panel.color.r, panel.color.g, panel.color.b, 0f);
            text.alpha = 0f;
        }

        public void ForceHide()
        {
            StopAllCoroutines();
            panel.color = new Color(panel.color.r, panel.color.g, panel.color.b, 0f);
            text.alpha = 0f;
        }

        public void Show(string location)
        {
            StopAllCoroutines();
            panel.color = new Color(panel.color.r, panel.color.g, panel.color.b, 1f);
            text.alpha = 1f;

            text.text = location;

            StartCoroutine(FadeOut());
        }

        private IEnumerator FadeOut()
        {
            alpha = 1f;
            yield return new WaitForSeconds(holdTime);

            while (alpha > 0f)
            {
                panel.color = new Color(panel.color.r, panel.color.g, panel.color.b, alpha);
                text.alpha = alpha;
                yield return null;

                alpha -= Time.deltaTime * fadeOutTime;
            }

            panel.color = new Color(panel.color.r, panel.color.g, panel.color.b, 0);
            text.alpha = 0;
        }
    }
}