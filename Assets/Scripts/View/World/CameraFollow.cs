using UnityEngine;

namespace View.World
{
    public class CameraFollow : MonoBehaviour
    {
        [SerializeField] private new Transform camera;
        [SerializeField] private Transform target;

        private void Update()
        {
            Follow();
        }

        public void Follow()
        {
            Vector3 position = target.position;
            camera.position = new Vector3(position.x, position.y, -10);
        }
    }
}