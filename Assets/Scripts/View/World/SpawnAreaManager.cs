using System.Collections.Generic;
using Managers.Data;
using Models.World;
using UnityEngine;
using Utils;
using View.Actor;

namespace View.World
{
    public class SpawnAreaManager : MonoBehaviour
    {
        [SerializeField] private BoxCollider2D area;
        [SerializeField] private GameObject spawnee;
        [SerializeField] private string spawnTable;
        [SerializeField] private int maxConcurrentEntities;
        [SerializeField] private float minSpawnDelaySeconds;
        [SerializeField] private float maxSpawnDelaySeconds;
        [SerializeField] private float minLifetimeSeconds;
        [SerializeField] private float maxLifetimeSeconds;

        private readonly List<GameObject> entities = new List<GameObject>();
        private SpawnTable spawnTableInstance;
        private Vector2 bounds;
        private float counter;
        private float counterTarget;
        private bool counterTargetSet;

        public Vector2 RandomPointWithin => new Vector2(
            RandomUtils.RangeFloat(-bounds.x, bounds.x),
            RandomUtils.RangeFloat(-bounds.y, bounds.y));

        private void Start()
        {
            spawnTableInstance = DataAccess.Encounters.Get(spawnTable);

            bounds = area.size / 2;
            area.enabled = false;
        }

        private void Update()
        {
            if (entities.Count >= maxConcurrentEntities)
            {
                return;
            }

            if (!counterTargetSet)
            {
                counterTarget = RandomUtils.RangeFloat(minSpawnDelaySeconds, maxSpawnDelaySeconds);
                counterTargetSet = true;
            }

            counter += Time.deltaTime;

            if (counter > counterTarget)
            {
                counter = 0f;
                counterTargetSet = false;

                entities.Add(Spawn());
            }
        }

        // TODO use an object pool in place of Instantiate/Destroy
        private GameObject Spawn()
        {
            SpawnTableEntry spawnData = spawnTableInstance.RandomEntry;
            GameObject spawned = Instantiate(spawnee, Vector3.zero, Quaternion.identity, transform);
            spawned.transform.localPosition = RandomPointWithin;
            spawned.GetComponent<RoamingEncounterInputModule>().Initialize(this);
            spawned.GetComponent<WildEncounter>().Initialize(spawnData.Species, spawnData.RandomLevel,
                RandomUtils.ShinyChance());
            spawned.GetComponent<TimedSpawn>().Initialize(this,
                RandomUtils.RangeFloat(minLifetimeSeconds, maxLifetimeSeconds));
            return spawned;
        }

        public void Despawn(GameObject despawnee)
        {
            entities.Remove(despawnee);
            Destroy(despawnee);
        }
    }
}