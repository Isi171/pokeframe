using UnityEngine;

namespace View.World
{
    public class LocationHolder : MonoBehaviour
    {
        [SerializeField] private string location;
        [SerializeField] private string music;

        public string Location => location;
        public string Music => music;
    }
}