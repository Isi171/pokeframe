using UnityEngine;

namespace View.World
{
    public class Warp : MonoBehaviour
    {
        [SerializeField] private string ownId;
        [SerializeField] private string targetScene;
        [SerializeField] private string targetId;

        public string OwnId => ownId;
        public string TargetScene => targetScene;
        public string TargetId => targetId;
    }
}