using UnityEngine;

namespace View.World
{
    public class Pickup : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer spriteRenderer;
        [SerializeField] private Collider2D collider2d;
        [SerializeField] private string lootId;

        private bool spent;
        public string LootId => lootId;

        public bool Spent
        {
            get => spent;
            set
            {
                spent = value;
                spriteRenderer.enabled = !value;
                collider2d.enabled = !value;
            }
        }
    }
}