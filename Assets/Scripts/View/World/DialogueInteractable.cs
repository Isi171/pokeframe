using UnityEngine;

namespace View.World
{
    public class DialogueInteractable : MonoBehaviour
    {
        [SerializeField] private string dialogueId;
        public string DialogueId => dialogueId;
    }
}