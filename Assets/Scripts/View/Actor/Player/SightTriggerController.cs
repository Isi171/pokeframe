using Managers.Cutscene;
using Managers.Cutscene.Events.Audio;
using Managers.Cutscene.Events.Dialogue;
using Managers.Cutscene.Events.Movement;
using Models;
using UnityEngine;
using Utils;
using View.World;

namespace View.Actor.Player
{
    public class SightTriggerController : MonoBehaviour
    {
        [SerializeField] private ActorMovementController movementController;
        [SerializeField] private CutsceneManager cutsceneManager;

        private void OnTriggerEnter2D(Collider2D other)
        {
            // Direct collision between player and actor trigger this method -- so skip them
            if (other.transform.parent == null)
            {
                return;
            }

            // The good trigger is one of the four child box colliders representing actor sight
            GameObject actor = other.transform.parent.gameObject;
            LineOfSight lineOfSight = actor.GetComponent<LineOfSight>();
            if (lineOfSight == null)
            {
                // It somehow triggers on the Grid object ???
                return;
            }

            if (lineOfSight.ShouldTrigger)
            {
                Direction turnTo = actor.GetComponent<ActorMovementController>().Direction.Opposite();
                cutsceneManager.Enqueue(new StartSound("Overworld#Exclamation"));
                
                if (!string.IsNullOrEmpty(lineOfSight.Music))
                {
                    cutsceneManager.Enqueue(new StartMusic(lineOfSight.Music));
                }

                cutsceneManager.Enqueue(new Exclaim(actor.GetComponentInChildren<ExclaimerController>()));
                cutsceneManager.Enqueue(new TurnTowards(movementController, turnTo));
                cutsceneManager.Enqueue(new MoveNpcTowards(actor.GetComponent<NpcInputModule>(), gameObject));
                cutsceneManager.Enqueue(new StartDialogue(actor.GetComponent<DialogueInteractable>().DialogueId));
            }
        }
    }
}