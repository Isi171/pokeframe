using UnityEngine;
using View.World;

namespace View.Actor.Player
{
    public class InteractionTriggerController : MonoBehaviour
    {
        [SerializeField] private Collider2D trigger;

        private Collider2D currentlyInteractable;

        public string DialogueId => currentlyInteractable.GetComponent<DialogueInteractable>().DialogueId;
        public GameObject DialogueSource => currentlyInteractable.gameObject;

        public string LootId
        {
            get
            {
                Pickup pickup = currentlyInteractable.GetComponent<Pickup>();
                pickup.Spent = true;
                return pickup.LootId;
            }
        }

        public void Enable()
        {
            trigger.enabled = true;
        }

        public void Disable()
        {
            trigger.enabled = false;
            currentlyInteractable = null;
        }

        public bool CanStartDialogue()
        {
            if (currentlyInteractable == null)
            {
                return false;
            }

            return currentlyInteractable.GetComponent<DialogueInteractable>() != null;
        }

        public bool CanPickUp()
        {
            if (currentlyInteractable == null)
            {
                return false;
            }

            return currentlyInteractable.GetComponent<Pickup>() != null;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.GetComponent<DialogueInteractable>() != null || other.GetComponent<Pickup>() != null)
            {
                currentlyInteractable = other;
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (currentlyInteractable == other)
            {
                currentlyInteractable = null;
            }
        }
    }
}