using System.Collections;
using UnityEngine;

namespace View.Actor.Player
{
    public class PlayerInputModule : ActorInputModule
    {
        protected override Vector2 GetMovementDirection()
        {
            return new Vector2(
                Threshold(Input.GetAxisRaw("Horizontal")),
                Threshold(Input.GetAxisRaw("Vertical"))
            );
        }

        private static float Threshold(float value)
        {
            return Mathf.Abs(value) < 0.2f ? 0f : value;
        }

        public override void EnableInput()
        {
            StartCoroutine(EnableInputCoroutine());
        }

        private IEnumerator EnableInputCoroutine()
        {
            // skip a frame here to avoid having the same keypress that ends a dialogue also start a new one
            yield return null;
            base.EnableInput();
        }
    }
}