using Managers.Cutscene;
using Managers.State;
using UnityEngine;
using View.World;

namespace View.Actor.Player
{
    public class WarpTriggerController : MonoBehaviour
    {
        [SerializeField] private CutsceneManager cutsceneManager;

        public bool Paused { private get; set; }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (Paused)
            {
                return;
            }

            if (other.GetComponent<Warp>() != null)
            {
                Warp warp = other.GetComponent<Warp>();
                if (warp.TargetId != "")
                {
                    Paused = true;
                    cutsceneManager.Enqueue(TransitionManager.Warp(warp.TargetScene, warp.TargetId));
                }
            }
        }
    }
}