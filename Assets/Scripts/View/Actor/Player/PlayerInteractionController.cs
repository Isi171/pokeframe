using System;
using System.Linq;
using Managers.Cutscene;
using Managers.Cutscene.Events.Audio;
using Managers.Cutscene.Events.Dialogue;
using Managers.Cutscene.Events.Items;
using Managers.Cutscene.Events.Movement;
using Models;
using UnityEngine;

namespace View.Actor.Player
{
    public class PlayerInteractionController : MonoBehaviour
    {
        [SerializeField] private CutsceneManager cutsceneManager;
        [SerializeField] private ActorInputModule inputModule;
        [SerializeField] private ActorMovementController movementController;
        [SerializeField] private InteractionTriggerController interactionTriggerUp;
        [SerializeField] private InteractionTriggerController interactionTriggerDown;
        [SerializeField] private InteractionTriggerController interactionTriggerLeft;
        [SerializeField] private InteractionTriggerController interactionTriggerRight;

        private InteractionTriggerController[] interactionTriggers;

        private void Start()
        {
            interactionTriggers = new[]
            {
                interactionTriggerUp,
                interactionTriggerDown,
                interactionTriggerLeft,
                interactionTriggerRight
            };
        }

        private void Update()
        {
            switch (movementController.Direction)
            {
                case Direction.Down:
                    interactionTriggerUp.Disable();
                    interactionTriggerDown.Enable();
                    interactionTriggerLeft.Disable();
                    interactionTriggerRight.Disable();
                    break;
                case Direction.Up:
                    interactionTriggerUp.Enable();
                    interactionTriggerDown.Disable();
                    interactionTriggerLeft.Disable();
                    interactionTriggerRight.Disable();
                    break;
                case Direction.Left:
                    interactionTriggerUp.Disable();
                    interactionTriggerDown.Disable();
                    interactionTriggerLeft.Enable();
                    interactionTriggerRight.Disable();
                    break;
                case Direction.Right:
                    interactionTriggerUp.Disable();
                    interactionTriggerDown.Disable();
                    interactionTriggerLeft.Disable();
                    interactionTriggerRight.Enable();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (!inputModule.InputEnabled)
            {
                return;
            }

            if (Input.GetButtonDown("Submit"))
            {
                InteractionTriggerController dialogueTrigger =
                    interactionTriggers.FirstOrDefault(trigger => trigger.CanStartDialogue());
                if (dialogueTrigger != null)
                {
                    movementController.SetIdle();

                    GameObject source = dialogueTrigger.DialogueSource;
                    LineOfSight lineOfSight = source.GetComponent<LineOfSight>();
                    if (lineOfSight != null && lineOfSight.isActiveAndEnabled && lineOfSight.ShouldTrigger)
                    {
                        lineOfSight.enabled = false;

                        TurnDialogueSource(dialogueTrigger);
                        cutsceneManager.Enqueue(new StartSound("Overworld#Exclamation"));
                        if (!string.IsNullOrEmpty(lineOfSight.Music))
                        {
                            cutsceneManager.Enqueue(new StartMusic(lineOfSight.Music));
                        }

                        cutsceneManager.Enqueue(new Exclaim(source.GetComponentInChildren<ExclaimerController>()));
                        cutsceneManager.Enqueue(new StartDialogue(dialogueTrigger.DialogueId));
                    }
                    else
                    {
                        TurnDialogueSource(dialogueTrigger);
                        cutsceneManager.Enqueue(new StartDialogue(dialogueTrigger.DialogueId));
                    }

                    return;
                }

                InteractionTriggerController pickUpTrigger =
                    interactionTriggers.FirstOrDefault(trigger => trigger.CanPickUp());
                if (pickUpTrigger != null)
                {
                    movementController.SetIdle();
                    cutsceneManager.Enqueue(new ItemsFound(pickUpTrigger.LootId, true));
                    return;
                }
            }
        }

        private void TurnDialogueSource(InteractionTriggerController trigger)
        {
            ActorMovementController actorMovementController =
                trigger.DialogueSource.GetComponent<ActorMovementController>();
            if (actorMovementController == null)
            {
                return;
            }

            if (trigger == interactionTriggerUp)
            {
                actorMovementController.Turn(Direction.Down);
            }
            else if (trigger == interactionTriggerDown)
            {
                actorMovementController.Turn(Direction.Up);
            }
            else if (trigger == interactionTriggerLeft)
            {
                actorMovementController.Turn(Direction.Right);
            }
            else if (trigger == interactionTriggerRight)
            {
                actorMovementController.Turn(Direction.Left);
            }
        }
    }
}