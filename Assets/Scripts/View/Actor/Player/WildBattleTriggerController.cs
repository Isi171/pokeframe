using Managers.Cutscene;
using Managers.Cutscene.Events.Transition;
using Managers.State;
using UnityEngine;

namespace View.Actor.Player
{
    public class WildBattleTriggerController : MonoBehaviour
    {
        [SerializeField] private ActorInputModule playerInputModule;
        [SerializeField] private CutsceneManager cutsceneManager;

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (!playerInputModule.InputEnabled)
            {
                return;
            }

            WildEncounter encounter = collision.gameObject.GetComponent<WildEncounter>();
            if (encounter != null)
            {
                // disable wild pokémon input module
                collision.gameObject.GetComponent<ActorInputModule>().DisableInput();

                // start battle
                cutsceneManager.Enqueue(new PrepareBattle(encounter.Encounter));
                cutsceneManager.Enqueue(TransitionManager.StartBattle());
            }
        }
    }
}