using Models;
using Models.Pokemon;
using UnityEngine;
using UnityEngine.Experimental.U2D.Animation;

namespace View.Actor
{
    public class WildEncounter : MonoBehaviour
    {
        [SerializeField] private SpriteLibrary spriteLibrary;
        [SerializeField] private SpriteResolver spriteResolver;

        public Pokemon Encounter { get; private set; }

        public void Initialize(string species, int level, bool shiny)
        {
            Encounter = Pokemon.Get(species, level);
            Encounter.SetMovesByLevel();
            Encounter.Shiny = shiny;

            string category =
                $"{(Encounter.Gender == Gender.Female ? "Female" : "Male")}{(Encounter.Shiny ? "Shiny" : "")}";

            spriteLibrary.spriteLibraryAsset = Resources.Load<SpriteLibraryAsset>($"pokemon/overworld/{species}");
            spriteResolver.SetCategoryAndLabel(category, "IdleDown");
        }
    }
}