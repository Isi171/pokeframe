using System;
using System.Collections.Generic;
using System.Linq;
using Managers.Data;
using Models;
using Models.Dialogue;
using Models.State;
using UnityEngine;

namespace View.Actor
{
    public class LineOfSight : MonoBehaviour
    {
        private const float Width = 0.2f;

        [SerializeField] [Range(1, 10)] private int length;
        [SerializeField] private string[] conditionsAnd;
        [SerializeField] private string music;
        [SerializeField] private BoxCollider2D up;
        [SerializeField] private BoxCollider2D down;
        [SerializeField] private BoxCollider2D left;
        [SerializeField] private BoxCollider2D right;
        [SerializeField] private ActorMovementController actorMovement;

        private IEnumerable<Condition> parsedConditionsAnd;
        public bool ShouldTrigger => GlobalState.Instance.Variables.And(parsedConditionsAnd);
        public string Music => music;

        private void Start()
        {
            up.size = new Vector2(Width, length);
            up.transform.localPosition = new Vector2(0f, (length + 1) / 2f);

            down.size = new Vector2(Width, length);
            down.transform.localPosition = new Vector2(0f, -(length + 1) / 2f);

            left.size = new Vector2(length, Width);
            left.transform.localPosition = new Vector2(-(length + 1) / 2f, 0f);

            right.size = new Vector2(length, Width);
            right.transform.localPosition = new Vector2((length + 1) / 2f, 0f);

            parsedConditionsAnd = conditionsAnd.Select(DataConverter.ParseCondition);
        }

        private void Update()
        {
            switch (actorMovement.Direction)
            {
                case Direction.Up:
                    up.gameObject.SetActive(true);
                    down.gameObject.SetActive(false);
                    left.gameObject.SetActive(false);
                    right.gameObject.SetActive(false);
                    break;
                case Direction.Down:
                    up.gameObject.SetActive(false);
                    down.gameObject.SetActive(true);
                    left.gameObject.SetActive(false);
                    right.gameObject.SetActive(false);
                    break;
                case Direction.Left:
                    up.gameObject.SetActive(false);
                    down.gameObject.SetActive(false);
                    left.gameObject.SetActive(true);
                    right.gameObject.SetActive(false);
                    break;
                case Direction.Right:
                    up.gameObject.SetActive(false);
                    down.gameObject.SetActive(false);
                    left.gameObject.SetActive(false);
                    right.gameObject.SetActive(true);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}