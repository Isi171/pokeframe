using System.Collections;
using Models;
using UnityEngine;

namespace View.Actor
{
    public abstract class ActorInputModule : MonoBehaviour
    {
        [SerializeField] private Direction startingDirection;
        [SerializeField] protected ActorMovementController movementController;

        public bool InputEnabled { get; private set; } = true;

        public Direction OverriddenStartingDirection
        {
            set => startingDirection = value;
        }

        public void DisableInput()
        {
            InputEnabled = false;

            movementController.SetIdle();
        }

        public virtual void EnableInput()
        {
            InputEnabled = true;
        }

        private void Start()
        {
            StartCoroutine(SetStartingDirectionCoroutine());
        }

        private IEnumerator SetStartingDirectionCoroutine()
        {
            yield return null;
            movementController.Turn(startingDirection);
        }

        private void FixedUpdate()
        {
            if (!InputEnabled)
            {
                movementController.SetIdle();
                return;
            }

            Vector2 direction = GetMovementDirection();
            if (direction == Vector2.zero)
            {
                movementController.SetIdle();
            }
            else
            {
                movementController.SetMoving(direction);
            }
        }

        protected abstract Vector2 GetMovementDirection();
    }
}