using UnityEngine;

namespace View.Actor
{
    public class NpcInputModule : ActorInputModule
    {
        public Vector2 Direction { private get; set; } = Vector2.zero;

        protected override Vector2 GetMovementDirection()
        {
            return Direction;
        }
    }
}