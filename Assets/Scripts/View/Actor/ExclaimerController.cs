using System;
using System.Collections;
using Managers;
using Managers.Battle.Waiting;
using Managers.Cutscene.Waiting;
using Managers.Shared;
using UnityEngine;

namespace View.Actor
{
    public class ExclaimerController : MonoBehaviour, IAnimationDoneSource
    {
        [SerializeField] private float time;
        [SerializeField] private SpriteRenderer spriteRenderer;

        private Action animationDoneCallback;

        public void SetAnimationDoneCallback(Action animationDoneCallback)
        {
            this.animationDoneCallback = animationDoneCallback;
        }

        public void StartAnimation()
        {
            StartCoroutine(DoAnimation());
        }

        // I can't for the life of me make this work with the animation system
        // Maybe it has something to do with having an animator controller on a child object?
        // So fuck me, I guess.
        private IEnumerator DoAnimation()
        {
            spriteRenderer.enabled = true;

            yield return new WaitForSeconds(time);

            spriteRenderer.enabled = false;
            AnimationDone();
        }

        /// Called by animator
        public void AnimationDone()
        {
            animationDoneCallback();
        }
    }
}