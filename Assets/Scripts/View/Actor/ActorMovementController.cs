using System;
using Models;
using UnityEngine;

namespace View.Actor
{
    public class ActorMovementController : MonoBehaviour
    {
        private static readonly int Idle = Animator.StringToHash("Idle");
        private static readonly int WalkingUp = Animator.StringToHash("WalkingUp");
        private static readonly int WalkingDown = Animator.StringToHash("WalkingDown");
        private static readonly int WalkingLeft = Animator.StringToHash("WalkingLeft");
        private static readonly int WalkingRight = Animator.StringToHash("WalkingRight");

        [SerializeField] private float speed;
        [SerializeField] private Rigidbody2D rigidbody2d;
        [SerializeField] private Animator animator;

        public Direction Direction { get; private set; }

        public void SetIdle()
        {
            animator.SetBool(Idle, true);
            animator.SetBool(WalkingUp, false);
            animator.SetBool(WalkingDown, false);
            animator.SetBool(WalkingLeft, false);
            animator.SetBool(WalkingRight, false);
        }

        public void SetMoving(Vector2 direction)
        {
            rigidbody2d.MovePosition(rigidbody2d.position + speed * Time.deltaTime * direction.normalized);

            Turn(direction);
        }

        private void Turn(Vector2 direction)
        {
            if (Math.Abs(direction.x) > Math.Abs(direction.y))
            {
                Turn(direction.x > 0 ? Direction.Right : Direction.Left);
            }
            else
            {
                Turn(direction.y > 0 ? Direction.Up : Direction.Down);
            }
        }

        public void Turn(Direction direction)
        {
            Direction = direction;

            switch (Direction)
            {
                case Direction.Down:
                    SetWalkingDown();
                    break;
                case Direction.Up:
                    SetWalkingUp();
                    break;
                case Direction.Left:
                    SetWalkingLeft();
                    break;
                case Direction.Right:
                    SetWalkingRight();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }
        }

        private void SetWalkingRight()
        {
            animator.SetBool(Idle, false);
            animator.SetBool(WalkingUp, false);
            animator.SetBool(WalkingDown, false);
            animator.SetBool(WalkingLeft, false);
            animator.SetBool(WalkingRight, true);
        }

        private void SetWalkingLeft()
        {
            animator.SetBool(Idle, false);
            animator.SetBool(WalkingUp, false);
            animator.SetBool(WalkingDown, false);
            animator.SetBool(WalkingLeft, true);
            animator.SetBool(WalkingRight, false);
        }

        private void SetWalkingUp()
        {
            animator.SetBool(Idle, false);
            animator.SetBool(WalkingUp, true);
            animator.SetBool(WalkingDown, false);
            animator.SetBool(WalkingLeft, false);
            animator.SetBool(WalkingRight, false);
        }

        private void SetWalkingDown()
        {
            animator.SetBool(Idle, false);
            animator.SetBool(WalkingUp, false);
            animator.SetBool(WalkingDown, true);
            animator.SetBool(WalkingLeft, false);
            animator.SetBool(WalkingRight, false);
        }
    }
}