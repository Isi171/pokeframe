using UnityEngine;
using View.World;

namespace View.Actor
{
    public class TimedSpawn : MonoBehaviour
    {
        private SpawnAreaManager spawnAreaManager;
        private float lifetime;

        private float counter;

        private void Update()
        {
            counter += Time.deltaTime;

            if (counter > lifetime)
            {
                spawnAreaManager.Despawn(gameObject);
            }
        }

        public void Initialize(SpawnAreaManager spawnAreaManager, float lifetime)
        {
            this.spawnAreaManager = spawnAreaManager;
            this.lifetime = lifetime;
        }
    }
}