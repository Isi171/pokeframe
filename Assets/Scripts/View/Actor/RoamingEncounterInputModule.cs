using UnityEngine;
using Utils;
using View.World;

namespace View.Actor
{
    public class RoamingEncounterInputModule : ActorInputModule
    {
        [SerializeField] private Vector2 nearThreshold;
        [SerializeField] private float maxPauseSeconds;

        private SpawnAreaManager spawnAreaManager;

        private bool targetSet;
        private Vector2 target;
        private float pauseTimer;

        protected override Vector2 GetMovementDirection()
        {
            if (pauseTimer > 0)
            {
                pauseTimer -= Time.deltaTime;
                return Vector2.zero;
            }

            if (!targetSet)
            {
                target = spawnAreaManager.RandomPointWithin;
                targetSet = true;
            }

            Vector2 direction = target - (Vector2) transform.localPosition;

            if (direction.x < nearThreshold.x && direction.y < nearThreshold.y)
            {
                targetSet = false;
                pauseTimer = RandomUtils.RangeFloat(0, maxPauseSeconds);
            }

            return direction;
        }

        public void Initialize(SpawnAreaManager spawnAreaManager)
        {
            this.spawnAreaManager = spawnAreaManager;
        }
    }
}