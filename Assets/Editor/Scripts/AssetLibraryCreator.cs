using Managers.Data;
using Models.Pokemon;
using UnityEditor;
using UnityEngine;
using UnityEngine.Experimental.U2D.Animation;

namespace Editor.Scripts
{
    public class AssetLibraryCreator : MonoBehaviour
    {
        [MenuItem("Tools/Create overworld Sprite libs")]
        private static void CreateOverworldSpriteLibraries()
        {
            string[] labels =
            {
                "IdleDown", "IdleUp", "IdleLeft", "WalkingDown", "WalkingUp", "WalkingLeft1", "WalkingLeft2"
            };

            DataAccess.EDITOR__LoadDataForTools();
            foreach (string species in DataAccess.PokemonBaseData.All)
            {
                if (species.StartsWith("Mega "))
                {
                    continue;
                }

                SpriteLibraryAsset library = ScriptableObject.CreateInstance<SpriteLibraryAsset>();
                PokemonBaseData data = DataAccess.PokemonBaseData.Get(species);
                int id = data.NationalDexNumber;

                Sprite[] male = LoadOverworldSprites(id, false, false);
                for (int i = 0; i < labels.Length; i++)
                {
                    library.AddCategoryLabel(male[i], "Male", labels[i]);
                }

                Sprite[] female = LoadOverworldSprites(id, true, false);
                for (int i = 0; i < labels.Length; i++)
                {
                    library.AddCategoryLabel(female[i], "Female", labels[i]);
                }

                Sprite[] maleShiny = LoadOverworldSprites(id, false, true);
                for (int i = 0; i < labels.Length; i++)
                {
                    library.AddCategoryLabel(maleShiny[i], "MaleShiny", labels[i]);
                }

                Sprite[] femaleShiny = LoadOverworldSprites(id, true, true);
                for (int i = 0; i < labels.Length; i++)
                {
                    library.AddCategoryLabel(femaleShiny[i], "FemaleShiny", labels[i]);
                }

                AssetDatabase.CreateAsset(library, $"Assets/Resources/pokemon/overworld/{species}.asset");
            }

            AssetDatabase.SaveAssets();
        }

        private static Sprite[] LoadOverworldSprites(int id, bool female, bool shiny)
        {
            if (!female)
            {
                return new[]
                {
                    Resources.Load<Sprite>($"overworld/{(shiny ? "shiny/" : "")}down/{id}"),
                    Resources.Load<Sprite>($"overworld/{(shiny ? "shiny/" : "")}up/{id}"),
                    Resources.Load<Sprite>($"overworld/{(shiny ? "shiny/" : "")}left/{id}"),
                    Resources.Load<Sprite>($"overworld/{(shiny ? "shiny/" : "")}down/frame2/{id}"),
                    Resources.Load<Sprite>($"overworld/{(shiny ? "shiny/" : "")}up/frame2/{id}"),
                    Resources.Load<Sprite>($"overworld/{(shiny ? "shiny/" : "")}left/frame2/{id}"),
                    Resources.Load<Sprite>($"overworld/{(shiny ? "shiny/" : "")}left/frame2/{id}"),
                };
            }

            bool exists = Resources.Load<Sprite>($"overworld/female/down/{id}") != null;

            return new[]
            {
                Resources.Load<Sprite>($"overworld/{(shiny ? "shiny/" : "")}{(exists ? "female/" : "")}down/{id}"),
                Resources.Load<Sprite>($"overworld/{(shiny ? "shiny/" : "")}{(exists ? "female/" : "")}up/{id}"),
                Resources.Load<Sprite>($"overworld/{(shiny ? "shiny/" : "")}{(exists ? "female/" : "")}left/{id}"),
                Resources.Load<Sprite>(
                    $"overworld/{(shiny ? "shiny/" : "")}{(exists ? "female/" : "")}down/frame2/{id}"),
                Resources.Load<Sprite>($"overworld/{(shiny ? "shiny/" : "")}{(exists ? "female/" : "")}up/frame2/{id}"),
                Resources.Load<Sprite>(
                    $"overworld/{(shiny ? "shiny/" : "")}{(exists ? "female/" : "")}left/frame2/{id}"),
                Resources.Load<Sprite>(
                    $"overworld/{(shiny ? "shiny/" : "")}{(exists ? "female/" : "")}left/frame2/{id}"),
            };
        }

        [MenuItem("Tools/Create balls Sprite lib")]
        private static void CreateBallsSpriteLibraries()
        {
            string[] labels =
            {
                "Normal", "Rolling1", "Rolling2", "Rolling3", "Rolling4", "Rolling5", "Rolling6", "Rolling7",
                "Rolling8", "Rolling9", "Open", "Shake1", "Shake2", "Shake3", "Shake4", "Shake5", "Shake6"
            };

            string[] balls =
            {
                "Master Ball", "Ultra Ball", "Great Ball", "Poké Ball", "Safari Ball", "Net Ball", "Dive Ball",
                "Nest Ball", "Repeat Ball", "Timer Ball", "Luxury Ball", "Premier Ball", "Dusk Ball", "Heal Ball",
                "Quick Ball", "Cherish Ball", "Dream Ball",
            };

            Sprite[] sheet = Resources.LoadAll<Sprite>($"balls/pokeballs");
            SpriteLibraryAsset library = ScriptableObject.CreateInstance<SpriteLibraryAsset>();
            for (int ballColumnIndex = 0; ballColumnIndex < balls.Length; ballColumnIndex++)
            {
                string ball = balls[ballColumnIndex];
                for (int labelRowIndex = 0; labelRowIndex < labels.Length; labelRowIndex++)
                {
                    string label = labels[labelRowIndex];
                    int index = balls.Length * labelRowIndex + ballColumnIndex;
                    library.AddCategoryLabel(sheet[index], ball, label);
                }
            }

            AssetDatabase.CreateAsset(library, $"Assets/Resources/balls/Balls.asset");
            AssetDatabase.SaveAssets();
            Debug.Log("Done");
        }
    }
}