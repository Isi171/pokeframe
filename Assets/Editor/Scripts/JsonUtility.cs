using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Managers.Data;
using Models;
using Models.Ext;
using Models.Items;
using Models.Pokemon;
using Newtonsoft.Json;
using UnityEditor;
using UnityEngine;
using Type = Models.Type;

namespace Editor.Scripts
{
    public class JsonUtility : MonoBehaviour
    {
        private const int Generations = 7;

        [MenuItem("Tools/Generate learnsets")]
        private static void GenerateLearnsets()
        {
            DataAccess.EDITOR__LoadDataForTools();

            HashSet<string> movesToImplement = new HashSet<string>();

            TextAsset rawText = Resources.Load<TextAsset>("learnsets/showdown-gen7");
            var rawSet = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, string[]>>>(rawText.text);
            Resources.UnloadAsset(rawText);

            TextAsset rawEvoText =
                Resources.Load<TextAsset>("learnsets/evolution-moves-gen7-usum-lgpe-no-regional-forms");
            var rawEvo = JsonConvert.DeserializeObject<Dictionary<string, string[]>>(rawEvoText.text);
            Resources.UnloadAsset(rawEvoText);

            var learnsetsByGen = new Dictionary<string, LearnsetExt>[Generations];
            for (int i = 0; i < learnsetsByGen.Length; i++)
            {
                learnsetsByGen[i] = new Dictionary<string, LearnsetExt>();
            }

            foreach (string species in DataAccess.PokemonBaseData.All)
            {
                if (species.StartsWith("Mega "))
                {
                    continue;
                }

                List<string> egg = new List<string>();
                List<string> machine = new List<string>();
                List<string> tutor = new List<string>();
                Dictionary<int, List<string>> byLevel = new Dictionary<int, List<string>>();

                string showdownKey = SpeciesToShowdownKey(species);
                Dictionary<string, string[]> learnset = rawSet[showdownKey];
                foreach (KeyValuePair<string, string[]> moveLearningPair in learnset)
                {
                    string move = ResolveMoveName(moveLearningPair.Key, movesToImplement);
                    string[] learnings = moveLearningPair.Value;
                    foreach (string learning in learnings)
                    {
                        if (!learning.StartsWith("7"))
                        {
                            continue;
                        }

                        if (learning[1] == 'L')
                        {
                            int level = int.Parse(learning.Remove(0, 2));
                            if (!byLevel.ContainsKey(level))
                            {
                                byLevel[level] = new List<string>();
                            }

                            byLevel[level].Add(move);
                        }
                        else if (learning[1] == 'E')
                        {
                            egg.Add(move);
                        }
                        else if (learning[1] == 'M')
                        {
                            machine.Add(move);
                        }
                        else if (learning[1] == 'T')
                        {
                            tutor.Add(move);
                        }
                    }
                }

                Dictionary<int, string[]> byLevelArray = new Dictionary<int, string[]>();
                List<int> keys = byLevel.Keys.ToList();
                keys.Sort();
                foreach (int lv in keys)
                {
                    byLevelArray[lv] = byLevel[lv].ToArray();
                }

                int gen = ResolvePokemonGenerationIndex(DataAccess.PokemonBaseData.Get(species).NationalDexNumber);
                learnsetsByGen[gen].Add(species, new LearnsetExt
                {
                    Egg = egg.ToArray(),
                    Machine = machine.ToArray(),
                    Tutor = tutor.ToArray(),
                    Evolution = rawEvo.ContainsKey(species) ? rawEvo[species] : Array.Empty<string>(),
                    Level = byLevelArray
                });
            }


            for (int i = 0; i < learnsetsByGen.Length; i++)
            {
                if (learnsetsByGen[i].Count > 0)
                {
                    File.WriteAllText($"Assets/Resources/json/learnsets/gen{i + 1}.json",
                        JsonConvert.SerializeObject(learnsetsByGen[i],
                            Formatting.Indented,
                            new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore }));
                }
            }

            if (movesToImplement.Count > 0)
            {
                Debug.LogWarning("Some moves are not implemented yet");
                Debug.LogWarning(string.Join(", ", movesToImplement));
            }

            Debug.Log("Done");
        }

        private static string ResolveMoveName(string rawName, HashSet<string> movesToImplement)
        {
            // TODO temporarily return raw name until all moves are implemented
            string name = DataAccess.Moves.All.FirstOrDefault(pokemon => pokemon
                .ToLower()
                .Replace(".", "")
                .Replace("'", "")
                .Replace("-", "")
                .Replace(" ", "")
                .Equals(rawName));

            if (name == null)
            {
                movesToImplement.Add(rawName);
                return rawName;
            }

            return name;
        }

        private static string SpeciesToShowdownKey(string name)
        {
            return name.ToLower()
                .Replace(".", "")
                .Replace("'", "")
                .Replace("-", "")
                .Replace(" ", "")
                .Replace("♀", "f")
                .Replace("♂", "m");
        }

        [MenuItem("Tools/Dump pokemon base data to Json")]
        private static void DumpBaseDataJson()
        {
            DataAccess.EDITOR__LoadDataForTools();

            var normalMonsByGen = new Dictionary<string, PokemonBaseDataExt>[Generations];
            for (int i = 0; i < normalMonsByGen.Length; i++)
            {
                normalMonsByGen[i] = new Dictionary<string, PokemonBaseDataExt>(DataAccess.PokemonBaseData.All.Count());
            }

            var megasByGen = new Dictionary<string, PokemonBaseDataExt>[Generations];
            for (int i = 0; i < megasByGen.Length; i++)
            {
                megasByGen[i] = new Dictionary<string, PokemonBaseDataExt>(DataAccess.PokemonBaseData.All.Count());
            }

            foreach (string species in DataAccess.PokemonBaseData.All)
            {
                PokemonBaseData data = DataAccess.PokemonBaseData.Get(species);
                PokemonBaseDataExt pokemonBaseDataExt = new PokemonBaseDataExt
                {
                    NationalDexNumber = data.NationalDexNumber,
                    MaleRatio = data.MaleRatio,
                    Type1 = Enum.GetName(typeof(Type), data.Type1),
                    Type2 = Enum.GetName(typeof(Type), data.Type2),
                    Ability1 = data.Ability1,
                    Ability2 = data.Ability2,
                    HiddenAbility = data.HiddenAbility,
                    Hp = data.Hp,
                    Attack = data.Attack,
                    Defense = data.Defense,
                    SpecialAttack = data.SpecialAttack,
                    SpecialDefense = data.SpecialDefense,
                    Speed = data.Speed,
                    EggGroup1 = Enum.GetName(typeof(EggGroup), data.EggGroup1),
                    EggGroup2 = Enum.GetName(typeof(EggGroup), data.EggGroup2),
                    HatchCounter = data.HatchCounter,
                    StepsToHatch = data.StepsToHatch,
                    Effort = data.Effort,
                    Evolutions = data.Evolutions?.Select(Convert).ToArray(),
                    ExpGroup = Enum.GetName(typeof(ExpGroup), data.ExpGroup),
                    BaseExperience = data.BaseExperience,
                    CaptureRate = data.CaptureRate,
                    BaseHappiness = data.BaseHappiness,
                    Height = data.Height,
                    Weight = data.Weight,
                    Genus = data.Genus,
                    Color = data.Color,
                    Habitat = data.Habitat,
                    Shape = data.Shape
                };
                if (species.StartsWith("Mega "))
                {
                    int gen = ResolvePokemonGenerationIndex(pokemonBaseDataExt.NationalDexNumber);
                    megasByGen[gen].Add(species, pokemonBaseDataExt);
                }
                else
                {
                    int gen = ResolvePokemonGenerationIndex(pokemonBaseDataExt.NationalDexNumber);
                    normalMonsByGen[gen].Add(species, pokemonBaseDataExt);
                }
            }

            for (int i = 0; i < normalMonsByGen.Length; i++)
            {
                if (normalMonsByGen[i].Count > 0)
                {
                    File.WriteAllText($"Assets/Resources/json/pokemon-base-data/gen{i + 1}.json",
                        JsonConvert.SerializeObject(normalMonsByGen[i], Formatting.Indented,
                            new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore }));
                }
            }

            for (int i = 0; i < megasByGen.Length; i++)
            {
                if (megasByGen[i].Count > 0)
                {
                    File.WriteAllText($"Assets/Resources/json/pokemon-base-data/mega-gen{i + 1}.json",
                        JsonConvert.SerializeObject(megasByGen[i], Formatting.Indented,
                            new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore }));
                }
            }

            Debug.Log("Done");
        }

        private static EvolutionExt Convert(Evolution data)
        {
            return new EvolutionExt
            {
                EvolutionMethod = Enum.GetName(typeof(EvolutionMethod), data.EvolutionMethod),
                Level = data.Level,
                Item = data.Item,
                Move = data.Move,
                Party = data.Party,
                Species = data.Species
            };
        }

        private static int ResolvePokemonGenerationIndex(int dexNo)
        {
            int[] genThresholds = { 151, 251, 386, 493, 649, 721, 809 };

            for (int i = 0; i < genThresholds.Length; i++)
            {
                if (dexNo <= genThresholds[i])
                {
                    return i;
                }
            }

            return genThresholds.Length;
        }

        [MenuItem("Tools/Dump abilities to Json")]
        private static void DumpAbilitiesJson()
        {
            DataAccess.EDITOR__LoadDataForTools();

            int gen = 0;
            Dictionary<string, AbilityExt>[] extsArray = new Dictionary<string, AbilityExt>[6];
            extsArray[0] = new Dictionary<string, AbilityExt>();
            foreach (string ability in DataAccess.Abilities.All)
            {
                AbilityExt ext = new AbilityExt
                {
                    Description = DataAccess.Abilities.Get(ability).Description
                };

                extsArray[gen].Add(ability, ext);
                if (ability == "Air Lock" || ability == "Bad Dreams" || ability == "Teravolt"
                    || ability == "Delta Stream" || ability == "Neuroforce")
                {
                    gen++;
                    extsArray[gen] = new Dictionary<string, AbilityExt>();
                }
            }

            gen = 3;
            foreach (Dictionary<string, AbilityExt> exts in extsArray)
            {
                File.WriteAllText($"Assets/Resources/json/abilities/gen{gen}.json", JsonConvert.SerializeObject(exts,
                    Formatting.Indented,
                    new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore }));
                gen++;
            }

            Debug.Log("Done");
        }

        [MenuItem("Tools/Dump items to Json")]
        private static void DumpItemsJson()
        {
            DataAccess.EDITOR__LoadDataForTools();

            string[] pocketNames =
            {
                "items", "medicine", "berries", "battle", "pokeballs", "vitamins", "evolution", "tm", "valuables", "key"
            };
            Bag bag = new Bag();
            Pocket[] pockets =
            {
                bag.Items, bag.Medicine, bag.Berries, bag.Battle, bag.PokeBalls, bag.Vitamins, bag.Evolution, bag.Tm,
                bag.Valuables, bag.Key
            };
            for (int i = 0; i < pockets.Length; i++)
            {
                Pocket pocket = pockets[i];
                string pocketName = pocketNames[i];
                Dictionary<string, ItemExt> exts = new Dictionary<string, ItemExt>();
                foreach (ItemCategory itemCategory in pocket.Filter)
                {
                    foreach (string itemName in DataAccess.Items.Filter(item => item.Category == itemCategory))
                    {
                        Item item = DataAccess.Items.Get(itemName);
                        exts.Add(item.Name, new ItemExt
                        {
                            Category = Enum.GetName(typeof(ItemCategory), item.Category),
                            Buy = item.Buy,
                            Sell = item.Sell,
                            Description = item.Description
                        });
                    }
                }

                File.WriteAllText($"Assets/Resources/json/items/{pocketName}.json", JsonConvert.SerializeObject(exts,
                    Formatting.Indented,
                    new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore }));
            }

            Debug.Log("Done");
        }

        [MenuItem("Tools/Dump moves to Json")]
        private static void DumpMovesJson()
        {
            DataAccess.EDITOR__LoadDataForTools();

            TextAsset rawDescriptions = Resources.Load<TextAsset>("moves/moves-description-gen6");
            var movesDescriptions = JsonConvert.DeserializeObject<Dictionary<string, string>>(rawDescriptions.text);
            Resources.UnloadAsset(rawDescriptions);

            List<string> movesWithoutDescription = new List<string>();
            Dictionary<string, MoveExt>[] movesByGen = new Dictionary<string, MoveExt>[Generations];
            for (int i = 0; i < movesByGen.Length; i++)
            {
                movesByGen[i] = new Dictionary<string, MoveExt>();
            }

            foreach (string name in DataAccess.Moves.All)
            {
                Move move = DataAccess.Moves.Get(name);
                MoveExt ext = new MoveExt
                {
                    Index = move.Index,
                    Type = Enum.GetName(typeof(Type), move.Type),
                    MoveType = Enum.GetName(typeof(MoveType), move.MoveType),
                    Pp = move.Pp,
                    Accuracy = move.Accuracy,
                    Target = Enum.GetName(typeof(MoveTarget), move.Target),
                    Priority = move.Priority,
                    ModifiedAccuracyInWeather = move.ModifiedAccuracyInWeather.Count == 0
                        ? null
                        : move.ModifiedAccuracyInWeather.Keys.ToDictionary(
                            key => Enum.GetName(typeof(Weather), key),
                            key => move.ModifiedAccuracyInWeather[key]),
                    BasePower = move.BasePower,
                    CriticalStage = move.CriticalStage,
                    RecoilDivisor = move.RecoilDivisor,
                    MultiStrike = move.MultiStrike,
                    FixedMultiStrike = move.FixedMultiStrike,
                    Leech = move.Leech,
                    SetDamage = move.SetDamage,
                    BurnChance = move.BurnChance,
                    FreezeChance = move.FreezeChance,
                    ParalysisChance = move.ParalysisChance,
                    PoisonChance = move.PoisonChance,
                    BadPoisonChance = move.BadPoisonChance,
                    SleepChance = move.SleepChance,
                    FlinchChance = move.FlinchChance,
                    ConfusionChance = move.ConfusionChance,
                    UserStatChangesChance = move.UserStatChangesChance,
                    UserStatChanges = move.UserStatChanges.Count == 0
                        ? null
                        : move.UserStatChanges.Keys.ToDictionary(
                            key => Enum.GetName(typeof(StatName), key),
                            key => move.UserStatChanges[key]),
                    TargetStatChangesChance = move.TargetStatChangesChance,
                    TargetStatChanges = move.TargetStatChanges.Count == 0
                        ? null
                        : move.TargetStatChanges.Keys.ToDictionary(
                            key => Enum.GetName(typeof(StatName), key),
                            key => move.TargetStatChanges[key]),
                    Weather = move.SetWeather == Weather.Clear
                        ? null
                        : Enum.GetName(typeof(Weather), move.SetWeather),
                    SpecialEffect = move.SpecialEffect == SpecialEffect.None
                        ? null
                        : Enum.GetName(typeof(SpecialEffect), move.SpecialEffect),
                    Flags = move.Flags.Length == 0
                        ? null
                        : move.Flags.Select(flag => Enum.GetName(typeof(MoveFlag), flag)).ToArray()
                };

                if (movesDescriptions.ContainsKey(name))
                {
                    ext.Description = movesDescriptions[name];
                }
                else
                {
                    movesWithoutDescription.Add(name);
                }

                int gen = ResolveMoveGenerationIndex(move.Index);
                movesByGen[gen].Add(name, ext);
            }

            for (int i = 0; i < movesByGen.Length; i++)
            {
                if (movesByGen[i].Count > 0)
                {
                    File.WriteAllText($"Assets/Resources/json/moves/gen{i + 1}.json",
                        JsonConvert.SerializeObject(movesByGen[i],
                            Formatting.Indented,
                            new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore }));
                }
            }

            if (movesWithoutDescription.Count > 0)
            {
                Debug.LogWarning($"Some moves have no description: {string.Join(", ", movesWithoutDescription)}");
            }

            Debug.Log("Done");
        }

        private static int ResolveMoveGenerationIndex(int index)
        {
            int[] genThresholds = { 165, 251, 354, 467, 559, 621, 742 };

            for (int i = 0; i < genThresholds.Length; i++)
            {
                if (index <= genThresholds[i])
                {
                    return i;
                }
            }

            return genThresholds.Length;
        }
    }
}